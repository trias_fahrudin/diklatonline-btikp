<?php

defined('BASEPATH') or exit('No direct script access allowed');

function IsNullOrEmptyString($question)
{
    return (!isset($question) || trim($question) === '');
}

function get_value($tabel, $kunci, $id) {
	$CI =& get_instance();
	$q_ambil	= $CI->db->query("SELECT * FROM ".$tabel." WHERE ".$kunci." = '".$id."'")->row();
	return $q_ambil;
}

function generateRandomString($length = 10)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; ++$i) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }

    return $randomString;
}

function strToHex($string)
{
    $hex = '';
    for ($i = 0; $i < strlen($string); ++$i) {
        $ord = ord($string[$i]);
        $hexCode = dechex($ord);
        $hex .= substr('0'.$hexCode, -2);
    }

    return strToUpper($hex);
}
function hexToStr($hex)
{
    $string = '';
    for ($i = 0; $i < strlen($hex) - 1; $i += 2) {
        $string .= chr(hexdec($hex[$i].$hex[$i + 1]));
    }

    return $string;
}

// function is_localhost()
// {
//     $CI = &get_instance();
//     $base_url = $CI->config->item('base_url');
//     if (strpos($base_url, 'localhost')) {
//         return true;
//     }
// }

function alert_msg($msg)
{
    $alert = '';
    $msg_arr = explode(':', $msg);
    $type = $msg_arr[0];
    $content = $msg_arr[1];

    $alert = '<div class="alert alert-'.$type.'" role="alert" id="alert-'.$type.'" style="padding-top: 10px;padding-bottom: 10px;border-bottom-width: 1px;">
                <h4 style="text-align:center;margin-bottom: 0px;">'.$content.'</h4>
              </div>';
    $alert .= '<script>
                $("#alert-'.$type.'").fadeTo(5000, 500).slideUp(500, function() {
                  $("#alert-'.$type.'").hide();
                });
              </script>';

    return $alert;
}

function nicetime($date)
{
    if (empty($date)) {
        return 'No date provided';
    }

    $periods = array('second', 'minute', 'hour', 'day', 'week', 'month', 'year', 'decade');
    $lengths = array('60','60','24','7','4.35','12','10');

    $now = time();
    $unix_date = strtotime($date);

       // check validity of date
    if (empty($unix_date)) {
        return 'Bad date';
    }

    // is it future date or past date
    if ($now > $unix_date) {
        $difference = $now - $unix_date;
        $tense = 'ago';
    } else {
        $difference = $unix_date - $now;
        $tense = 'from now';
    }

    for ($j = 0; $difference >= $lengths[$j] && $j < count($lengths) - 1; ++$j) {
        $difference /= $lengths[$j];
    }

    $difference = round($difference);

    // if($difference != 1) {
    //     $periods[$j].= "s";
    // }

    $find_this = array('ago','week','day','second','minute','year','from now','month');
    $replace_with = array('yang lalu','minggu','hari','detik','menit','tahun','dari sekarang','bulan');

    return str_replace($find_this, $replace_with, "$difference $periods[$j] {$tense}");
}

function rupiah($angka)
{
    $jadi = 'Rp '.number_format($angka, 0, ',', '.');

    return $jadi;
}

function pajak($angka, $pajak, $nilai_saja = false)
{
    $ret = 0;
    if ($nilai_saja) {
        $ret = $angka * ($pajak / 100);
    } else {
        $ret = $angka + ($angka * ($pajak / 100));
    }

    return $ret;
}

function mysqlDate($theDate)
{
    //theDate = 13/09/2015 00:00:00
    $pieces = explode('/', $theDate);

    return $pieces[2].'-'.$pieces[1].'-'.$pieces[0];
}

/* format mysql default date or datetime to indonesian format*/
function appDate($theDate, $show_time = false)
{
    //2015-10-13 00:00:00
    //strpos(haystack, needle)
    $result = null;
    if ($show_time) {
        $pieces = explode(' ', $theDate);
        $date_part = $pieces[0];
        $time_part = $pieces[1];

        $date_explode = explode('-', $date_part);
        $result = $date_explode[2].'/'.$date_explode[1].'/'.$date_explode[0].' '.$time_part;
    } else {
        $pieces = explode('-', $theDate);
        $result = $pieces[2].'/'.$pieces[1].'/'.$pieces[0];
    }

    return $result;
}

function tgl_panjang($tgl, $tipe, $time = false)
{
    $tgl_pc = explode(' ', $tgl);
    $tgl_depan = $tgl_pc[0];
    $waktu = $time ? $tgl_pc[1] : '';

    $tgl_depan_pc = explode('-', $tgl_depan);
    $tgl = $tgl_depan_pc[2];
    $bln = $tgl_depan_pc[1];
    $thn = $tgl_depan_pc[0];

    $bln_txt = '';

    if ($tipe == 'lm') {
        if ($bln == '01') {
            $bln_txt = 'Januari';
        } elseif ($bln == '02') {
            $bln_txt = 'Februari';
        } elseif ($bln == '03') {
            $bln_txt = 'Maret';
        } elseif ($bln == '04') {
            $bln_txt = 'April';
        } elseif ($bln == '05') {
            $bln_txt = 'Mei';
        } elseif ($bln == '06') {
            $bln_txt = 'Juni';
        } elseif ($bln == '07') {
            $bln_txt = 'Juli';
        } elseif ($bln == '08') {
            $bln_txt = 'Agustus';
        } elseif ($bln == '09') {
            $bln_txt = 'September';
        } elseif ($bln == '10') {
            $bln_txt = 'Oktober';
        } elseif ($bln == '11') {
            $bln_txt = 'November';
        } elseif ($bln == '12') {
            $bln_txt = 'Desember';
        }
    } elseif ($tipe == 'sm') {
        if ($bln == '01') {
            $bln_txt = 'Jan';
        } elseif ($bln == '02') {
            $bln_txt = 'Feb';
        } elseif ($bln == '03') {
            $bln_txt = 'Mar';
        } elseif ($bln == '04') {
            $bln_txt = 'Apr';
        } elseif ($bln == '05') {
            $bln_txt = 'Mei';
        } elseif ($bln == '06') {
            $bln_txt = 'Jun';
        } elseif ($bln == '07') {
            $bln_txt = 'Jul';
        } elseif ($bln == '08') {
            $bln_txt = 'Ags';
        } elseif ($bln == '09') {
            $bln_txt = 'Sep';
        } elseif ($bln == '10') {
            $bln_txt = 'Okt';
        } elseif ($bln == '11') {
            $bln_txt = 'Nov';
        } elseif ($bln == '12') {
            $bln_txt = 'Des';
        }
    }

    $tm = $time ? ' '.$waktu : '';

    return $tgl.' '.$bln_txt.' '.$thn.$tm;
}

function getURLFriendly($str)
{
    $string = preg_replace('/[-]+/', '-', preg_replace('/[^a-z0-9-]/', '',
        strtolower(str_replace(' ', '-', $str))));

    return $string;
}
