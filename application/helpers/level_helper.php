<?php defined('BASEPATH') OR exit('No direct script access allowed');

function is_authorized($level_authorized){
  $CI =& get_instance();

  $current_level = $CI->session->userdata('user_level');
  if(in_array($current_level,$level_authorized)){
    return TRUE;
  }else{
    return FALSE;
  }

}


function page_auth($level_authorized){
  $CI =& get_instance();

  $current_level = $CI->session->userdata('user_level');

  if(!$current_level){
    redirect('frontpage/signin', 'refresh');
  }
  
  if(in_array($current_level,$level_authorized)){
    return TRUE;
  }else{
    redirect('frontpage/signin', 'refresh');
  }

}
