<?php defined('BASEPATH') OR exit('No direct script access allowed');

function district($selected_value = null)
{
   $CI =& get_instance();
   $select = '<select name="district" id="district" style="width: 100%;" class="form-control" data-toggle="select2" placeholder="Kabupaten atau Kota">';

   $district = $CI->Basecrud_m->get_all('districts','name','ASC');
   $select .= '<option value="">Pilih Kab/Kota</option>';

   foreach ($district->result() as $dist) {
      $select .= '<option ' . ($dist->name === $selected_value ? "selected" : "") . ' value ="' . $dist->name . '">' . $dist->name . '</option>';
   };

   $select .='</select>';

   return $select;
}

// function institute($selected_value = null)
// {
//    $CI =& get_instance();
//    $select = '<select name="institute" id="institute" style="width: 100%;" class="form-control" data-toggle="select2" placeholder="Instansi">';
//
//    $institute = $CI->Basecrud_m->get_all('institutes','name','ASC');
//    $select .= '<option value="">Pilih Instansi</option>';
//
//    foreach ($institute->result() as $ins) {
//       $select .= '<option ' . ($ins->name === $selected_value ? "selected" : "") . ' value ="' . $ins->name . '">' . $ins->name . '</option>';
//    };
//
//    $select .='</select>';
//
//    return $select;
// }

// function faculty($selected_value = null)
// {
//    $CI =& get_instance();
//    $select = '<select name="faculty" id="faculty" style="width: 100%;" class="form-control" data-toggle="select2" placeholder="Fakultas">';
//
//    $faculty = $CI->Basecrud_m->get_all('faculties','name','ASC');
//    $select .= '<option value="">Pilih Fakultas</option>';
//
//    foreach ($faculty->result() as $fac) {
//       $select .= '<option ' . ($fac->name === $selected_value ? "selected" : "") . ' value="' . $fac->name . '">' . $fac->name . '</option>';
//    };
//
//    $select .='</select>';
//
//    return $select;
// }

// function departement($selected_value = null)
// {
//    $CI =& get_instance();
//    $select = '<select name="departement" id="departement" style="width: 100%;" class="form-control" data-toggle="select2" placeholder="Fakultas">';
//
//    $departement = $CI->Basecrud_m->get_all('departements','name','ASC');
//    $select .= '<option value="">Pilih Jurusan</option>';
//
//    foreach ($departement->result() as $dep) {
//       $select .= '<option ' . ($dep->name === $selected_value ? "selected" : "") . ' value="' . $dep->name . '">' . $dep->name . '</option>';
//    };
//
//    $select .='</select>';
//
//    return $select;
// }

// function prog_studi($selected_value = null)
// {
//    $CI =& get_instance();
//    $select = '<select name="prog_studi" id="prog_studi" style="width: 100%;" class="form-control" data-toggle="select2" placeholder="Fakultas">';
//
//    $prog_studi = $CI->Basecrud_m->get_all('program_studi','name','ASC');
//    $select .= '<option value="">Program Studi</option>';
//
//    foreach ($prog_studi->result() as $ps) {
//       $select .= '<option ' . ($ps->name === $selected_value ? "selected" : "") . ' value="' . $ps->name . '">' . $ps->name . '</option>';
//    };
//    $select .='</select>';
//
//    return $select;
// }

function menu_kelas($classroom_slug = null,$active_menu = null){

  $CI =& get_instance();
  $classroom = $CI->Basecrud_m->get_where('classrooms',array('slug' => $classroom_slug))->row();
  $menu = '';
  if($classroom_slug != null){
    $menu =
    '<div class="panel panel-default" data-toggle="panel-collapse" data-open="true">
       <div class="panel-heading panel-collapse-trigger">
          <h4 class="panel-title">Menu Kelas</h4>
       </div>
       <div class="panel-body list-group">
          <ul class="list-group list-group-menu">
             <li class="list-group-item ' . ($active_menu === 'dashboard' ? 'active' : '')  .'">
                <a class="link-text-color" href="' . base_url() . 'participator/classroom/' . $classroom_slug . '">
                <i class="fa fa-bar-chart-o"></i> Dashboard
                </a>
             </li>



          </ul>
       </div>
    </div>';

  }

  return $menu;

}

// function main_menu($active_menu)
// {
// 	$menu = '<div class="panel panel-default" data-toggle="panel-collapse" data-open="true">
//                <div class="panel-heading panel-collapse-trigger">
//                   <h4 class="panel-title">Menu Peserta</h4>
//                </div>
//                <div class="panel-body list-group">
//                   <ul class="list-group list-group-menu">
//
//
//                      <li class="list-group-item ' . ($active_menu === 'profile' ? 'active' : '')  .'">
//                         <a class="link-text-color" href="' . base_url() . 'profile">
//                         <i class="fa fa-user"></i> Profile
//                         </a>
//                      </li>
//
//                      <li class="list-group-item ' . ($active_menu === 'signout' ? 'active' : '')  .'">
//                         <a class="link-text-color" href="' . base_url() . 'signout">
//                            <i class="fa fa-sign-out"></i> Keluar
//                         </a>
//                      </li>
//                   </ul>
//                </div>
//             </div>';
//     return $menu;
// }



function main_menu($active_menu){

  $CI =& get_instance();

  $user_level = $CI->session->userdata('user_level');

  $menu ='<ul class="list-group list-group-menu">';

  $menu .= '<li class="list-group-item ' . ($active_menu === 'dashboard' ? 'active' : '')  .'">
              <a class="link-text-color" href="' . base_url() . $user_level . '">
                <i class="fa fa-bar-chart-o"></i> Dashboard
              </a>
            </li>';

  if($user_level === 'participator'){

    $menu .=
            '<!-- <li class="list-group-item ' . ($active_menu === 'dashboard' ? 'active' : '')  .'">
               <a class="link-text-color" href="' . base_url() . 'participator">
               <i class="fa fa-bar-chart-o"></i> Dashboard
               </a>
            </li>-->
            <li class="list-group-item ' . ($active_menu === 'classroom' ? 'active' : '')  .'">
               <a class="link-text-color" href="' . base_url() . 'participator/classroom">
               <i class="fa fa-mortar-board"></i> Kelas
               </a>
            </li>';

  }elseif($user_level === 'instructor'){

    $menu .='<li class="list-group-item ' . ($active_menu === 'classroom' ? 'active' : '')  .'">
                  <a class="link-text-color" href="' . base_url() . 'classroom">
                    <i class="fa fa-mortar-board"></i> Kelas
                  </a>
               </li>
              <li class="list-group-item ' . ($active_menu === 'data' ? 'active' : '')  .'">
                <a class="link-text-color" href="' . base_url() . 'data">
                  <i class="fa fa-database"></i> Data
                </a>
              </li>';

  }elseif($user_level === 'admin'){

    $menu .='<li class="list-group-item ' . ($active_menu === 'classroom' ? 'active' : '')  .'">
                  <a class="link-text-color" href="' . base_url() . 'classroom">
                    <i class="fa fa-mortar-board"></i> Kelas
                  </a>
               </li>
              <li class="list-group-item ' . ($active_menu === 'data' ? 'active' : '')  .'">
                <a class="link-text-color" href="' . base_url() . 'data">
                  <i class="fa fa-database"></i> Data
                </a>
              </li>

              <li class="list-group-item ' . ($active_menu === 'galeri' ? 'active' : '')  .'">
                <a class="link-text-color" href="' . base_url() . 'galeri">
                  <i class="fa fa-picture-o"></i> Galeri
                </a>
              </li>

              <li class="list-group-item ' . ($active_menu === 'ekspresi' ? 'active' : '')  .'">
                <a class="link-text-color" href="' . base_url() . 'ekspresi">
                  <i class="fa fa-bullhorn"></i> Ekspresi
                </a>
              </li>

              <li class="list-group-item ' . ($active_menu === 'ide_saran' ? 'active' : '')  .'">
                <a class="link-text-color" href="' . base_url() . 'ide_saran">
                  <i class="fa fa-envelope-o"></i> Ide & Saran
                </a>
              </li>

              <li class="list-group-item ' . ($active_menu === 'pertanyaan' ? 'active' : '')  .'">
                <a class="link-text-color" href="' . base_url() . 'pertanyaan">
                  <i class="fa fa-newspaper-o"></i> Pertanyaan
                </a>
              </li>


              <li class="list-group-item ' . ($active_menu === 'settings' ? 'active' : '')  .'">
                <a class="link-text-color" href="' . base_url() . 'admin/settings">
                  <i class="fa fa-cogs"></i> Settings
                </a>
              </li>';
  }

  $menu .=
            '<li class="list-group-item ' . ($active_menu === 'profile' ? 'active' : '')  .'">
                <a class="link-text-color" href="' . base_url() . 'profile">
                <i class="fa fa-user"></i> Profile
                </a>
             </li>
             <li class="list-group-item ' . ($active_menu === 'signout' ? 'active' : '')  .'">
                <a class="link-text-color" href="' . base_url() . 'signout">
                   <i class="fa fa-sign-out"></i> Keluar
                </a>
             </li>
          </ul>
        ';
    return $menu;

}
