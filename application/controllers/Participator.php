<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Participator extends CI_Controller
{
    private $data       = array();
    private $user_level = null;
    private $user_id;

    public function __construct()
    {
        parent::__construct();

        date_default_timezone_set('Asia/Jakarta');

        $this->load->database();
        $this->load->helper(array('url'));

        $this->load->model(array('Basecrud_m', 'Classroom_m'));
        $this->load->library('form_validation');

        $this->user_level = $this->session->userdata('user_level');
        $this->user_id    = $this->session->userdata('user_id');

        // if (!$this->user_level || $this->user_level !== 'participator') {
        //     redirect('frontpage/signin', 'refresh');
        // }
        page_auth(array('participator'));
    }

    public function _page_output($data = null)
    {
        $this->load->view('participator/masterpage', $data);
    }

    public function _setting($name)
    {
        $rs = $this->Basecrud_m->get_where('settings', array('name' => $name))->row();

        return $rs->value;
    }

    public function index()
    {
        $this->data['dashboard_preface'] = $this->_setting('dashboard_preface');

        $this->data['page'] = 'participator/dashboard';
        $this->_page_output($this->data);
    }

    public function update_status_kehadiran()
    {
        $classroom_id = $this->input->post('classroom_id');
        $status       = $this->input->post('status');

        //update status kehadiran
        $this->db->where('classroom_id', $classroom_id);
        $this->db->where('user_id', $this->user_id);
        $this->db->update('classroom_users', array('kehadiran' => $status));

    }

    private function generateRandomString($length = 20)
    {
        $seed = '0123456789' .
            'abcdefghijklmnopqrstuvwxyz' .
            'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        //shuffle($seed);
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $seed[rand(0, strlen($seed) - 1)];
        }
        return $randomString;
    }

    public function generate_barcode($file_name, $barcode_text, $scale = 2, $fontsize = 18, $thickness = 30, $dpi = 72)
    {
        // CREATE BARCODE GENERATOR
        // Including all required classes
        require_once APPPATH . 'libraries/Barcode/BCGFontFile.php';
        require_once APPPATH . 'libraries/Barcode/BCGColor.php';
        require_once APPPATH . 'libraries/Barcode/BCGDrawing.php';
        // require_once( APPPATH . 'libraries/Barcode/BCGcode39extended.barcode.php');

        // Including the barcode technology
        // Ini bisa diganti-ganti mau yang 39, ato 128, dll, liat di folder Barcode
        require_once APPPATH . 'libraries/Barcode/BCGcode39.barcode.php';
        // require_once( APPPATH . 'libraries/Barcode/BCGcode128.barcode.php');
        // require_once( APPPATH . 'libraries/Barcode/BCGcode93.barcode.php');

        // Loading Font
        // kalo mau ganti font, jangan lupa tambahin dulu ke folder font, baru loadnya di sini
        $font = new BCGFontFile(APPPATH . 'libraries/font/Arial.ttf', $fontsize);

        // Text apa yang mau dijadiin barcode, biasanya kode produk
        // $text = md5($barcode_text);

        // The arguments are R, G, B for color.
        $color_black = new BCGColor(0, 0, 0);
        $color_white = new BCGColor(255, 255, 255);

        $drawException = null;
        try {
            $code = new BCGcode39(); // kalo pake yg code39, klo yg lain mesti disesuaikan
            $code->setScale($scale); // Resolution
            $code->setThickness($thickness); // Thickness
            $code->setForegroundColor($color_black); // Color of bars
            $code->setBackgroundColor($color_white); // Color of spaces
            $code->setFont($font); // Font (or 0)
            $code->parse($barcode_text); // Text
        } catch (Exception $exception) {
            $drawException = $exception;
        }

        /* Here is the list of the arguments
        1 - Filename (empty : display on screen)
        2 - Background color */
        $drawing = new BCGDrawing('', $color_white);
        if ($drawException) {
            $drawing->drawException($drawException);
        } else {
            $drawing->setDPI($dpi);
            $drawing->setBarcode($code);
            $drawing->draw();
        }
        // ini cuma labeling dari sisi aplikasi saya, penamaan file menjadi png barcode.
        $filename_img_barcode = $file_name . '.png';
        // folder untuk menyimpan barcode
        $drawing->setFilename(FCPATH . 'upload/barcode/' . $filename_img_barcode);
        // proses penyimpanan barcode hasil generate
        $drawing->finish(BCGDrawing::IMG_FORMAT_PNG);

        return $filename_img_barcode;
    }

    public function generate_qrcode($file_name, $barcode_text){

      //require APPPATH . '/libraries/QRCode/Ciqrcode.php';
      $this->load->library('ciqrcode');

      $filename_img_barcode = $file_name . '.png';

      $params['data'] = $barcode_text;
      $params['level'] = 'H';
      $params['size'] = 10;
      $params['savename'] = FCPATH . 'upload/barcode/' . $filename_img_barcode;
      $this->ciqrcode->generate($params);

      return $filename_img_barcode;

    }

    public function card_receipts($classroom_slug)
    {

        $user      = $this->db->get_where('users', array('id' => $this->user_id))->row();
        $classroom = $this->db->get_where('classrooms', array('slug' => $classroom_slug))->row();

        /*
        PELATIHAN
        ANGKATAN
        JENJANG

        NAMA_PESERTA
        ASAL_SEKOLAH_PESERTA
         */

        //$this->load->library('PHPWord');
        require APPPATH . '/third_party/phpword/PHPWord.php';
        $PHPWord  = new PHPWord();
        

        if (strpos('pertama', $classroom_slug) !== false) {
            //$document = $this->phpword->loadTemplate(APPPATH . 'docs/tanda_bukti_merah.docx');
            $document = $PHPWord->loadTemplate(APPPATH . 'docs/tanda_bukti_merah.docx');
        } else {
            //$document = $this->phpword->loadTemplate(APPPATH . 'docs/tanda_bukti_biru.docx');
            $document = $PHPWord->loadTemplate(APPPATH . 'docs/tanda_bukti_merah.docx');
        }

        $document->setValue('PELATIHAN', strtoupper($classroom->name));
        $document->setValue('ANGKATAN', strtoupper($classroom->generation));
        $document->setValue('JENJANG', strtoupper($classroom->peserta));

        $document->setValue('NAMA_PESERTA', strtoupper($user->fullname));
        $document->setValue('ASAL_SEKOLAH_PESERTA', strtoupper($user->origin_of_school));

        //classroom_id#user_id#random
        //$barcode_file = $this->generate_barcode($user->nuptk . "-" . $user->id, $classroom->id . "-" . $user->id . "-" . $user->nuptk);
        $barcode_file = $this->generate_qrcode($user->nuptk . "-" . $user->id,$classroom->id . "-" . $user->id . "-" . $user->nuptk);

        $barcode = array(
            array(
                'img'  => FCPATH . "upload/barcode/" . $barcode_file,
                'size' => array(170, 170),
            ),
        );

        $document->replaceStrToImg('BARCODE', $barcode);

        // $this->load->helper('string');

        $doc_name = $this->generateRandomString(8);
        $document->save(APPPATH . 'docs/temp/' . $doc_name . '.docx');

        $this->load->helper('download');
        $data = file_get_contents(APPPATH . 'docs/temp/' . $doc_name . '.docx'); // Read the file's contents

        force_download($user->username . '.docx', $data);

    }

    public function classroom($classroom_slug = null)
    {
        if ($classroom_slug !== null) {

            $this->data['page'] = 'participator/classroom_details';
            // $this->data['classroom_list'] = $this->Classroom_m->get_by_user($this->user_id);
            $classroom = $this->Basecrud_m->get_where('classrooms', array('slug' => $classroom_slug))->row();

            $courses = $this->Basecrud_m->get_where('courses', array('classroom_id' => $classroom->id));

            // var_dump($courses->result());
            // exit(0);
            $curriculum = '';

            foreach ($courses->result() as $course) {
                $time_status = '';
                if (new DateTime() > new DateTime($course->start)) {
                    if (new DateTime() > new DateTime($course->end)) {
                        $time_status = 'closed';
                    } else {
                        $time_status = 'available';
                    }
                } else {
                    $time_status = 'not-available';
                }

                if ($time_status !== 'not-available') {
                    //jika waktu pelajaran sudah mulai, maka tampilkan pelajaran
                    $curriculum .=
                    '<div class="panel panel-default curriculum ' . ($time_status === 'available' ? 'open' : '') . ' paper-shadow" data-z="0.5">
                            <div class="panel-heading panel-heading-gray" data-toggle="collapse" data-target="#curriculum-' . $course->id . '">
                                <div class="media">
                                    <div class="media-left">
                                        <span class="icon-block img-circle bg-green-300 half text-white"><i class="fa fa-graduation-cap"></i></span>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="text-headline" style="' . ($time_status === 'available' ? 'color:#64b5f6' : 'color:#9e9e9e') . '">' . $course->title . '</h4>
                                        <p class="' . ($time_status === 'available' ? '' : 'text-grey-500') . '">' . strip_tags($course->description) . '</p>
                                    </div>
                                </div>
                                <span class="collapse-status collapse-open"><i class="fa fa-chevron-down"></i></span>
                                <span class="collapse-status collapse-close"><i class="fa fa-chevron-down"></i></span>
                            </div>
                            <div class="list-group collapse ' . ($time_status === 'available' ? 'in' : '') . '" id="curriculum-' . $course->id . '">';

                    $materials_array = explode(',', $course->material_list);

                    if (!empty($course->material_list)) {
                        $number = 1;

                        $curriculum .= '<!-- item course -->';
                        $open_link = true;
                        foreach ($materials_array as $material_id) {
                            $material = $this->Basecrud_m->get_where('materials_bank', array('id' => $material_id))->row();

                            $task = $this->Basecrud_m->get_where('course_tasks', array('material_id' => $material->id, 'course_id' => $course->id));

                            $task_div = '';

                            if ($task->num_rows() > 0) {
                                $task_file = $this->Basecrud_m->get_where('course_task_files', array('course_task_id' => $task->row()->id, 'user_id' => $this->session->userdata('user_id')));
                                if ($task_file->num_rows() > 0) {
                                    $task_div .= '<div class="width-100 text-right text-caption"><span class="label label-warning" style="display:inline;margin-right:5px"><i class="fa fa-file"></i> Tugas</span><span class="label label-primary" style="display:inline"><i class="fa fa-check"></i></span></div>';
                                } else {
                                    $task_div .= '<div class="width-100 text-right text-caption"><span class="label label-warning" style="display:inline;margin-right:5px"><i class="fa fa-file"></i> Tugas</span><span class="label label-danger" style="display:inline"><i class="fa fa-close"></i></span></div>';
                                }
                            }

                            $link = '#';
                            if ($open_link) {
                                $link = base_url() . 'participator/material/' . $course->slug . '/' . $material->slug;
                            }

                            $curriculum .=
                            '<div ' . ($link === '#' ? 'onClick="alert(\'Anda harus menyelesaikan test sebelumnya untuk melanjutkan\');return false"' : '') . ' class="list-group-item media" data-target="' . $link . '">
                                   <div class="media-left">
                                      <div class="text-crt">' . $number . '.</div>
                                   </div>
                                   <div class="media-body">
                                      <i class="fa fa-fw fa-circle ' . ($time_status === 'available' ? 'text-green-300' : 'text-grey-200') . '"></i>' . $material->title . '
                                   </div>
                                   <div class="media-right">
                                   ' . $task_div . '
                                   </div>
                                </div>';

                            ++$number;
                        }

                        $curriculum .= '<!-- end item course -->';
                    }

                    $curriculum .= '
                           </div>
                        </div>';
                }
            }

            $this->data['classroom']  = $classroom;
            $this->data['curriculum'] = $curriculum;
            $this->_page_output($this->data);
        } else {
            //upload surat tugas
            if (!empty($_POST)) {
                //cek apakah surat_tugas di sertakan ?
                $surat_tugas_filename = "";
                if (!empty($_FILES['surat_tugas']['name'])) {
                    $upload                  = array();
                    $upload['upload_path']   = './upload/stugas';
                    $upload['allowed_types'] = 'jpeg|jpg|png|pdf';
                    $upload['encrypt_name']  = true;

                    $this->load->library('upload', $upload);

                    if (!$this->upload->do_upload('surat_tugas')) {
                        //$data['msg'] = $this->upload->display_errors();
                        //$cap = $this->_captcha();
                        //$this->session->set_userdata('captcha_word', strtolower($cap['word']));

                        // echo json_encode(array(
                        //     'status' => 'ERROR',
                        //     'msg' => 'File surat tugas harus berformat jpeg,jpg,png atau pdf',
                        //     'image' => $cap['image']
                        // ));
                        // exit(0);
                    } else {
                        $success              = $this->upload->data();
                        $surat_tugas_filename = $success['file_name'];
                        // $this->session->set_userdata('user_st', $file_name);
                        $classroom_id = $this->input->post('classroom_id');
                        $user_id      = $this->input->post('user_id');
                        //update surat tugas
                        $this->db->where('classroom_id', $classroom_id);
                        $this->db->where('user_id', $user_id);
                        $this->db->update('classroom_users', array('surat_tugas' => $surat_tugas_filename));
                    }
                } else {
                    // exit(0);
                }
            }

            $this->data['page']           = 'participator/classroom';
            $this->data['classroom_list'] = $this->Classroom_m->get_active(false, $this->user_id);
            $this->_page_output($this->data);
        }
    }

    public function material($course_slug = null, $material_slug = null)
    {
        $course   = $this->Basecrud_m->get_where('courses', array('slug' => $course_slug));
        $material = $this->Basecrud_m->get_where('materials_bank', array('slug' => $material_slug));

        $course_tasks = $this->Basecrud_m->get_where('course_tasks', array('course_id' => $course->row()->id, 'material_id' => $material->row()->id));

        if (!empty($_FILES['task_file']['name'])) {
            $upload                  = array();
            $upload['upload_path']   = './upload/task_files';
            $upload['allowed_types'] = 'jpeg|jpg|png|pdf|docx|doc|zip|rar';
            $upload['encrypt_name']  = true;

            $this->load->library('upload', $upload);

            if (!$this->upload->do_upload('task_file')) {
                $this->data['msg'] = 'danger:' . strip_tags($this->upload->display_errors());
                //echo "<script>alert('" . strip_tags($this->upload->display_errors()) ."');</script>";
            } else {
                $success   = $this->upload->data();
                $file_name = $success['file_name'];
                $task      = $course_tasks->row();

                $this->db->query("INSERT INTO course_task_files(course_task_id,user_id,file)
                                  VALUES($task->id,$this->user_id,'$file_name')
                                  ON DUPLICATE KEY UPDATE file = '$file_name',checked = 'n'");

                //echo "<script>alert('File anda telah terkirim');</script>";
                $this->data['msg'] = 'success:File telah terkirim';
            }
        }

        $classroom = $this->Basecrud_m->get_where('classrooms', array('id' => $course->row()->classroom_id));
        // echo $classroom->num_rows();
        // exit(0);
        $this->data['classroom'] = $classroom->row();
        // var_dump($this->data['classroom']);
        // exit(0);
        $this->data['material']     = $material->row();
        $this->data['course']       = $course->row();
        $this->data['course_tasks'] = $course_tasks;
        $this->data['page']         = 'participator/material';
        $this->_page_output($this->data);
    }

    public function classroom_signin($classroom_slug = null)
    {

        // $classroom = $this->Basecrud_m->get_where('classrooms', array('slug' => $classroom_slug))->row();
        //
        // $this->db->where('classroom_id', $classroom->id);
        // $this->db->where('status', 'enable');
        // $registerant = $this->db->count_all_results('classroom_users');
        //
        // $is_me_signup_class = $this->Basecrud_m->get_where('classroom_users',
        //                                     array('classroom_id' => $classroom->id,
        //                                           'user_id' => $this->user_id, )
        //                            );
        //
        // if ($registerant >= $classroom->registration_quota && $is_me_signup_class->num_rows() == 0) {
        //     redirect('participator/classroom', 'reload');
        // } else {
        //     $this->Basecrud_m->insert('classroom_users',
        //                             array('classroom_id' => $classroom->id,
        //                                   'user_id' => $this->user_id,
        //                                   'status' => 'disable', )
        //                           );
        //     redirect('participator/classroom', 'reload');
        // }
    }
}
