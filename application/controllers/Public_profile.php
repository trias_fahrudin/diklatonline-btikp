<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Public_profile extends CI_Controller
{
    private $user_level;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        $this->load->model('Basecrud_m');

        $this->user_level = $this->session->userdata('user_level');
    }

    public function _page_output($data = null)
    {
        $this->load->view($this->user_level . '/masterpage', $data);
    }

    public function index($id)
    {
        $profile = $this->Basecrud_m->get_where('users', array('id' => $id))->row();
        $this->data['profile'] = $profile;
        $this->data['page'] = 'public_profile/' . $profile->level . '_profile';
        $this->data['past_course'] = $this->Basecrud_m->get_where('user_past_courses', array('user_id' => $profile->id));
        //get list surat tugas
        $this->db->select("CONCAT(b.name,' ',b.generation) AS nama_kelas,a.surat_tugas");
        $this->db->join('classrooms b','a.classroom_id = b.id','left');
        $this->data['surat_tugas'] = $this->db->get_where('classroom_users a',array('user_id' => $id));

        $this->_page_output($this->data);
    }
}
