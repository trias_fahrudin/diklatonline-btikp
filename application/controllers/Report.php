<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Report extends CI_Controller
{
    private $data = array();
    private $user_level;
    private $user_id;

    public function __construct()
    {
        parent::__construct();

        date_default_timezone_set('Asia/Jakarta');

        $this->load->database();
        $this->load->helper(array('url'));
        $this->load->model(array('Basecrud_m', 'Classroom_m','Score_m'));

        $this->load->library('form_validation');

        $this->user_level = $this->session->userdata('user_level');
        $this->user_id = $this->session->userdata('user_id');

        if (!$this->user_level) {
            redirect('frontpage/signin', 'refresh');
        }
    }

    public function _page_output($data = null)
    {
        $this->load->view($this->user_level.'/masterpage', $data);
    }

    public function index($classroom_slug = null, $act = null)
    {
      if($act === 'scores'){

          $classroom = $this->Basecrud_m->get_where('classrooms',array('slug' => $classroom_slug))->row();

          /*udah deh generate manual ajah*/
          $classroom_id = $classroom->id;
          //1.delete all data related to classroom_id
          $this->db->where('classroom_id', $classroom_id);
          $this->db->delete('user_scores');
                    
          //2.insert user to user_scores

          $participators = $this->Classroom_m->get_users($classroom->id,'participator','enable');
          foreach ($participators->result() as $part) {
            $this->Basecrud_m->insert('user_scores',
                        array(
                          'user_id' => $part->user_id,
                          'classroom_id' => $classroom_id
                        )
            );
          }


          //update blog post comments count
          $blog_post_comments =
              $this->db->query("SELECT b.user_id AS user_id, COUNT(b.id) AS blog_post_comments
                                FROM blogs a
                                LEFT JOIN blog_comments b ON a.id = b.blog_id
                                WHERE a.classroom_id = $classroom_id
                                GROUP BY b.user_id");

          foreach ($blog_post_comments->result() as $bpc) {
              $this->db->where('user_id',$bpc->user_id);
              $this->db->where('classroom_id',$classroom_id);
              $this->db->update('user_scores',array('blog_post_comments' => $bpc->blog_post_comments));
          }

          //update task_scores
          $task_scores = $this->db->query("SELECT a.user_id,SUM(a.poin) AS task_scores
                                          FROM course_task_files a
                                          LEFT JOIN course_tasks b ON a.course_task_id = b.id
                                          LEFT JOIN courses c ON b.course_id = c.id
                                          WHERE c.classroom_id = $classroom_id
                                          GROUP BY a.user_id");

          foreach ($task_scores->result() as $tsc) {
              $this->db->where('user_id',$tsc->user_id);
              $this->db->where('classroom_id',$classroom_id);
              $this->db->update('user_scores',array('task_scores' => $tsc->task_scores));
          }


          $this->data['scores'] = $this->Score_m->get($classroom_id);
          $this->data['classroom'] = $classroom;
          $this->data['page'] = 'report/scores';
          $this->_page_output($this->data);

        }else{
          $classroom = $this->Basecrud_m->get_where('classrooms',array('slug' => $classroom_slug))->row();
          $this->data['classroom'] = $classroom;
          $this->data['page'] = 'report/home';
          $this->_page_output($this->data);
        }



    }
}
