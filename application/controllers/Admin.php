<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{
    private $data = array();
    private $user_level = null;
    private $user_id;

    public function __construct()
    {
        parent::__construct();

        date_default_timezone_set('Asia/Jakarta');

        $this->load->database();
        $this->load->helper('url');
        $this->load->model(array('Basecrud_m', 'Classroom_m','Settings_m'));

        $this->load->library('form_validation');

        $this->user_level = $this->session->userdata('user_level');
        $this->user_id = $this->session->userdata('user_id');

        // if (!$this->user_level || $this->user_level !== 'admin') {
        //     redirect('frontpage/signin', 'refresh');
        // }
        page_auth(array('admin'));
    }

    public function _page_output($data = null)
    {
        $this->load->view('admin/masterpage', $data);
    }

    public function _setting($name)
    {
        $rs = $this->Basecrud_m->get_where('settings', array('name' => $name))->row();

        return $rs->value;
    }

    public function settings($act = null, $param = null)
    {
        // $this->load->model(array('basecrud_m', 'settings_m'));

        if ($act === 'upload') {
            if (!empty($_FILES['img']['name'])) {
                $upload = array();
                $upload['upload_path'] = './upload';
                $upload['allowed_types'] = 'jpeg|jpg|png|swf|gif';
                $upload['encrypt_name'] = true;

                $this->load->library('upload', $upload);

                if (!$this->upload->do_upload('img')) {
                    $data['msg'] = $this->upload->display_errors();
                } else {
                    $success = $this->upload->data();
                    $value = $success['file_name'];
                    $file_ext = $success['file_ext'];

                    $type = $file_ext === '.swf' ? 'swf' : 'image';

                    $this->Settings_m->update($param, array('value' => $value,'type' => $type));
                    redirect('admin/settings');
                }
            }
        } elseif ($act === 'edt') {
            $value = $this->input->post('value');
            $this->Settings_m->update($param, array('value' => $value));
            exit(0);
        }

        $this->data['setting'] = $this->Basecrud_m->get_where('settings', array('show' => 'Y'),'name','ASC');
        $this->data['page'] = 'admin/settings';
        $this->data['title'] = 'Data Settings';

        $this->_page_output($this->data);
    }

    public function index($act = null)
    {
        if ($act === 'dashboard_preface') {
            $this->form_validation->set_rules('value', 'Nilai', 'required');

            if ($this->form_validation->run() == true) {
                $val = $this->input->post('value');

                $this->db->where('name', 'dashboard_preface');
                $this->db->update('settings', array('value' => $val));

                redirect('admin', 'reload');
            }
        }

        /**/
        $this->db->where('level','participator');
        $this->db->where('active','y');
        $this->data['participator_count'] = $this->db->count_all_results('users');

        $this->db->where('level','instructor');
        $this->db->where('active','y');
        $this->data['instructor_count'] = $this->db->count_all_results('users');

        $this->data['dashboard_preface'] = $this->_setting('dashboard_preface');

        $this->data['page'] = 'admin/dashboard';
        $this->_page_output($this->data);
    }
}
