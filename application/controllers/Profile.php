<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Profile extends CI_Controller
{
    private $data = array();
    private $user_level = null;
    private $user_id;

    public function __construct()
    {
        parent::__construct();

        date_default_timezone_set('Asia/Jakarta');

        $this->load->database();
        $this->load->helper('url');
        $this->load->model(array('Basecrud_m'));

        $this->load->library('form_validation');

        $this->user_level = $this->session->userdata('user_level');
        $this->user_id = $this->session->userdata('user_id');

        if (!$this->user_level) {
            redirect('frontpage/signin', 'refresh');
        }
        // page_auth(array('instructor'));
    }

    public function _page_output($data = null)
    {
        $this->load->view($this->user_level.'/masterpage', $data);
    }

    public function upload_foto()
    {
        if (!empty($_FILES['foto']['name'])) {
            $upload = array();
            $upload['upload_path'] = './upload/people';
            $upload['allowed_types'] = 'jpeg|jpg|png';
            $upload['encrypt_name'] = true;

            $this->load->library('upload', $upload);

            if (!$this->upload->do_upload('foto')) {
                //$data['msg'] = $this->upload->display_errors();
            } else {
                $success = $this->upload->data();
                $file_name = $success['file_name'];
                $this->session->set_userdata('user_foto', $file_name);
                $this->Basecrud_m->update('users', $this->user_id, array('foto' => $file_name));

                redirect('profile', 'reload');
            }
        }
    }
	 
	 
    public function index()
    {
        $this->data['page'] = $this->user_level . '/profile';

        $user_level = $this->user_level;
        if(!empty($_POST)){

            header('content-type: application/json');

            if($user_level === 'admin'){
                $this->form_validation->set_rules('fullname', 'Nama Lengkap', 'required');
                $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
                // $this->form_validation->set_rules('telp_number', 'Nomor Telephon', 'required');
                // $this->form_validation->set_rules('kota_lahir', 'Kota Lahir', 'required');
                // $this->form_validation->set_rules('tgl_lahir', 'Tanggal Lahir', 'required');
                // $this->form_validation->set_rules('jk', 'Jenis Kelamin', 'required');
                // $this->form_validation->set_rules('alamat', 'Alamat', 'required');
                // $this->form_validation->set_rules('status', 'Status', 'required');
                // $this->form_validation->set_rules('nip', 'NIP', 'required');
                // $this->form_validation->set_rules('nuptk', 'NUPTK', 'required');
                // $this->form_validation->set_rules('asal_sekolah', 'Asal Sekolah', 'required');
                // $this->form_validation->set_rules('kab_kota', 'Kabupaten / Kota Sekolah', 'required');
                // $this->form_validation->set_rules('mapel', 'Pengampu Mata Pelajaran', 'required');

                if ($this->form_validation->run() == true) {

                    $profile = array(
                                'fullname' => $this->input->post('fullname'),
                                'email' => $this->input->post('email'),
                                'telp_number' => $this->input->post('telp_number'),
                                'city_of_birth' => $this->input->post('city_of_birth'),
                                'date_of_birth' => mysqlDate($this->input->post('date_of_birth')),
                                'gender' => $this->input->post('gender'),
                                'home_address' => $this->input->post('home_address'),
                                // 'status'           => $this->input->post('status'),
                                // 'nip'              => $this->input->post('nip'),
                                // 'nuptk'            => $this->input->post('nuptk'),
                                // 'origin_of_school' => $this->input->post('origin_of_school'),
                                // 'district_id'      => $this->input->post('district_id'),
                                // 'mapel'            => $this->input->post('mapel')
                            );

                    $this->Basecrud_m->update('users', $this->user_id,$profile);

                    $pass = trim($this->input->post('password'));
                    if("" !== $pass){
                      $this->Basecrud_m->update('users', $this->user_id,array('password' => md5($pass)));
                    }

                    echo json_encode(array('msg' => 'DATA-UPDATED','status' => 'OK'));
                } else {
                    echo json_encode(array('msg' => validation_errors(),'status' => 'ERROR'));
                }
            }elseif($user_level === 'participator'){
                $this->form_validation->set_rules('fullname', 'Nama Lengkap', 'required');
                $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
                // $this->form_validation->set_rules('telp_number', 'Nomor Telephon', 'required');
                // $this->form_validation->set_rules('kota_lahir', 'Kota Lahir', 'required');
                // $this->form_validation->set_rules('tgl_lahir', 'Tanggal Lahir', 'required');
                // $this->form_validation->set_rules('jk', 'Jenis Kelamin', 'required');
                // $this->form_validation->set_rules('alamat', 'Alamat', 'required');
                // $this->form_validation->set_rules('status', 'Status', 'required');
                // $this->form_validation->set_rules('nip', 'NIP', 'required');
                // $this->form_validation->set_rules('nuptk', 'NUPTK', 'required');
                // $this->form_validation->set_rules('npwp', 'NPWP', '');
                // $this->form_validation->set_rules('asal_sekolah', 'Asal Sekolah', 'required');
                // $this->form_validation->set_rules('kab_kota', 'Kabupaten / Kota Sekolah', 'required');
                // $this->form_validation->set_rules('mapel', 'Pengampu Mata Pelajaran', 'required');

                if ($this->form_validation->run() == true) {

                    $profile = array(
                        'fullname' => $this->input->post('fullname'),
                        'email' => $this->input->post('email'),
                        'telp_number' => $this->input->post('telp_number'),
                        'city_of_birth' => $this->input->post('city_of_birth'),
                        'date_of_birth' => mysqlDate($this->input->post('date_of_birth')),
                        'gender' => $this->input->post('gender'),
                        'home_address' => $this->input->post('home_address'),
                        'status' => $this->input->post('status'),
                        'nip' => $this->input->post('nip'),
                        'nuptk' => $this->input->post('nuptk'),
                        'origin_of_school' => $this->input->post('origin_of_school'),
                        'district' => $this->input->post('district'),
                        'mapel' => $this->input->post('mapel'),
                    );

                    $this->Basecrud_m->update('users', $this->user_id,$profile);

                    $pass = trim($this->input->post('password'));
                    if("" !== $pass){
                      $this->Basecrud_m->update('users', $this->user_id,array('password' => md5($pass)));
                    }


                    echo json_encode(array('msg' => 'DATA-UPDATED', 'status' => 'OK'));
                } else {
                    echo json_encode(array('msg' => validation_errors(), 'status' => 'ERROR'));
                }
            }elseif ($user_level === 'instructor') {
                $this->form_validation->set_rules('fullname', 'Nama Lengkap', 'required');
                //$this->form_validation->set_rules('telp_number', 'Nomor Telephon', 'required');
                $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
                //$this->form_validation->set_rules('home_address', 'Alamat', 'required');
                //$this->form_validation->set_rules('city_of_birth', 'Kota Lahir', 'required');
		/*                
		$this->form_validation->set_rules('date_of_birth', 'Tanggal Lahir', 'required');
                $this->form_validation->set_rules('gender', 'Jenis Kelamin', 'required');
                $this->form_validation->set_rules('nip', 'NIP', 'required');
                $this->form_validation->set_rules('npwp', 'NPWP', '');
                $this->form_validation->set_rules('institute', 'Instansi', 'required');
                $this->form_validation->set_rules('faculty', 'Fakultas', 'required');
                $this->form_validation->set_rules('departement', 'Jurusan', 'required');
                $this->form_validation->set_rules('prog_studi', 'Program Studi', 'required');
                $this->form_validation->set_rules('expertise', 'Keahlian', 'required');
                $this->form_validation->set_rules('golongan', 'Golongan', 'required');
                $this->form_validation->set_rules('position', 'Jabatan', 'required');
                $this->form_validation->set_rules('last_education', 'Pendidikan Terakhir', 'required');
		*/
                if ($this->form_validation->run() == TRUE) {

                  $profile = array(
                        'fullname'       => $this->input->post('fullname'),
                        'telp_number'    => $this->input->post('telp_number'),
                        'email'          => $this->input->post('email'),
                        'home_address'   => $this->input->post('home_address'),
                        'city_of_birth'  => $this->input->post('city_of_birth'),
                        'date_of_birth'  => mysqlDate($this->input->post('date_of_birth')),
                        'gender'         => $this->input->post('gender'),
                        'nip'            => $this->input->post('nip'),
                        'npwp'            => $this->input->post('npwp'),
                        'institute'      => $this->input->post('institute'),
                        'faculty'        => $this->input->post('faculty'),
                        'prog_studi'     => $this->input->post('prog_studi'),
                        'expertise'      => $this->input->post('expertise'),
                        'golongan'       => $this->input->post('golongan'),
                        'position'       => $this->input->post('position'),
                        'last_education' => $this->input->post('last_education')
                    );

                    $this->Basecrud_m->update('users',$this->user_id,$profile);

                    $pass = trim($this->input->post('password'));
                    if("" !== $pass){
                      $this->Basecrud_m->update('users', $this->user_id,array('password' => md5($pass)));
                    }

                    echo json_encode(array('msg'    => 'DATA-UPDATED','status' => 'OK'));

                }else{
                    echo json_encode(array('msg'    => validation_errors(),'status' => 'ERROR'));
                }
            }
        }else{
          $this->_page_output($this->data);
        }
    }
}
