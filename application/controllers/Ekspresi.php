<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Ekspresi extends CI_Controller
{
    private $user_level = null;
    private $user_id;

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');

        $this->load->database();
        $this->load->helper('url');
        $this->load->model(array('Basecrud_m','Ekspresi_m'));
        $this->load->library('form_validation');

        $this->user_level = $this->session->userdata('user_level');
        $this->user_id = $this->session->userdata('user_id');

        // if (!$this->user_level) {
        //     redirect('frontpage/signin', 'refresh');
        // }

        // if (!$this->user_level || ($this->user_level !== 'admin' && $this->user_level !== 'instructor')) {
        //     redirect('frontpage/signin', 'refresh');
        // }
        page_auth(array('admin'));
    }

    public function _generate_page($data = null)
    {
        $this->load->view($this->user_level.'/masterpage', $data);
    }

    public function index()
    {
        $this->Ekspresi_m->sort = 'inserted_at';
        $this->Ekspresi_m->order = 'DESC';

        $data['data'] = $this->Ekspresi_m->get('showall', 'TRUE');
        $data['page'] = 'ekspresi/v_ekspresi';

        $this->_generate_page($data);
    }

    public function manage($cmd = null, $param = null)
    {
        $this->load->model('Ekspresi_m');

        $data = array();
        $data['title'] = 'Kolom Ekspresi Pengunjung';

        if ($cmd === 'set_tampil') {
            $tampil = $this->input->post('tampil');
            $id = $this->input->post('id');
            $this->Basecrud_m->update('ekspresi', $id, array('tampil' => $tampil));
            exit(0);
        } elseif ($cmd === 'reply_set_tampil') {
            $tampil = $this->input->post('tampil');
            $id = $this->input->post('id');
            $this->Basecrud_m->update('ekspresi_tanggapan', $id, array('tampil' => $tampil));
            exit(0);
        } elseif ($cmd === 'reply') {
            $data['post'] = $this->db->query(
                                      "SELECT nama,judul,isi_ekspresi,
                                              DATE(inserted_at) as tgl
  				                             FROM ekspresi
  				                             WHERE id = $param")->row();

            $data['tanggapan'] = $this->db->query(
                                           "SELECT id,nama,komentar,
                                                   tampil,DATE(inserted_at) as tgl
  				                                  FROM ekspresi_tanggapan
  				                                  WHERE id_ekspresi = $param
  				                                  ORDER BY inserted_at ASC");

            if (!empty($_POST)) {
                $this->form_validation->set_rules('komentar', 'Komentar', 'required');

                if ($this->form_validation->run() == true) {
                    $date_now = date('Y-m-d H:i:s');

                    $in = array(
                        'id_ekspresi' => $param,
                        'nama' => 'Administrator',
                        'komentar' => $this->input->post('komentar'),
                        'tampil' => 'Y',
                        'inserted_at' => $date_now,
                    );

                    $this->Basecrud_m->insert('ekspresi_tanggapan', $in);
                    $this->session->set_flashdata('k', "<div class='alert alert-success'>Tanggapan terkirim</div>");
                    redirect('ekspresi/manage/reply/'.$param, 'reload');
                } else {
                    $web['msg'] = validation_errors();
                }
            }

            $data['page'] = 'ekspresi/f_ekspresi_tanggapan';
        } elseif ($cmd === 'del') {
            $this->Basecrud_m->delete('ekspresi', array('id' => $param));
            redirect('ekspresi', 'reload');
        }

        $this->_generate_page($data);
    }
}
