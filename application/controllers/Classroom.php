<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Classroom extends CI_Controller
{
    private $data       = array();
    private $user_level = null;
    private $user_id;

    public function __construct()
    {
        parent::__construct();

        date_default_timezone_set('Asia/Jakarta');

        $this->load->database();
        $this->load->helper('url');
        $this->load->model(array(
            'Basecrud_m', 'Classroom_m',
            'Course_m', 'Material_m',
            'Task_files_m'));

        $this->load->library('form_validation');

        $this->user_level = $this->session->userdata('user_level');
        $this->user_id    = $this->session->userdata('user_id');

        // if (!$this->user_level) {
        //     redirect('frontpage/signin', 'refresh');
        // }

        // if (!$this->user_level || $this->user_level !== 'admin') {
        //     redirect('frontpage/signin', 'refresh');
        // }

        page_auth(array('admin', 'instructor'));
    }

    public function _page_output($data = null)
    {
        $this->load->view($this->user_level . '/masterpage', $data);
    }

    public function export_data($classroom_slug, $user_level = 'participator', $status_user = 'all')
    {

        if ($user_level === 'participator') {

            $this->db->select("c.fullname AS `Nama Peserta`,
                             c.nip AS NIP,
                             c.golongan AS `Pangkat/Golongan`,
                             c.position AS Jabatan,
                             IF(c.gender = 'male','Laki-laki','Perempuan') AS `Jenis Kelamin`,
                             CONCAT(c.city_of_birth,' , ', DATE_FORMAT(c.date_of_birth,'%d-%m-%Y')) AS `Tempat, Tgl Lahir`,
                             c.origin_of_school AS `Asal Sekolah`,
                             c.district AS `Kab/Kota`,
                             c.home_address AS `Alamat Rumah`,
                             c.office_address AS `Alamat Kantor`,
                             c.telp_number AS `HP`,
                             c.email,
                             c.npwp AS NPWP,
                             IF(a.status = 'enable','Online','Offline') AS `Status Pendaftaran`", false);

            $this->db->join('classrooms b', 'a.classroom_id = b.id', 'left');
            $this->db->join('users c', 'a.user_id = c.id', 'left');
            $this->db->where('b.slug', $classroom_slug);
            $this->db->where('c.level', 'participator');
            $this->db->order_by('a.status', 'ASC');

            if ($status_user === 'enable') {

                $this->db->where('a.status', 'enable');
                $query = $this->db->get('classroom_users a');

                if (!$query) {
                    return false;
                }

            } elseif ($status_user === 'disable') {

                $this->db->where('a.status', 'disable');
                $query = $this->db->get('classroom_users a');

                if (!$query) {
                    return false;
                }

            } else {
                //get all

                $query = $this->db->get('classroom_users a');

                if (!$query) {
                    return false;
                }

            }

        }

        //REKAP BIODATA PESERTA KEGIATAN PELATIHAN MODEL PEMBELAJARAN TERINTEGRASI TIK PADA KEGIATAN PENGEMBANGAN TIK PENDIDIKAN TAHUN ANGGARAN 2016

        // Starting the PHPExcel library
        $this->load->library('PHPExcel');
        $this->load->library('PHPExcel/IOFactory');

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle('export')->setDescription('none');

        $objPHPExcel->setActiveSheetIndex(0);

        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, 'REKAP BIODATA PESERTA KEGIATAN PELATIHAN ' . str_replace('-', ' ', strtoupper($classroom_slug)) . ' TAHUN ANGGARAN 2016');
        $objPHPExcel->getActiveSheet()->mergeCells("A1:M1");

        // Field names in the first row
        $fields = $query->list_fields();
        $col    = 0;
        foreach ($fields as $field) {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 2, $field);
            ++$col;
        }

        // Fetching the table data
        $row = 3;
        foreach ($query->result() as $data) {
            $col = 0;
            foreach ($fields as $field) {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
                ++$col;
            }
            ++$row;
        }

        foreach (range('A', 'N') as $columnID) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        // $num_rows = $query->num_rows() + 2;
        //
        // foreach(range('A','M') as $columnID) {
        //     $objPHPExcel->getActiveSheet()->getStyle( $columnID . '1:' . $columnID .$num_rows)
        //                 ->getAlignment()
        //                 ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        // }

        //$objPHPExcel->setActiveSheetIndex(0);

        $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');

        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Download_data_' . date('dMy') . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');
    }

    public function index($act = null, $param0 = null)
    {
        if ($act === 'set_availability') {

            $classroom_id = $this->input->post('classroom_id');
            $status       = $this->input->post('status');

            $this->db->where('id', $classroom_id);
            $this->db->update('classrooms', array('availability' => $status));

        } elseif ($act === 'edit') {
            $classroom               = $this->Basecrud_m->get_where('classrooms', array('slug' => $param0))->row();
            $this->data['courses']   = $this->Basecrud_m->get_where('courses', array('classroom_id' => $classroom->id));
            $this->data['classroom'] = $classroom;
            $this->data['page']      = 'classroom/classroom_form';
            $this->_page_output($this->data);
        } elseif ($act === 'edit_act') {
            $this->form_validation->set_rules('name', 'Nama Kelas', 'required');
            $this->form_validation->set_rules('generation', 'Angkatan', 'required');
            // $this->form_validation->set_rules('description', 'Deskripsi', 'required');

            $this->form_validation->set_rules('detail_kegiatan', 'Detail kegiatan', 'required');
            $this->form_validation->set_rules('narasumber', 'Narasumber', 'required');
            $this->form_validation->set_rules('peserta', 'Peserta', 'required');
            $this->form_validation->set_rules('tempat', 'Tempat', 'required');

            $this->form_validation->set_rules('preface', 'Kata Pengantar', 'required');
            $this->form_validation->set_rules('start', 'Tanggal Mulai', 'required');
            $this->form_validation->set_rules('end', 'Tanggal Selesai', 'required');
            $this->form_validation->set_rules('registration_quota', 'Kuota Pendaftaran', 'required');

            if ($this->form_validation->run() == true) {
                $up = array(
                    'name'               => trim($this->input->post('name')),
                    'generation'         => $this->input->post('generation'),
                    // 'description' => $this->input->post('description'),

                    'detail_kegiatan'    => $this->input->post('detail_kegiatan'),
                    'narasumber'         => $this->input->post('narasumber'),
                    'peserta'            => $this->input->post('peserta'),
                    'tempat'             => $this->input->post('tempat'),

                    'preface'            => $this->input->post('preface'),
                    'start'              => mysqlDate($this->input->post('start')),
                    'end'                => mysqlDate($this->input->post('end')),
                    'registration_quota' => $this->input->post('registration_quota'),
                );

                $classroom_id = $this->input->post('classroom_id');

                $this->Basecrud_m->update('classrooms', $classroom_id, $up);

                $this->session->set_flashdata('msg_ok', 'Data berhasil diupdate');
                $classroom = $this->Basecrud_m->get_where('classrooms', array('id' => $classroom_id))->row();
                redirect('classroom/index/edit/' . $classroom->slug, 'reload');
            } else {
                $classroom = $this->Basecrud_m->get_where('classrooms', array('slug' => $param0))->row();

                $this->data['courses'] = $this->Basecrud_m->get_where('courses', array('classroom_id' => $classroom->id));

                $this->data['classroom'] = $classroom;
                $this->data['msg_error'] = validation_errors();
                $this->data['page']      = 'classroom/classroom_form';
                $this->_page_output($this->data);
            }
        } elseif ($act === 'add') {
            $this->data['classroom_name'] = null;

            if ($param0 != null) {
                $this->db->select('name');
                $this->db->where('slugify(name)', $param0);
                $this->db->limit(1);

                $this->data['classroom_name'] = $this->db->get('classrooms')->row()->name;
            }

            $this->data['page'] = 'classroom/classroom_form';
            $this->_page_output($this->data);
        } elseif ($act === 'add_act') {
            $this->form_validation->set_rules('name', 'Nama Kelas', 'required');
            $this->form_validation->set_rules('generation', 'Angkatan', 'required');
            // $this->form_validation->set_rules('description', 'Deskripsi', 'required');

            $this->form_validation->set_rules('detail_kegiatan', 'Detail kegiatan', 'required');
            $this->form_validation->set_rules('narasumber', 'Narasumber', 'required');
            $this->form_validation->set_rules('peserta', 'Peserta', 'required');
            $this->form_validation->set_rules('tempat', 'Tempat', 'required');

            $this->form_validation->set_rules('preface', 'Kata Pengantar', 'required');
            $this->form_validation->set_rules('start', 'Tanggal Mulai', 'required');
            $this->form_validation->set_rules('end', 'Tanggal Selesai', 'required');
            $this->form_validation->set_rules('registration_quota', 'Kuota Pendaftaran', 'required');

            if ($this->form_validation->run() == true) {
                $in = array(
                    'name'               => trim($this->input->post('name')),
                    'generation'         => $this->input->post('generation'),
                    // 'description' => $this->input->post('description'),

                    'detail_kegiatan'    => $this->input->post('detail_kegiatan'),
                    'narasumber'         => $this->input->post('narasumber'),
                    'peserta'            => $this->input->post('peserta'),
                    'tempat'             => $this->input->post('tempat'),

                    'preface'            => $this->input->post('preface'),
                    'start'              => mysqlDate($this->input->post('start')),
                    'end'                => mysqlDate($this->input->post('end')),
                    'registration_quota' => $this->input->post('registration_quota'),
                );

                $classroom_id = $this->Basecrud_m->insert('classrooms', $in);

                $classroom = $this->Basecrud_m->get_where('classrooms', array('id' => $classroom_id))->row();
                redirect('classroom/index/edit/' . $classroom->slug, 'reload');
            } else {
                $this->data['classroom_name'] = null;

                if ($param0 != null) {
                    $this->db->select('name');
                    $this->db->where('slugify(name)', $param0);
                    $this->db->limit(1);

                    $this->data['classroom_name'] = $this->db->get('classrooms')->row()->name;
                }

                $this->data['msg_error'] = validation_errors();
                $this->data['page']      = 'classroom/classroom_form';
                $this->_page_output($this->data);
            }
        } elseif ($act === 'delete') {
            $this->db->where('slug', $param0);
            $this->db->delete('classrooms');

        } elseif ($act === 'details') {
            $user_classrooms = array();
            if ($this->user_level === 'admin') {
                $classrooms = $this->Classroom_m->get_by_user('666', 'admin');
                foreach ($classrooms->result() as $cl) {
                    array_push($user_classrooms, $cl->id);
                }
            } else {
                $classrooms = $this->Classroom_m->get_by_user($this->user_id, 'instructor');
                foreach ($classrooms->result() as $cl) {
                    array_push($user_classrooms, $cl->id);
                }
            }

            // $url = base_url().'classroom/details/' . $param0 . '/';
            // $res = $this->Classroom_m->get_generation('numrows', $user_classrooms,$param0);
            // $per_page = 1;
            // $config = paginate($url, $res, $per_page, 4);
            // $this->pagination->initialize($config);
            //
            // $this->Classroom_m->limit = $per_page;
            // if ($this->uri->segment(4) == true) {
            //     $this->Classroom_m->offset = $this->uri->segment(4);
            // } else {
            //     $this->Classroom_m->offset = 0;
            // }

            // $this->Classroom_m->sort = 'a.start';
            // $this->Classroom_m->order = 'ASC';

            $this->data['classroom_slug'] = $param0;
            $this->data['data']           = $this->Classroom_m->get_generation('showall', $user_classrooms, $param0);

            $this->data['page'] = 'classroom/classroom_details';
            $this->_page_output($this->data);
        } else {
            $user_classrooms = array();
            if ($this->user_level === 'admin') {
                //admin
                $classrooms = $this->Classroom_m->get_by_user('666', 'admin');
                foreach ($classrooms->result() as $cl) {
                    array_push($user_classrooms, $cl->id);
                }
            } else {
                //instructor
                $classrooms = $this->Classroom_m->get_by_user($this->user_id, 'instructor');
                foreach ($classrooms->result() as $cl) {
                    array_push($user_classrooms, $cl->id);
                }
            }

            // $url = base_url().'classroom/';
            // $res = $this->Classroom_m->get('numrows', $user_classrooms);
            // $per_page = 10;
            // $config = paginate($url, $res, $per_page, 2);
            // $this->pagination->initialize($config);
            //
            // $this->Classroom_m->limit = $per_page;
            // if ($this->uri->segment(2) == true) {
            //     $this->Classroom_m->offset = $this->uri->segment(2);
            // } else {
            //     $this->Classroom_m->offset = 0;
            // }

            $this->Classroom_m->sort  = 'a.name';
            $this->Classroom_m->order = 'ASC';

            // if($this->user_level)

            if (count($user_classrooms) == 0) {
                $this->data['data'] = $this->Classroom_m->get('showall', 'all_classroom');
            } else {
                $this->data['data'] = $this->Classroom_m->get('showall', $user_classrooms);
            }

            $this->data['page'] = 'classroom/classroom_index';
            $this->_page_output($this->data);
        }
    }

    public function get_materi_details()
    {
        $param = $this->input->post('param');

        $ex_param = explode('-', $param);

        $details = '';

        if ($ex_param[0] === 'material') {
            $det = $this->Basecrud_m->get_where('materials_bank', array('id' => $ex_param[1]))->row();
            $details .= '<h2>' . $det->title . '</h2>';
            $details .= $det->content;
        }
        echo $details;
    }

    private function instructor_list($classroom_id, $course_id, $material_id)
    {

        $this->db->select('b.id AS user_id,b.fullname');
        $this->db->join('users b', 'a.user_id = b.id', 'left');
        $this->db->where('a.classroom_id', $classroom_id);
        $this->db->where('b.level', 'instructor');
        $instructors = $this->db->get('classroom_users a');

        $instructor_option = "<select name=\"instruktor_list\" class=\"instruktor_list\" courseid=\"$course_id\" materialid=\"$material_id\">";
        $instructor_option .= "  <option value=''>Pilih instruktur</option>";
        foreach ($instructors->result() as $il) {

            $this->db->where('course_id', $course_id);
            $this->db->where('material_id', $material_id);
            $this->db->where('instructor_id', $il->user_id);
            $ins_num = $this->db->count_all_results('course_instructor');

            if ($ins_num > 0) {
                $instructor_option .= '<option selected value="' . $il->user_id . '">' . $il->fullname . '</option>';
            } else {
                $instructor_option .= '<option value="' . $il->user_id . '">' . $il->fullname . '</option>';
            }

        }

        $instructor_option .= "</select>";

        return $instructor_option;
    }

    private function materi($course)
    {
        $materi = '<ul class="nestable-list">';

        $classroom_id = $course->classroom_id;

        if (!empty($course->material_list)) {

            $materials = explode(',', $course->material_list);
            $course_id = $course->id;

            foreach ($materials as $mat) {
                $material = $this->Basecrud_m->get_where('materials_bank', array('id' => $mat))->row();
                $materi .=
                '<li class="nestable-item nestable-item-handle" data-id="' . $material->id . '">
                      <div class="nestable-handle"><i class="md md-menu"></i></div>
                      <div class="nestable-content">
                        <div class="media v-middle">
                          <div class="media-left">
                            <div class="icon-block half bg-blue-400 text-white">
                                <i class="fa fa-book"></i>
                            </div>
                          </div>
                          <div class="media-body">
                            <h4 class="text-title media-heading margin-none">
                                <a href="" class="link-text-color">' . $material->title . '</a>
                                <div class="text-caption"></div>
                            </h4>
                            <h4 class="text-title media-heading margin-none">' . $this->instructor_list($classroom_id, $course_id, $material->id) . '</h4>
                          </div>
                          <div class="media-right">
                            <a data-to-load="material-' . $material->id . '" data-toggle="modal" data-target="#myModal" class="modal-materi-quiz btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="Detail"><i class="fa fa-search-plus fa-fw"></i></a>
                            <!-- <a href="' . base_url() . 'classroom/material-details/' . $material->slug . '" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="Detail"><i class="fa fa-search-plus fa-fw"></i></a>-->
                          </div>
                          <div class="media-right">
                            <a course-id="' . $course->id . '" material-id="' . $material->id . '" class="modal-task-form btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit Tugas"><i class="fa fa-tasks fa-fw"></i></a>
                            <!--<a href="' . base_url() . 'classroom/edit-task/' . $course->slug . '/' . $material->slug . '" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit Tugas"><i class="fa fa-tasks fa-fw"></i></a>-->
                          </div>
                          <div class="media-right" style="padding-left:5px">
                            <a href="' . base_url() . 'classroom/course/remove_material/' . $course->slug . '/' . $material->slug . '" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus"><i class="fa fa-times"></i></a>
                          </div>
                        </div>
                      </div>
                    </li>';
            }
        }
        $materi .= '</ul>';

        return $materi;
    }

    public function course($act = null, $param0 = null, $param1 = null)
    {
        /***********BASE ACTIONS*************************************************/
        if ($act === 'add') {
            $classroom = $this->Basecrud_m->get_where('classrooms', array('slug' => $param0))->row();

            $this->data['classroom'] = $classroom;
            // $this->data['redirect_url'] = ;
            $this->data['page'] = 'classroom/course_form';
            $this->_page_output($this->data);
        } elseif ($act === 'add_act') {
            $this->form_validation->set_rules('title', 'Judul Course', 'required');
            $this->form_validation->set_rules('description', 'Deskripsi', 'required');
            $this->form_validation->set_rules('start', 'Tanggal Mulai', 'required');
            $this->form_validation->set_rules('end', 'Tanggal Selesai', 'required');

            if ($this->form_validation->run() == true) {
                $in = array(
                    'title'        => $this->input->post('title'),
                    'description'  => $this->input->post('description'),
                    'start'        => mysqlDate($this->input->post('start')) . ' 00:00:00',
                    'end'          => mysqlDate($this->input->post('end')) . ' 23:59:59',
                    'classroom_id' => $this->input->post('classroom_id'),
                );

                //$course_id = $this->input->post('course_id');
                $course_id = $this->Basecrud_m->insert('courses', $in);
                $course    = $this->Basecrud_m->get_where('courses', array('id' => $course_id))->row();
                $this->session->set_flashdata('msg_ok', 'Data berhasil dimasukkan');

                redirect('classroom/course/edit/' . $course->slug, 'reload');
            } else {
                $classroom = $this->Basecrud_m->get_where('classrooms', array('slug' => $param0))->row();

                $this->data['slug']         = $param0;
                $this->data['course']       = $this->Basecrud_m->get_where('courses', array('slug' => $param0))->row();
                $this->data['classroom']    = $classroom;
                $this->data['redirect_url'] = base_url() . 'classroom/edit/' . $classroom->slug;

                $this->data['msg_error'] = validation_errors();
                $this->data['page']      = 'classroom/course_form';
                $this->_page_output($this->data);
            }
        } elseif ($act === 'change_course_instructor') {

            $course_id     = $this->input->post('course_id');
            $instructor_id = $this->input->post('instructor_id');
            $material_id   = $this->input->post('material_id');

            $this->db->query("INSERT INTO course_instructor(course_id,material_id,instructor_id)
                            VALUES($course_id,$material_id,$instructor_id)
                            ON DUPLICATE KEY UPDATE instructor_id = $instructor_id");
            /*
        var instructor_id = $(this).val();
        var course_id = $(this).attr('courseid');
        var material_id = $(this).attr('materialid');
         */

        } elseif ($act === 'edit') {
            $classroom          = $this->Classroom_m->get_custom('get-by-course-slug', $param0)->row();
            $this->data['slug'] = $param0;
            $course             = $this->Basecrud_m->get_where('courses', array('slug' => $param0))->row();

            // var_dump($course);
            // exit(0);

            $this->data['course']       = $course;
            $this->data['classroom']    = $classroom;
            $this->data['redirect_url'] = base_url() . 'classroom/edit/' . $classroom->slug;

            $this->data['materi'] = $this->materi($course);
            $this->data['page']   = 'classroom/course_form';

            //test-test-test
            if (!empty($_POST)) {

                $course_id     = $this->input->post('course_id');
                $material_list = $this->input->post('material_list');

                $this->db->where('id', $course_id);
                $this->db->update('courses', array('material_list' => $material_list));

                // $act = $this->input->post('act');
                // $slug = $param0;
                //
                // if ($act === 'add') {
                //     $new_material_id = $this->input->post('id');
                //     $array_material_list = explode(',', $course->material_list);
                //     if (!in_array($new_material_id, $array_material_list)) {
                //         array_push($array_material_list, $new_material_id);
                //     }
                //
                //     $updated_list = implode(',', $array_material_list);
                //     if (substr(implode(',', $array_material_list), 0, 1) === ',') {
                //         $updated_list = substr(implode(',', $array_material_list), 1);
                //     }
                //
                //     $this->Course_m->update_material_list($slug, $updated_list);
                // } else {
                //     $deleted_material_id[] = $this->input->post('id');
                //     $array_material_list = explode(',', $course->material_list);
                //     $diff = array_diff($array_material_list, $deleted_material_id);
                //
                //     $updated_list = implode(',', $diff);
                //     if (substr(implode(',', $diff), 0, 1) === ',') {
                //         $updated_list = substr(implode(',', $diff), 1);
                //     }
                //
                //     $this->Course_m->update_material_list($slug, $updated_list);
                // }

                exit(0);
            }
            $this->data['active_materials'] = explode(',', $course->material_list);
            $this->Material_m->sort         = 'a.id';
            $this->Material_m->order        = 'asc';
            $this->data['materials']        = $this->Material_m->get('showall');
            //test-test-test

            $this->_page_output($this->data);
        } elseif ($act === 'edit_act') {
            $this->form_validation->set_rules('title', 'Judul Course', 'required');
            $this->form_validation->set_rules('description', 'Deskripsi', 'required');
            $this->form_validation->set_rules('start', 'Tanggal Mulai', 'required');
            $this->form_validation->set_rules('end', 'Tanggal Selesai', 'required');

            if ($this->form_validation->run() == true) {
                $up = array(
                    'title'       => $this->input->post('title'),
                    'description' => $this->input->post('description'),
                    'start'       => mysqlDate($this->input->post('start')) . ' 00:00:00',
                    'end'         => mysqlDate($this->input->post('end')) . ' 23:59:59',
                );

                $course_id = $this->input->post('course_id');
                $this->Basecrud_m->update('courses', $course_id, $up);

                $course = $this->Basecrud_m->get_where('courses', array('id' => $course_id))->row();
                $this->session->set_flashdata('msg_ok', 'Data berhasil diupdate');

                redirect('classroom/course/edit/' . $course->slug, 'reload');
            } else {
                $classroom                  = $this->Classroom_m->get_custom('get-by-course-slug', $param0)->row();
                $this->data['slug']         = $param0;
                $course                     = $this->Basecrud_m->get_where('courses', array('slug' => $param0))->row();
                $this->data['course']       = $course;
                $this->data['classroom']    = $classroom;
                $this->data['redirect_url'] = base_url() . 'classroom/edit/' . $classroom->slug;

                $this->data['materi']    = $this->materi($course);
                $this->data['msg_error'] = validation_errors();
                $this->data['page']      = 'classroom/course_form';
                $this->_page_output($this->data);
            }
        } elseif ($act === 'remove') {
            $classroom = $this->Classroom_m->get_custom('get-by-course-slug', $param0)->row();
            $this->db->where('slug', $param0);
            $this->db->delete('courses');

            //TODO : delete course_instructor

            // http://localhost/diklatonline/classroom/index/edit/lokakarya-pemanfaatan-media-pembelajaran-berbasis-tik-untuk-kegiatan-belajar-mengajar-2015-angkatan-pertama
            redirect('classroom/index/edit/' . $classroom->slug, 'reload');
        } elseif ($act === 'task_files') {
            $classroom                 = $this->Basecrud_m->get_where('classrooms', array('slug' => $param0))->row();
            $this->data['classroom']   = $classroom;
            $this->Task_files_m->sort  = 'a.upload_time';
            $this->Task_files_m->order = 'DESC';
            $this->data['task_files']  = $this->Task_files_m->get('showall', $classroom->id);
            $this->data['page']        = 'classroom/task_files';
            $this->_page_output($this->data);
        } elseif ($act === 'edit_task') {
            header('content-type: application/json');

            $course_id   = $this->input->get('course_id');
            $material_id = $this->input->get('material_id');

            $data = $this->Basecrud_m->get_where('course_tasks', array(
                'course_id'   => $course_id,
                'material_id' => $material_id)
            );
            if ($data->num_rows() > 0) {
                $row = $data->row();
                echo json_encode(array(
                    'file'     => base_url() . 'upload/task_files/' . $row->file,
                    'note'     => $row->note,
                    'due_date' => appDate($row->due_date, false),
                    'status'   => 'OK'));
            } else {
                echo json_encode(array('status' => 'NO-DATA'));
            }
            //$form =  '';
            //echo $form;
        } elseif ($act === 'task_save') {
            $this->form_validation->set_rules('due_date', 'Tanggal akhir', 'required');
            $this->form_validation->set_rules('note', 'Catatan', 'required');

            if ($this->form_validation->run() == true) {
                $upload                  = array();
                $upload['upload_path']   = './upload/task_files';
                $upload['allowed_types'] = 'pdf|doc|docx|rar|zip|ppt';
                $upload['encrypt_name']  = true;

                $this->load->library('upload', $upload);

                if (!$this->upload->do_upload('file_task')) {
                    //$data['msg'] = $this->upload->display_errors();
                    echo $this->upload->display_errors();
                } else {
                    $success   = $this->upload->data();
                    $file_name = $success['file_name'];
                    /*
                    ------WebKitFormBoundaryQE0y5Jrf2UiL9RIl
                    Content-Disposition: form-data; name="task_course_id"

                    1
                    ------WebKitFormBoundaryQE0y5Jrf2UiL9RIl
                    Content-Disposition: form-data; name="task_material_id"

                    2
                    ------WebKitFormBoundaryQE0y5Jrf2UiL9RIl
                    Content-Disposition: form-data; name="file_task"; filename=""
                    Content-Type: application/octet-stream

                    ------WebKitFormBoundaryQE0y5Jrf2UiL9RIl
                    Content-Disposition: form-data; name="due_date"

                    ------WebKitFormBoundaryQE0y5Jrf2UiL9RIl
                    Content-Disposition: form-data; name="note"

                     */
                    //delete old data
                    $this->Basecrud_m->delete('course_tasks',
                        array('course_id' => $this->input->post('task_course_id'),
                            'material_id'     => $this->input->post('task_material_id'),
                        ));

                    //then insert new row
                    $this->Basecrud_m->insert('course_tasks',
                        array('course_id' => $this->input->post('task_course_id'),
                            'material_id'     => $this->input->post('task_material_id'),
                            'note'            => $this->input->post('note'),
                            'file'            => $file_name,
                            'due_date'        => mysqlDate($this->input->post('due_date')),

                        ));

                    echo 'OK';
                    // $this->session->set_userdata('user_foto', $file_name);
                    // $this->Basecrud_m->update('users', $this->user_id, array('foto' => $file_name));
                    //
                    // redirect('profile', 'reload');
                }
            } else {
                echo validation_errors();
            }
        } elseif ($act === 'task_files_poin') {
            $task_file_id = $this->input->post('task_file_id');
            $poin         = $this->input->post('poin');

            $this->Basecrud_m->update('course_task_files', $task_file_id,
                array('poin' => $poin, 'checked' => 'y')
            );
        } elseif ($act === 'rearrange_material_list') {
            $material_list = $this->input->post('list');
            $slug          = $this->input->post('slug');

            $this->Course_m->update_material_list($slug, $material_list);
        } elseif ($act === 'remove_material') {
            //remove course material

            $course   = $this->Basecrud_m->get_where('courses', array('slug' => $param0))->row();
            $material = $this->Basecrud_m->get_where('materials_bank', array('slug' => $param1))->row();

            //http://localhost/diklatonline/classroom/course/remove_material/lokakarya-pemanfaatan-media-pembelajaran-berbasis-tik-untuk-kegiatan-belajar-mengajar-2015-angkatan-pertama-pelajaran-hari-pertama/materi-3-pada-kategori-materi-1-af13857a-c8d8-11e5-a06a-8b0d9ff5efcd

            // http://localhost/diklatonline/classroom/course/edit/lokakarya-pemanfaatan-media-pembelajaran-berbasis-tik-untuk-kegiatan-belajar-mengajar-2015-angkatan-pertama-pelajaran-hari-pertama

            $deleted_material_id[] = $material->id;
            $array_material_list   = explode(',', $course->material_list);
            $diff                  = array_diff($array_material_list, $deleted_material_id);

            $updated_list = implode(',', $diff);
            if (substr(implode(',', $diff), 0, 1) === ',') {
                $updated_list = substr(implode(',', $diff), 1);
            }

            $this->Course_m->update_material_list($param0, $updated_list);

            //delete course_instructor
            $this->db->delete('course_instructor', array('course_id' => $course->id,
                'material_id'                                            => $material->id));

            redirect('classroom/course/edit/' . $param0, 'reload');
        }
    }

    public function instructor($act = null, $param0 = null)
    {
        if ($act === 'update_list') {
            header('content-type: application/json');

            $classroom_id    = $this->input->post('classroomid');
            $instructor_list = $this->input->post('instructorlist');
            $deleted_id      = $this->input->post('deleted_id');

            $this->Basecrud_m->update('classrooms', $classroom_id,
                array('instructor_list' => $instructor_list)
            );

            $deleted_instructor = $this->Basecrud_m->get_where('users',
                array('id' => $deleted_id))->row();

            echo json_encode(
                array('id' => $deleted_id,
                    'fullname' => $deleted_instructor->fullname)
            );
        } elseif ($act === 'add_list') {
            header('content-type: application/json');

            $classroom_id     = $this->input->post('classroomid');
            $added_instructor = $this->input->post('addedinstructor');

            $classroom       = $this->Basecrud_m->get_where('classrooms', array('id' => $classroom_id))->row();
            $instructor_list = explode(',', $classroom->instructor_list);

            array_push($instructor_list, $added_instructor);

            $te_list = implode(',', $instructor_list);

            //cek apakah karakter pertama dari st_list adalah ","
            $first_char_te_list = substr($te_list, 0, 1);
            if ($first_char_te_list === ',') {
                $te_list = substr($te_list, 1);
            }

            $this->Basecrud_m->update('classrooms', $classroom_id, array('instructor_list' => $te_list));

            echo json_encode(array('instructorlist' => $te_list));
        }
    }

    private function generateRandomString($length = 20)
    {
        $seed = '0123456789' .
            'abcdefghijklmnopqrstuvwxyz' .
            'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        //shuffle($seed);
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $seed[rand(0, strlen($seed) - 1)];
        }
        return $randomString;
    }

    public function generate_qrcode($file_name, $barcode_text){

      //require APPPATH . '/libraries/QRCode/Ciqrcode.php';
      $this->load->library('ciqrcode');

      $filename_img_barcode = $file_name . '.png';

      $params['data'] = $barcode_text;
      $params['level'] = 'H';
      $params['size'] = 10;
      $params['savename'] = FCPATH . 'upload/barcode/' . $filename_img_barcode;
      $this->ciqrcode->generate($params);

      return $filename_img_barcode;

    }

    public function generate_barcode($file_name, $barcode_text, $scale = 2, $fontsize = 18, $thickness = 30, $dpi = 72)
    {
        // CREATE BARCODE GENERATOR
        // Including all required classes
        require_once APPPATH . 'libraries/Barcode/BCGFontFile.php';
        require_once APPPATH . 'libraries/Barcode/BCGColor.php';
        require_once APPPATH . 'libraries/Barcode/BCGDrawing.php';
        // require_once( APPPATH . 'libraries/Barcode/BCGcode39extended.barcode.php');

        // Including the barcode technology
        // Ini bisa diganti-ganti mau yang 39, ato 128, dll, liat di folder Barcode
        require_once APPPATH . 'libraries/Barcode/BCGcode39.barcode.php';
        // require_once( APPPATH . 'libraries/Barcode/BCGcode128.barcode.php');
        // require_once( APPPATH . 'libraries/Barcode/BCGcode93.barcode.php');

        // Loading Font
        // kalo mau ganti font, jangan lupa tambahin dulu ke folder font, baru loadnya di sini
        $font = new BCGFontFile(APPPATH . 'libraries/font/Arial.ttf', $fontsize);

        // Text apa yang mau dijadiin barcode, biasanya kode produk
        // $text = md5($barcode_text);

        // The arguments are R, G, B for color.
        $color_black = new BCGColor(0, 0, 0);
        $color_white = new BCGColor(255, 255, 255);

        $drawException = null;
        try {
            $code = new BCGcode39(); // kalo pake yg code39, klo yg lain mesti disesuaikan
            $code->setScale($scale); // Resolution
            $code->setThickness($thickness); // Thickness
            $code->setForegroundColor($color_black); // Color of bars
            $code->setBackgroundColor($color_white); // Color of spaces
            $code->setFont($font); // Font (or 0)
            $code->parse($barcode_text); // Text
        } catch (Exception $exception) {
            $drawException = $exception;
        }

        /* Here is the list of the arguments
        1 - Filename (empty : display on screen)
        2 - Background color */
        $drawing = new BCGDrawing('', $color_white);
        if ($drawException) {
            $drawing->drawException($drawException);
        } else {
            $drawing->setDPI($dpi);
            $drawing->setBarcode($code);
            $drawing->draw();
        }
        // ini cuma labeling dari sisi aplikasi saya, penamaan file menjadi png barcode.
        $filename_img_barcode = $file_name . '.png';
        // folder untuk menyimpan barcode
        $drawing->setFilename(FCPATH . 'upload/barcode/' . $filename_img_barcode);
        // proses penyimpanan barcode hasil generate
        $drawing->finish(BCGDrawing::IMG_FORMAT_PNG);

        return $filename_img_barcode;
    }
    
    public function participator($act = null, $param0 = null, $param1 = null)
    {
        if ($act === 'card_receipts') {

            $classroom = $this->db->get_where('classrooms', array('slug' => $param0))->row();
            $user      = $this->db->get_where('users', array('id' => $param1))->row();

            // $this->load->library('PHPWord');

            // if(strpos('pertama',$param0) !== false){
            //   $document = $this->phpword->loadTemplate(APPPATH .'docs/tanda_bukti_merah.docx');
            // }else{
            //   $document = $this->phpword->loadTemplate(APPPATH .'docs/tanda_bukti_biru.docx');
            // }

            require APPPATH . '/third_party/phpword/PHPWord.php';
            $PHPWord = new PHPWord();

            if (strpos('pertama', $classroom_slug) !== false) {
                //$document = $this->phpword->loadTemplate(APPPATH . 'docs/tanda_bukti_merah.docx');
                $document = $PHPWord->loadTemplate(APPPATH . 'docs/tanda_bukti_merah.docx');
            } else {
                //$document = $this->phpword->loadTemplate(APPPATH . 'docs/tanda_bukti_biru.docx');
                $document = $PHPWord->loadTemplate(APPPATH . 'docs/tanda_bukti_merah.docx');
            }

            $document->setValue('PELATIHAN', strtoupper($classroom->name));
            $document->setValue('ANGKATAN', strtoupper($classroom->generation));
            $document->setValue('JENJANG', strtoupper($classroom->peserta));

            $document->setValue('NAMA_PESERTA', strtoupper($user->fullname));
            $document->setValue('ASAL_SEKOLAH_PESERTA', strtoupper($user->origin_of_school));

            //classroom_id#user_id#random
            $barcode_file = $this->generate_qrcode($user->nuptk . "-" . $user->id,$classroom->id . "-" . $user->id . "-" . $user->nuptk);

            $barcode = array(
                array(
                    'img'  => FCPATH . "upload/barcode/" . $barcode_file,
                    'size' => array(170, 170),
                ),
            );

            $document->replaceStrToImg('BARCODE', $barcode);


            // $this->load->helper('string');

            $doc_name = $this->generateRandomString(8);
            $document->save(APPPATH . 'docs/temp/' . $doc_name . '.docx');

            $this->load->helper('download');
            $data = file_get_contents(APPPATH . 'docs/temp/' . $doc_name . '.docx'); // Read the file's contents

            force_download($user->username . '.docx', $data);

        } elseif ($act === 'delete_classroom_user') {

            header('content-type: application/json');
            $id = $this->input->post('id');

            $this->Basecrud_m->delete('classroom_users', array('id' => $id));

            echo json_encode(array('status' => 'deleted'));

        } elseif ($act === 'change_classroom_user') {

            header('content-type: application/json');
            $id           = $this->input->post('id');
            $classroom_id = $this->input->post('classroom_id');

            // $this->Basecrud_m->update('classroom_users',$id,array('id' => $id));
            $this->db->where('id', $id);
            $this->db->update('classroom_users', array(
                'classroom_id' => $classroom_id,
                'status'       => 'disable'));

            echo json_encode(array('status' => 'changed'));

        } elseif ($act === 'update_status') {
            //update status di classroom_users
            $classroom_id = $this->input->post('classroom_id');
            $user_id      = $this->input->post('user_id');
            $status       = $this->input->post('status');

            //cek apakah participator atau instructor
            $cek = $this->Basecrud_m->get_where('users', array('id' => $user_id))->row();

            if ($cek->level === 'participator') {
                $this->db->where('classroom_id', $classroom_id);
                $this->db->where('user_id', $user_id);

                $this->db->update('classroom_users', array('status' => $status));

                if ($status === 'enable') {
                    $user           = $this->Basecrud_m->get_where('users', array('id' => $user_id))->row();
                    $data['detail'] = $this->Basecrud_m->get_where('classrooms', array('id' => $classroom_id))->row();

                    $this->load->library('My_PHPMailer');

                    $mail = new PHPMailer();

                    // $mail->isSMTP();
                    // $mail->Host = 'smtp.gmail.com';
                    // $mail->Port = 465;
                    // $mail->SMTPSecure = 'ssl';
                    // $mail->SMTPAuth = true;
                    $mail->isSMTP();
                    $mail->Host       = 'smtp.gmail.com';
                    $mail->Port       = 587;
                    $mail->SMTPSecure = 'tls';
                    $mail->SMTPAuth   = true;

                    $mail->Username = $this->config->item('mail_Username');
                    $mail->Password = $this->config->item('mail_Password');
                    $mail->setFrom($this->config->item('mail_setFrom'), 'Admin');
                    $mail->addReplyTo($this->config->item('mail_setFrom'), 'Admin');
                    $mail->addAddress($user->email, $user->fullname);
                    $mail->Subject = 'Status registrasi kelas di ' . $this->config->item('diklat_title');
                    $mail->msgHTML($this->load->view('accepted_request_classroom_signup', $data, true));

                    if (!$mail->send()) {
                        //jika email ngga terkirim, aktifkan saja user ini
                        $this->Basecrud_m->update('users', $insert_id, array('active' => 'y'));
                        //echo 'Message could not be sent.';
                        //echo 'Mailer Error: ' . $mail->ErrorInfo;
                        //exit(0);
                    }
                }
            } else {
                if ($status === 'enable') {
                    $this->Basecrud_m->insert('classroom_users', array('classroom_id' => $classroom_id, 'user_id' => $user_id, 'status' => 'enable'));
                } else {
                    $this->db->delete('classroom_users', array('classroom_id' => $classroom_id, 'user_id' => $user_id));
                }
            }
        } elseif ($act === 'add') {

            if (!empty($_POST)) {

                header('content-type: application/json');

                $this->form_validation->set_rules('status', 'Status', 'required');
                $this->form_validation->set_rules('fullname', 'Nama Lengkap', 'required');
                $this->form_validation->set_rules('username', 'Username', 'required|alpha_numeric');

                $this->form_validation->set_rules('nip', 'NIP', 'is_unique[users.nip]');
                $this->form_validation->set_rules('nuptk', 'NUPTK', 'is_unique[users.nuptk]');

                $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
                $this->form_validation->set_rules('telp_number', 'Nomor telephon', 'numeric');

                $this->form_validation->set_rules('city_of_birth', 'Kota lahir', '');
                // $this->form_validation->set_rules('date_of_birth', 'Tanggal lahir', 'required');
                // $this->form_validation->set_rules('gender', 'Jenis kelamin', 'required');
                // $this->form_validation->set_rules('home_address', 'Alamat rumah', 'required');
                $this->form_validation->set_rules('golongan', 'Golongan', '');
                $this->form_validation->set_rules('position', 'Jabatan', '');
                $this->form_validation->set_rules('office_address', 'Alamat Kantor', 'required');
                $this->form_validation->set_rules('npwp', 'NPWP', '');

                $this->form_validation->set_rules('origin_of_school', 'Asal sekolah', 'required');
                $this->form_validation->set_rules('district', 'Kab/kota', 'required');
                $this->form_validation->set_rules('mapel', 'Matapelajaran', 'required');

                if ($this->form_validation->run() == true) {
                    //cek apakah surat_tugas di sertakan ?
                    $surat_tugas_filename = "";
                    if (!empty($_FILES['surat_tugas']['name'])) {
                        $upload                  = array();
                        $upload['upload_path']   = './upload/stugas';
                        $upload['allowed_types'] = 'jpeg|jpg|png|pdf';
                        $upload['encrypt_name']  = true;

                        $this->load->library('upload', $upload);

                        if (!$this->upload->do_upload('surat_tugas')) {
                            //$data['msg'] = $this->upload->display_errors();

                            echo json_encode(array(
                                'status' => 'ERROR',
                                'msg'    => 'File surat tugas harus berformat jpeg,jpg,png atau pdf',
                                'image'  => $cap['image'],
                            ));
                            exit(0);
                        } else {
                            $success              = $this->upload->data();
                            $surat_tugas_filename = $success['file_name'];
                            // $this->session->set_userdata('user_st', $file_name);

                        }
                    }

                    $insert_id = $this->Basecrud_m->insert('users', array(
                        'status'           => $this->input->post('status'),
                        'fullname'         => $this->input->post('fullname'),
                        'username'         => $this->input->post('username'),
                        'nip'              => $this->input->post('nip'),
                        'nuptk'            => $this->input->post('nuptk'),
                        'email'            => $this->input->post('email'),
                        'telp_number'      => $this->input->post('telp_number'),
                        'city_of_birth'    => $this->input->post('city_of_birth'),
                        'date_of_birth'    => mysqlDate($this->input->post('date_of_birth')),
                        'gender'           => $this->input->post('gender'),
                        'home_address'     => $this->input->post('home_address'),
                        'golongan'         => $this->input->post('golongan'),
                        'position'         => $this->input->post('position'),
                        'office_address'   => $this->input->post('office_address'),
                        'npwp'             => $this->input->post('npwp'),

                        'origin_of_school' => $this->input->post('origin_of_school'),
                        'district'         => $this->input->post('district'),
                        'mapel'            => $this->input->post('mapel'),

                        'computers_apps'   => $this->input->post('computers_apps'),

                        'level'            => 'participator',
                    )
                    );

                    //user_past_courses
                    $course_name = $this->input->post('course_name');
                    $organizer   = $this->input->post('organizer');
                    $place       = $this->input->post('place');
                    $course_date = $this->input->post('course_date');

                    $loop = 0;
                    if (!empty($course_name)) {
                        $ex_course_name = explode(',', $course_name);
                        $ex_organizer   = explode(',', $organizer);
                        $ex_place       = explode(',', $place);
                        $ex_course_date = explode(',', $course_date);

                        foreach ($ex_course_name as $course) {

                            if (trim($course) === "") {
                                $loop++;
                                continue;
                            }

                            $this->Basecrud_m->insert('user_past_courses',
                                array('user_id' => $insert_id,
                                    'course_name'   => $course,
                                    'organizer'     => $ex_organizer[$loop],
                                    'place'         => $ex_place[$loop],
                                    'course_date'   => $ex_course_date[$loop])
                            );
                            $loop++;
                        }
                    }

                    $classroom = $this->Basecrud_m->get_where('classrooms', array('slug' => $param0))->row();

                    $this->db->where('classroom_id', $classroom->id);
                    $this->db->where('user_id', $insert_id);
                    $row_count = $this->db->count_all_results('classroom_users');

                    if ($row_count == 0) {
                        //jika belum ada pendaftaran dengan user id ini, maka masukkin aja
                        $this->Basecrud_m->insert('classroom_users', array('classroom_id' => $classroom->id,
                            'user_id'                                                         => $insert_id,
                            'status'                                                          => 'enable',
                            'surat_tugas'                                                     => $surat_tugas_filename,
                            'tipe'                                                            => 'utusan'));

                    } else {
                        //jangan kerjain apa pun
                    }

                    // $data['email_detail'] = $this->Basecrud_m->get_where('users', array('id' => $insert_id))->row();
                    // $data['classroom_slug'] = $param0;
                    //
                    // $fullname =  $this->input->post('fullname');
                    // $email =  $this->input->post('email');
                    //
                    // $this->load->library('My_PHPMailer');
                    //
                    // $mail = new PHPMailer();
                    //
                    //
                    // $mail->isSMTP();
                    // $mail->Host = 'smtp.gmail.com';
                    // $mail->Port = 587;
                    // $mail->SMTPSecure = 'tls';
                    // $mail->SMTPAuth = true;
                    //
                    // $mail->Username = $this->config->item('mail_Username');
                    // $mail->Password = $this->config->item('mail_Password');
                    // $mail->setFrom($this->config->item('mail_setFrom'), 'Admin');
                    // $mail->addReplyTo($this->config->item('mail_setFrom'), 'Admin');
                    // $mail->addAddress($email, $fullname);
                    // $mail->Subject = 'Selamat bergabung di '. $this->config->item('diklat_title');
                    // $mail->msgHTML($this->load->view('signup_email', $data, true));
                    //
                    // if (!$mail->send()) {
                    $this->Basecrud_m->update('users', $insert_id, array('active' => 'y'));
                    // }
                    //
                    // $cap = $this->_captcha();
                    // $this->session->set_userdata('captcha_word', strtolower($cap['word']));

                    echo json_encode(array(
                        'status' => 'OK',
                        'msg'    => 'Data Tersimpan')
                    );
                    exit(0);
                } else {

                    echo json_encode(array('status' => 'ERROR',
                        'msg'                           => validation_errors(),
                    )
                    );
                    exit(0);
                }

            }

            $classroom               = $this->Basecrud_m->get_where('classrooms', array('slug' => $param0))->row();
            $this->data['classroom'] = $classroom;
            $this->data['page']      = 'classroom/pendaftaran_participator';

            $this->_page_output($this->data);

        } elseif ($act === 'show') {

            $classroom                  = $this->Basecrud_m->get_where('classrooms', array('slug' => $param0))->row();
            $this->data['participator'] = $this->Classroom_m->get_users($classroom->id, 'participator');
            $this->data['classroom']    = $classroom;
            $this->data['page']         = 'classroom/participator_list';

            $this->_page_output($this->data);
        } elseif ($act === 'show_for_print') {
            // echo 'lol';
            $this->data['classroom'] = $this->Basecrud_m->get_where('classrooms', array('slug' => $param1))->row();
            $this->data['biodata']   = $this->Basecrud_m->get_where('users', array('id' => $param0))->row();
            $this->load->view('classroom/participator_print', $this->data);

            //$this->_page_output($this->data);
        } elseif ($act === 'get_data') {

            $term         = $this->input->post('term');
            $classroom_id = $this->input->post('classroom_id');

            $this->db->select('user_id');
            $this->db->where('classroom_id', $classroom_id);
            $ignore = $this->db->get('classroom_users');

            $arr_ignore = array();
            foreach ($ignore->result() as $arr) {
                array_push($arr_ignore, $arr->user_id);
            }

            $this->db->like('fullname', $term);
            // $this->db->or_like('email',$term);
            // $this->db->or_like('origin_of_school',$term);
            $this->db->where_not_in('id', $arr_ignore);
            $this->db->where('level', 'participator');
            $users = $this->db->get('users');

            $result = '[';
            foreach ($users->result() as $u) {
                $result .= '{"fullname":"' . $u->fullname . '","origin_of_school":"' . $u->origin_of_school . '", "id":' . $u->id . '},';
            }

            echo $result = $users->num_rows() > 0 ? substr($result, 0, -1) . ']' : '[]';

        } elseif ($act === 'add_user_manual') {

            $id_user      = $this->input->post('id_user');
            $classroom_id = $this->input->post('classroom_id');

            foreach ($id_user as $id) {
                //echo $id . '<br/>';
                $this->db->insert('classroom_users',
                    array('classroom_id' => $classroom_id,
                        'user_id'            => $id,
                        'status'             => 'enable',
                        'tipe'               => 'utusan'));
            }

            // echo "<script>alert('Data telah ditambahkan')</script>";
            redirect(base_url() . 'classroom/participator/show/' . $param0, 'reload');

        }
    }
}
