<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Signout extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->helper('url');

    }

    public function index()
    {
        $user_id = $this->session->userdata('user_id');
        $this->db->where('id', $user_id);
        $this->db->update('users', array('online'=>'0'));

        $this->session->sess_destroy();
        redirect(base_url(), 'reload');
    }
}
