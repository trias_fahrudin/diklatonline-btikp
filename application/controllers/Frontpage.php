<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Frontpage extends CI_Controller
{
    private $data = array();
    private $level;

    public function __construct()
    {
        parent::__construct();

        date_default_timezone_set('Asia/Jakarta');

        $this->load->database();
        $this->load->helper('url');
        $this->load->model(array(
            'Basecrud_m',
            'Classroom_m',
        ));
        $this->load->library('form_validation');

        $this->level = $this->session->userdata('user_level');

        // if ($this->level) {
        //     redirect($this->level, 'refresh');
        // }
    }

    public function _view_page($data)
    {
        $this->load->view('frontpage/web', $data);
    }

    public function index()
    {
        $this->data['data'] = $this->Classroom_m->get_active(true);
        $this->data['page'] = 'home';
        $this->_view_page($this->data);
    }

    public function classroom($classroom_name)
    {

        $this->db->like('slug', $classroom_name, 'after');
        $this->db->order_by('start', 'ASC');
        $this->data['data'] = $this->db->get('classrooms');
        $this->data['page'] = 'home_details';
        $this->_view_page($this->data);

    }

    public function signin()
    {
        if ($this->level) {
            redirect($this->level, 'refresh');
        }

        $this->load->library('recaptcha');

        if (!empty($_POST)) {

            if (strrpos(current_url(), 'localhost') === false) {
                $captcha_answer = $this->input->post('g-recaptcha-response');
                $response       = $this->recaptcha->verifyResponse($captcha_answer);

                if (!$response['success']) {
                    //$this->session->set_flashdata('k', "<div class='alert alert-danger'>Kode reCaptcha tidak Valid!</div>");
                    redirect(site_url('frontpage/signin'), 'reload');
                }
            }

            $this->form_validation->set_rules('username', 'User Name', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');

            if ($this->form_validation->run() == true) {
                $username = $this->input->post('username');
                $password = md5($this->input->post('password'));

                $captcha_answer = $this->input->post('g-recaptcha-response');
                $response       = $this->recaptcha->verifyResponse($captcha_answer);

                $qry = $this->Basecrud_m->get_where('users', array(
                    'username' => $username,
                    'password' => $password,
                    'active'   => 'y',
                ));

                if ($qry->num_rows() > 0) {

                    //valid user
                    $user = $qry->row();

                    $this->session->set_userdata('user_level', $user->level);
                    $this->session->set_userdata('user_id', $user->id);
                    $this->session->set_userdata('user_name', $user->username);
                    $this->session->set_userdata('user_fullname', $user->fullname);

                    $this->db->where('id', $user->id);
                    $this->db->update('users', array(
                        'online' => '1',
                    ));

                    // $this->session->set_userdata('user_email', $participator->email);
                    // $this->session->set_userdata('user_kotalahir', $participator->kota_lahir);
                    // $this->session->set_userdata('user_tgllahir', $participator->tgl_lahir);
                    // $this->session->set_userdata('user_jk', $participator->jk);
                    // $this->session->set_userdata('user_alamat', $participator->alamat);
                    // $this->session->set_userdata('user_status', $participator->status);
                    // $this->session->set_userdata('user_nip', $participator->nip);
                    // $this->session->set_userdata('user_nuptk', $participator->nuptk);
                    // $this->session->set_userdata('user_asalsekolah', $participator->asal_sekolah);
                    // $this->session->set_userdata('user_kabkota', $participator->kab_kota);
                    // $this->session->set_userdata('user_mapel', $participator->mapel);

                    $this->session->set_userdata('user_foto', $user->foto);
                    $this->session->set_userdata('user_st', $user->st);

                    $this->level = $this->session->userdata('user_level');

                    redirect($this->level, 'reload');
                }
            }
        }

        $this->data['page'] = 'signin';
        $this->_view_page($this->data);
    }

    public function _data_diklat()
    {
        return $this->db->query('SELECT DISTINCT name FROM classrooms');
    }

    public function get_generation()
    {
        $classroom_name = urldecode($_GET['name']);
        $this->db->select('id,generation');
        $this->db->where('name', $classroom_name);
        $generations = $this->db->get('classrooms');

        $result = '<option value="">Semua Angkatan</option>';
        foreach ($generations->result() as $g) {
            $result .= '<option value="' . $g->id . '">' . ucwords($g->generation) . '</option>';
        }

        echo $result;
    }

    public function datapeserta()
    {
        $this->db->select("a.user_id,
                             c.fullname,
                             c.foto,
                             c.origin_of_school,
                             c.mapel,
                             GROUP_CONCAT(CONCAT(TRIM(b.name),' ',b.generation,'') ORDER BY CONCAT(TRIM(b.name),' ',b.generation,'') SEPARATOR ',') AS classroom");
        $this->db->join('classrooms b', 'a.classroom_id = b.id', 'left');
        $this->db->join('users c', 'a.user_id = c.id', 'left');
        $this->db->where('a.status', 'enable');
        $this->db->where('c.level', 'participator');
        $this->db->group_by('user_id');
        $this->data['data_peserta'] = $this->db->get('classroom_users a');

        $this->data['data_diklat'] = $this->_data_diklat();
        $this->data['page']        = 'data_peserta';
        $this->_view_page($this->data);
    }

    public function datafasilitator()
    {
        /*
        <th>Username</th>
        <th>Nama Lengkap</th>
        <th>Instansi</th>
        <th>Keahlian</th>
        <th>Aksi</th>
         */

        $this->db->select('fullname,foto,institute,expertise,email');
        $this->data['data_fasilitator'] = $this->db->get('view_instructors');

        $this->data['page'] = 'data_fasilitator';
        $this->_view_page($this->data);
    }

    public function categories($categori_id)
    {

        $this->load->library('pagination');
        $total_rows = $this->db->query("SELECT * FROM news WHERE publish = '1' AND kategori = $categori_id")->num_rows();

        $url         = base_URL() . 'frontpage/categories/' . $categori_id . '/page/';
        $total_rows  = $total_rows;
        $uri_segment = 5;
        $per_page    = 10;

        $config = paginate($url, $total_rows, $per_page, $uri_segment);
        $this->pagination->initialize($config);

        $awal = $this->uri->segment(5);
        if (empty($awal) || $awal == 1) {
            $awal = 0;
        }{
            $awal = $awal;
        }
        $akhir = $config['per_page'];

        $frontpage['blog'] = $this->db->query("SELECT a.*,IF(a.sticky = 'Y',0,1) as order_sticky
                                               FROM news a
                                               WHERE publish = '1' AND kategori = $categori_id
                                               ORDER BY order_sticky ASC, tglPost DESC
                                               LIMIT $awal, $akhir");

        $frontpage['pagination'] = $this->pagination->create_links();

        $frontpage['page'] = 'news';

        $this->_view_page($frontpage);
    }

    public function _generateRandomString($length = 20)
    {
        $seed = '1234567890';

        $randomString = '';

        for ($i = 0; $i < $length; ++$i) {
            $randomString .= $seed[rand(0, strlen($seed) - 1)];
        }

        return $randomString;
    }

    public function check_captcha()
    {
        $cap = strtolower($this->input->post('captcha_word'));
        if ($this->session->userdata('captcha_word') === $cap) {
            return true;
        } else {
            $this->form_validation->set_message('check_captcha', 'Kode Captcha tidak sama. ');

            return false;
        }
    }

    public function CaptchaAjax()
    {
        $cap = $this->_captcha();
        $this->session->set_userdata('captcha_word', strtolower($cap['word']));
        echo $cap['image'];
    }

    public function _captcha()
    {
        $this->load->helper('captcha');

        $vals = array(
            'word'       => $this->_generateRandomString(4),
            'img_path'   => './captcha/',
            'img_url'    => base_url() . 'captcha/',
            'font_path'  => './captcha/font/ebrima.ttf',
            'img_width'  => '150',
            'img_height' => 30,
            'expiration' => 300, //5 menit
            // 'word_length' => 5,
            'font_size'  => 15,
            'img_id'     => 'captchaId',
            'pool'       => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',

            // White background and border, black text and red grid
            'colors'     => array(
                'background' => array(
                    255,
                    255,
                    255,
                ),
                'border'     => array(
                    255,
                    255,
                    255,
                ),
                'text'       => array(
                    0,
                    0,
                    0,
                ),
                'grid'       => array(
                    255,
                    40,
                    40,
                ),
            ),
        );

        return create_captcha($vals);
    }

    // public function classroom_user_confirmation($classroom_id,$){
    //
    // }

    public function activate($activation_code, $classroom_slug = null)
    {
        $this->db->where('activation_code', $activation_code);
        $this->db->update('users', array(
            'active' => 'Y',
        ));

        // ======== send activation link to admin =======================

        // $data['confirmation_link'] = base_url() . 'frontpage/classroom_user_confirmation/';
        //
        // $this->load->library('My_PHPMailer');
        //
        // $mail = new PHPMailer();
        //
        // //$mail->SMTPDebug = 3;
        //
        // $mail->isSMTP();
        // $mail->Host = 'smtp.gmail.com';
        // $mail->Port = 587;
        // $mail->SMTPSecure = 'tls';
        // $mail->SMTPAuth = true;
        //
        // $mail->Username = $this->config->item('mail_Username');
        // $mail->Password = $this->config->item('mail_Password');
        // $mail->setFrom($this->config->item('mail_setFrom'), 'Admin');
        // $mail->addReplyTo($this->config->item('mail_setFrom'), 'Admin');
        // $mail->addAddress($this->config->item('mail_Username'), $this->config->item('diklat_title'));
        // $mail->Subject = 'Akun baru membutuhkan konfirmasi '. $this->config->item('diklat_title');
        // $mail->msgHTML($this->load->view('admin_confirmation_email', $data, true));
        //
        // if (!$mail->send()) {
        //   echo 'Message could not be sent.';
        //   echo 'Mailer Error: ' . $mail->ErrorInfo;
        //   exit(0);
        // }

        redirect('frontpage/pendaftaran/step_4/' . $classroom_slug, 'reload');
    }

    /*
    <table id="table-normal">
    <tbody>
    <tr>
    <td>User Name:</td>
    <td><?php echo $email_detail->username; ?></td>
    </tr>
    <tr>
    <td>Password:</td>
    <td>
    <?php echo $email_detail->status === 'pns' ? $email_detail->nip  : $email_detail->nuptk;?>
    </td>
    </tr>
    </tbody>
    </table>
     */
    public function send_test_email()
    {

        //https://support.google.com/mail/answer/14257?rd=1
        $fullname = 'triasfahrudin';
        $email    = 'triasfahrudin@gmail.com';

        $data['email_detail'] = array(
            'status' => 'pns',
            'nip'    => '1111',
        );
        $this->load->library('My_PHPMailer');
        $mail = new PHPMailer();

        $mail->SMTPDebug = 3;
        $mail->isSMTP();
        $mail->Host       = 'smtp.gmail.com';
        $mail->Port       = 587;
        $mail->SMTPSecure = 'tls';
        $mail->SMTPAuth   = true;

        // $mail->isSMTP();
        // $mail->Host = 'smtp.gmail.com';
        // $mail->Port = 465;
        // $mail->SMTPSecure = 'ssl';
        // $mail->SMTPAuth = true;
        $mail->Username = 'seminar.mpipa.unja@gmail.com';
        $mail->Password = '0EWzNQhcrixo';
        $mail->setFrom('seminar.mpipa.unja@gmail.com', 'Admin');
        $mail->addReplyTo('seminar.mpipa.unja@gmail.com', 'Admin');
        $mail->addAddress($email, $fullname);
        $mail->Subject = 'Selamat bergabung di ' . $this->config->item('diklat_title');
        $mail->msgHTML($this->load->view('signup_email', $data, true));

        if (!$mail->send()) {
            //jika email ngga terkirim, aktifkan saja user ini
            //$this->Basecrud_m->update('users', $insert_id, array('active' => 'y'));
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
            exit(0);
        }

    }

    public function pendaftaran($step = null, $param0 = null)
    {
        if ($step == null || $step === 'step_1') {

        } elseif ($step === 'step_2') {

            $this->data['active_classrooms'] = $this->Classroom_m->get_active(true);

        } elseif ($step === 'step_3') {

            $cek = $this->Basecrud_m->get_where('classrooms', array(
                'slug' => $param0,
            ));

            if ($cek->num_rows() == 0) {
                redirect(base_url() . 'frontpage/pendaftaran/step_2', 'reload');
            }

            //cek apakah kelas ini sudah tutup ?
            if ($cek->row()->availability === 'close') {
                redirect(base_url() . 'frontpage/pendaftaran/step_2', 'reload');
            }

            // $course_name = $this->input->post('course_name');
            // echo $course_name[0];
            // echo $course_name[1];
            //
            // foreach ($course_name as $cn) {
            //   echo $cn . '\n';
            // }
            //
            // exit(0);

            if (!empty($_POST)) {

                $this->form_validation->set_rules('status', 'Status', 'required');
                $this->form_validation->set_rules('fullname', 'Nama Lengkap', 'required');
                $this->form_validation->set_rules('username', 'Username', 'required|alpha_numeric');

                $this->form_validation->set_rules('nip', 'NIP', 'is_unique[users.nip]');
                $this->form_validation->set_rules('nuptk', 'NUPTK', 'is_unique[users.nuptk]');

                $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
                $this->form_validation->set_rules('telp_number', 'Nomor telephon', 'numeric');

                $this->form_validation->set_rules('city_of_birth', 'Kota lahir', 'required');
                // $this->form_validation->set_rules('date_of_birth', 'Tanggal lahir', 'required');
                // $this->form_validation->set_rules('gender', 'Jenis kelamin', 'required');
                // $this->form_validation->set_rules('home_address', 'Alamat rumah', 'required');
                $this->form_validation->set_rules('golongan', 'Golongan', '');
                $this->form_validation->set_rules('position', 'Jabatan', '');
                $this->form_validation->set_rules('office_address', 'Alamat Kantor', 'required');
                $this->form_validation->set_rules('npwp', 'NPWP', '');

                $this->form_validation->set_rules('origin_of_school', 'Asal sekolah', 'required');
                $this->form_validation->set_rules('district', 'Kab/kota', 'required');
                $this->form_validation->set_rules('mapel', 'Matapelajaran', 'required');

                $this->form_validation->set_rules('captcha_word', 'Kode tidak sama!', 'callback_check_captcha');

                if ($this->form_validation->run() == true) {

                    //cek apakah surat_tugas di sertakan ?
                    $surat_tugas_filename = "";
                    if (!empty($_FILES['surat_tugas']['name'])) {
                        $upload                  = array();
                        $upload['upload_path']   = './upload/stugas';
                        $upload['allowed_types'] = 'jpeg|jpg|png|pdf';
                        $upload['encrypt_name']  = true;

                        $this->load->library('upload', $upload);

                        if (!$this->upload->do_upload('surat_tugas')) {
                            header('content-type: application/json');
                            //$data['msg'] = $this->upload->display_errors();
                            //$cap = $this->_captcha();
                            //$this->session->set_userdata('captcha_word', strtolower($cap['word']));

                            echo json_encode(array(
                                'status' => 'ERROR',
                                'msg'    => 'File surat tugas harus berformat jpeg,jpg,png atau pdf',
                                'image'  => $cap['image'],
                            ));
                            exit(0);
                        } else {
                            $success              = $this->upload->data();
                            $surat_tugas_filename = $success['file_name'];
                            // $this->session->set_userdata('user_st', $file_name);

                        }
                    }

                    $insert_id = $this->Basecrud_m->insert('users', array(
                        'status'           => $this->input->post('status'),
                        'fullname'         => $this->input->post('fullname'),
                        'username'         => $this->input->post('username'),
                        'nip'              => $this->input->post('nip'),
                        'nuptk'            => $this->input->post('nuptk'),
                        'email'            => $this->input->post('email'),
                        'telp_number'      => $this->input->post('telp_number'),
                        'city_of_birth'    => $this->input->post('city_of_birth'),
                        'date_of_birth'    => mysqlDate($this->input->post('date_of_birth')),
                        'gender'           => $this->input->post('gender'),
                        'home_address'     => $this->input->post('home_address'),
                        'golongan'         => $this->input->post('golongan'),
                        'position'         => $this->input->post('position'),
                        'office_address'   => $this->input->post('office_address'),
                        'npwp'             => $this->input->post('npwp'),

                        'origin_of_school' => $this->input->post('origin_of_school'),
                        'district'         => $this->input->post('district'),
                        'mapel'            => $this->input->post('mapel'),

                        'computers_apps'   => $this->input->post('computers_apps'),
                        'level'            => 'participator',
                    ));

                    //user_past_courses
                    $course_name = $this->input->post('course_name');
                    $organizer   = $this->input->post('organizer');
                    $place       = $this->input->post('place');
                    $course_date = $this->input->post('course_date');

                    $loop = 0;
                    if (!empty($course_name)) {

                        $ex_course_name = explode(',', $course_name);
                        $ex_organizer   = explode(',', $organizer);
                        $ex_place       = explode(',', $place);
                        $ex_course_date = explode(',', $course_date);

                        foreach ($ex_course_name as $course) {

                            if (trim($course) === "") {
                                $loop++;
                                continue;
                            }

                            $this->Basecrud_m->insert('user_past_courses', array(
                                'user_id'     => $insert_id,
                                'course_name' => $course,
                                'organizer'   => @$ex_organizer[$loop],
                                'place'       => @$ex_place[$loop],
                                'course_date' => @$ex_course_date[$loop],
                            ));
                            $loop++;
                        }
                    }

                    $classroom = $this->Basecrud_m->get_where('classrooms', array(
                        'slug' => $param0,
                    ))->row();

                    $this->db->where('classroom_id', $classroom->id);
                    $this->db->where('user_id', $insert_id);
                    $row_count = $this->db->count_all_results('classroom_users');

                    if ($row_count == 0) {
                        //jika belum ada pendaftaran dengan user id ini, maka masukkin aja
                        $this->Basecrud_m->insert('classroom_users', array(
                            'classroom_id' => $classroom->id,
                            'user_id'      => $insert_id,
                            'surat_tugas'  => $surat_tugas_filename,
                        ));

                    } else {
                        //jangan kerjain apa pun
                    }

                    // exit(0);
                    $data['email_detail'] = $this->Basecrud_m->get_where('users', array(
                        'id' => $insert_id,
                    ))->row();
                    $data['classroom_slug'] = $param0;

                    $fullname = $this->input->post('fullname');
                    $email    = $this->input->post('email');

                    $this->load->library('My_PHPMailer');

                    $mail = new PHPMailer();

                    //$mail->SMTPDebug = 3;

                    $mail->isSMTP();
                    $mail->Host       = 'smtp.gmail.com';
                    $mail->Port       = 587;
                    $mail->SMTPSecure = 'tls';
                    $mail->SMTPAuth   = true;
                    // $mail->isSMTP();
                    // $mail->Host = 'smtp.gmail.com';
                    // $mail->Port = 465;
                    // $mail->SMTPSecure = 'ssl';
                    // $mail->SMTPAuth = true;

                    /*
                    $config['mail_Username'] = 'seminar.mpipa.unja@gmail.com';
                    $config['mail_Password'] = '0EWzNQhcrixo';
                    $config['mail_setFrom'] = 'seminar.mpipa.unja@gmail.com';
                    $config['mail_addReplyTo'] = 'seminar.mpipa.unja@gmail.com';
                     */

                    $mail->Username = $this->config->item('mail_Username');
                    $mail->Password = $this->config->item('mail_Password');
                    $mail->setFrom($this->config->item('mail_setFrom'), 'Admin');
                    $mail->addReplyTo($this->config->item('mail_setFrom'), 'Admin');
                    $mail->addAddress($email, $fullname);
                    $mail->Subject = 'Selamat bergabung di ' . $this->config->item('diklat_title');
                    $mail->msgHTML($this->load->view('signup_email', $data, true));

                    if (!$mail->send()) {
                        //jika email ngga terkirim, aktifkan saja user ini
                        $this->Basecrud_m->update('users', $insert_id, array(
                            'active' => 'y',
                        ));
                        //echo 'Message could not be sent.';
                        //echo 'Mailer Error: ' . $mail->ErrorInfo;
                        //exit(0);
                    }

                    $cap = $this->_captcha();
                    $this->session->set_userdata('captcha_word', strtolower($cap['word']));
                    header('content-type: application/json');

                    echo json_encode(array(
                        'status' => 'OK',
                        'msg'    => 'Data Tersimpan',
                        'image'  => $cap['image'],
                    ));
                    exit(0);
                } else {
                    $cap = $this->_captcha();
                    $this->session->set_userdata('captcha_word', strtolower($cap['word']));
                    header('content-type: application/json');

                    echo json_encode(array(
                        'status' => 'ERROR',
                        'msg'    => validation_errors(),
                        'image'  => $cap['image'],
                    ));
                    exit(0);
                }
            }

            $cap = $this->_captcha();
            $this->session->set_userdata('captcha_word', strtolower($cap['word']));
            $this->data['captcha_image'] = $cap['image'];

            // $this->load->view('signup',$this->data);
        } elseif ($step === 'step_4') {
            $cek = $this->Basecrud_m->get_where('classrooms', array(
                'slug' => $param0,
            ));

            if ($cek->num_rows() == 0) {
                redirect(base_url() . 'frontpage/pendaftaran/step_2', 'reload');
            }

            //cek apakah kelas ini sudah tutup ?
            if ($cek->row()->availability === 'close') {
                redirect(base_url() . 'frontpage/pendaftaran/step_2', 'reload');
            }

            if (!empty($_POST)) {
                $this->form_validation->set_rules('username', 'User Name', 'required');
                $this->form_validation->set_rules('password', 'Password', 'required');

                if ($this->form_validation->run() == true) {
                    $username = $this->input->post('username');
                    $password = md5($this->input->post('password'));

                    $qry = $this->Basecrud_m->get_where('users', array(
                        'username' => $username,
                        'password' => $password,
                        'active'   => 'y',
                    ));

                    if ($qry->num_rows() > 0) {
                        $user = $qry->row();

                        $this->session->set_userdata('user_level', $user->level);
                        $this->session->set_userdata('user_id', $user->id);
                        $this->session->set_userdata('user_name', $user->username);
                        $this->session->set_userdata('user_fullname', $user->fullname);

                        $this->db->where('id', $user->id);
                        $this->db->update('users', array(
                            'online' => '1',
                        ));

                        $this->session->set_userdata('user_foto', $user->foto);

                        //echo json_encode(array('status' => 'signin_valid'));

                        $classroom = $this->Basecrud_m->get_where('classrooms', array(
                            'slug' => $param0,
                        ))->row();

                        $this->db->where('classroom_id', $classroom->id);
                        $this->db->where('user_id', $user->id);
                        $row_count = $this->db->count_all_results('classroom_users');

                        if ($row_count == 0) {
                            //jika belum ada pendaftaran dengan user id ini, maka masukkin aja
                            $this->Basecrud_m->insert('classroom_users', array(
                                'classroom_id' => $classroom->id,
                                'user_id'      => $user->id,
                            ));

                        } else {
                            //TODO : kirim konfirmasi link ke admin
                        }

                        redirect(base_url() . $user->level);
                    } else {
                        //echo json_encode(array('status' => 'not_found'));
                    }
                }

                //exit(0);
            }
        }

        $this->data['page'] = 'pendaftaran_' . $step;
        $this->_view_page($this->data);
    }

    public function absensi()
    {

        if (!empty($_POST)) {
            $barcode = $this->input->post('barcode');
            $arr     = explode('-', $barcode);

            if (count($arr) != 3) {
                $this->data['msg'] = array(
                    'msg_type' => 'error',
                    'msg'      => "[E:001] Data barcode tidak valid",
                    'nama'     => '-',
                    'kelas'    => '-',
                    'angkatan' => '-',
                    'foto'     => 'no-foto.png',

                );
            } else {

                $classroom_id = $arr[0];
                $user_id      = $arr[1];

                $this->db->select("
                    a.status,a.id,a.user_id,
                    b.name AS classroom_name,b.generation,
                    c.fullname,c.foto,
                    c.nip,c.nuptk,
                    c.origin_of_school,
                    c.district
                    "
                );

                $this->db->join('classrooms b', 'a.classroom_id = b.id', 'left');
                $this->db->join('users c', 'a.user_id = c.id', 'left');
                $this->db->where('a.classroom_id', $classroom_id);
                $this->db->where('a.user_id', $user_id);
                $data_absen = $this->db->get('classroom_users a');

                if ($data_absen->num_rows() > 0) {
                    $r = $data_absen->row_array();

                    if ($r['status'] === 'disable') {
                        $this->data['msg'] = array(
                            'msg_type' => 'warning',
                            'msg'      => "[W:003] Akun anda untuk kelas ini belum mendapat persetujuan panitia",
                            'nama'     => $r['fullname'],
                            'kelas'    => $r['classroom_name'],
                            'angkatan' => $r['generation'],
                            'foto'     => $r['foto'],

                        );
                    }else{
                        $this->data['msg'] = array(
                            'msg_type' => 'success',
                            'msg'      => "Terimakasih telah melakukan konfirmasi kehadiran",
                            'nama'     => $r['fullname'],
                            'kelas'    => $r['classroom_name'],
                            'angkatan' => $r['generation'],
                            'foto'     => $r['foto'],
                        );


                        $this->db->where(
                            array(
                                'classroom_id' => $classroom_id,
                                'user_id'      => $user_id
                            )
                        );

                        $this->db->update('classroom_users',array('kehadiran' => 'ya'));
                    }

                } else {
                    $this->data['msg'] = array(
                        'msg_type' => 'danger',
                        'msg'      => "[E:002] Data barcode tidak ditemukan!",
                        'nama'     => '-',
                        'kelas'    => '-',
                        'angkatan' => '-',
                        'foto'     => 'no-foto.png',

                    );
                }

            }

        }

        //$this->data['msg'] = array('msg' => "test");

        $this->data['page'] = 'absensi';
        $this->_view_page($this->data);
    }

    public function pertanyaan($cmd = null, $param = null)
    {
        $this->load->model('Pertanyaan_m');

        $web['title'] = 'Pertanyaan Pengunjung';
        //$this->load->view('t_atas', $web);

        $date_now = date('Y-m-d H:i:s');

        //$cap = $this->_captcha();
        //$web['captcha_image'] = $cap['image'];

        if ($cmd === 'add') {
            $cap                  = $this->_captcha();
            $web['captcha_image'] = $cap['image'];
            //$this->load->view('f_pertanyaan', $web);
            $web['page'] = 'f_pertanyaan';
        } elseif ($cmd === 'add_act') {
            $this->form_validation->set_rules('nama', 'Nama', 'required');
            $this->form_validation->set_rules('email', 'Email', 'valid_email');
            $this->form_validation->set_rules('alamat', 'Alamat', '');
            // $this->form_validation->set_rules('website', 'Website', '');
            $this->form_validation->set_rules('topik', 'Topik', 'required');
            $this->form_validation->set_rules('captcha_word', 'Captcha', 'required|callback_check_captcha');

            if ($this->form_validation->run() == true) {
                $nama = $this->input->post('nama');
                if (strtolower($nama) === 'administrator' || strtolower($nama) === 'admin') {
                    $nama = 'Guest';
                }

                $in = array(
                    'nama'        => $nama,
                    'email'       => $this->input->post('email'),
                    'alamat'      => $this->input->post('alamat'),
                    // 'website' => $this->input->post('website'),
                    'topik'       => $this->input->post('topik'),
                    'inserted_at' => $date_now,
                );

                $this->Basecrud_m->insert('pertanyaan', $in);
                $this->session->set_flashdata('k', "<div class='alert alert-success'>Saran terkirim dan Menunggu untuk di Moderasi</div>");
                redirect('frontpage/pertanyaan', 'reload');
            } else {
                $cap = $this->_captcha();
                $this->session->set_userdata('captcha_word', strtolower($cap['word']));
                $web['captcha_image'] = $cap['image'];
                $web['msg']           = validation_errors();
                //$this->load->view('f_pertanyaan', $web);
                $web['page'] = 'f_pertanyaan';
            }
        } elseif ($cmd === 'reply') {
            $r = $this->Basecrud_m->get_where('pertanyaan', array(
                'id' => $param,
            ));
            if ($r->num_rows() == 0) {

                //$this->load->view('invalid', $web);
                $web['page'] = 'invalid';
            } else {
                $web['post'] = $this->db->query("SELECT nama,topik,DATE(inserted_at) as tgl
                                       FROM pertanyaan
                                       WHERE id = $param")->row();

                $web['tanggapan'] = $this->db->query("SELECT nama,komentar,DATE(inserted_at) as tgl
                                              FROM pertanyaan_tanggapan
                                              WHERE id_pertanyaan = $param AND tampil = 'Y'
                                              ORDER BY inserted_at ASC");

                $cap = $this->_captcha();
                $this->session->set_userdata('captcha_word', strtolower($cap['word']));
                $web['captcha_image'] = $cap['image'];
                //$this->load->view('f_pertanyaan_tanggapan', $web);
                $web['page'] = 'f_pertanyaan_tanggapan';
            }
        } elseif ($cmd === 'reply_add') {
            $r = $this->Basecrud_m->get_where('pertanyaan', array(
                'id' => $param,
            ));
            if ($r->num_rows() == 0) {

                //$this->load->view('invalid', $web);
                // $web['page'] = 'invalid';
                redirect(base_url(), 'reload');
            } else {
                $this->form_validation->set_rules('nama', 'Nama', 'required');
                $this->form_validation->set_rules('email', 'Email', 'valid_email');
                $this->form_validation->set_rules('alamat', 'Alamat', '');
                // $this->form_validation->set_rules('website', 'Website', '');
                $this->form_validation->set_rules('komentar', 'Komentar', 'required');
                $this->form_validation->set_rules('captcha_word', 'Captcha', 'required|callback_check_captcha');

                if ($this->form_validation->run() == true) {

                    //$date_now = date('Y-m-d H:i:s');
                    $nama = $this->input->post('nama');

                    if (strtolower($nama) === 'administrator' || strtolower($nama) === 'admin') {
                        $nama = 'Guest';
                    }

                    $in = array(
                        'id_pertanyaan' => $param,
                        'nama'          => $nama,
                        'email'         => $this->input->post('email'),
                        'alamat'        => $this->input->post('alamat'),
                        // 'website' => $this->input->post('website'),
                        'komentar'      => $this->input->post('komentar'),
                        'inserted_at'   => $date_now,
                    );

                    $this->Basecrud_m->insert('pertanyaan_tanggapan', $in);
                    $this->session->set_flashdata('k', "<div class='alert alert-success'>Tanggapan terkirim dan Menunggu untuk di Moderasi</div>");
                    redirect('frontpage/pertanyaan/reply/' . $param, 'reload');
                } else {
                    $web['post'] = $this->db->query("SELECT nama,topik,DATE(inserted_at) as tgl
                                          FROM pertanyaan
                                          WHERE id = $param")->row();

                    $web['tanggapan'] = $this->db->query("SELECT nama,komentar,DATE(inserted_at) as tgl
                                                FROM pertanyaan_tanggapan
                                                WHERE id_pertanyaan = $param AND tampil = 'Y'
                                                ORDER BY inserted_at ASC");
                    $cap = $this->_captcha();
                    $this->session->set_userdata('captcha_word', strtolower($cap['word']));
                    $web['captcha_image'] = $cap['image'];
                    $web['msg']           = validation_errors();
                }

                //$this->load->view('f_pertanyaan_tanggapan', $web);
                $web['page'] = 'f_pertanyaan_tanggapan';
            }
        } else {

            //pagination
            $url      = base_url() . 'frontpage/pertanyaan/';
            $res      = $this->Pertanyaan_m->get('numrows');
            $per_page = 3;
            $config   = paginate($url, $res, $per_page, 3);
            $this->pagination->initialize($config);

            $this->Pertanyaan_m->limit = $per_page;
            if ($this->uri->segment(3) == true) {
                $this->Pertanyaan_m->offset = $this->uri->segment(3);
            } else {
                $this->Pertanyaan_m->offset = 0;
            }

            $this->Pertanyaan_m->sort  = 'inserted_at';
            $this->Pertanyaan_m->order = 'DESC';
            //end pagination

            $web['data'] = $this->Pertanyaan_m->get('pagging');
            //$this->load->view('v_pertanyaan', $web);
            $web['page'] = 'v_pertanyaan';
        }

        //$this->load->view('t_footer');

        $this->_view_page($web);
    }

    public function ekspresi($cmd = null, $param = null)
    {
        $this->load->model('Ekspresi_m');

        $web['title'] = 'Ekspresi Pengunjung';
        //$this->load->view('t_atas', $web);

        $date_now = date('Y-m-d H:i:s');

        //$cap = $this->_captcha();
        //$web['captcha_image'] = $cap['image'];

        if ($cmd === 'add') {
            $cap                  = $this->_captcha();
            $web['captcha_image'] = $cap['image'];
            //$this->load->view('f_ekspresi', $web);
            $web['page'] = 'f_ekspresi';
        } elseif ($cmd === 'add_act') {
            $this->form_validation->set_rules('nama', 'Nama', 'required');
            $this->form_validation->set_rules('email', 'Email', 'valid_email');
            $this->form_validation->set_rules('alamat', 'Alamat', '');
            $this->form_validation->set_rules('judul', 'Judul', 'required');
            $this->form_validation->set_rules('isi_ekspresi', 'Isi Ekspresi', 'required');
            $this->form_validation->set_rules('captcha_word', 'Captcha', 'required|callback_check_captcha');

            if ($this->form_validation->run() == true) {
                $nama = $this->input->post('nama');
                if (strtolower($nama) === 'administrator' || strtolower($nama) === 'admin') {
                    $nama = 'Guest';
                }

                $in = array(
                    'nama'         => $nama,
                    'email'        => $this->input->post('email'),
                    'alamat'       => $this->input->post('alamat'),
                    'judul'        => $this->input->post('judul'),
                    'isi_ekspresi' => $this->input->post('isi_ekspresi'),
                    'inserted_at'  => $date_now,
                );

                $this->Basecrud_m->insert('ekspresi', $in);
                $this->session->set_flashdata('k', "<div class='alert alert-success'>Ekspresi terkirim dan Menunggu untuk di Moderasi</div>");
                redirect('frontpage/ekspresi', 'reload');
            } else {
                $cap = $this->_captcha();
                $this->session->set_userdata('captcha_word', strtolower($cap['word']));
                $web['captcha_image'] = $cap['image'];
                $web['msg']           = validation_errors();
                //$this->load->view('f_ekspresi', $web);
                $web['page'] = 'f_ekspresi';
            }
        } elseif ($cmd === 'reply') {
            $r = $this->Basecrud_m->get_where('ekspresi', array(
                'id' => $param,
            ));
            if ($r->num_rows() == 0) {

                //$this->load->view('invalid', $web);
                $web['page'] = 'invalid';
            } else {
                $web['post'] = $this->db->query("SELECT nama,judul,isi_ekspresi,DATE(inserted_at) as tgl
                                       FROM ekspresi
                                       WHERE id = $param")->row();

                $web['tanggapan'] = $this->db->query("SELECT nama,komentar,DATE(inserted_at) as tgl
                                              FROM ekspresi_tanggapan
                                              WHERE id_ekspresi = $param AND tampil = 'Y'
                                              ORDER BY inserted_at ASC");

                $cap = $this->_captcha();
                $this->session->set_userdata('captcha_word', strtolower($cap['word']));
                $web['captcha_image'] = $cap['image'];
                //$this->load->view('f_ekspresi_tanggapan', $web);
                $web['page'] = 'f_ekspresi_tanggapan';
            }
        } elseif ($cmd === 'reply_add') {
            $r = $this->Basecrud_m->get_where('ekspresi', array(
                'id' => $param,
            ));
            if ($r->num_rows() == 0) {

                //$this->load->view('invalid', $web);
                // $web['page'] = 'invalid';
                redirect(base_url(), 'reload');
            } else {
                $this->form_validation->set_rules('nama', 'Nama', 'required');
                $this->form_validation->set_rules('email', 'Email', 'valid_email');
                $this->form_validation->set_rules('alamat', 'Alamat', '');
                // $this->form_validation->set_rules('website', 'Website', '');
                $this->form_validation->set_rules('komentar', 'Komentar', 'required');
                $this->form_validation->set_rules('captcha_word', 'Captcha', 'required|callback_check_captcha');

                if ($this->form_validation->run() == true) {

                    //$date_now = date('Y-m-d H:i:s');
                    $nama = $this->input->post('nama');

                    if (strtolower($nama) === 'administrator' || strtolower($nama) === 'admin') {
                        $nama = 'Guest';
                    }

                    $in = array(
                        'id_ekspresi' => $param,
                        'nama'        => $nama,
                        'email'       => $this->input->post('email'),
                        'alamat'      => $this->input->post('alamat'),
                        // 'website' => $this->input->post('website'),
                        'komentar'    => $this->input->post('komentar'),
                        'inserted_at' => $date_now,
                    );

                    $this->Basecrud_m->insert('ekspresi_tanggapan', $in);
                    $this->session->set_flashdata('k', "<div class='alert alert-success'>Tanggapan terkirim dan Menunggu untuk di Moderasi</div>");
                    redirect('frontpage/ekspresi/reply/' . $param, 'reload');
                } else {
                    $web['post'] = $this->db->query("SELECT nama,judul,isi_ekspresi,
                                                 DATE(inserted_at) as tgl
                                          FROM ekspresi
                                          WHERE id = $param")->row();

                    $web['tanggapan'] = $this->db->query("SELECT nama,komentar,DATE(inserted_at) as tgl
                                                FROM ekspresi_tanggapan
                                                WHERE id_ekspresi = $param AND tampil = 'Y'
                                                ORDER BY inserted_at ASC");
                    $cap = $this->_captcha();
                    $this->session->set_userdata('captcha_word', strtolower($cap['word']));
                    $web['captcha_image'] = $cap['image'];
                    $web['msg']           = validation_errors();
                }

                //$this->load->view('f_ekspresi_tanggapan', $web);
                $web['page'] = 'f_ekspresi_tanggapan';
            }
        } else {

            //pagination
            $url      = base_url() . 'frontpage/ekspresi/';
            $res      = $this->Ekspresi_m->get('numrows');
            $per_page = 3;
            $config   = paginate($url, $res, $per_page, 3);
            $this->pagination->initialize($config);

            $this->Ekspresi_m->limit = $per_page;
            if ($this->uri->segment(3) == true) {
                $this->Ekspresi_m->offset = $this->uri->segment(3);
            } else {
                $this->Ekspresi_m->offset = 0;
            }

            $this->Ekspresi_m->sort  = 'inserted_at';
            $this->Ekspresi_m->order = 'DESC';
            //end pagination

            $web['data'] = $this->Ekspresi_m->get('pagging');
            //$this->load->view('v_ekspresi', $web);
            $web['page'] = 'v_ekspresi';
        }

        //$this->load->view('t_footer');

        $this->_view_page($web);
    }

    public function ide_saran($cmd = null, $param = null)
    {
        $this->load->model('Ide_saran_m');

        $web['title'] = 'Ide & Saran Pengunjung';
        //$this->load->view('t_atas', $web);

        $date_now = date('Y-m-d H:i:s');

        //$cap = $this->_captcha();
        //$web['captcha_image'] = $cap['image'];

        if ($cmd === 'add') {
            $cap                  = $this->_captcha();
            $web['captcha_image'] = $cap['image'];
            //$this->load->view('f_ide_saran', $web);
            $web['page'] = 'f_ide_saran';
        } elseif ($cmd === 'add_act') {
            $this->form_validation->set_rules('nama', 'Nama', 'required');
            $this->form_validation->set_rules('email', 'Email', 'valid_email');
            $this->form_validation->set_rules('alamat', 'Alamat', '');
            // $this->form_validation->set_rules('website', 'Website', '');
            $this->form_validation->set_rules('topik', 'Topik', 'required');
            $this->form_validation->set_rules('captcha_word', 'Captcha', 'required|callback_check_captcha');

            if ($this->form_validation->run() == true) {
                $nama = $this->input->post('nama');
                if (strtolower($nama) === 'administrator' || strtolower($nama) === 'admin') {
                    $nama = 'Guest';
                }

                $in = array(
                    'nama'        => $nama,
                    'email'       => $this->input->post('email'),
                    'alamat'      => $this->input->post('alamat'),
                    // 'website' => $this->input->post('website'),
                    'topik'       => $this->input->post('topik'),
                    'inserted_at' => $date_now,
                );

                $this->Basecrud_m->insert('ide_saran', $in);
                $this->session->set_flashdata('k', "<div class='alert alert-success'>Saran terkirim dan Menunggu untuk di Moderasi</div>");
                redirect('frontpage/ide_saran', 'reload');
            } else {
                $cap = $this->_captcha();
                $this->session->set_userdata('captcha_word', strtolower($cap['word']));
                $web['captcha_image'] = $cap['image'];
                $web['msg']           = validation_errors();
                //$this->load->view('f_ide_saran', $web);
                $web['page'] = 'f_ide_saran';
            }
        } elseif ($cmd === 'reply') {
            $r = $this->Basecrud_m->get_where('ide_saran', array(
                'id' => $param,
            ));
            if ($r->num_rows() == 0) {

                //$this->load->view('invalid', $web);
                $web['page'] = 'invalid';
            } else {
                $web['post'] = $this->db->query("SELECT nama,topik,DATE(inserted_at) as tgl
                                       FROM ide_saran
                                       WHERE id = $param")->row();

                $web['tanggapan'] = $this->db->query("SELECT nama,komentar,DATE(inserted_at) as tgl
                                              FROM ide_saran_tanggapan
                                              WHERE id_ide_saran = $param AND tampil = 'Y'
                                              ORDER BY inserted_at ASC");

                $cap = $this->_captcha();
                $this->session->set_userdata('captcha_word', strtolower($cap['word']));
                $web['captcha_image'] = $cap['image'];
                //$this->load->view('f_ide_saran_tanggapan', $web);
                $web['page'] = 'f_ide_saran_tanggapan';
            }
        } elseif ($cmd === 'reply_add') {
            $r = $this->Basecrud_m->get_where('ide_saran', array(
                'id' => $param,
            ));
            if ($r->num_rows() == 0) {

                //$this->load->view('invalid', $web);
                // $web['page'] = 'invalid';
                redirect(base_url(), 'reload');
            } else {
                $this->form_validation->set_rules('nama', 'Nama', 'required');
                $this->form_validation->set_rules('email', 'Email', 'valid_email');
                $this->form_validation->set_rules('alamat', 'Alamat', '');
                // $this->form_validation->set_rules('website', 'Website', '');
                $this->form_validation->set_rules('komentar', 'Komentar', 'required');
                $this->form_validation->set_rules('captcha_word', 'Captcha', 'required|callback_check_captcha');

                if ($this->form_validation->run() == true) {

                    //$date_now = date('Y-m-d H:i:s');
                    $nama = $this->input->post('nama');

                    if (strtolower($nama) === 'administrator' || strtolower($nama) === 'admin') {
                        $nama = 'Guest';
                    }

                    $in = array(
                        'id_ide_saran' => $param,
                        'nama'         => $nama,
                        'email'        => $this->input->post('email'),
                        'alamat'       => $this->input->post('alamat'),
                        // 'website' => $this->input->post('website'),
                        'komentar'     => $this->input->post('komentar'),
                        'inserted_at'  => $date_now,
                    );

                    $this->Basecrud_m->insert('ide_saran_tanggapan', $in);
                    $this->session->set_flashdata('k', "<div class='alert alert-success'>Tanggapan terkirim dan Menunggu untuk di Moderasi</div>");
                    redirect('frontpage/ide_saran/reply/' . $param, 'reload');
                } else {
                    $web['post'] = $this->db->query("SELECT nama,topik,DATE(inserted_at) as tgl
                                          FROM ide_saran
                                          WHERE id = $param")->row();

                    $web['tanggapan'] = $this->db->query("SELECT nama,komentar,DATE(inserted_at) as tgl
                                                FROM ide_saran_tanggapan
                                                WHERE id_ide_saran = $param AND tampil = 'Y'
                                                ORDER BY inserted_at ASC");
                    $cap = $this->_captcha();
                    $this->session->set_userdata('captcha_word', strtolower($cap['word']));
                    $web['captcha_image'] = $cap['image'];
                    $web['msg']           = validation_errors();
                }

                //$this->load->view('f_ide_saran_tanggapan', $web);
                $web['page'] = 'f_ide_saran_tanggapan';
            }
        } else {

            //pagination
            $url      = base_url() . 'frontpage/ide_saran/';
            $res      = $this->Ide_saran_m->get('numrows');
            $per_page = 3;
            $config   = paginate($url, $res, $per_page, 3);
            $this->pagination->initialize($config);

            $this->Ide_saran_m->limit = $per_page;
            if ($this->uri->segment(3) == true) {
                $this->Ide_saran_m->offset = $this->uri->segment(3);
            } else {
                $this->Ide_saran_m->offset = 0;
            }

            $this->Ide_saran_m->sort  = 'inserted_at';
            $this->Ide_saran_m->order = 'DESC';
            //end pagination

            $web['data'] = $this->Ide_saran_m->get('pagging');
            //$this->load->view('v_ide_saran', $web);
            $web['page'] = 'v_ide_saran';
        }

        //$this->load->view('t_footer');

        $this->_view_page($web);
    }

    public function classroom_details($only_detail_kegiatan = false)
    {
        $classroomid = $this->input->get('classroom_id');

        if ($only_detail_kegiatan !== false) {
            //show only detail kegiatan
            $this->db->where('a.id', $classroomid);
            $classroom = $this->db->get('classrooms a')->row();
            echo $classroom->detail_kegiatan;

        } else {
            $this->db->select("a.name,a.generation,a.registration_quota,
                           DATE_FORMAT(a.start,'%d-%m-%Y') AS date_start,
                           DATE_FORMAT(a.end,'%d-%m-%Y') AS date_end,
                           a.tempat,a.narasumber", false);
            $this->db->where('a.id', $classroomid);
            $classroom = $this->db->get('classrooms a')->row();

            $this->db->select("b.fullname");
            $this->db->join('users b', 'a.user_id = b.id', 'left');
            $this->db->where('a.classroom_id', $classroomid);
            $this->db->where('b.level', 'instructor');
            $participator = $this->db->get('classroom_users a');

            $participator_row = "";
            $par_count        = 1;
            foreach ($participator->result() as $par) {
                $participator_row .= $par_count . "." . strtoupper($par->fullname) . "<br/>";
                $par_count++;
            }

            //get enable user on current classroom
            $this->db->join('users b', 'a.user_id = b.id', 'left');
            $this->db->where('a.status', 'enable');
            $this->db->where('a.classroom_id', $classroomid);
            $this->db->where('b.level', 'participator');
            $this->db->where('a.tipe', 'reguler');

            $signup_online = $this->db->count_all_results('classroom_users a');

            $signup_offline = $classroom->registration_quota - $signup_online;

            $total_signup = $signup_online + $signup_offline;

            //MATERI
            $this->db->select("c.fullname,d.title,d.file_attachment");
            $this->db->join("courses b", "a.course_id = b.id", "left");
            $this->db->join("users c", "a.instructor_id = c.id", "left");
            $this->db->join("materials_bank d", "a.material_id = d.id", "left");
            $this->db->where("b.classroom_id", $classroomid);
            $this->db->order_by("c.fullname", "ASC");
            $this->db->order_by("d.title", "ASC");
            $material_rs = $this->db->get("course_instructor a");

            // echo "<pre>";
            // var_dump($material_rs->result());
            // echo "</pre>";

            $curr_instructor = "";
            $material_td     = "";
            $i               = 1;
            foreach ($material_rs->result() as $mat) {

                $instructor = $mat->fullname;

                if ($curr_instructor !== $instructor) {

                    $material_td .= "<new_instructor style=\"font: bold 15px/30px Georgia, serif;\"> - " . $i . "." . strtoupper($mat->fullname) . "</new_instructor><br/>";
                    if ($mat->file_attachment !== "") {
                        $material_td .= "<a href=\"" . base_url() . "upload/" . $mat->file_attachment . "\"> - " . strtoupper($mat->title) . "</a><br/>";
                    } else {
                        $material_td .= " - " . strtoupper($mat->title) . "<br/>";
                    }

                    $curr_instructor = $mat->fullname;
                    $i++;
                } else {

                    if ($mat->file_attachment !== "") {
                        $material_td .= "<a href=\"" . base_url() . "upload/" . $mat->file_attachment . "\"> - " . strtoupper($mat->title) . "</a><br/>";
                    } else {
                        $material_td .= " - " . strtoupper($mat->title) . "<br/>";
                    }

                }

            }

            $output = "<table class=\"table table-striped table-bordered display nowrap\" cellspacing=\"0\" width=\"100%\">
            <tr>
              <td width=200px>NAMA KEGIATAN</td>
              <td width=20px> : </td>
              <td width=400px> " . strtoupper($classroom->name) . "</td>
            </tr>
            <tr>
              <td width=200px>ANGKATAN</td>
              <td width=20px> : </td>
              <td width=400px> " . strtoupper($classroom->generation) . "</td>
            </tr>
            <tr>
              <td width=200px>WAKTU PELAKSANAAN</td>
              <td width=20px> : </td>
              <td width=400px> $classroom->date_start s/d $classroom->date_end </td>
            </tr>
            <tr>
              <td width=200px>TEMPAT</td>
              <td width=20px> : </td>
              <td width=400px> " . strtoupper($classroom->tempat) . "</td>
            </tr>
            <tr>
              <td width=200px>
                PESERTA </br>
                - PENDAFTAR ONLINE <br/>
                - UTUSAN KAB/KOTA <br/>
              </td>
              <td width=20px> : </td>
              <td width=400px>
                " . $total_signup . " Orang<br/>
                " . $signup_online . " Orang<br/>
                " . $signup_offline . " Orang
              </td>
            </tr>
            <tr>
              <td width=200px>NARASUMBER</td>
              <td width=20px> : </td>
              <td width=400px> " . strtoupper($classroom->narasumber) . '<br/>' . $participator_row . "</td>
            </tr>
            <tr>
              <td width=200px>MATERI</td>
              <td width=20px> : </td>
              <td width=400px>" . $material_td . "</td>
            </tr>

        </table>";
            echo $output;
        }

    }

    public function laporankegiatan()
    {
        // $this->data['active_classrooms'] = $this->Classroom_m->get_active(true);
        // $this->db->order_by("name","asc");
        // $this->db->order_by("start","asc");
        $this->data['active_classrooms'] = $this->db->get("classrooms");
        $this->data['page']              = 'laporan_kegiatan';
        $this->_view_page($this->data);
    }

    public function galeri()
    {

        $this->data['title'] = '.:: Album Foto Galeri Website Dinas Pendidikan Provinsi Jambi ::.';

        $ke  = $this->uri->segment(3);
        $idu = $this->uri->segment(4);

        $this->data['data'] = $this->db->query("SELECT * FROM galeri_album ORDER BY id DESC")->result();

        if ($ke == "lihat") {

            $gal = $this->Basecrud_m->get_where('galeri_album', array(
                'id' => $idu,
            ));
            if ($gal->num_rows() == 0) {
                $this->data['page'] = 'invalid';
            } else {
                $this->data['datdet'] = $this->db->query("SELECT * FROM galeri WHERE id_album = '$idu'")->result();
                $this->data['datalb'] = $this->db->query("SELECT * FROM galeri_album WHERE id = '$idu'")->row();

                $this->data['page'] = 'v_galeri_detil';
            }

        } else {
            //$this->load->view('v_galeri', $web);
            $this->data['page'] = 'v_galeri';
        }

        //$this->load->view('t_footer');
        $this->_view_page($this->data);
    }
}
