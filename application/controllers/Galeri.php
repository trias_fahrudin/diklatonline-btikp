<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Galeri extends CI_Controller
{
    private $data = array();
    private $user_level;

    public function __construct()
    {
        parent::__construct();

        date_default_timezone_set('Asia/Jakarta');

        $this->load->database();
        $this->load->helper('url');
        $this->load->model(array('Basecrud_m', 'Classroom_m'));
        $this->load->library('form_validation');

        $this->user_level = $this->session->userdata('user_level');


        // if (!$this->user_level || $this->user_level !== 'admin') {
        //     redirect('frontpage/signin', 'refresh');
        // }

        page_auth(array('admin'));
        // if ($this->level) {
        //     redirect($this->level, 'refresh');
        // }
    }

    public function _generate_page($data = null)
    {
        $this->load->view($this->user_level.'/masterpage', $data);
    }


    public function index(){
      $m['data'] = $this->db->query('SELECT * FROM galeri_album')->result();

      $m['page'] = 'galeri/v_galeri';
      $this->_generate_page($m);
    }

    public function manage()
    {
        $config['upload_path'] = 'upload/galeri';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
        $config['max_size'] = '6000';
        $config['max_width'] = '6000';
        $config['max_height'] = '6000';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        $ke = $this->uri->segment(3);
        $idu = $this->uri->segment(4);

        $m['data'] = $this->db->query('SELECT * FROM galeri_album')->result();

        $m['page'] = 'galeri/v_galeri';

        if ($ke === 'set_slideshow') {
            $id = addslashes($this->input->post('id'));
            $act = addslashes($this->input->post('act'));

            if ($act === 'add_slide') {
                $this->Basecrud_m->update('galeri', $id, array('slideshow' => 'Y'));
            } else {
                $this->Basecrud_m->update('galeri', $id, array('slideshow' => 'N'));
            }
        } elseif ($ke === 'add_album') {
            $nama_album = addslashes($this->input->post('nama_album'));
            $this->db->query("INSERT INTO galeri_album VALUES ('', '$nama_album')");
            $this->session->set_flashdata('k', '<div class="alert alert-success">Album berhasil ditambahkan</div>');
            redirect('galeri/manage');
        } elseif ($ke === 'del_album') {
            $gambar = $this->db->query("SELECT file FROM galeri WHERE id_album = '$idu'")->result();
            foreach ($gambar as $g) {
                @unlink('./upload/galeri/'.$g->file);
                @unlink('./upload/galeri/small/S_'.$g->file);
            }
            $this->db->query("DELETE FROM galeri WHERE id_album = '$idu'");
            $this->db->query("DELETE FROM galeri_album WHERE id = '$idu'");
            $this->session->set_flashdata('k', '<div class="alert alert-success">Album berhasil dihapus</div>');
            redirect('galeri/manage');
        } elseif ($ke === 'atur') {
            $m['datdet'] = $this->db->query("SELECT * FROM galeri WHERE id_album = '$idu'")->result();
            $m['detalb'] = $this->db->query("SELECT * FROM galeri_album WHERE id = '$idu'")->row();

            $m['page'] = 'galeri/v_galeri_detil';
        } elseif ($ke === 'rename_album') {
            $id_alb1 = $this->input->post('id_alb1');
            $nama_album = addslashes($this->input->post('nama_album'));
            $this->db->query("UPDATE galeri_album SET nama = '$nama_album' WHERE id = '$id_alb1'");
            $this->session->set_flashdata('k', '<div class="alert alert-success">Album berhasil diubah namanya</div>');
            redirect('galeri/manage/atur/'.$id_alb1);
        } elseif ($ke === 'upload_foto') {
            $id_alb2 = $this->input->post('id_alb2');
            $judul = addslashes($this->input->post('judul'));
            $ket = addslashes($this->input->post('ket'));
            $slideshow = addslashes($this->input->post('slideshow'));

            if (!empty($_FILES['foto']['name'])) {
                if ($this->upload->do_upload('foto') == true) {
                    $this->upload->do_upload('foto');
                    $up_data = $this->upload->data();

                    $this->Basecrud_m->insert('galeri', array('id_album' => $id_alb2,
                                 'file' => $up_data['file_name'],
                                 'judul' => $judul,
                                 'ket' => $ket,
                                 'slideshow' => $slideshow, ));
                } else {
                    $this->session->set_flashdata('k', '<div class="alert alert-error">'.$this->upload->display_errors().'</div>');
                    redirect('galeri/manage/atur/'.$id_alb2);
                }

                $this->session->set_flashdata('k', '<div class="alert alert-success">Gambar berhasil diupload</div>');
                redirect('galeri/manage/atur/'.$id_alb2);
            } else {
                $this->session->set_flashdata('k', '<div class="alert alert-error">Gambar masih kosong</div>');
                redirect('galeri/manage/atur/'.$id_alb2);
            }
        } elseif ($ke === 'del_foto') {
            $id_foto = $this->uri->segment(5);

            $q_ambil_foto = $this->db->query("SELECT file FROM galeri WHERE id = '$id_foto'")->row();

            @unlink('./upload/galeri/'.$q_ambil_foto->file);

            $this->db->query("DELETE FROM galeri WHERE id = '$id_foto'");
            $this->session->set_flashdata('k', '<div class="alert alert-success">Foto berhasil dihapus</div>');
            redirect('galeri/manage/atur/'.$idu);
        } elseif ($ke === 'edit_foto') {
            $id_foto = $this->uri->segment(5);

            if (!empty($_POST)) {
                $this->form_validation->set_rules('judul', 'Judul', 'xss_clean|required');
                $this->form_validation->set_rules('ket', 'keterangan', 'xss_clean');

                $msg = null;
                if ($this->form_validation->run() == true) {
                    $up = array('judul' => $this->input->post('judul'),
                  'ket' => $this->input->post('ket'),
                  'slideshow' => $this->input->post('slideshow'), );

                    if (!empty($_FILES['foto']['name'])) {
                        if (!$this->upload->do_upload('foto')) {
                            $msg = $this->upload->display_errors();
                        } else {
                            $success = $this->upload->data();
                            $up['file'] = $success['file_name'];
                        }
                    }

                    $this->Basecrud_m->update('galeri', $id_foto, $up);
                    $this->session->set_flashdata('k', '<div class="alert alert-success">Data berhasil diupdate</div>');
                    redirect('galeri/manage/atur/'.$idu);
                } else {
                    $m['msg'] = $msg.' <br>'.validation_errors();
                }
            }

            $m['data'] = $this->db->query("SELECT * FROM galeri WHERE id = $id_foto")->row();
            $m['datdet'] = $this->db->query("SELECT * FROM galeri WHERE id_album = '$idu'")->result();
            $m['detalb'] = $this->db->query("SELECT * FROM galeri_album WHERE id = '$idu'")->row();

            $m['page'] = 'galeri/v_galeri_detil';
        } else {
            $m['page'] = 'galeri/v_galeri';
        }

        // $this->load->view('manage/tampil', $m);
        $this->_generate_page($m);
    }
}
