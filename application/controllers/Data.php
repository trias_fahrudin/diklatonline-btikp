<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Data extends CI_Controller
{
    private $data = array();
    private $user_level = null;
    private $user_id;

    public function __construct()
    {
        parent::__construct();

        date_default_timezone_set('Asia/Jakarta');

        $this->load->database();
        $this->load->helper('url');
        $this->load->model(array('Basecrud_m', 'Material_m','Classroom_m'));

        $this->load->library('form_validation');

        $this->user_level = $this->session->userdata('user_level');
        $this->user_id = $this->session->userdata('user_id');

        // if (!$this->user_level) {
        //     redirect('frontpage/signin', 'refresh');
        // }

        //true || (false && true)
        //false || true

        // if (!$this->user_level || ($this->user_level !== 'admin' && $this->user_level !== 'instructor')) {
        //     redirect('frontpage/signin', 'refresh');
        // }

        page_auth(array('admin','instructor'));
    }

    public function _page_output($data = null)
    {
        $this->load->view($this->user_level.'/masterpage', $data);
    }

    public function index($act = null, $param0 = null)
    {
        $this->data['page'] = 'data/home';
        $this->_page_output($this->data);
    }

    public function import($type = null)
    {
      if ($type === 'instructor') {
          $this->data['title'] = 'Instruktur';
        }elseif ($type === 'material_categories') {
          $this->data['title'] = 'Kategori Materi';
        }elseif ($type === 'material') {
          $this->data['title'] = 'Materi';
        }


        if (!empty($_POST)) {
            $config = array();

            $config['upload_path'] = './upload';
            $config['allowed_types'] = 'xls';
            $config['encrypt_name'] = true;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('userfile')) {
                $data['msg'] = 'danger:'.$this->upload->display_errors();
            } else {
                include_once APPPATH.'libraries/excel_reader2.php';
                $xl_data = new Spreadsheet_Excel_Reader($_FILES['userfile']['tmp_name']);

                $j = 0;
                $new_rows = 0;
                $updated_rows = 0;

                if ($type === 'instructor') {
                    //username,fullname,telp_number,email
                    for ($i = 1; $i <= ($xl_data->rowcount($sheet_index = 0)); ++$i) {
                        $username = $xl_data->val($i, 1);
                        $fullname = $xl_data->val($i, 2);
                        $telp_number = $xl_data->val($i, 3);
                        $email = $xl_data->val($i, 4);

                        $cek = $this->Basecrud_m->get_where('users', array('username' => $trim($username)));
                        if ($cek->num_rows() == 0) {
                            $this->Basecrud_m->insert('users', array(
                                'username' => $username,
                                'fullname' => $fullname,
                                'telp_number' => $telp_number,
                                'email' => $email,
                                'level' => 'instructor', )
                            );

                            ++$new_rows;
                        } else {
                            $this->db->where('username', $username);
                            $this->db->update('users', array(
                                'fullname' => $fullname,
                                'telp_number' => $telp_number,
                                'email' => $email,
                                'level' => 'instructor', )
                            );

                            ++$updated_rows;
                        }
                    }
                } elseif ($act === 'material_categories') {
                  //name*,description
                  for ($i = 1; $i <= ($xl_data->rowcount($sheet_index = 0)); ++$i) {
                      $name = $xl_data->val($i, 1);
                      $description = $xl_data->val($i, 2);

                      $cek = $this->Basecrud_m->get_where('material_categories', array('name' => $trim($name)));

                      if ($cek->num_rows() == 0) {
                          //insert new data
                          $this->Basecrud_m->insert('material_categories', array(
                              'name' => $name,
                              'description' => $description)
                          );

                          ++$new_rows;
                      } else {

                          $this->db->where('name', $name);

                          $this->db->update('material_categories', array(
                              'name' => $name,
                              'description' => $description)
                          );

                          ++$updated_rows;
                      }
                  }
                }elseif ($act === 'material') {
                  //material_categories,title*,content
                  for ($i = 1; $i <= ($xl_data->rowcount($sheet_index = 0)); ++$i) {
                      $material_categories = $xl_data->val($i, 1);
                      $rs_cat = $this->db->query("SELECT id FROM material_categories
                                                  WHERE name LIKE '%$material_categories%'");

                      $material_categories_id = 0;
                      if($rs_cat->num_rows() > 0){
                        $material_categories_id = $rs_cat->row()->id;
                      }

                      $title = $xl_data->val($i, 2);
                      $content = $xl_data->val($i, 3);

                      $cek = $this->Basecrud_m->get_where('materials_bank', array('title' => $trim($title)));

                      if ($cek->num_rows() == 0) {
                          //insert new data
                          $this->Basecrud_m->insert(
                              'materials_bank', array(
                              'material_categories_id' => $material_categories_id,
                              'title' => $title,
                              'content' => $content
                            )
                          );

                          ++$new_rows;
                      } else {

                          $this->db->where('title', $title);

                          $this->db->update('materials_bank',array(
                              'material_categories_id' => $material_categories_id,
                              'title' => $title,
                              'content' => $content
                            )
                          );

                          ++$updated_rows;
                      }
                  }
                }

                $success = $this->upload->data();
                $file_name = $success['file_name'];

                @unlink('./upload/'.$file_name);
                $this->data['msg'] = 'success:Data berhasil dimasukkan dengan '.$new_rows.' Data baru '.'dan '.$updated_rows.' Data Lama / Data Update';
            }
        }

        $this->data['page'] = 'data/import_data';
        $this->_page_output($this->data);
    }

    public function instructor($act = null, $param0 = null)
    {
        if ($act === 'add') {
            $this->data['page'] = 'data/instructor_form';
            $this->_page_output($this->data);
        } elseif ($act === 'add_act') {


            $this->form_validation->set_rules('username', 'User name', 'required|is_unique[users.username]');
            // $this->form_validation->set_rules('password', 'Deskripsi', 'required');
            $this->form_validation->set_rules('fullname', 'Nama lengkap', 'required');
            $this->form_validation->set_rules('telp_number', 'Nomor telephon');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
            $this->form_validation->set_rules('home_address', 'Alamat rumah');
            $this->form_validation->set_rules('city_of_birth', 'Kota kelahiran');
            $this->form_validation->set_rules('date_of_birth', 'Tanggal lahir','required');
            $this->form_validation->set_rules('gender', 'Jenis kelamin', 'required');
            $this->form_validation->set_rules('nip', 'NIP', 'required');
            $this->form_validation->set_rules('institute', 'Instansi');
            // $this->form_validation->set_rules('faculty', 'Fakultas');
            // $this->form_validation->set_rules('departement', 'Jurusan');
            // $this->form_validation->set_rules('prog_studi', 'Program studi');
            $this->form_validation->set_rules('expertise', 'Keahlian', 'required');
            $this->form_validation->set_rules('golongan', 'Golongan');
            $this->form_validation->set_rules('position', 'Jabatan');
            $this->form_validation->set_rules('last_education', 'Pendidikan terakhir');
            // $this->form_validation->set_rules('active', 'Tanggal Selesai', 'required');


            if ($this->form_validation->run() == true) {
                $in = array(
                        'username' => $this->input->post('username'),
                        'fullname' => $this->input->post('fullname'),
                        'telp_number' => $this->input->post('telp_number'),
                        'email' => $this->input->post('email'),
                        'home_address' => $this->input->post('home_address'),
                        'city_of_birth' => $this->input->post('city_of_birth'),
                        'date_of_birth' => mysqlDate($this->input->post('date_of_birth')),
                        'gender' => $this->input->post('gender'),
                        'level' => 'instructor',
                        'nip' => $this->input->post('nip'),
                        'npwp' => $this->input->post('npwp'),
                        'institute' => $this->input->post('institute'),
                        // 'faculty' => $this->input->post('faculty'),
                        // 'departement' => $this->input->post('departement'),
                        // 'prog_studi' => $this->input->post('prog_studi'),
                        'expertise' => $this->input->post('expertise'),
                        'golongan' => $this->input->post('golongan'),
                        'position' => $this->input->post('position'),
                        'last_education' => $this->input->post('last_education'),
                        'active' => 'y',
                    );

                $this->Basecrud_m->insert('users', $in);
                redirect('data/instructor', 'reload');
            } else {
                $this->data['msg'] = 'danger:'.validation_errors();
                $this->data['page'] = 'data/instructor_form';
                $this->_page_output($this->data);
            }
        } elseif ($act === 'edit') {
            $this->data['page'] = 'data/instructor_form';
            $this->data['instructor'] = $this->Basecrud_m->get_where('users', array('id' => $param0))->row();
            $this->_page_output($this->data);
        } elseif ($act === 'edit_act') {
            $this->form_validation->set_rules('username', 'User name', 'required');
            // $this->form_validation->set_rules('password', 'Deskripsi', 'required');
            $this->form_validation->set_rules('fullname', 'Nama lengkap', 'required');
            $this->form_validation->set_rules('telp_number', 'Nomor telephon');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
            $this->form_validation->set_rules('home_address', 'Alamat rumah');
            $this->form_validation->set_rules('city_of_birth', 'Kota kelahiran');
            $this->form_validation->set_rules('date_of_birth', 'Tanggal lahir','required');
            $this->form_validation->set_rules('gender', 'Jenis kelamin', 'required');
            $this->form_validation->set_rules('nip', 'NIP', 'required');
            $this->form_validation->set_rules('npwp', 'NPWP', 'required');
            $this->form_validation->set_rules('institute', 'Instansi');
            // $this->form_validation->set_rules('faculty', 'Fakultas');
            // $this->form_validation->set_rules('departement', 'Jurusan');
            // $this->form_validation->set_rules('prog_studi', 'Program studi');
            $this->form_validation->set_rules('expertise', 'Keahlian', 'required');
            $this->form_validation->set_rules('golongan', 'Golongan');
            $this->form_validation->set_rules('position', 'Jabatan');
            $this->form_validation->set_rules('last_education', 'Pendidikan terakhir');
            // $this->form_validation->set_rules('active', 'Tanggal Selesai', 'required');


            if ($this->form_validation->run() == true) {
                $up = array(
                        // 'username'       => $this->input->post('username'),
                        'fullname' => $this->input->post('fullname'),
                        'telp_number' => $this->input->post('telp_number'),
                        'email' => $this->input->post('email'),
                        'home_address' => $this->input->post('home_address'),
                        'city_of_birth' => $this->input->post('city_of_birth'),
                        'date_of_birth' => mysqlDate($this->input->post('date_of_birth')),
                        'gender' => $this->input->post('gender'),
                        'nip' => $this->input->post('nip'),
                        'npwp' => $this->input->post('npwp'),
                        'institute' => $this->input->post('institute'),
                        // 'faculty' => $this->input->post('faculty'),
                        // 'departement' => $this->input->post('departement'),
                        // 'prog_studi' => $this->input->post('prog_studi'),
                        'expertise' => $this->input->post('expertise'),
                        'golongan' => $this->input->post('golongan'),
                        'position' => $this->input->post('position'),
                        'last_education' => $this->input->post('last_education'),
                );

                $this->Basecrud_m->update('users', $param0, $up);
                redirect('data/instructor', 'reload');
            } else {
                $this->data['msg'] = 'danger:'.validation_errors();
                $this->data['instructor'] = $this->Basecrud_m->get_where('users', array('id' => $param0))->row();
                $this->data['page'] = 'data/instructor_form';
                $this->_page_output($this->data);
            }
        } elseif ($act === 'delete') {

            //delete users
            $this->db->where('id',$param0);
            $this->db->delete('users');

            //delete classroom_users
            $this->db->where('user_id',$param0);
            $this->db->delete('classroom_users');

            redirect('data/instructor', 'reload');
        } else {
            $this->data['instructor'] = $this->Basecrud_m->get_where('users', array('level' => 'instructor'), 'username', 'ASC');
            $this->data['page'] = 'data/instructor_list';
            $this->_page_output($this->data);
        }
    }

    public function participator($act = null, $param0 = null)
    {
        if ($act === 'delete') {

          //delete users
          $this->db->where('id',$param0);
          $this->db->delete('users');

          //delete classroom_users
          $this->db->where('user_id',$param0);
          $this->db->delete('classroom_users');

          //delete course_task_files
          $this->db->where('user_id',$param0);
          $this->db->delete('course_task_files');

          //delete user_past_courses
          $this->db->where('user_id',$param0);
          $this->db->delete('user_past_courses');

          //delete user_scores
          $this->db->where('user_id',$param0);
          $this->db->delete('user_scores');

          redirect('data/participator', 'reload');

        } elseif($act === 'show') {

            $classroom = $this->Basecrud_m->get_where('classrooms',array('slug' => $param0))->row();
            $this->data['participator'] = $this->Classroom_m->get_users($classroom->id,'participator');
            $this->data['classroom'] = $classroom;
            $this->data['page'] = 'data/participator_list';

            $this->_page_output($this->data);

        }else{

            $this->data['participator'] = $this->Basecrud_m->get_where('users', array('level' => 'participator'), 'username', 'ASC');
            $this->data['page'] = 'data/participator_list';
            $this->_page_output($this->data);

        }
    }

    public function material_categories($act = null, $param0 = null)
    {
        if ($act === 'add') {
            $this->data['page'] = 'data/material_categories_form';
            $this->_page_output($this->data);
        } elseif ($act === 'add_act') {
            $this->form_validation->set_rules('name', 'Nama kategori', 'required');
            $this->form_validation->set_rules('description', 'Deskripsi', 'required');

            if ($this->form_validation->run() == true) {
                $in = array(
                  'name' => $this->input->post('name'),
                  'description' => $this->input->post('description'),
                );

                $this->Basecrud_m->insert('material_categories', $in);
                redirect('data/material_categories', 'reload');
            } else {
                $this->data['msg'] = 'danger:'.validation_errors();
                $this->data['page'] = 'data/material_categories_form';
                $this->_page_output($this->data);
            }
        } elseif ($act === 'edit') {
            $this->data['page'] = 'data/material_categories_form';
            $this->data['categori'] = $this->Basecrud_m->get_where('material_categories', array('id' => $param0))->row();
            $this->_page_output($this->data);
        } elseif ($act === 'edit_act') {
            $this->form_validation->set_rules('name', 'Nama kategori', 'required');
            $this->form_validation->set_rules('description', 'Deskripsi', 'required');

            if ($this->form_validation->run() == true) {
                $up = array(
                  'name' => $this->input->post('name'),
                  'description' => $this->input->post('description'));

                $this->Basecrud_m->update('material_categories', $param0, $up);
                redirect('data/material_categories', 'reload');
            } else {
                $this->data['msg'] = 'danger:'.validation_errors();
                $this->data['categori'] = $this->Basecrud_m->get_where('material_categories', array('id' => $param0))->row();
                $this->data['page'] = 'data/material_categories_form';
                $this->_page_output($this->data);
            }
        } elseif ($act === 'delete') {

            $this->db->where('id', $param0);
            $this->db->delete('material_categories');

            redirect('data/material_categories', 'reload');
        } else {
            // $this->Material_m->sort = 'a.id';
            // $this->Material_m->order = 'ASC';

            $this->data['categories'] = $this->Basecrud_m->get('material_categories');
            $this->data['page'] = 'data/material_categories';
            $this->_page_output($this->data);
        }
    }

    public function material_home()
    {
        $this->data['page'] = 'data/material_home';
        $this->_page_output($this->data);
    }

    public function material($act = null, $param0 = null)
    {
        if ($act === 'add') {
            $this->data['material_categories'] = $this->Basecrud_m->get('material_categories');
            $this->data['page'] = 'data/material_form';
            $this->_page_output($this->data);
        } elseif ($act === 'add_act') {
            $this->form_validation->set_rules('material_categori_id', 'Kategori ID', 'required');
            $this->form_validation->set_rules('title', 'Judul', 'required');
            $this->form_validation->set_rules('content', 'Konten', 'required');

            if ($this->form_validation->run() == true) {

                $file_name = "";

                if (!empty($_FILES['file_attachment']['name'])) {
                    $upload = array();
                    $upload['upload_path'] = './upload';
                    $upload['allowed_types'] = 'rar|zip|pdf|doc|docx|ppt';
                    $upload['encrypt_name'] = true;

                    $this->load->library('upload', $upload);

                    if (!$this->upload->do_upload('file_attachment')) {
                        //$data['msg'] = $this->upload->display_errors();
                        echo $this->upload->display_errors();
                    } else {
                        $success = $this->upload->data();
                        $file_name = $success['file_name'];
                      }
                }

                $in = array(
                'material_categori_id' => $this->input->post('material_categori_id'),
                'title' => $this->input->post('title'),
                'content' => $this->input->post('content'),
                'file_attachment' => $file_name);

                $this->Basecrud_m->insert('materials_bank', $in);
                redirect('data/material', 'reload');
            } else {
                $this->data['material_categories'] = $this->Basecrud_m->get_where('material_categories');
                $this->data['msg'] = 'danger:'.validation_errors();
                $this->data['page'] = 'data/material_form';
                $this->_page_output($this->data);
            }
        } elseif ($act === 'edit') {

            $this->data['page'] = 'data/material_form';
            $this->data['material_categories'] = $this->Basecrud_m->get('material_categories');
            $this->data['material'] = $this->Basecrud_m->get_where('materials_bank', array('id' => $param0))->row();
            $this->_page_output($this->data);

        } elseif ($act === 'edit_act') {

            $this->form_validation->set_rules('material_categori_id', 'Kategori ID', 'required');
            $this->form_validation->set_rules('title', 'Judul', 'required');
            $this->form_validation->set_rules('content', 'Konten', 'required');

            if ($this->form_validation->run() == true) {

                $file_name = "";

                if (!empty($_FILES['file_attachment']['name'])) {
                    $upload = array();
                    $upload['upload_path'] = './upload';
                    $upload['allowed_types'] = 'rar|zip|pdf|doc|docx|ppt';
                    $upload['encrypt_name'] = true;

                    $this->load->library('upload', $upload);

                    if (!$this->upload->do_upload('file_attachment')) {
                        //$data['msg'] = $this->upload->display_errors();
                        echo $this->upload->display_errors();
                        exit(0);
                    } else {
                        $success = $this->upload->data();
                        $file_name = $success['file_name'];
                      }
                }

                $up = array(
                  'material_categori_id' => $this->input->post('material_categori_id'),
                  'title' => $this->input->post('title'),
                  'content' => $this->input->post('content'),
                  'file_attachment' => $file_name
                );

                $this->Basecrud_m->update('materials_bank', $param0, $up);
                redirect('data/material', 'reload');
            } else {
                $this->data['msg'] = 'danger:'.validation_errors();
                $this->data['material_categories'] = $this->Basecrud_m->get_where('material_categories');
                $this->data['material'] = $this->Basecrud_m->get_where('materials_bank', array('id' => $param0))->row();
                $this->data['page'] = 'data/material_form';
                $this->_page_output($this->data);
            }
        } elseif ($act === 'delete') {
            $this->db->where('id', $param0);
            $this->db->delete('materials_bank');

            redirect('data/material', 'reload');
        } else {
            $this->Material_m->sort = 'a.id';
            $this->Material_m->order = 'ASC';

            $this->data['materials'] = $this->Material_m->get('showall');
            $this->data['page'] = 'data/material';
            $this->_page_output($this->data);
        }
    }


}
