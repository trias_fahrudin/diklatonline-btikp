<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Instructor extends CI_Controller
{

    private $data = array();
    private $user_level = null;
    private $user_id;

    public function __construct()
    {
        parent::__construct();

        date_default_timezone_set('Asia/Jakarta');

        $this->load->database();
        $this->load->helper('url');
        $this->load->model(array('Basecrud_m','Classroom_m'));

        $this->load->library('form_validation');

        $this->user_level        = $this->session->userdata('user_level');
        $this->user_id           = $this->session->userdata('user_id');

        // if(!$this->user_level || $this->user_level !== 'instructor'){
        //     redirect('frontpage/signin','refresh');
        // }
        page_auth(array('instructor'));

    }

    public function _page_output($data = null)
    {
        $this->load->view('instructor/masterpage',$data);
    }

    function _setting($name){
      $rs = $this->Basecrud_m->get_where('settings',array('name' => $name))->row();
      return $rs->value;
    }

    public function index()
    {
        $this->data['dashboard_preface'] = $this->_setting('dashboard_preface');

        $user_classrooms = array();
        //instructor
        $classrooms = $this->Classroom_m->get_by_user($this->user_id,'instructor');
        foreach ($classrooms->result() as $cl) {
            array_push($user_classrooms,$cl->id);
        }

        $this->data['page'] = 'instructor/dashboard';
        $this->_page_output($this->data);
    }

}
