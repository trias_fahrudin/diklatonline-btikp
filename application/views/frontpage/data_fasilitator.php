<style>
  div.dataTables_wrapper {
      width: 800px;
      margin: 0 auto;
  }
</style>
<div style="background-color: #f7f7f7;padding:10px">
   <div class="panel panel-default">
      <div class="panel-heading">
         <h3 class="panel-title"><strong>Data Fasilitator </strong></h3>

      </div>
      <div class="panel-body">
        <table id="example" class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%">
           <thead>
              <tr>
                <th>Nama Lengkap</th>
                <th>Email</th>
                <th>Instansi</th>
                <th>Keahlian</th>
              </tr>
           </thead>
           <tfoot>
              <tr>
                <th>Nama Lengkap</th>
                <th>Email</th>
                <th>Instansi</th>
                <th>Keahlian</th>
              </tr>
           </tfoot>
           <tbody>
             <?php foreach ($data_fasilitator->result() as $df) { ?>
               <tr>
                  <td style="text-align:center;vertical-align:middle;">
                    <img src="<?php echo base_url() . 'timthumb?src=' . base_url() . 'upload/people/' . $df->foto . '&h=40&w=40&zc=0'; ?>" alt="person" class="img-circle width-40" />
                    <br/>
                    <?php echo ucwords($df->fullname);?>
                  </td>
                  <td style="text-align:center;vertical-align:middle;"><?php echo ucwords($df->email);?></td>
                  <td style="text-align:center;vertical-align:middle;"><?php echo ucwords($df->institute);?></td>
                  <td style="text-align:center;vertical-align:middle;"><?php echo ucwords($df->expertise);?></td>
               </tr>
             <?php } ?>

           </tbody>
        </table>

      </div>
    </div>
</div>


<script>
   $(document).ready(function() {
     $('#example').DataTable( {
      "scrollX": true
      } );
   } );

  //  $('#diklat').on('change', function() {
  //    //alert( this.value ); // or $(this).val()
  //    $.get(base + 'frontpage/get_generation', { name: this.value } )
  //      .done(function( data ) {
  //        //alert( "Data Loaded: " + data );
  //        $('#angkatan').html(data);
  //    });
  //  });

</script>
