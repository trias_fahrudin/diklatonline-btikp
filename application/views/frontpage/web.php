<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Sistem Informasi Diklat Online Balai Teknologi Informasi Komunikasi Pendidikan</title>
  <link rel="shortcut icon" href="images/webicon.ico">

  <meta name="description" content="Source code generated using layoutit.com">
  <meta name="author" content="LayoutIt!">

  <style type="text/css">
    #loading-div-background {
      display: none;
      position: fixed;
      top: 0;
      left: 0;
      background: #fff;
      width: 100%;
      height: 100%;
    }

    #loading-div {
      width: 300px;
      height: 150px;
      background-color: #fff;
      border: 5px solid #1468b3;
      text-align: center;
      color: #202020;
      position: absolute;
      left: 50%;
      top: 50%;
      margin-left: -150px;
      margin-top: -100px;
      -webkit-border-radius: 5px;
      -moz-border-radius: 5px;
      border-radius: 5px;
    }

    .block {
      /*background: none repeat scroll 0 0 #FFFFFF;*/
      border: 0px solid #CCCCCC;
      margin: 1em 0;
    }

    .form-signin {
      max-width: 330px;
      padding: 15px;
      margin: 0 auto;
    }

    .form-signin .form-signin-heading,
    .form-signin .checkbox {
      margin-bottom: 10px;
    }

    .form-signin .checkbox {
      font-weight: normal;
    }

    .form-signin .form-control {
      position: relative;
      font-size: 16px;
      height: auto;
      padding: 10px;
      -webkit-box-sizing: border-box;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
    }

    .form-signin .form-control:focus {
      z-index: 2;
    }

    .form-signin input[type="text"] {
      margin-bottom: -1px;
      border-bottom-left-radius: 0;
      border-bottom-right-radius: 0;
    }

    .form-signin input[type="password"] {
      margin-bottom: 10px;
      border-top-left-radius: 0;
      border-top-right-radius: 0;
    }

    .account-wall {
      margin-top: 20px;
      padding: 40px 0px 20px 0px;
      background-color: #f7f7f7;
      -moz-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
      -webkit-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
      box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    }

    .login-title {
      color: #555;
      font-size: 18px;
      font-weight: 400;
      display: block;
    }
  </style>


  <?php
  $first_uri = $this->uri->segment(1,'index');
  if($first_uri === 'index'){
  ?>
  <!-- just for index page -->
  <link href="<?php echo base_url();?>assets/back/css/theme-core.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/back/css/module-essentials.min.css" rel="stylesheet" />
  <!-- <link href="<?php echo base_url();?>assets/back/css/module-material.min.css" rel="stylesheet" /> -->
  <link href="<?php echo base_url();?>assets/back/css/module-colors-buttons.min.css" rel="stylesheet" />
   <!-- <link href="http://localhost/diklatonline/assets/back/css/module-layout.min.css" rel="stylesheet" /> -->
  <!------------------------>
  <style>
  /*.navbar-default {
    background-color: #f8f8f8;
    border-color: #e7e7e7;
  }

  .navbar-default .navbar-brand {
    color: #777;
  }

  .navbar-default .navbar-nav>li>a {
    color: #212121;
  }

  .navbar-default .navbar-brand:focus, .navbar-default .navbar-brand:hover {
    color: #5e5e5e;
    background-color: transparent;
  }

  .navbar-default .navbar-nav>.active>a, .navbar-default .navbar-nav>.active>a:focus, .navbar-default .navbar-nav>.active>a:hover {
    color: #555;
    background-color: #e7e7e7;
  }*/
  </style>
  <?php } ?>

  <link href="<?php echo base_url() . 'assets/front/'?>css/bootstrap.min.css" rel="stylesheet">

  <link href="<?php echo base_url() . 'assets/front/'?>css/style.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/front/'?>jquery.datepick/jquery.datepick.css">
  <link href="<?php echo base_url() . 'assets/front/fancybox/jquery.fancybox.css'?>" rel="stylesheet" type="text/css"  media="screen" />

  <script type="text/javascript">
    var base = "<?php echo base_url()?>";
  </script>


  <script src="<?php echo base_url() . 'assets/front/'?>js/jquery.min.js"></script>

  <script type="text/javascript" src="<?php echo base_url() . 'assets/front/'?>jquery.datepick/jquery.plugin.js"></script>
  <script type="text/javascript" src="<?php echo base_url() . 'assets/front/'?>jquery.datepick/jquery.datepick.js"></script>
  <script type="text/javascript" src="<?php echo base_url() . 'assets/front/'?>jquery.datepick/jquery.datepick-id.js"></script>

  <script src="<?php echo base_url() . 'assets/front/'?>js/bootstrap.min.js"></script>
  <script src="<?php echo base_url() . 'assets/front/'?>js/scripts.js"></script>

  <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/front/'?>DataTables/datatables.min.css"/>

  <script type="text/javascript" src="<?php echo base_url() . 'assets/front/'?>DataTables/datatables.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url() . 'assets/front/'?>jquery-easy-ticker/jquery.easy-ticker.js"></script>
  <script src="<?php echo base_url() . 'assets/front/'?>jquery.maskedinput.min.js" type="text/javascript"></script>



  <script type="text/javascript" src="<?php echo base_url() . 'assets/front/fancybox/jquery.fancybox.js'?>"></script>
  <script type="text/javascript" src="<?php echo base_url() . 'assets/front/fancybox/jquery.mousewheel.js'?>"></script>

  <script>
    $(document).ready(function () {




       	$('.ticker1').easyTicker({
         		direction: 'up',
         		easing: 'swing',
         		speed: 'slow',
         		interval: 2000,
         		height: 'auto',
         		visible: 4,
         		mousePause: 1,
         		controls: {
         			up: '',
         			down: '',
         			toggle: '',
         			playText: 'Play',
         			stopText: 'Stop'
         		}
       	});


    });
  </script>
</head>

<body style="background-color: #E9E9E9;">

  <div class="container-fluid" style="margin-bottom:50px;margin-top:15px">
    <div class="row" style="margin-right:50px;margin-left:50px;background-color:#f7f7f7;padding-bottom:50px">
      <div class="col-md-12">
        <div class="carousel slide" id="carousel-253173" data-ride="carousel">
          <ol class="carousel-indicators">
            <li class="active" data-slide-to="0" data-target="#carousel-253173">
            </li>
            <li data-slide-to="1" data-target="#carousel-253173">
            </li>
            <li data-slide-to="2" data-target="#carousel-253173">
            </li>
          </ol>
          <div class="carousel-inner">

            <?php
            $this->db->select('value');
            $this->db->like('name','slideshow','after');
            $rs = $this->db->get('settings');

            $i = 1;
            foreach($rs->result() as $slide){
            ?>

            <div class="item <?php echo $i == 1 ? 'active':'' ?>">
              <img style="display:block;margin:0 auto" alt="Carousel Bootstrap First" src="<?php echo base_url() . 'upload/' . $slide->value;?>">
              <div class="carousel-caption">
                <!-- <h4>
								First Thumbnail label
							</h4>
                <p>
                  Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
                </p> -->
              </div>
            </div>
            <?php $i++;} ?>

            <!-- <div class="item">
              <img style="display:block;margin:0 auto" alt="Carousel Bootstrap Second" src="<?php echo base_url() . 'assets/front/';?>sliderimg/02.jpg">
              <div class="carousel-caption">
                <h4>
								Second Thumbnail label
							</h4>
                <p>
                  Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
                </p>
              </div>
            </div>
            <div class="item">
              <img style="display:block;margin:0 auto" alt="Carousel Bootstrap Third" src="<?php echo base_url() . 'assets/front/';?>sliderimg/03.jpg">
              <div class="carousel-caption">
                <h4>
								Third Thumbnail label
							</h4>
                <p>
                  Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
                </p>
              </div>
            </div> -->
          </div> <a class="left carousel-control" href="#carousel-253173" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a> <a class="right carousel-control" href="#carousel-253173" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
        </div>

        <nav class="navbar navbar-default" role="navigation" style="margin-top:10px">
          <div class="navbar-header">

            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
              <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
            </button> <a class="navbar-brand" href="<?php echo base_url()?>"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Beranda</a>
          </div>

          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
              <?php $active_segment =  $this->uri->segment(2);?>
			   <li <?php echo $active_segment ==='galeri' ? 'class="active"' : ''?>>
                  <a href="http://cloud.disdik.jambiprov.go.id/index.php/s/gT6KccOE0PG0JSu"><span class="glyphicon glyphicon-download" aria-hidden="true"></span> Download</a>
                </li>
                <li <?php echo $active_segment ==='galeri' ? 'class="active"' : ''?>>
                  <a href="<?php echo base_url() . 'frontpage/galeri'?>"><span class="glyphicon glyphicon-picture" aria-hidden="true"></span> Galeri</a>
                </li>

                <li <?php echo $active_segment ==='ide_saran' ? 'class="active"' : ''?>>
                  <a href="<?php echo base_url() . 'frontpage/ide_saran'?>"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> Ide & Saran</a>
                </li>
                <li <?php echo $active_segment ==='ekspresi' ? 'class="active"' : ''?>>
                  <a href="<?php echo base_url() . 'frontpage/ekspresi'?>"><span class="glyphicon glyphicon-bullhorn" aria-hidden="true"></span> Ekspresi</a>
                </li>

                <li <?php echo $active_segment ==='pertanyaan' ? 'class="active"' : ''?>>
                  <a href="<?php echo base_url() . 'frontpage/pertanyaan'?>"><span class="glyphicon glyphicon-comment" aria-hidden="true"></span> Pertanyaan</a>
                </li>
                <li <?php echo $active_segment==='signin' ? 'class="active"' : ''?>>
                  <a href="<?php echo base_url() . 'frontpage/signin'?>"><span class="glyphicon glyphicon-log-in" aria-hidden="true"></span> Login</a>
                </li>
            </ul>
          </div>

        </nav>
        <div class="row">
          <div class="col-md-3">
            <div class="list-group">
              <!-- <a href="#" class="list-group-item active">Home</a> -->
              <div class="list-group-item">
                <a href="<?php echo base_url() . 'frontpage/pendaftaran/step_1'?>">
                  <img src="<?php echo base_url() . 'assets/front/images/pendaftaran.gif' ?>">
                </a>

              </div>
              <div class="list-group-item">
                <a href="<?php echo base_url() . 'frontpage/datapeserta'?>">
                  <img src="<?php echo base_url() . 'assets/front/images/datapeserta.gif' ?>">
                </a>
              </div>
              <div class="list-group-item">
                <a href="<?php echo base_url() . 'frontpage/datafasilitator'?>">
                  <img src="<?php echo base_url() . 'assets/front/images/datafasilitator.gif' ?>">
                </a>
              </div>

              <div class="list-group-item">
                <a href="<?php echo base_url() . 'frontpage/laporankegiatan'?>">
                  <img src="<?php echo base_url() . 'assets/front/images/laporankegiatan.gif' ?>">
                </a>
              </div>

              <!-- <div class="list-group-item">
                <h4 class="list-group-item-heading">
								List group item heading
							  </h4>
                <p class="list-group-item-text">
                  ...
                </p>
              </div> -->
              <!-- <div class="list-group-item">
                <span class="badge">14</span>Help
              </div> <a class="list-group-item active"><span class="badge">14</span>Help</a> -->
            </div>
            <ul style="background-color: #fff;padding-top:10px;padding-bottom:10px">
              <b>Link Tautan</b>
              <li>
                 <a href="http://disdik.jambiprov.go.id/">Portal Disdik Prov.Jambi</a>
              </li>
              <li>
               <a href="http://dapodik.jambiprov.go.id/">Dapodik</a>
              </li>
              <li>
               <a href="http://disdik.jambiprov.go.id/web">Official Website</a>
              </li>
              <li>
               <a href="http://jambi-belajar.org/">Jambi Belajar</a>
              </li>
              <li>
                <a href="http://e-office.disdik.jambiprov.go.id/">e-Office</a>
              </li>
              <li>
               <a href="http://disdik.jambiprov.go.id/web/forum">e-Forum</a>
              </li>
              <li>
                <a href="http://simpeg.disdik.jambiprov.go.id/">e-Kepegawaian</a>
              </li>
<li>
                <a href="http://dapodik.jambiprov.go.id/mirroring">Sudung Data Pendidikan</a>
              </li>
<li>
                <a href="https://mail.jambiprov.go.id/">Mail Disdik</a>
              </li>

            </ul>

            <ul style="background-color: #fff;padding-top:10px;padding-bottom:10px">

               <b> Technical Support</b></br>

               <li>Hely Kurniawan, S.Kom, M.S.I</br>
                 <a href="mailto:helykurniawan@gmail.com">helykurniawan@gmail.com</a><br>
               </li>

              <li>Zainul Haviz, S.Kom</br>
                <a href="mailto:zainoelh@yahoo.com">zainoelh@yahoo.com</a><br>
              </li>

              <li>Aris Setiawan, S.Kom, M.S.I</br>
                <a href="mailto:aris.mahera@gmail.com">aris.mahera@gmail.com</a><br>
              </li>

            </ul>

          </div>
          <div class="col-md-9">
            <?php include $page . '.php' ?>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id="loading-div-background">
    <div id="loading-div" class="ui-corner-all">
    <img style="height:50px;width:50px;margin:20px;" src="<?php echo base_url()?>assets/back/images/please_wait.gif" alt="Loading.."/><br>Memproses<br>Mohon tunggu
    </div>
  </div>

    <footer>
            <div>
               <center>
                 Sistem Informasi Diklat Online Kegiatan Pengembangan TIK Pendidikan
               </center>
 <center>
                 Balai Teknologi Informasi Komunikasi Pendidikan (BTIKP) Dinas Pendidikan Provinsi Jambi
               </center>
               <center>
                 Copyright &copy; 2017. All right Reserved.
               </center>
            </div>
         </footer>




  <!-- <script src="<?php echo base_url() . 'assets/back/js-user/essential.js'?>" type="text/javascript"></script> -->
  <!-- <script src="<?php echo base_url() . 'assets/back/js-user/signup.js'?>" type="text/javascript"></script> -->
  <script>
  $(".fancybox").fancybox();
  $(".various").fancybox({
         maxWidth    : 800,
         maxHeight   : 600,
         fitToView   : false,
         width       : '70%',
         height      : '70%',
         autoSize    : false,
         closeClick  : false,
         openEffect  : 'none',
         closeEffect : 'none'
      });
  </script>
</body>

</html>