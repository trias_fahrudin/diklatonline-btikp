<div class="panel panel-default" style="margin:10px 100px 50px 100px">
   <div class="panel-heading">
      <h3 class="panel-title"><strong>Form Absensi </strong></h3>
      <!-- <div style="float:right; font-size: 80%; position: relative; top:-10px"><a href="#">Forgot password?</a></div> -->
   </div>
   <div class="panel-body">
      <?php if(!empty($_POST)){ ?>
        
        <form class="form" action="" method="POST">            
            <input style="margin-bottom:5px" type="text" class="form-control" placeholder="Barcode" required autofocus name="barcode">            
            <!-- <button class="btn btn-lg btn-primary btn-block" type="submit">Masuk</button> -->
        </form>

        <?php if (isset($msg['msg'])) { ?>
        <div class='alert alert-<?php echo $msg["msg_type"]; ?>' role="alert"><?php echo $msg["msg"]; ?></div>
        <?php } ?>
        
        <img src="<?php echo site_url('timthumb?src=' . site_url('upload/people/' . $msg['foto'] .'&h=200&w=200&zc=0'))?>" alt="Shoe" class="img-responsive center-block"/>

         <table class="table table-bordered">          
          <tbody>
            <tr>          
              <td>Nama</td>
              <td><?php echo $msg['nama']?></td>              
            </tr>
            <tr>          
              <td>Kelas</td>
              <td><?php echo $msg['kelas']?></td>              
            </tr>
            <tr>          
              <td>Angkatan</td>
              <td><?php echo $msg['angkatan']?></td>              
            </tr>            
          </tbody>
        </table>
        
        
      <?php }else{ ?>
        <img src="<?php echo site_url('timthumb?src=' . site_url('upload/people/no-foto.png&h=200&w=200&zc=0'))?>" alt="Shoe" class="img-responsive center-block"/>
        <table class="table table-bordered">
          
          <tbody>
            <tr>          
              <td>Nama</td>
              <td>-</td>              
            </tr>
            <tr>          
              <td>Kelas</td>
              <td>-</td>              
            </tr>
            <tr>          
              <td>Angkatan</td>
              <td>-</td>              
            </tr>
            <tr>          
              <td>Nama</td>
              <td>-</td>              
            </tr>
          </tbody>
        </table>
        <form class="form" action="" method="POST">
            
            <input style="margin-bottom:5px" type="text" class="form-control" placeholder="Barcode" required autofocus name="barcode">
            
            <!-- <button class="btn btn-lg btn-primary btn-block" type="submit">Masuk</button> -->
        </form>
      <?php } ?> 
    </div>
</div>
