<style>
.thumbnails {
	margin-left: -20px;
	list-style: none;
	*zoom: 1;
}

.thumbnails:before,
.thumbnails:after {
	display: table;
	line-height: 0;
	content: "";
}

.thumbnails:after {
	clear: both;
}

.row-fluid .thumbnails {
	margin-left: 0;
}

.thumbnails > li {
	float: left;
	margin-bottom: 20px;
	margin-left: 20px;
}

.thumbnail {
	display: block;
	padding: 4px;
	line-height: 20px;
	border: 1px solid #ddd;
	-webkit-border-radius: 4px;
		 -moz-border-radius: 4px;
					border-radius: 4px;
	-webkit-box-shadow: 0 1px 3px rgba(0, 0, 0, 0.055);
		 -moz-box-shadow: 0 1px 3px rgba(0, 0, 0, 0.055);
					box-shadow: 0 1px 3px rgba(0, 0, 0, 0.055);
	-webkit-transition: all 0.2s ease-in-out;
		 -moz-transition: all 0.2s ease-in-out;
			 -o-transition: all 0.2s ease-in-out;
					transition: all 0.2s ease-in-out;
}

a.thumbnail:hover {
	border-color: #0088cc;
	-webkit-box-shadow: 0 1px 4px rgba(0, 105, 214, 0.25);
		 -moz-box-shadow: 0 1px 4px rgba(0, 105, 214, 0.25);
					box-shadow: 0 1px 4px rgba(0, 105, 214, 0.25);
}

.thumbnail > img {
	display: block;
	min-width: 100%;
	margin-right: auto;
	margin-left: auto;
}

.thumbnail .caption {
	padding: 9px;
	color: #555555;
}
</style>

<div class="col-md-12 well" style="margin-left: 0px;background-color: #fff;">
	<ol class="breadcrumb">
		<!-- <li><a href="<?php echo base_URL()?>">Beranda</a></li> -->
		<li class="active">Galeri</li>
	</ol>

	<h3>Album Galeri Foto</h3>
	<div class="span12 wellwhite" style="margin-left: 0px">
	<!-- <legend style="margin-bottom: 10px"></legend> -->
		<div class="row-fluid">
			<ul class="thumbnails">
				<?php foreach ($data as $d) { ?>
				<li class="span4" style="margin-left: 0px; margin-right: 8px">
					<div class="thumbnail" style="height: 230px">
						<?php $sampul = get_value('galeri', 'id_album', $d->id); ?>
						<?php if($sampul != null){ ?>
						<a href="<?php echo base_URL()?>frontpage/galeri/lihat/<?php echo $d->id?>">
							<img src="<?php echo base_URL()?>timthumb?src=/upload/galeri/<?php echo $sampul->file.'&w=204&h=170&zc=0'?>" style="width:204px;height: 170px" class="span12" alt="<?php echo $d->nama?>">
						</a>
						<?php }else{ ?>
						<a href="<?php echo base_URL()?>frontpage/galeri/lihat/<?php echo $d->id?>">
							<img src="<?php echo base_URL()?>timthumb?src=/upload/galeri/____no_foto.jpg&w=204&h=170&zc=0'?>" style="width:204px;height: 170px" class="span12" alt="<?php echo $d->nama?>">
						</a>
						<?php } ?>

						<div class="caption">
							<h6 style="text-align: center; margin-top: 0">Album <?php echo $d->nama?><br>
								<?php $q_jumlah_foto = $this->db->query("SELECT id FROM galeri WHERE id_album = '".$d->id."'")->num_rows(); ?>
								Jumlah foto : <?php echo $q_jumlah_foto?>
							</h6>
						</div>
					</div>
				</li>
				<?php } ?>
			</ul>
		</div>
	</div>
</div>


<!--/span-->
