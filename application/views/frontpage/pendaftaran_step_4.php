<div class="row">
    <div class="col-md-12 well" style="margin-left: 0px;background-color: #fff;">
     <img src="<?php echo base_url().'assets/front/images/step_4.gif'?>" style="display:block;margin:0 auto">
     <br/>
     <div class="panel panel-default" style="margin:10px 100px 50px 100px">
        <div class="panel-heading">
           <h3 class="panel-title"><strong>Form Login </strong></h3>
           <!-- <div style="float:right; font-size: 80%; position: relative; top:-10px"><a href="#">Forgot password?</a></div> -->
        </div>
        <div class="panel-body">
          <div class="alert alert-warning" role="alert" style="padding-top: 10px;padding-bottom: 10px;border-bottom-width: 1px;">
            <h4 style="text-align:center;margin-bottom: 0px;">** Pastikan anda telah mengaktifkan akun anda** <br/>Link aktivasi beserta info login sudah terkirim ke email yang anda gunakan untuk mendaftar</a></h4>
          </div>
             <form class="form-signin" action="" method="POST">
                 <input style="margin-bottom:5px" type="text" class="form-control" placeholder="Username" required autofocus name="username">
                 <input type="password" class="form-control" placeholder="Password" required name="password">
                 <button class="btn btn-lg btn-primary btn-block" type="submit">Masuk</button>
             </form>
         </div>
     </div>
   </div>
</div>   
