 <?php
    $mode = $this->uri->segment(3);
    $id_ide_saran = $this->uri->segment(4);

    $nama = $mode === 'add' ? '' : set_value('nama');
    $email = $mode === 'add' ? '' : set_value('email');
    $alamat = $mode === 'add' ? '' : set_value('alamat');

    $komentar = $mode === 'add' ? '' : set_value('komentar');
    $act = base_URL().'frontpage/ide_saran/reply_add/'.$id_ide_saran;
 ?>

<div class="col-md-12 well" style="margin-left: 0px;background-color: #fff;">
	<ol class="breadcrumb">
		<li><a href="<?php echo base_URL()?>">Beranda</a> </li>
		<li><a href="<?php echo base_URL()?>frontpage/ide_saran">Ide & Saran</a></li>
    <li class="active">Tambah tanggapan</li>
	</ol>
	<h3>Ide & Saran Pengunjung</h3>
	<div class="alert alert-success fade in">
		<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
		Pada laman ini pengunjung dapat memberikan Ide atau Gagasannya seputar diklat. Anda juga dapat menanggapi ide yang ditulis pengunjung lainnya..
	</div>
	<p align="left"><a href="<?php echo base_URL().'frontpage/ide_saran'?>" button="" type="button" class="btn btn-success"><i class="icon-chevron-left icon-white"></i>Index Ide & Saran</a></p>
	<?php echo $this->session->flashdata('k');?>

	<div class="alert alert-info">
		<div class="row-fluid">
			<div class="span2"><b><span class="icon-user"></span> <?php echo $post->nama;?></b><br><small> Ditulis: <?php echo $post->tgl?> </small></div>
			<div class="span10"><p><?php echo $post->topik;?></p></div>
		</div>
	</div>

	<?php foreach ($tanggapan->result() as $t) { ?>
	<div class="row-fluid">
		<div class="span1"></div>
		<div class="span11">
			<div class="alert alert-warning">
				<span class="icon-user"></span> <b><?php echo $t->nama;?></b> <small>- ditulis: <?php echo $t->tgl;?> </small> <br>
				<p><?php echo $t->komentar; ?></p>
			</div>
		</div>
	</div>
	<?php } ?>

	<div class="row-fluid">
		<h4><i class="icon-plus"></i>Tambahkan tanggapan anda: </h4>
		<?php if (isset($msg)) { ?>
		<div class='alert alert-danger'><?php echo $msg; ?></div>
		<?php } ?>

    <form role="form" method="post" id="f_bukutamu" action="<?php echo $act?>">


      <div class="form-group">
        <label for="exampleInputEmail1">Nama lengkap</label>
        <input type="text" name="nama" id="nama" class="form-control " placeholder="Nama lengkap" tabindex="1" required value="<?php echo $nama?>">
      </div>

      <div class="form-group">
        <label for="exampleInputEmail1">Email</label>
        <input type="text" name="email" id="email" class="form-control " placeholder="Alamat email" tabindex="1" required value="<?php echo $email?>">
      </div>

      <div class="form-group">
        <label for="exampleInputEmail1">Alamat</label>
        <input type="text" name="alamat" id="alamat" class="form-control " placeholder="Alamat rumah" tabindex="1" required value="<?php echo $alamat?>">
      </div>

      <div class="form-group">
        <label for="exampleInputEmail1">Komentar</label>
        <textarea name="komentar" rows="5" style="width:98%" required><?php echo $komentar?></textarea>
      </div>

      <?php echo $captcha_image; ?>
      <br/><br/>
      <div class="row">
         <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
               <input type="text" name="captcha_word" id="captcha_word" class="form-control" placeholder="Masukkan kode diatas" tabindex="5" required>
            </div>
         </div>
      </div>
      <button type="submit" value="Kirim" id="tombol" class="btn btn-success">Kirim</button>
    </form>

	</div>
</div>
