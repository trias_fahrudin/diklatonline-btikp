 <?php
    $mode = $this->uri->segment(3);

    $nama = $mode === 'add' ? '' : set_value('nama');
    $email = $mode === 'add' ? '' : set_value('email');
    $alamat = $mode === 'add' ? '' : set_value('alamat');

    $topik = $mode === 'add' ? '' : set_value('topik');
 ?>

<div class="col-md-12 well" style="margin-left: 0px;background-color: #fff;">
  <ol class="breadcrumb">
    <li><a href="<?php echo base_URL()?>">Beranda</a> </li>
    <li><a href="<?php echo base_URL()?>frontpage/ide_saran">Ide & Saran</a></li>
    <li class="active">Tambah topik</li>
  </ol>
  <h3>Ide & Saran Pengunjung</h3>
  <div class="alert alert-success fade in">
    <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
    Pada laman ini pengunjung dapat memberikan Ide atau Gagasannya seputar diklat. Anda juga dapat menanggapi ide yang ditulis pengunjung lainnya..
  </div>
  <p align="left"><a href="<?php echo base_URL().'frontpage/ide_saran'?>" button="" type="button" class="btn btn-success"><i class="icon-chevron-left icon-white"></i>Index Ide & Saran</a></p>

  <?php if (isset($msg)) { ?>
  <div class='alert alert-danger' role="alert"><?php echo $msg; ?></div>
  <?php } ?>
  <form role="form" method="post" id="f_bukutamu" action="<?php echo base_URL()?>frontpage/ide_saran/add_act">

    <div class="form-group">
      <label for="exampleInputEmail1">Nama lengkap</label>
      <input type="text" name="nama" id="nama" class="form-control " placeholder="Nama lengkap" tabindex="1" required value="<?php echo $nama?>">
    </div>

    <div class="form-group">
      <label for="exampleInputEmail1">Email</label>
      <input type="text" name="email" id="email" class="form-control " placeholder="Alamat email" tabindex="1" required value="<?php echo $email?>">
    </div>

    <div class="form-group">
      <label for="exampleInputEmail1">Alamat</label>
      <input type="text" name="alamat" id="alamat" class="form-control " placeholder="Alamat rumah" tabindex="1" required value="<?php echo $alamat?>">
    </div>

    <div class="form-group">
      <label for="exampleInputEmail1">Ide & Saran</label>
      <textarea name="topik" rows="5" style="width:98%" required><?php echo $topik?></textarea>
    </div>

    <?php echo $captcha_image; ?>
    <br/><br/>
    <div class="row">
       <div class="col-xs-12 col-sm-6 col-md-3">
          <div class="form-group">
            <input type="text" name="captcha_word" id="captcha_word" class="form-control" placeholder="Masukkan kode diatas" tabindex="5" required>
          </div>
       </div>
    </div>


    <button type="submit" value="Kirim" id="tombol" class="btn btn-success">Kirim</button>
  </form>
</div>
