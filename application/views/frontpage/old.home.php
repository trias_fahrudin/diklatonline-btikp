<div style="background-color: #f7f7f7;padding:10px">
<style type="text/css">

    .panel-body {
    border: 0px none;
    }
    .nextDiklat {
    margin-top: 10px;
    overflow: hidden;
    font-size: 10px;
    height:350px;
    }
    .innerWrap {
    width: 100%;
    }
    .list {
    display: table;
    table-layout: fixed;
    width: 100%;
    }
    .list div {
    padding-right: 5px;
    display: table-cell;
    }
    .list div.detailKegiatan {
    width: 190px;
    }
    .list div.tanggalDiklat {
    width: 70px;
    text-align: justify;
    text-align: center;
    text-justify: inter-word;
    }
    .list div.tandaHubung {
    width: 15px;
    }
    .list div.tempat {
    width: 60px;
    padding-right: 0px!important;
    text-align: right;
    }
 </style>
 <div class="panel panel-info nextDiklat">
    <div class="panel-heading">
       <h4><strong>Diklat Yang Akan Dilaksanakan</strong></h4>
    </div>
    <div class="panel-body">
      <!-- <table class="table table-bordered">
        <thead>
          <tr>
            <th>Jadwal</th>
            <th>Kegiatan</th>
            <th>Detail</th>
            <th>Narasumber</th>
            <th>Peserta</th>
            <th>Kapasitas</th>
            <th>Tempat</th>
          </tr>
        </thead>
      </table> -->

       <div class="ticker1">

          <div class="innerWrap">
            <?php $i = 1;?>
            <?php foreach ($active_classrooms->result() as $active) { ?>
              <?php $style = ($i % 2 == 0) ? 'style="background-color:#eee;  border-top: 1px solid #ddd;"':'' ?>
              <div class='list'>
                 <div <?php echo $style;?> class='tanggalDiklat'><?php echo tgl_panjang($active->start,'sm')?></div>
                 <div <?php echo $style;?> class='tandaHubung'>s/d</div>
                 <div <?php echo $style;?> class='tanggalDiklat'><?php echo tgl_panjang($active->end,'sm')?></div>
                 <div <?php echo $style;?> class='kegiatan'><?php echo $active->name?></div>
                 <div <?php echo $style;?> class='detailKegiatan'><?php echo $active->detail_kegiatan?></div>
                 <div <?php echo $style;?> ><?php echo $active->narasumber?></div>
                 <div <?php echo $style;?> ><?php echo $active->peserta?></div>
                 <div <?php echo $style;?> ><?php echo $active->registration_quota?> Peserta</div>
                 <div <?php echo $style;?> class="tempat"><?php echo $active->tempat?></div>

              </div>
            <?php $i++;} ?>
          </div>
       </div>
    </div>

</div>
