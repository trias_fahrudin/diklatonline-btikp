<div class="row">
    <div class="col-md-12 well" style="margin-left: 0px;background-color: #fff;">
      <img src="<?php echo base_url().'assets/front/images/step_2.gif'?>" style="display:block;margin:0 auto">
      <br/>
        <table id="example" class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%">
         <thead>
            <tr>
               <th>No</th>
               <th>Nama Diklat</th>
               <th>Detail Kegiatan</th>
               <th>Angkatan</th>
               <th>Peserta</th>
               <th>Info</th>
               <!-- <th>Aksi</th> -->
            </tr>
         </thead>
         <tbody>

          <?php if($active_classrooms->num_rows() == 0){ ?>
            <tr>
               <td colspan="5">
                 <div class="alert alert-warning" role="alert" style="text-align:center">Belum ada diklat</div>
               </td>
            </tr>
          <?php }else{ ?>

            <?php $no = 1;?>
            <?php foreach ($active_classrooms->result() as $class) { ?>
              <?php
                  $this->db->where('classroom_id',$class->id);
                  $this->db->where('status','enable');
                  $registerant = $this->db->count_all_results('classroom_users');

                  $pieces = explode(" ",$class->start);
                  $start = $pieces[0];

                  $pieces = explode(" ",$class->end);
                  $end = $pieces[0];

                  $availability = $class->availability;

              ?>
              <tr>
                <td><?php echo $no?></td>
                <td><?php echo $class->name?></td>
                <td>
                  <!-- <a href="" classroomid="<?php echo $class->id;?>" class="modal-detail-kegiatan btn btn-primary paper-shadow relative">Lihat Detail</a> -->
                  <?php echo $class->detail_kegiatan;?>
                </td>
                <td><?php echo $class->generation?></td>
                <td><?php echo $class->peserta?></td>
                <td>
                  Quota : <?php echo $class->registration_quota . ' Peserta'?> <br/>
                  Teregister : <?php echo $registerant . ' Peserta'?><br/>
                  Mulai : <?php echo appDate($start)?><br/>
                  Selesai : <?php echo appDate($end)?><br/>
                  <?php if($registerant >= $class->registration_quota || $availability === 'close'){ ?>
                  <h4></h4><span class="label label-danger">PENDAFTARAN TELAH DITUTUP</span></h4>
                  <?php }else{ ?>
                  <a href="<?php echo base_url() . 'frontpage/pendaftaran/step_3/' . $class->slug?>" role="button" class="btn btn-primary" style="display:block;margin:0 auto">Daftar <span class="glyphicon glyphicon-forward" aria-hidden="true"></span></a>
                  <?php } ?>
                </td>
                <!-- <td>

                </td> -->

              </tr>
            <?php $no++;} ?>

          <?php } ?>
         </tbody>
      </table>

      <div class="col-md-6">
        <a href="<?php echo base_url() . 'frontpage/pendaftaran/step_1'?>" role="button" class="btn btn-primary" style="display:block;margin:0 auto"><span class="glyphicon glyphicon-backward" aria-hidden="true"></span> Sebelumnya</a>
      </div>
      <div class="col-md-6">
        <!-- <a href="<?php echo base_url() . 'frontpage/pendaftaran/step_3'?>" role="button" class="btn btn-primary" style="display:block;margin:0 auto">Lanjut <span class="glyphicon glyphicon-forward" aria-hidden="true"></span></a> -->
      </div>


  </div>
</div>

<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title" id="myModalLabel">Details</h4>
         </div>
         <div class="modal-body" id="show-result-here">

         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
         </div>
      </div>
   </div>
</div>

<script>
    $('.modal-detail-kegiatan').click(function(){

        var classroom_id = $(this).attr('classroomid');


        $.get(base + 'frontpage/classroom_details/true',
          { classroom_id: classroom_id}
        )

        .done(function( data ) {
           $('#show-result-here').html(data);
           $('#myModal').modal('show');
        });
        return false;
    });

   $(document).ready(function() {
     $('#example').DataTable( {
      "scrollX": true,
      "bSort" : false,
      "pageLength": 10,
      "dom": '<"top"iflp<"clear">>rt<"bottom"iflp<"clear">>'
      } );
   } );
</script>
