<style>
  div.dataTables_wrapper {
      width: 800px;
      margin: 0 auto;
  }
</style>
<ol class="breadcrumb">
  <li><a href="<?php echo base_url()?>">Home</a></li>
  <li class="active">Detail Kelas</li>
</ol>

<div style="background-color: #f7f7f7;padding:10px">
   <div class="panel panel-default">
      <div class="panel-heading">
         <h3 class="panel-title"><strong>Detail Kelas </strong></h3>

      </div>
      <div class="panel-body">
        <table id="example" class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%">
           <thead>
              <tr>
                 <th>Tanggal Pelaksanaan</th>
                 <th>Angkatan</th>
                 <th>Nama Kegiatan</th>
                 <th>Narasumber</th>
                 <th>Peserta</th>
                 <th>Kuota</th>
                 <th>Lokasi</th>
              </tr>
           </thead>
           <tfoot>
              <tr>
                <th>Tanggal Pelaksanaan</th>
                <th>Angkatan</th>
                <th>Nama Kegiatan</th>
                <th>Narasumber</th>
                <th>Peserta</th>
                <th>Kuota</th>
                <th>Lokasi</th>
              </tr>
           </tfoot>
           <tbody>
             <?php foreach ($data->result() as $d) { ?>
               <tr>
                  <td style="text-align:center;vertical-align:middle;"><?php echo tgl_panjang($d->start,'sm')?> s/d <?php echo tgl_panjang($d->end,'sm')?></td>
                  <td style="text-align:center;vertical-align:middle;"><?php echo ucwords($d->generation);?></td>
                  <td style="text-align:center;vertical-align:middle;"><?php echo ucwords($d->name);?></td>
                  <td style="text-align:center;vertical-align:middle;"><?php echo ucwords($d->narasumber);?></td>
                  <td style="text-align:center;vertical-align:middle;"><?php echo ucwords($d->peserta);?></td>
                  <td style="text-align:center;vertical-align:middle;"><?php echo ucwords($d->registration_quota);?></td>
                  <td style="text-align:center;vertical-align:middle;"><?php echo ucwords($d->tempat);?></td>
               </tr>
             <?php } ?>

           </tbody>
        </table>

      </div>
    </div>
</div>


<script>
   $(document).ready(function() {
     $('#example').DataTable( {
      "scrollX": true,
      "bSort" : false
      } );
   } );
</script>
