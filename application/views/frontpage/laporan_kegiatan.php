<div class="row">
    <div class="col-md-12 well" style="margin-left: 0px;background-color: #fff;">

      <br/>
        <table id="example" class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%">
         <thead>
            <tr>
               <th>No</th>
               <th>Nama Diklat</th>
               <th>Angkatan</th>
               <th>Peserta</th>
               <th>Laporan</th>
               <!-- <th>Aksi</th> -->
            </tr>
         </thead>
         <tbody>

          <?php if($active_classrooms->num_rows() == 0){ ?>
            <tr>
               <td colspan="5">
                 <div class="alert alert-warning" role="alert" style="text-align:center">Belum ada diklat</div>
               </td>
            </tr>
          <?php }else{ ?>

            <?php $no = 1;?>
            <?php foreach ($active_classrooms->result() as $class) { ?>

              <tr>
                <td><?php echo $no?></td>
                <td width="30%"><?php echo $class->name?></td>
                <td><?php echo $class->generation?></td>
                <td><?php echo $class->peserta?></td>
                <td>
                  <a href="" classroomid="<?php echo $class->id;?>" class="modal-laporan btn btn-primary paper-shadow relative">Lihat Informasi</a>
                </td>
                <!-- <td>

                </td> -->

              </tr>
            <?php $no++;} ?>

          <?php } ?>
         </tbody>
      </table>




  </div>
</div>

<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title" id="myModalLabel">Laporan Kegiatan</h4>
         </div>
         <div class="modal-body" id="show-result-here">

         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
         </div>
      </div>
   </div>
</div>

<script>

  $('.modal-laporan').click(function(){

    var classroom_id = $(this).attr('classroomid');


    $.get(base + 'frontpage/classroom_details',
      { classroom_id: classroom_id}
    )

    .done(function( data ) {
       $('#show-result-here').html(data);
       $('#myModal').modal('show');
    });
    return false;
  });

   $(document).ready(function() {
     $('#example').DataTable( {
      "scrollX": true,
      "bSort" : false,
      "pageLength": 10,
      "dom": '<"top"iflp<"clear">>rt<"bottom"iflp<"clear">>'
      } );
   } );
</script>