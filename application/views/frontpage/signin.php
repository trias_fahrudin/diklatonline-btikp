<div class="panel panel-default" style="margin:10px 100px 50px 100px">
   <div class="panel-heading">
      <h3 class="panel-title"><strong>Form Login </strong></h3>
      <!-- <div style="float:right; font-size: 80%; position: relative; top:-10px"><a href="#">Forgot password?</a></div> -->
   </div>
   <div class="panel-body">
        <form class="form-signin" action="" method="POST">
            <input style="margin-bottom:5px" type="text" class="form-control" placeholder="Username" required autofocus name="username">
            <input type="password" class="form-control" placeholder="Password" required name="password">
            <?php echo $this->recaptcha->render() ?>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Masuk</button>
        </form>
    </div>
</div>
