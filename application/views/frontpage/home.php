<?php $color_array = array('bg-default','bg-primary','bg-lightred','bg-brown','bg-purple','bg-gray-dark')?>

<?php if($data->num_rows() == 0){ ?>
<div class="alert alert-warning" role="alert" style="padding-top: 10px;padding-bottom: 10px;border-bottom-width: 1px;">
  <h4 style="text-align:center;margin-bottom: 0px;">Belum ada data kelas</h4>
</div>
<?php } ?>

<div class="row" data-toggle="isotope" style="position: relative; height: 328px;">
<?php
$i = 0;
$temp_title = "";

foreach ($data->result() as $classroom) {

  if($classroom->name !== $temp_title){

    $temp_title = $classroom->name;
  }else{
    continue;
}
?>
  <div class="item col-xs-12 col-sm-6 col-lg-4">
      <div class="panel panel-default paper-shadow" data-z="0.5">
        <div class="cover overlay cover-image-full hover" style="height: 150px;">
            <span class="img icon-block height-150 <?php echo $color_array[$i]?>"></span>
            <a href="<?php echo base_url() . 'frontpage/classroom/' . getURLFriendly($classroom->name)?>" class="padding-none overlay overlay-full icon-block <?php echo $color_array[$i]?>">
                <span class="v-center"><i class="fa fa-building"></i></span>
            </a>
            <a href="<?php echo base_url() . 'frontpage/classroom/' . getURLFriendly($classroom->name)?>" class="overlay overlay-full overlay-hover overlay-bg-white">
                <span class="v-center">
                    <span class="btn btn-circle btn-red-500 btn-lg"><i class="fa fa-graduation-cap"></i></span>
                </span>
            </a>
        </div>
        <div class="panel-body" style="padding-top: 16px;padding-left: 16px;padding-bottom: 2px;padding-right: 16px;">
             <h4 class="text-headline margin-v-0-10" style="font-size: 1.5rem;line-height:2.0rem">
                <a href="<?php echo base_url() . 'frontpage/classroom/' . getURLFriendly($classroom->name)?>"><?php echo $classroom->name;?></a><br/>

             </h4>
        </div>
    </div>
  </div>
<?php $i <= 6 ? $i += 1 : $i = 0;} ?>
 <div class="item col-xs-12">
   <div class="panel panel-info">
     <div class="panel-heading">
       <h3 class="panel-title">Penjelasan & Latar belakang</h3>
     </div>
     <div class="panel-body">
       <?php echo $this->Basecrud_m->get_where('settings',array('name'=> 'penjelasan'))->row()->value?>
     </div>
   </div>
 </div>
</div>

<!-- <div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> -->
                <h4 class="modal-title">TATA CARA PENDAFTARAN</h4>
            </div>
            <div class="modal-body">
                <?php echo $this->Basecrud_m->get_where('settings',array('name'=> 'indexpage_popup'))->row()->value?>
            </div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                <a href="<?php echo site_url('frontpage/pendaftaran/step_1')?>" class="btn btn-white btn-default active">DAFTAR <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
</div> -->


<script type="text/javascript">
	$(document).ready(function(){
		$("#myModal").modal({
      backdrop: 'static',
      keyboard: false  // to prevent closing with Esc button (if you want this too)
    });
	});
</script>
