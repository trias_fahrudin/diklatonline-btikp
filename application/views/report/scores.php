<div class="main-container">
  <div class="parallax overflow-hidden page-section bg-purple page-section third">
      <div class="parallax-layer container" data-opacity="true">
          <div class="media v-middle">
              <div class="media-left">
                  <span class="icon-block s60 bg-lightred"><i class="fa fa-line-chart"></i></span>
              </div>
              <div class="media-body">
                  <h1 class="text-display-1 text-white margin-none">Laporan Nilai Peserta <?php echo $classroom->name . '( ' . $classroom->generation . ')'?></h1>
              </div>
              <div class="media-right">
              </div>
          </div>
      </div>
  </div>
  <div class="container">
    <div class="page-section">
      <div class="row">
        <div class="col-md-9">

          <div class="panel panel-default">
            <div class="panel-body" style="padding:0px">
              <ol class="breadcrumb">
                <li><a href="<?php echo base_url() . 'classroom';?>">Kelas</a></li>
                <!-- http://localhost/diklatonline/classroom/index/details/lokakarya-pemanfaatan-media-pembelajaran-berbasis-tik-untuk-kegiatan-belajar-mengajar-2015 -->
                <li><a href="<?php echo base_url() . 'classroom/index/details/' . $classroom->slug ;?>">Data Angkatan</a></li>
                <li><a href="<?php echo base_url() . 'report/index/' . $classroom->slug;?>">Laporan</a></li>
                <li class="active">Nilai Peserta</li>
              </ol>
            </div>
          </div>

          <div class="panel panel-default">
            <table data-toggle="data-table" class="table v-middle" cellspacing="0" width="100%">
                <thead>
                    <tr>
                      <th>Nama</th>
                      <th style="text-align:center">Blog</th>
                      <th style="text-align:center">Tugas</th>
                      <th style="text-align:center">TOTAL</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                      <th style="text-align:center">Nama</th>
                      <th style="text-align:center">Blog</th>
                      <th style="text-align:center">Tugas</th>
                      <th style="text-align:center">TOTAL</th>
                    </tr>
                </tfoot>
                <tbody>

                    <?php foreach ($scores->result() as $score) { ?>
                    <tr>
                      <td><img src="<?php echo base_url() . 'timthumb?src=' . base_url() . 'upload/people/' . $score->foto . '&h=40&w=40&zc=0'; ?>" class="img-circle" alt="" /> <?php echo ucwords($score->fullname);?></td>

                      <td style="text-align:center"><?php echo ($score->blog_post_poin + $score->blog_comments_poin)?></td>

                      <td style="text-align:center"><?php echo ($score->task_scores)?></td>
                      
                      <td style="text-align:center"><?php echo ($score->total)?></td>
                    </tr>
                    <?php } ?>

                </tbody>
            </table>
          </div>
        </div>
        <div class="col-md-3">
          <div class="panel panel-default" data-toggle="panel-collapse" data-open="true">
             <div class="panel-heading panel-collapse-trigger">
                <h4 class="panel-title">Menu</h4>
             </div>
             <div class="panel-body list-group">
                  <?php echo main_menu('classroom');?>
             </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
