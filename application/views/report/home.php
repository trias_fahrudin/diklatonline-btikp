<div class="parallax bg-white page-section third">
   <div class="parallax-layer container" data-opacity="true">
      <div class="media v-middle">
         <div class="media-left">
            <span class="icon-block s60 bg-lightred"><i class="fa fa-database"></i></span>
         </div>
         <div class="media-body">
            <h1 class="text-display-1 margin-none"><?php echo $classroom->name . ' (' . $classroom->generation . ')'?></h1>
         </div>
         <!-- <div class="media-right">
         </div> -->
      </div>
   </div>
</div>
<div class="container">
   <div class="page-section">
      <div class="row">
         <div class="col-md-9">

           <div class="panel panel-default">
             <div class="panel-body" style="padding:0px">
               <ol class="breadcrumb">
                 <li><a href="<?php echo base_url() . 'classroom';?>">Kelas</a></li>
                 <!-- http://localhost/diklatonline/classroom/index/details/lokakarya-pemanfaatan-media-pembelajaran-berbasis-tik-untuk-kegiatan-belajar-mengajar-2015 -->
                 <li><a href="<?php echo base_url() . 'classroom/index/details/' . $classroom->slug ;?>">Data Angkatan</a></li>
                 <li class="active">Laporan</li>
                 <!-- <li class="active">Presensi</li> -->
               </ol>
             </div>
           </div>

            <div class="row" data-toggle="isotope">
               
               <div class="item col-xs-12 col-sm-6 col-lg-4">
                  <div class="panel panel-default paper-shadow" data-z="0.5">
                     <div class="cover overlay cover-image-full hover">
                        <span class="img icon-block height-150 bg-default"></span>
                        <a href="<?php echo base_url() . 'report/index/' . $classroom->slug . '/scores'?>" class="padding-none overlay overlay-full icon-block bg-purple">
                        <span class="v-center">
                        <i class="fa fa-line-chart"></i> Nilai Peserta
                        </span>
                        </a>
                        <a href="<?php echo base_url() . 'report/index/' .$classroom->slug . '/scores'?>" class="overlay overlay-full overlay-hover overlay-bg-white">
                        <span class="v-center">
                        <span class="btn btn-circle btn-white btn-lg"><i class="fa fa-graduation-cap"></i></span>
                        </span>
                        </a>
                     </div>
                  </div>
               </div>
            </div>
            <br/>
            <br/>
         </div>
         <div class="col-md-3">
            <div class="panel panel-default" data-toggle="panel-collapse" data-open="true">
               <div class="panel-heading panel-collapse-trigger">
                  <h4 class="panel-title">Menu</h4>
               </div>
               <div class="panel-body list-group">
                  <?php echo main_menu('classroom');?>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
