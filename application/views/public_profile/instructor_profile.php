<div class="main-container">
   <div class="parallax overflow-hidden bg-blue-400 page-section third">
      <div class="container parallax-layer" data-opacity="true">
         <div class="media v-middle">
            <div class="media-left text-center">
               <a href="#">
               <img src="<?php echo base_url().'timthumb?src='.base_url().'upload/people/'.$profile->foto . '&h=80&w=80&zc=0'; ?>" alt="" class="img-circle width-80" />
               </a>
            </div>
            <div class="media-body">
               <h1 class="text-white text-display-1 margin-v-0"><?php echo ucwords($profile->fullname);?></h1>
               <p class="text-subhead text-white">Public Profile</p>
            </div>
            <div class="media-right">
               <span class="label bg-blue-500"><?php echo $profile->level === 'participator' ? 'Peserta':'Instruktur'?></span>
            </div>
         </div>
      </div>
   </div>
   <div class="container">
      <div class="page-section">
         <div class="row">
            <div class="col-md-9">
               <div class="tabbable paper-shadow relative" data-z="0.5">
                  <!-- Tabs -->
                  <ul class="nav nav-tabs">
                     <li class="active"><a href="#"><i class="fa fa-fw fa-lock"></i> <span class="hidden-sm hidden-xs">Public Profile</span></a></li>
                  </ul>
                  <!-- // END Tabs -->
                  <!-- Panes -->
                  <div class="tab-content">
                     <div id="account" class="tab-pane active">
                       <form id="form-foto" class="form-horizontal">
                        <div class="form-group">
                           <label for="inputEmail3" class="col-sm-2 control-label">Foto</label>
                           <div class="col-md-6">
                              <div class="media v-middle">
                                 <div class="media-left">
                                    <div class="icon-block width-100 bg-grey-100">
                                       <img src="<?php echo base_url() . 'timthumb?src=' . base_url() . 'upload/people/' . $profile->foto . '&h=100&w=100&zc=0'; ?>" width="100" height="100" alt="Shoe" />
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputEmail3" class="col-md-2 control-label">User Name</label>
                           <div class="col-md-6">
                              <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                 <input type="text" class="form-control" id="username" placeholder="Username anda" readonly="" value="<?php echo ucwords($profile->username);?>" disabled="">
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputEmail3" class="col-md-2 control-label">Nama Lengkap</label>
                           <div class="col-md-6">
                              <div class="form-control-material required" style="padding-top: 5px;height: 31px;">
                                 <input type="text" class="form-control" id="fullname" placeholder="Nama lengkap anda" value="<?php echo ucwords($profile->fullname);?>">
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputEmail3" class="col-md-2 control-label">Nomor Telephon</label>
                           <div class="col-md-6">
                              <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                 <input type="text" readonly class="form-control used" id="telp_number" value="<?php echo $profile->telp_number;?>">
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputEmail3" class="col-md-2 control-label">Email</label>
                           <div class="col-md-6">
                              <div class="form-control-material required" style="padding-top: 5px;height: 31px;">
                                 <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                    <input type="email" readonly class="form-control used" id="email" placeholder="Email" value="<?php echo $profile->email;?>">
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div class="form-group">
                           <label for="inputEmail3" class="col-md-2 control-label">Kota Lahir</label>
                           <div class="col-md-6">
                              <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                 <input type="text" readonly class="form-control used" id="city_of_birth" value="<?php echo ucwords($profile->city_of_birth);?>">
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputEmail3" class="col-md-2 control-label">Tanggal Lahir</label>
                           <div class="col-md-6">
                              <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                 <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" disabled class="form-control datepicker" id="date_of_birth" value="<?php echo appDate($profile->date_of_birth)?>">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputEmail3" class="col-md-2 control-label">Jenis Kelamin</label>
                           <div class="col-md-6">
                              <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                 <div class="input-group">
                                    <select id="gender" disabled="" class="form-control" data-style="btn-white" data-live-search="true" data-size="5" placeholder="Jenis Kelamin">
                                       <option <?php echo $profile->gender === 'male' ? 'selected':'';?> value="male">Laki-laki</option>
                                       <option <?php echo $profile->gender === 'female' ? 'selected':'';?> value="female">Perempuan</option>
                                    </select>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputEmail3" class="col-md-2 control-label">Alamat</label>
                           <div class="col-md-6">
                              <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                 <input type="text" readonly class="form-control used" id="home_address" value="<?php echo strip_tags($profile->home_address);?>">
                              </div>
                           </div>
                        </div>   

                        <div class="form-group">
                           <label for="inputEmail3" class="col-md-2 control-label">NIP</label>
                           <div class="col-md-6">
                              <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                 <input type="text" readonly class="form-control used" id="nip" value="<?php echo $profile->nip;?>">
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputEmail3" class="col-md-2 control-label">Instansi</label>
                           <div class="col-md-6">
                              <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                 <input type="text" readonly class="form-control used" id="institute" value="<?php echo $profile->institute;?>">
                              </div>
                           </div>
                        </div>

                        <div class="form-group">
                           <label for="inputEmail3" class="col-md-2 control-label">Keahlian</label>
                           <div class="col-md-6">
                              <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                 <input type="text" readonly class="form-control used" id="expertise " value="<?php echo $profile->expertise ;?>">
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputEmail3" class="col-md-2 control-label">Golongan</label>
                           <div class="col-md-6">
                              <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                 <input type="text" readonly class="form-control used" id="golongan " value="<?php echo $profile->golongan ;?>">
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputEmail3" class="col-md-2 control-label">Posisi</label>
                           <div class="col-md-6">
                              <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                 <input type="text" readonly class="form-control used" id="position " value="<?php echo $profile->position ;?>">
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputEmail3" class="col-md-2 control-label" style="padding-left: 2px;">Pendidikan Terakhir</label>
                           <div class="col-md-6">
                              <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                 <input type="text" readonly class="form-control used" id="last_education " value="<?php echo $profile->last_education ;?>">
                              </div>
                           </div>
                        </div>


                      </form>
                     </div>
                  </div>
                  <!-- // END Panes -->
               </div>
            </div>
            <div class="col-md-3">
               <?php echo main_menu('classroom');?>
            </div>
            <!-- end right menu -->
         </div>
      </div>
   </div>
</div>
</div>
