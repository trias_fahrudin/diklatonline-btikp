<div class="main-container">
   <div class="parallax overflow-hidden bg-blue-400 page-section third">
      <div class="container parallax-layer" data-opacity="true">
         <div class="media v-middle">
            <div class="media-left text-center">
               <a href="#">
               <img src="<?php echo base_url().'timthumb?src='.base_url().'upload/people/'.$profile->foto . '&h=80&w=80&zc=0'; ?>" alt="" class="img-circle width-80" />
               </a>
            </div>
            <div class="media-body">
               <h1 class="text-white text-display-1 margin-v-0"><?php echo ucwords($profile->fullname);?></h1>
               <p class="text-subhead text-white">Public Profile</p>
            </div>
            <div class="media-right">
               <span class="label bg-blue-500"><?php echo $profile->level === 'participator' ? 'Peserta':'Instruktur'?></span>
            </div>
         </div>
      </div>
   </div>
   <div class="container">
      <div class="page-section">
         <div class="row">
            <div class="col-md-9">
               <div class="tabbable paper-shadow relative" data-z="0.5">
                  <!-- Tabs -->
                  <ul class="nav nav-tabs">
                     <li class="active"><a href="#"><i class="fa fa-fw fa-lock"></i> <span class="hidden-sm hidden-xs">Public Profile</span></a></li>
                  </ul>
                  <!-- // END Tabs -->
                  <!-- Panes -->
                  <div class="tab-content">
                     <div id="account" class="tab-pane active">
                       <form id="form-foto" class="form-horizontal">
                        <div class="form-group">
                           <label for="inputEmail3" class="col-sm-2 control-label">Foto</label>
                           <div class="col-md-6">
                              <div class="media v-middle">
                                 <div class="media-left">
                                    <div class="icon-block width-100 bg-grey-100">
                                       <img src="<?php echo base_url() . 'timthumb?src=' . base_url() . 'upload/people/' . $profile->foto . '&h=100&w=100&zc=0'; ?>" width="100" height="100" alt="Shoe" />
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputEmail3" class="col-md-2 control-label">User Name</label>
                           <div class="col-md-6">
                              <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                 <input type="text" class="form-control" id="username" placeholder="Username anda" readonly="" value="<?php echo ucwords($profile->username);?>" disabled="">
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputEmail3" class="col-md-2 control-label">Nama Lengkap</label>
                           <div class="col-md-6">
                              <div class="form-control-material required" style="padding-top: 5px;height: 31px;">
                                 <input type="text" class="form-control" id="fullname" placeholder="Nama lengkap anda" value="<?php echo ucwords($profile->fullname);?>">
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputEmail3" class="col-md-2 control-label"></label>
                           <div class="col-md-10">
                             <div class="panel panel-primary">
                               <div class="panel-heading">
                                 <h3 class="panel-title">Daftar surat tugas</h3>
                               </div>
                               <div class="panel-body">
                                 <table class="table v-middle table-bordered table-hover">
                                     <thead>
                                         <tr>
                                             <th>Diklat</th>
                                             <th>Surat Tugas</th>
                                         </tr>
                                     </thead>
                                     <tbody id="responsive-table-body">
                                       <?php if($surat_tugas->num_rows() == 0){ ?>
                                       <tr>
                                         <td colspan="4" style="text-align:center">
                                           Tidak ada data
                                         </td>
                                       </tr>
                                       <?php }else{ ?>
                                           <?php foreach ($surat_tugas->result() as $pc) { ?>
                                               <tr>
                                                 <td><?php echo $pc->nama_kelas;?></td>
                                                 <td>
                                                   <?php if(!empty($pc->surat_tugas)){ ?>
                                                   <a target="_blank" href="<?php echo site_url('upload/stugas/' . $pc->surat_tugas)?>">Lihat</a>
                                                   <?php }else{ ?>
                                                   Tidak ada
                                                   <?php } ?>
                                                 </td>

                                             </tr>
                                           <?php } ?>
                                       <?php } ?>
                                     </tbody>
                                 </table>
                               </div>
                             </div>
                           </div>
                        </div>



                        <div class="form-group">
                           <label for="inputEmail3" class="col-md-2 control-label">Nomor Telephon</label>
                           <div class="col-md-6">
                              <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                 <input type="text" readonly class="form-control used" id="telp_number" value="<?php echo $profile->telp_number;?>">
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputEmail3" class="col-md-2 control-label">Email</label>
                           <div class="col-md-6">
                              <div class="form-control-material required" style="padding-top: 5px;height: 31px;">
                                 <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                    <input type="email" readonly class="form-control used" id="email" placeholder="Email" value="<?php echo $profile->email;?>">
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div class="form-group">
                           <label for="inputEmail3" class="col-md-2 control-label">Kota Lahir</label>
                           <div class="col-md-6">
                              <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                 <input type="text" readonly class="form-control used" id="city_of_birth" value="<?php echo ucwords($profile->city_of_birth);?>">
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputEmail3" class="col-md-2 control-label">Tanggal Lahir</label>
                           <div class="col-md-6">
                              <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                 <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" disabled class="form-control datepicker" id="date_of_birth" value="<?php echo appDate($profile->date_of_birth)?>">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputEmail3" class="col-md-2 control-label">Jenis Kelamin</label>
                           <div class="col-md-6">
                              <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                 <div class="input-group">
                                    <select id="gender" disabled="" class="form-control" data-style="btn-white" data-live-search="true" data-size="5" placeholder="Jenis Kelamin">
                                       <option <?php echo $profile->gender === 'male' ? 'selected':'';?> value="male">Laki-laki</option>
                                       <option <?php echo $profile->gender === 'female' ? 'selected':'';?> value="female">Perempuan</option>
                                    </select>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputEmail3" class="col-md-2 control-label">Alamat</label>
                           <div class="col-md-6">
                              <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                 <input type="text" readonly class="form-control used" id="home_address" value="<?php echo strip_tags($profile->home_address);?>">
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputEmail3" class="col-md-2 control-label">Status Pegawai</label>
                           <div class="col-md-6">
                              <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                 <div class="input-group">
                                    <select id="status" class="form-control" data-style="btn-white" data-live-search="true" data-size="5" placeholder="Status PNS">
                                       <option <?php echo $profile->status === 'PNS' ? 'selected':'';?> value="PNS">PNS</option>
                                       <option <?php echo $profile->status === 'NON-PNS' ? 'selected':'';?> value="NON-PNS">NON PNS</option>
                                    </select>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputEmail3" class="col-md-2 control-label">NIP</label>
                           <div class="col-md-6">
                              <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                 <input type="text" readonly class="form-control used" id="nip" value="<?php echo $profile->nip;?>">
                              </div>
                           </div>
                        </div>


                        <div class="form-group">
                           <label for="inputEmail3" class="col-md-2 control-label">NUPTK</label>
                           <div class="col-md-6">
                              <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                 <input type="text" readonly class="form-control used" id="nuptk" value="<?php echo $profile->nuptk;?>">
                              </div>
                           </div>
                        </div>

                        <div class="form-group">
                           <label for="inputEmail3" class="col-md-2 control-label">NPWP</label>
                           <div class="col-md-6">
                              <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                 <input type="text" readonly class="form-control used" id="nuptk" value="<?php echo $profile->npwp;?>">
                              </div>
                           </div>
                        </div>

                        <div class="form-group">
                           <label for="inputEmail3" class="col-md-2 control-label">Pangkat / Golongan</label>
                           <div class="col-md-6">
                              <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                 <input type="text" readonly class="form-control used" id="golongan" value="<?php echo strtoupper($profile->golongan);?>">
                              </div>
                           </div>
                        </div>

                        <div class="form-group">
                           <label for="inputEmail3" class="col-md-2 control-label">Jabatan</label>
                           <div class="col-md-6">
                              <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                 <input type="text" readonly class="form-control used" id="position" value="<?php echo strtoupper($profile->position);?>">
                              </div>
                           </div>
                        </div>


                        <div class="form-group">
                           <label for="inputEmail3" class="col-md-2 control-label">Asal Sekolah</label>
                           <div class="col-md-6">
                              <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                 <input type="text" readonly class="form-control used" id="origin_of_school" value="<?php echo strtoupper($profile->origin_of_school);?>">
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputEmail3" class="col-md-2 control-label">Kota Sekolah</label>
                           <div class="col-md-6">
                              <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                 <input type="text" readonly class="form-control used" id="district" value="<?php echo strtoupper($profile->district);?>">
                              </div>
                           </div>
                        </div>


                        <!-- <div class="form-group">
                           <label for="inputEmail3" class="col-md-2 control-label">Kab/Kota Sekolah</label>
                           <div class="col-md-6">
                              <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                 <select readonly disabled id="district_id" class="form-control" data-style="btn-white" data-live-search="true" data-size="5" placeholder="Kab/Kota Sekolah">
                                    <?php
                                    $wil = $this->Basecrud_m->get('districts');
                                    foreach ($wil->result() as $w) { ?>
                                    <option <?php echo $profile->district === $w->id ? 'selected' : '';?> value="<?php echo $w->id?>"><?php echo $w->name;?></option>
                                    <?php } ?>
                                 </select>
                              </div>
                           </div>
                        </div> -->
                        <div class="form-group">
                           <label for="inputEmail3" class="col-md-2 control-label">Mata Pelajaran</label>
                           <div class="col-md-6">
                              <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                 <input type="text" readonly class="form-control used" id="mapel" value="<?php echo strtoupper($profile->mapel);?>">
                              </div>
                           </div>
                        </div>

                        <div class="panel panel-primary">
                          <div class="panel-heading">
                            <h3 class="panel-title">Pelatihan yang pernah diikuti</h3>
                          </div>
                          <div class="panel-body">
                            <table class="table v-middle table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Nama Pelatihan</th>
                                        <th>Penyelenggara</th>
                                        <th>Tempat</th>
                                        <th>Tanggal Penyelenggaraan</th>
                                    </tr>
                                </thead>
                                <tbody id="responsive-table-body">
                                  <?php if($past_course->num_rows() == 0){ ?>
                                  <tr>
                                    <td colspan="4" style="text-align:center">
                                      Tidak ada data
                                    </td>
                                  </tr>
                                  <?php }else{ ?>
                                      <?php foreach ($past_course->result() as $pc) { ?>
                                          <tr>
                                        <td><?php echo $pc->course_name;?></td>
                                        <td><?php echo $pc->organizer;?></td>
                                        <td><?php echo $pc->place;?></td>
                                        <td><?php echo $pc->course_date;?></td>
                                        </tr>
                                      <?php } ?>
                                  <?php } ?>
                                </tbody>
                            </table>
                          </div>
                        </div>

                        <div class="panel panel-primary">
                          <div class="panel-heading">
                            <h3 class="panel-title">Aplikasi komputer yang dikuasai</h3>
                          </div>
                          <div class="panel-body">
                            <div class="row">
                                <?php $arr_penguasaan = explode(',',$profile->computers_apps);?>
                                <div class="col-sm-6 col-md-4" style="margin-left:20px">
                                    <div class="checkbox checkbox-primary">
                                        <input name="computer_apps" value="word" type="checkbox" <?php echo in_array('word',$arr_penguasaan) ? 'checked':''?> tabindex="90">
                                        <label for="checkbox1">MS Word</label>
                                    </div>
                                    <div class="checkbox checkbox-primary">
                                        <input name="computer_apps" value="excel" type="checkbox" <?php echo in_array('excel',$arr_penguasaan) ? 'checked':''?> tabindex="91">
                                        <label for="checkbox2">MS Excel</label>
                                    </div>
                                    <div class="checkbox checkbox-primary">
                                        <input name="computer_apps" value="powerpoint" type="checkbox" <?php echo in_array('powerpoint',$arr_penguasaan) ? 'checked':''?> tabindex="92">
                                        <label for="checkbox5">MS Powerpoint</label>
                                    </div>
                                    <div class="checkbox checkbox-primary">
                                        <input name="computer_apps" value="access" type="checkbox" <?php echo in_array('access',$arr_penguasaan) ? 'checked':''?> tabindex="94">
                                        <label for="checkbox5">MS Access</label>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="checkbox checkbox-primary">
                                        <input name="computer_apps" value="video_editing" type="checkbox" <?php echo in_array('video_editing',$arr_penguasaan) ? 'checked':''?> tabindex="95">
                                        <label for="checkbox5">Video Editing</label>
                                    </div>
                                    <div class="checkbox checkbox-primary">
                                        <input name="computer_apps" value="animasi" type="checkbox" <?php echo in_array('animasi',$arr_penguasaan) ? 'checked':''?> tabindex="96">
                                        <label for="checkbox5">Animasi</label>
                                    </div>
                                    <div class="checkbox checkbox-primary">
                                        <input name="computer_apps" value="design_grafis_dan_editing_gambar" type="checkbox" <?php echo in_array('design_grafis_dan_editing_gambar',$arr_penguasaan) ? 'checked':''?> tabindex="97">
                                        <label for="checkbox5">Design Grafis dan Editing Gambar</label>
                                    </div>
                                </div>
                            </div>
                          </div>
                        </div>


                      </form>
                     </div>
                  </div>
                  <!-- // END Panes -->
               </div>
            </div>
            <div class="col-md-3">
               <?php echo main_menu('classroom');?>
            </div>
            <!-- end right menu -->
         </div>
      </div>
   </div>
</div>
</div>
