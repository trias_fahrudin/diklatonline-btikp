<?php $color_array = array('bg-default','bg-primary','bg-lightred','bg-brown','bg-purple','.bg-gray-dark')?>

<div class="parallax bg-white page-section third">
    <div class="parallax-layer container" data-opacity="true">
        <div class="media v-middle">
            <div class="media-left">
                <span class="icon-block s60 bg-lightred"><i class="fa fa-mortar-board"></i></span>
            </div>
            <div class="media-body">
                <h1 class="text-display-1 margin-none"><?php echo ucwords(str_replace('-',' ',$this->uri->segment(4)))?></h1>
            </div>
            <div class="media-right">
                <a class="btn btn-white" href="<?php echo base_url() . 'classroom'?>">Kelas</a>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="page-section">
        <div class="row">
            <div class="col-md-9">
              <div class="panel panel-default">
                  <div class="panel-body" style="padding:0px">
                    <ol class="breadcrumb">
                      <li><a href="<?php echo base_url() . 'classroom';?>">Kelas</a></li>
                      <li class="active">Data Angkatan</li>
                    </ol>
                </div>
              </div>


                <div class="row" data-toggle="isotope">
                    <?php $i = 0;?>
                    <?php foreach ($data->result() as $classroom) { ?>
                    <div class="item col-xs-12 col-sm-6 col-lg-4">
                        <div class="panel panel-default paper-shadow" data-z="0.5">
                            <div class="cover overlay cover-image-full hover">
                                <span class="img icon-block height-150 <?php echo $color_array[$i]?>"></span>
                                <a href="<?php echo base_url() . 'classroom/edit/' . $classroom->slug?>" class="padding-none overlay overlay-full icon-block <?php echo $color_array[$i]?>">
                                    <span class="v-center"><i class="fa fa-building"></i></span>
                                </a>
                                <a href="<?php echo base_url() . 'classroom/index/edit/' . $classroom->slug?>" class="overlay overlay-full overlay-hover overlay-bg-white">
                                    <span class="v-center">
                                        <span class="btn btn-circle btn-red-500 btn-lg"><i class="fa fa-graduation-cap"></i></span>
                                    </span>
                                </a>
                            </div>
                            <div class="panel-body" style="padding-top: 16px;padding-left: 16px;padding-bottom: 2px;padding-right: 16px;">
                                 <h4 class="text-headline margin-v-0-10">
                                    <a href="<?php echo base_url() . 'classroom/index/edit/' . $classroom->slug?>"><?php echo ucwords($classroom->generation);?></a><br/>
                                     <hr class="margin-none" />
                                    <a style="padding-right: 2px;padding-left: 5px;" class="btn btn-white btn-flat paper-shadow relative" data-z="0" data-hover-z="1" data-animated href="<?php echo base_url() . 'classroom/index/edit/' . $classroom->slug?>"><i class="fa fa-fw fa-pencil"></i>Edit</a>


                                    <?php if(is_authorized(array('instructor','admin'))){ ?>
                                    <a style="padding-right: 2px;padding-left: 5px;" class="btn btn-white btn-flat paper-shadow relative" data-z="0" data-hover-z="1" data-animated href="<?php echo base_url() . 'classroom/participator/show/'  . $classroom->slug?>"><i class="fa fa-fw fa-user"></i>Lihat Peserta</a>
                                    <?php } ?>

                                    <hr class="margin-none" />
                                    <!-- <a class="btn btn-white btn-flat paper-shadow relative" data-z="0" data-hover-z="1" data-animated href="<?php echo base_url() . 'messages/index/' . $classroom->slug?>" style="padding-right: 2px;padding-left: 2px;"><i class="fa fa-fw fa-group"></i> Chat</a> -->


                                    <hr class="margin-none" />
                                    <?php
                                    if(is_authorized(array('admin'))){
                                    $status = ($classroom->availability === 'open' ? 'checked' : '');
                                    ?>

                                    <input class="availability" classroomid="<?php echo $classroom->id?>" type="checkbox"  data-toggle="toggle" <?php echo $status ?> data-on="OPEN" data-off="CLOSED">
                                    <a style="padding-right: 2px;padding-left: 5px;" class="btn btn-white btn-flat paper-shadow relative delete-classroom pull-right" style="padding: 5px 12px 0px 12px; border-bottom-width: 0px; border-top-width: 0px; border-left-width: 0px; border-right-width: 0px;" data-z="0" data-hover-z="1" data-animated url="<?php echo base_url() . 'classroom/index/delete/' . $classroom->slug?>"><span class="label label-danger"><i class="fa fa-fw fa-close"></i>Hapus</span></a>

                                    <?php } ?>


                                 </h4>
                            </div>
                        </div>
                    </div>
                    <?php $i <= 4 ? $i += 1 : $i = 0;} ?>

                    <?php if(is_authorized(array('admin'))){ ?>
                    <div class="item col-xs-12 col-sm-6 col-lg-4">
                        <div class="panel panel-default paper-shadow" data-z="0.5">
                            <div class="cover overlay cover-image-full hover">
                                <span class="img icon-block height-150 bg-grey-200"></span>
                                <a href="<?php echo base_url() . 'classroom/index/add/' . $classroom_slug?>" class="padding-none overlay overlay-full icon-block bg-grey-200">
                                    <span class="v-center">
                                        <i class="fa fa-plus text-grey-600"></i>
                                        Angkatan
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php } ?>

                </div>
                <ul class="pagination margin-top-none">
                    <?php echo $this->pagination->create_links();?>
                </ul>
                <br/>
                <br/>
            </div>
            <div class="col-md-3">
                <div class="panel panel-default" data-toggle="panel-collapse" data-open="true">
                   <div class="panel-heading panel-collapse-trigger">
                      <h4 class="panel-title">Menu</h4>
                   </div>
                   <div class="panel-body list-group">
                        <?php echo main_menu('classroom');?>
                   </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
  $('.delete-classroom').click(function() {
    var url = $(this).attr('url');
    var confirmText = "Apakah anda yakin ingin menghapus kelas ini ?";

    if (confirm(confirmText)) {
      $.ajax({
        type: "POST",
        url: url,
        success: function() {
          location.reload();
        },
      });
    }
    return false;
  });


    $(".availability").change(function() {
      //alert('test');
      //alert($(this).prop('checked'));
      $.post(base + 'classroom/index/set_availability', {
        classroom_id: $(this).attr('classroomid'),
        status: $(this).prop('checked') == true ? 'open':'close'
      }).done(function(data) {

      });
    })

</script>
