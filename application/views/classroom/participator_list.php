<div class="main-container">
  <div class="parallax overflow-hidden page-section bg-red-300 page-section third">
      <div class="parallax-layer container" data-opacity="true">
          <div class="media v-middle">
              <div class="media-left">
                  <span class="icon-block s60 bg-lightred"><i class="fa fa-user"></i></span>
              </div>
              <div class="media-body">
                <h1 class="text-display-1 text-white margin-none"><?php echo $classroom->name . ' (' . $classroom->generation . ')' ;?></h1>
              </div>
              <div class="media-right">
              </div>
          </div>
      </div>
  </div>

  <div class="container">
    <div class="page-section">
        <div class="row">
            <div class="col-md-9">
              <!-- http://localhost/diklatonline/data/participator/show/kelas-a -->

              <div class="panel panel-default">
                  <div class="panel-body" style="padding:0px">
                    <ol class="breadcrumb">
                      <li><a href="<?php echo base_url() . 'classroom';?>">Kelas</a></li>
                      <li><a href="<?php echo base_url() . 'classroom/index/details/' . getURLFriendly($classroom->name) ;?>">Data Angkatan</a></li>
                      <!-- http://localhost/diklatonline/classroom/index/details/lokakarya-pemanfaatan-media-pembelajaran-berbasis-tik-untuk-kegiatan-belajar-mengajar-2015 -->
                      <li class="active">Data Peserta</li>
                    </ol>
                </div>
              </div>

              <div class="panel panel-default">
                  <!-- Data table -->
                  <a href="<?php echo base_url() . 'classroom/export_data/' . $classroom->slug . '/participator/all'?>" class="navbar-btn btn btn-success" style="margin:10px"><i class="fa fa-fw fa-table"></i> Export peserta</a>

                  <?php

                    $this->db->select('id');
                    $this->db->where('level <>','participator');
                    $ignore = $this->db->get('users');

                    $arr_ignore = array();
                    foreach ($ignore->result() as $arr) {
                      array_push($arr_ignore,$arr->id);
                    }

                    $this->db->where_not_in('user_id', $arr_ignore);
                    $this->db->where(array('classroom_id' => $classroom->id,
                                           'status' => 'enable'));
                    $classroom_user_count = $this->db->count_all_results('classroom_users');

                    $cl = $this->db->get_where('classrooms',array('id' => $classroom->id))->row();
                    $classroom_quota = $cl->registration_quota;

                    if($classroom_user_count < $classroom_quota){
                  ?>
                  <a href="<?php echo base_url() . 'classroom/participator/add/' . $classroom->slug?>" class="navbar-btn btn btn-info" style="margin:10px"><i class="fa fa-fw fa-table"></i> Tambah peserta</a>
                  <?php } ?>

                  <table id="participator_table" class="table v-middle table-striped table-bordered display nowrap" cellspacing="0" width="100%">
                      <thead>
                          <tr>
                              <th>Username</th>
                              <th>Nama Lengkap</th>
                              <th>Asal Sekolah</th>
                              <th>Kab/Kota</th>
                              <th>Mata Pelajaran</th>

                              <?php if(is_authorized(array('admin'))){ ?>
                              <th>Tanggal Pendaftaran</th>
                              <th>Jumlah diklat yang diikuti</th>
                              <th style="text-align:center">Register</th>
                              <th>Cetak</th>
                              <?php } ?>

                              <?php if(is_authorized(array('instructor'))){ ?>
                              <th>Details</th>
                              <?php } ?>
                          </tr>
                      </thead>
                      <tfoot>
                          <tr>
                              <th>Username</th>
                              <th>Nama Lengkap</th>
                              <th>Asal Sekolah</th>
                              <th>Kab/Kota</th>
                              <th>Mata pelajaran</th>

                              <?php if(is_authorized(array('admin'))){ ?>
                                <th>Tanggal Pendaftaran</th>
                                <th>Jumlah diklat yang diikuti</th>
                                <th style="text-align:center">Register</th>
                                <th>Cetak</th>
                              <?php } ?>

                              <?php if(is_authorized(array('instructor'))){ ?>
                              <th>Details</th>
                              <?php } ?>

                          </tr>
                      </tfoot>
                      <tbody>
                          <?php foreach ($participator->result() as $par) { ?>
                          <tr>
                              <td><img src="<?php echo base_url() . 'timthumb?src=' . base_url() . 'upload/people/' . $par->foto . '&h=40&w=40&zc=0'; ?>" class="img-circle" alt="" /> <a href="<?php echo base_url() . 'public_profile/index/' . $par->id?>" target="_blank"><?php echo $par->username;?></a></td>
                              <td><?php echo ucwords($par->fullname);?></td>
                              <td><?php echo $par->origin_of_school;?></td>
                              <td><?php echo $par->district;?></td>
                              <td><?php echo $par->mapel;?></td>

                              <?php
                              if(is_authorized(array('admin'))){
                                $status = ($par->status_classroom_users === 'enable' ? 'checked' : '');
                              ?>
                              <td style="text-align:center"><?php echo tgl_panjang($par->register_at,"lm",true)?></td>
                              <td style="text-align:center" id="td_<?php echo $par->id?>" jmldiklat="<?php echo $par->jml_diklat?>"><?php echo $par->jml_diklat;?></td>
                              <td style="text-align:center">
                                <input class="status" classroomid="<?php echo $classroom->id ?>" userid="<?php echo $par->id ?>" type="checkbox" <?php echo $status ?>  data-on="ON" data-off="OFF">
                              </td>

                              <td>
                                <a href="#" onclick="return popitup('<?php echo base_url() . 'classroom/participator/show_for_print/' . $par->id . '/' . $classroom->slug ?>')" class="btn btn-info btn-circle" role="button"><i class="fa fa-print"></i> </a>
                                <a href="<?php echo base_url() . 'classroom/participator/card_receipts/' . $classroom->slug . '/' . $par->id ?>" target="_BLANK" class="btn btn-success btn-circle" role="button"><i class="fa fa-credit-card"></i></a>
                              </td>
                              <?php } ?>



                              <?php if(is_authorized(array('instructor'))){ ?>
                              <td>
                                  <a href="<?php echo base_url() . 'public_profile/index/' . $par->id;?>" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Details"><i class="fa fa-search"></i></a>
                              </td>
                              <?php } ?>

                          </tr>
                          <?php } ?>
                      </tbody>
                  </table>
              </div>
            </div>
            <div class="col-md-3">
              <div class="panel panel-default" data-toggle="panel-collapse" data-open="true">
                 <div class="panel-heading panel-collapse-trigger">
                    <h4 class="panel-title">Menu</h4>
                 </div>
                 <div class="panel-body list-group">
                      <?php echo main_menu('classroom');?>
                 </div>
              </div>
            </div>
        </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function() {
    $('#participator_table').DataTable( {
      "fnDrawCallback": function() {
            $('.status').bootstrapToggle();
      },
      "scrollX": true
    });
  });

  function popitup(url) {
    newwindow = window.open(url,'name','height=500,width=550');
    if (window.focus) {newwindow.focus()}
    return false;
  }

  $(".status").change(function() {
    //alert('test');
    //alert($(this).prop('checked'));

    var status = $(this).prop('checked') == true ? 'enable':'disable';
    var user_id = $(this).attr('userid');
    var jml_diklat = $('#td_' + user_id).attr('jmldiklat');

    $.post(base + 'classroom/participator/update_status', {
      classroom_id: $(this).attr('classroomid'),
      user_id: user_id,
      status:status
    }).done(function(data) {

      var nilai = parseInt(jml_diklat == '' ? '0' : jml_diklat);

      if(status === 'enable'){
        nilai = nilai + 1;
      }else{
        if(nilai == 0){
          nilai = 0
        }else{
          nilai = nilai - 1;
        }
      }

      // alert(nilai);
      $('#td_' + user_id).attr('jmldiklat',nilai);
      $('#td_' + user_id).html(nilai);
    });
  })
</script>
