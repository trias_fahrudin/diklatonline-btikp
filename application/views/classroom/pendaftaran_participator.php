<style>
  /*.checkbox label, .radio label {
      min-height: 20px;
      padding-left: 5px;
      margin-bottom: 0;
      font-weight: 400;
      cursor: pointer;
  }*/

  /*.checkbox, .radio {
    position: relative;
    display: block;
    margin-top: 10px;
    margin-bottom: 10px;
}*/

.well {
    min-height: 20px;
    padding: 19px;
    margin-bottom: 20px;
    background-color: transparent;
    border: 1px solid #e3e3e3;
    border-radius: 2px;
    box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
}
.form-control {
    display: block;
    width: 100%;
    height: 36px;
    padding: 8px 12px;
    font-size: 13px;
    line-height: 1.42857143;
    color: #333;
    background-color: transparent;
    background-image: none;
    border: 1px solid #efefef;
    border-radius: 2px;
    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
}


</style>

<div class="main-container">
  <div class="parallax overflow-hidden page-section bg-red-300 page-section third">
      <div class="parallax-layer container" data-opacity="true">
          <div class="media v-middle">
              <div class="media-left">
                  <span class="icon-block s60 bg-lightred"><i class="fa fa-user"></i></span>
              </div>
              <div class="media-body">
                <h1 class="text-display-1 text-white margin-none"><?php echo $classroom->name . ' (' . $classroom->generation . ')' ;?></h1>
              </div>
              <div class="media-right">
              </div>
          </div>
      </div>
  </div>

  <div class="container">
    <div class="page-section">
      <div class="row">
          <div class="col-md-9">

            <div class="panel panel-default">
                <div class="panel-body" style="padding:0px">
                  <ol class="breadcrumb">
                    <li><a href="<?php echo base_url() . 'classroom';?>">Kelas</a></li>
                    <li><a href="<?php echo base_url() . 'classroom/index/details/' . getURLFriendly($classroom->name) ;?>">Data Angkatan</a></li>
                    <!-- http://localhost/diklatonline/classroom/index/details/lokakarya-pemanfaatan-media-pembelajaran-berbasis-tik-untuk-kegiatan-belajar-mengajar-2015 -->
                    <li><a href="<?php echo base_url() . 'classroom/participator/show/' . $this->uri->segment(4) ;?>">Data Peserta</a></li>
                    <li class="active">Tambah</li>
                  </ol>
              </div>
            </div>
              <!-- <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title"><strong>Tambahkan user lama</strong></h3>
                </div>
                <div class="panel-body">
                  <form method="post" action="<?php echo base_url() . 'classroom/participator/add_user_manual/' . $this->uri->segment(4)?>">
                    <h5>Cari nama peserta</h5>
                    <div class="form-group">
                        <select name="id_user[]" id="tambah-user-lama-ajax" style="width: 100%;height:50%">
                        </select>
                    </div>
                    <input type="hidden" name="classroom_id" value="<?php echo $classroom->id;?>">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </form>
                </div>
              </div> -->

              <div class="panel panel-default" >
                 <div class="panel-heading">
                    <h3 class="panel-title"><strong>Form penambahan peserta baru</strong></h3>
                    <!-- <div style="float:right; font-size: 80%; position: relative; top:-10px"><a href="#">Forgot password?</a></div> -->
                 </div>
                 <div class="panel-body">
                    <form role="form" id="form-signup" action="" method="POST">
                      <div class="alert alert-success" role="alert" id="alert-signin-success" style="display:none">
                        Penambahan peserta berhasil
                      </div>

                      <div class="alert alert-danger" role="alert" id="alert-signin-danger" style="display:none">

                        Pastikan NIP dan NUPTK yang anda masukkan benar <br/>
                        Pastikan Email yang anda masukkan valid
                      </div>

                      <div class="form-group">
                        <select id="status" class="form-control" data-style="btn-white" data-size="5" placeholder="">
                          <option value="PNS">PNS</option>
                          <option value="NON-PNS">NON PNS</option>
                        </select>
                      </div>

                       <div class="form-group">
                          <input type="text" name="fullname" id="fullname" class="form-control " placeholder="Nama lengkap" tabindex="1" required>
                       </div>

                       <div class="form-group">
                          <input type="text" name="username" id="username" class="form-control " placeholder="Username" tabindex="1" required>
                       </div>


                       <div class="row">
                          <div class="col-xs-12 col-sm-6 col-md-6">
                             <div class="form-group">
                                <input type="text" name="nip" id="nip" class="form-control" placeholder="NIP" tabindex="2">
                             </div>
                          </div>
                          <div class="col-xs-12 col-sm-6 col-md-6">
                             <div class="form-group">
                                <input type="text" name="nuptk" id="nuptk" class="form-control" placeholder="NUPTK" tabindex="3">
                             </div>
                          </div>
                       </div>

                       <div class="row">
                          <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                               <input type="email" name="email" id="email" class="form-control " placeholder="Alamat Email" tabindex="4" required>
                            </div>
                          </div>

                          <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                               <input type="text" name="telp_number" id="telp_number" class="form-control " placeholder="Nomor Telp" tabindex="5" required>
                            </div>
                          </div>
                       </div>

                       <div class="row">
                          <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                               <input type="text" name="city_of_birth" id="city_of_birth" class="form-control " placeholder="Kota lahir" tabindex="6" required>
                            </div>
                          </div>

                          <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                               <input name="date_of_birth" id="date_of_birth" class="datepicker form-control " placeholder="Tanggal lahir (Misal : 26/12/1980)" tabindex="7" required>
                            </div>
                          </div>
                       </div>

                       <div class="row">
                          <div class="col-xs-12 col-sm-6 col-md-12">
                            <div class="form-group">
                              <select id="gender" class="form-control" placeholder="Jenis Kelamin" tabindex="8">
                                <option value="">[ Jenis Kelamin]</option>
                                <option value="male">Laki-laki</option>
                                <option value="female">Perempuan</option>
                              </select>
                            </div>
                          </div>
                       </div>

                       <div class="form-group">
                          <input type="text" name="home_address" id="home_address" class="form-control " placeholder="Alamat Rumah" tabindex="9" required>
                       </div>



                       <div class="row">
                          <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                               <input type="text" name="golongan" id="golongan" class="form-control " placeholder="Pangkat/Golongan" tabindex="10" required>
                            </div>
                          </div>

                          <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                               <input type="text" name="position" id="position" class="form-control " placeholder="Jabatan" tabindex="11" required>
                            </div>
                          </div>
                       </div>

                       <div class="form-group">
                          <input type="text" name="office_address" id="office_address" class="form-control " placeholder="Alamat Kantor" tabindex="12" required>
                       </div>

                       <div class="form-group">
                          <input type="text" name="npwp" id="npwp" class="form-control " placeholder="NPWP" tabindex="13" required>
                       </div>

                       <div class="row">
                          <div class="col-xs-12 col-sm-6 col-md-4">
                            <div class="form-group">
                               <input type="text" name="origin_of_school" id="origin_of_school" class="form-control " placeholder="Asal sekolah" tabindex="14" required>
                            </div>
                          </div>
                          <div class="col-xs-12 col-sm-6 col-md-4">
                            <div class="form-group">
                              <select id="district" class="form-control" placeholder="Kota sekolah" tabindex="15">
                                <option value="">[ Pilih Kab/Kota ]</option>
                                <?php
                                $districts = $this->Basecrud_m->get('districts');
                                foreach ($districts->result() as $d) { ?>
                                <option value="<?php echo $d->name;?>"><?php echo $d->name;?></option>
                                <?php }  ?>
                              </select>
                            </div>
                          </div>

                          <div class="col-xs-12 col-sm-6 col-md-4">
                            <div class="form-group">
                               <input type="text" name="mapel" id="mapel" class="form-control " placeholder="Mata pelajaran" tabindex="16" required>
                            </div>
                          </div>
                          <!-- <div class="panel-body">
                             <table class="table table-striped">
                                <tr>
                                   <td style="width:15%;">Surat Tugas</td>
                                   <td>
                                      <div class="col-sm-6">
                                         <input type="file" id="surat_tugas" name="surat_tugas" class="form-control">
                                      </div>
                                   </td>
                                </tr>
                              </table>
                          </div> -->
                          <div class="col-sm-12">
                            <div class="form-group">
                              <label for="exampleInputFile">Upload Surat Penugasan</label>
                              <input type="file" id="surat_tugas" name="surat_tugas" class="form-control">
                              <p class="help-block">File format yang diijinkan : jpeg,jpg,png atau pdf</p>
                            </div>
                          </div>

                       </div>

                       <div class="panel panel-info">
                          <div class="panel-heading">
                             <h3 class="panel-title"><strong>Pelatihan yang pernah diikuti </strong></h3>
                             <!-- <div style="float:right; font-size: 80%; position: relative; top:-10px"><a href="#">Forgot password?</a></div> -->
                          </div>

                          <div class="panel-body">

                            <div class="multi-field-wrapper">
                              <div class="multi-fields">
                                <div class="multi-field well">
                                  <div class="row">
                                      <div class="col-xs-12 col-sm-6 col-md-12">
                                        <div class="form-group">
                                           <input type="text" name="course_name[]" class="form-control" id="course_name" placeholder="Nama pelatihan" tabindex="17" required>
                                        </div>
                                      </div>

                                     <div class="col-xs-12 col-sm-6 col-md-12">
                                       <div class="form-group">
                                          <input type="text" name="organizer[]" class="form-control " placeholder="Penyelenggara" tabindex="18" required>
                                       </div>
                                     </div>

                                     <div class="col-xs-12 col-sm-6 col-md-12">
                                       <div class="form-group">
                                          <input type="text" name="place[]" class="form-control " placeholder="Tempat" tabindex="19" required>
                                       </div>
                                     </div>

                                     <div class="col-xs-12 col-sm-6 col-md-12">
                                       <div class="form-group">
                                          <!-- <label for="exampleInputEmail1">Tanggal penyelenggaraan</label> -->
                                          <input name="course_date[]" class="datepicker form-control " placeholder="Tanggal penyelenggaraan (Misal : 26/12/2007)" tabindex="20" required>

                                          <!-- <input type="text" name="" class="datepicker" class="form-control" placeholder="Tanggal penyelenggaraan" tabindex="6" required> -->
                                       </div>
                                     </div>

                                  </div>
                                  <button type="button" class="remove-field" tabindex="21">Hapus</button>

                                </div>
                              </div>

                              <div class="form-group">
                                <button type="button" class="add-field" tabindex="22">Tambah data</button>
                              </div>
                            </div>
                          </div>
                       </div>

                       <div class="panel panel-info">
                          <div class="panel-heading">
                             <h3 class="panel-title"><strong>Aplikasi komputer yang dikuasai </strong></h3>
                          </div>

                          <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-6 col-md-4" style="margin-left:20px">
                                    <div class="checkbox checkbox-primary">
                                        <input id="word" name="computer_apps" value="word" type="checkbox" tabindex="90">
                                        <label for="word">MS Word</label>
                                    </div>
                                    <div class="checkbox checkbox-primary">
                                        <input id="excel" name="computer_apps" value="excel" type="checkbox" tabindex="91">
                                        <label for="excel">MS Excel</label>
                                    </div>
                                    <div class="checkbox checkbox-primary">
                                        <input id="powerpoint" name="computer_apps" value="powerpoint" type="checkbox" tabindex="92">
                                        <label for="powerpoint">MS Powerpoint</label>
                                    </div>
                                    <div class="checkbox checkbox-primary">
                                        <input id="access" name="computer_apps" value="access" type="checkbox" tabindex="94">
                                        <label for="access">MS Access</label>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="checkbox checkbox-primary">
                                        <input id="video_editing" name="computer_apps" value="video_editing" type="checkbox" tabindex="95">
                                        <label for="video_editing">Video Editing</label>
                                    </div>
                                    <div class="checkbox checkbox-primary">
                                        <input id="animasi" name="computer_apps" value="animasi" type="checkbox" tabindex="96">
                                        <label for="animasi">Animasi</label>
                                    </div>
                                    <div class="checkbox checkbox-primary">
                                        <input id="design_grafis_dan_editing_gambar" name="computer_apps" value="design_grafis_dan_editing_gambar" type="checkbox" tabindex="97">
                                        <label for="design_grafis_dan_editing_gambar">Design Grafis dan Editing Gambar</label>
                                    </div>
                                </div>
                            </div>
                          </div>
                       </div>



                       <a href="#" id="btn-signup" class="btn btn-primary pull-right" tabindex="101"><i class="fa fa-fw fa-play"></i> Buat Akun</a>

                    </form>
                 </div>
              </div>
           <!-- </div> -->
         </div>

         <div class="col-md-3">
           <div class="panel panel-default" data-toggle="panel-collapse" data-open="true">
              <div class="panel-heading panel-collapse-trigger">
                 <h4 class="panel-title">Menu</h4>
              </div>
              <div class="panel-body list-group">
                   <?php echo main_menu('classroom');?>
              </div>
           </div>
         </div>
      </div>
    </div>
  </div>
</div>

<?php

  $this->db->select('id');
  $this->db->where('level <>','participator');
  $ignore = $this->db->get('users');

  $arr_ignore = array();
  foreach ($ignore->result() as $arr) {
    array_push($arr_ignore,$arr->id);
  }

  $this->db->where_not_in('user_id', $arr_ignore);
  $this->db->where(array('classroom_id' => $classroom->id,
                         'status' => 'enable'));
  $classroom_user_count = $this->db->count_all_results('classroom_users');

  $cl = $this->db->get_where('classrooms',array('id' => $classroom->id))->row();
  $classroom_quota = $cl->registration_quota;
?>

<script type="text/javascript">

      $("#tambah-user-lama-ajax").select2({
          // tags: true,
          multiple: true,
          // tokenSeparators: [',', ' '],
          minimumInputLength: 2,
          minimumResultsForSearch: 10,
          allowClear: true,
          maximumSelectionLength: <?php echo $classroom_quota - $classroom_user_count;?>,
          // theme: "classic",
          ajax: {
              url: base + 'classroom/participator/get_data',
              dataType: "json",
              type: "POST",
              data: function (params) {

                  var queryParameters = {
                      term: params.term,
                      classroom_id: <?php echo $classroom->id ?>
                  }
                  return queryParameters;
              },
              processResults: function (data) {
                  return {
                      results: $.map(data, function (item) {
                          return {
                              text: item.fullname + ' (' + item.origin_of_school + ')',
                              id: item.id
                          }
                      })
                  };
              }
          }
      });
    // $('.datepicker').datepick(
    //   {dateFormat: 'dd/mm/yyyy'}
    // );

    // $(".datepicker").mask("99/99/9999",{placeholder:"00/00/0000"});

    $('.multi-field-wrapper').each(function() {
        var $wrapper = $('.multi-fields', this);
        $(".add-field", $(this)).click(function(e) {
            // $(".datepicker").mask("99/99/9999",{placeholder:"00/00/0000"});
            $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
        });

        $('.multi-field .remove-field', $wrapper).click(function() {
            if ($('.multi-field', $wrapper).length > 1)
                $(this).parent('.multi-field').remove();
        });
    });

    function isValidDate(dtValue){
      var dtRegex = new RegExp(/\b\d{1,2}[\/-]\d{1,2}[\/-]\d{4}\b/);
      return dtRegex.test(dtValue);
    }

    function isValidEmailAddress(emailAddress) {
        var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        return pattern.test(emailAddress);
    }

    function myTrim(x) {
      return x.replace(/^\s+|\s+$/gm,'');
    }

    $(document).ready(function() {
      $("#btn-signup").click(function() {

          $('#alert-signin-success').hide();
          $('#alert-signin-danger').hide();

          var isFormValid = true;

          $(".required input").each(function(){
              if ($.trim($(this).val()).length === 0){
                  $(this).addClass("has-error");
                  isFormValid = false;
              }
              else{
                  $(this).removeClass("has-error");
              }
          });

          if (!isFormValid){
              alert("Mohon isi semua kolom yang diperlukan (*)");
              return isFormValid;
          }


      	if (!isValidEmailAddress($('#email').val())){
      		alert('Alamat email tidak valid!');
      		return false;
      	}

        if(!isValidDate($('#date_of_birth').val())){
          alert('Tanggal lahir tidak valid!');
      		return false;
        }

        if($('#status').val() === 'PNS'){
          var nip = myTrim($('#nip').val());
          var golongan = myTrim($('#golongan').val());

          if( nip.length == 0 || golongan.length == 0 ){
            alert('NIP dan Pangkat/Golongan harus diisi');
            return false;
          }
        }

        $("#loading-div-background").show();
        $("#loading-div-background").css({ opacity: 0.7 });
        $(".block").css({ opacity: 0.5 });

        //get list of computers_apps
        var computers_apps = [];
        $.each($("input[name='computer_apps']:checked"), function(){
            computers_apps.push($(this).val());
        });

        var classroom = '<?php echo $this->uri->segment(4);?>';
      	/*$.post( base + 'classroom/participator/add/' + classroom , {
              status: $('#status').val(),
              fullname: $('#fullname').val(),
              username: $('#username').val(),
              nip: $('#nip').val(),
              nuptk: $('#nuptk').val(),
              email: $('#email').val(),
              telp_number : $('#telp_number').val(),
              city_of_birth : $('#city_of_birth').val(),
              date_of_birth : $('#date_of_birth').val(),
              gender : $('#gender').val(),
              home_address : $('#home_address').val(),
              golongan : $('#golongan').val(),
              position : $('#position').val(),
              office_address : $('#office_address').val(),
              npwp : $('#npwp').val(),

              origin_of_school : $('#origin_of_school').val(),
              district : $('#district').val(),
              mapel : $('#mapel').val(),

              course_name : $('[name="course_name[]"]').map(function () {return this.value;}).get(),
              organizer : $('[name="organizer[]"]').map(function () {return this.value;}).get(),
              place : $('[name="place[]"]').map(function () {return this.value;}).get(),
              course_date : $('[name="course_date[]"]').map(function () {return this.value;}).get(),

              computers_apps : computers_apps.join(",")
            })

       	 .done(function( data ) {
         		if (data.status !== 'OK') {
                  $('#alert-signin-danger').show();
                  $('#alert-signin-danger').html(data.msg);

                  $("#loading-div-background").hide();
                  $("#loading-div-background").css({ opacity: 0.5 });
                  $(".block").css({ opacity: 1.0 });

         		}else{
                  $("input").each(function(){
                     $(this).val("");
                  });

                  $('#alert-signin-success').show();

                  $("#loading-div-background").hide();
                  $("#loading-div-background").css({ opacity: 0.5 });
                  $(".block").css({ opacity: 1.0 });
              }


      	 });*/

         /*function baru untuk mengakomodasi upload file*/
         var fd = new FormData();
         fd.append('status', $('#status').val());
         fd.append('fullname', $('#fullname').val());
         fd.append('username', $('#username').val());
         fd.append('nip', $('#nip').val());
         fd.append('nuptk', $('#nuptk').val());
         fd.append('email', $('#email').val());
         fd.append('telp_number', $('#telp_number').val());
         fd.append('city_of_birth', $('#city_of_birth').val());
         fd.append('date_of_birth', $('#date_of_birth').val());
         fd.append('gender', $('#gender').val());
         fd.append('home_address', $('#home_address').val());
         fd.append('golongan', $('#golongan').val());
         fd.append('position', $('#position').val());
         fd.append('office_address', $('#office_address').val());
         fd.append('npwp', $('#npwp').val());
         fd.append('origin_of_school', $('#origin_of_school').val());
         fd.append('district', $('#district').val());
         fd.append('mapel', $('#mapel').val());
         //file
         fd.append('surat_tugas',$("#surat_tugas").prop("files")[0]);
         fd.append('course_name', $('[name="course_name[]"]').map(function () {return this.value;}).get());
         fd.append('organizer', $('[name="organizer[]"]').map(function () {return this.value;}).get());
         fd.append('place', $('[name="place[]"]').map(function () {return this.value;}).get());
         fd.append('course_date', $('[name="course_date[]"]').map(function () {return this.value;}).get());

        //  fd.append('captcha_word', $('#captcha_word').val());
         fd.append('computers_apps', computers_apps.join(","));

         $.ajax({
             url: base + 'classroom/participator/add/' + classroom,
             type: 'POST',
             data: fd,                         // Setting the data attribute of ajax with file_data
             dataType: 'json',
             cache: false,
             contentType: false,
             processData: false,
             success:function(data,textStatus,jqXHR){
               if (data.status !== 'OK') {
                    $('#alert-signin-danger').show();
                    $('#alert-signin-danger').html(data.msg);

                    $("#loading-div-background").hide();
                    $("#loading-div-background").css({ opacity: 0.5 });
                    $(".block").css({ opacity: 1.0 });

           		}else{
                    $("input").each(function(){
                       $(this).val("");
                    });

                    $('#alert-signin-success').show();

                    $("#loading-div-background").hide();
                    $("#loading-div-background").css({ opacity: 0.5 });
                    $(".block").css({ opacity: 1.0 });
                }

                // $('#captchaId').replaceWith(data.image);
             }
         })


      });
    });



</script>
