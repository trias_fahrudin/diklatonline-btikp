<?php $color_array = array('bg-default','bg-primary','bg-lightred','bg-brown','bg-purple','bg-gray-dark')?>

<div class="parallax bg-white page-section third">
    <div class="parallax-layer container" data-opacity="true">
        <div class="media v-middle">
            <div class="media-left">
                <span class="icon-block s60 bg-lightred"><i class="fa fa-mortar-board"></i></span>
            </div>
            <div class="media-body">
                <h1 class="text-display-1 margin-none">Kelas</h1>
            </div>
            <div class="media-right">
                <a class="btn btn-white" href="<?php echo base_url() . 'classroom'?>">Kelas</a>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="page-section">
        <div class="row">
            <div class="col-md-9">
              <?php if($data->num_rows() == 0){ ?>
              <div class="alert alert-warning" role="alert" style="padding-top: 10px;padding-bottom: 10px;border-bottom-width: 1px;">
                <h4 style="text-align:center;margin-bottom: 0px;">Belum ada data kelas</h4>
              </div>
              <?php } ?>

                <div class="row" data-toggle="isotope">
                    <?php
                      $i = 0;
                      $temp_title = "";

                      foreach ($data->result() as $classroom) {
                        if($classroom->name !== $temp_title){
                          $temp_title = $classroom->name;
                        }else{
                          continue;
                        }
                    ?>
                    <div class="item col-xs-12 col-sm-6 col-lg-4">
                        <div class="panel panel-default paper-shadow" data-z="0.5">
                            <div class="cover overlay cover-image-full hover">
                                <span class="img icon-block height-150 <?php echo $color_array[$i]?>"></span>
                                <a href="<?php echo base_url() . 'classroom/index/details/' . $classroom->name_slug?>" class="padding-none overlay overlay-full icon-block <?php echo $color_array[$i]?>">
                                    <span class="v-center"><i class="fa fa-building"></i></span>
                                </a>
                                <a href="<?php echo base_url() . 'classroom/index/details/' . $classroom->name_slug?>" class="overlay overlay-full overlay-hover overlay-bg-white">
                                    <span class="v-center">
                                        <span class="btn btn-circle btn-red-500 btn-lg"><i class="fa fa-graduation-cap"></i></span>
                                    </span>
                                </a>
                            </div>
                            <div class="panel-body" style="padding-top: 16px;padding-left: 16px;padding-bottom: 2px;padding-right: 16px;">
                                 <h4 class="text-headline margin-v-0-10" style="font-size: 1.5rem;line-height:2.0rem">
                                    <a href="<?php echo base_url() . 'classroom/index/details/' . $classroom->name_slug?>"><?php echo $classroom->name;?></a><br/>
                                    
                                 </h4>
                            </div>
                        </div>
                    </div>
                    <?php $i <= 6 ? $i += 1 : $i = 0;} ?>

                    <?php if(is_authorized(array('admin'))){ ?>
                    <div class="item col-xs-12 col-sm-6 col-lg-4">
                        <div class="panel panel-default paper-shadow" data-z="0.5">
                            <div class="cover overlay cover-image-full hover">
                                <span class="img icon-block height-150 bg-grey-200"></span>
                                <a href="<?php echo base_url() . 'classroom/index/add'?>" class="padding-none overlay overlay-full icon-block bg-grey-200">
                                    <span class="v-center">
                                        <i class="fa fa-plus text-grey-600"></i>
                                        Kelas
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php } ?>

                </div>
                <ul class="pagination margin-top-none">
                    <?php echo $this->pagination->create_links();?>
                </ul>
                <br/>
                <br/>
            </div>
            <div class="col-md-3">
                <div class="panel panel-default" data-toggle="panel-collapse" data-open="true">
                   <div class="panel-heading panel-collapse-trigger">
                      <h4 class="panel-title">Menu</h4>
                   </div>
                   <div class="panel-body list-group">
                        <?php echo main_menu('classroom');?>
                   </div>
                </div>
            </div>
        </div>
    </div>
</div>
