<!-- http://localhost/diklatonline/classroom/edit/kelas-a -->

<?php
    $mode = $this->uri->segment(3);

    // $participator_list = "";
    // $instructor_list   = "";
    // $rs_participator   = null;
    // $rs_instructor     = null;

    if($mode === 'edit' || $mode === 'edit_act')
    {

        // $participator_list    = $classroom->participator_list;
        // $instructor_list      = $classroom->instructor_list;

        $classroom_id         = $classroom->id;

        $rs_participator      = $this->Classroom_m->get_users($classroom_id,'participator');
        $rs_instructor        = $this->Basecrud_m->get('view_instructors');


        $name                 = $mode === 'edit' ? $classroom->name : set_value('name');
        $generation           = $mode === 'edit' ? $classroom->generation : set_value('generation');
        $registration_quota   = $mode === 'edit' ? $classroom->registration_quota : set_value('registration_quota');

        $slug            = $classroom->slug;


        $detail_kegiatan = $mode === 'edit' ? $classroom->detail_kegiatan : set_value('detail_kegiatan');
        $narasumber      = $mode === 'edit' ? $classroom->narasumber : set_value('narasumber');
        $peserta         = $mode === 'edit' ? $classroom->peserta : set_value('peserta');
        $tempat          = $mode === 'edit' ? $classroom->tempat : set_value('tempat');

        $preface              = $mode === 'edit' ? $classroom->preface : set_value('preface');
        // $participator_list = $classroom->participator_list;
        // $instructor_list   = $classroom->instructor_list;
        $start                = $mode === 'edit' ? appDate(substr($classroom->start,0,strpos($classroom->start, ' '))) : set_value('start');
        $end                  = $mode === 'edit' ? appDate(substr($classroom->end,0,strpos($classroom->end, ' '))) : set_value('end');

        $act = base_url() .  'classroom/index/edit_act/' . $slug;

    }else{

        $classroom_id         = "";
        $name                 = $mode === 'add' ? '' : set_value('name');
        $generation           = $mode === 'add' ? '' : set_value('generation');
        $registration_quota           = $mode === 'add' ? '' : set_value('registration_quota');

        $slug                 = "";



        $detail_kegiatan = $mode === 'add' ? '' : set_value('detail_kegiatan');
        $narasumber      = $mode === 'add' ? '' : set_value('narasumber');
        $peserta         = $mode === 'add' ? '' : set_value('peserta');
        $tempat          = $mode === 'add' ? '' : set_value('tempat');


        $preface              = $mode === 'add' ? '' : set_value('preface');
        // $participator_list = $classroom->participator_list;
        // $instructor_list   = $classroom->instructor_list;
        $start                = $mode === 'add' ? '' : set_value('start');
        $end                  = $mode === 'add' ? '' : set_value('end');

        $act = base_url() . 'classroom/index/add_act/' . $this->uri->segment(4,'');

    }
?>


<div class="main-container">
  <?php if($mode === 'edit' || $mode === 'edit_act'){ ?>
  <div class="parallax bg-white third page-section">
      <div class="parallax-layer container" data-opacity="true">
          <div class="media v-middle">
              <div class="media-left">
                  <span class="icon-block half bg-cyan-500 text-white"><i class="fa fa-edit"></i></span>
              </div>
              <div class="media-body">
                  <h1 class="text-display-1 margin-none"><?php echo ucwords($classroom->name) . ' (' . ucwords($classroom->generation) . ')'?></h1>
              </div>

              <div class="media-right">
                  <a class="btn btn-white" href="<?php echo base_url() . 'classroom'?>">Kelas</a>
              </div>
          </div>
      </div>
  </div>
  <?php }else{ ?>
  <div class="parallax bg-white page-section third">
      <div class="parallax-layer container" data-opacity="true">
          <div class="media v-middle">
              <div class="media-left">
                  <span class="icon-block s60 bg-lightred"><i class="fa fa-mortar-board"></i></span>
              </div>
              <div class="media-body">
                  <h1 class="text-display-1 margin-none"><?php echo ucwords(str_replace('-',' ',$this->uri->segment(4,'Tambah Kelas')))?></h1>
              </div>
              <div class="media-right">
                  <a class="btn btn-white" href="<?php echo base_url() . 'classroom'?>">Kelas</a>
              </div>
          </div>
      </div>
  </div>
  <?php } ?>

  <div class="container">
      <div class="page-section">
          <div class="row">
              <div class="col-md-9">
                  <!-- Tabbable Widget -->

                  <?php  if($mode === 'edit' || $mode === 'edit_act'){ ?>
                  <div class="panel panel-default">
                      <div class="panel-body" style="padding:0px">
                        <ol class="breadcrumb">
                          <li><a href="<?php echo base_url() . 'classroom';?>">Kelas</a></li>
                          <li><a href="<?php echo base_url() . 'classroom/index/details/' .  getURLFriendly($classroom->name);?>">Data Angkatan</a></li>
                          <!-- http://localhost/diklatonline/classroom/index/details/lokakarya-pemanfaatan-media-pembelajaran-berbasis-tik-untuk-kegiatan-belajar-mengajar-2015 -->
                          <li class="active"><span class="label label-warning">Edit</span> <!--<?php echo ucwords($classroom->name)?>--></li>
                        </ol>
                    </div>
                  </div>

                  <div class="tabbable paper-shadow relative" data-z="0.5">
                      <!-- Tabs -->
                      <ul class="nav nav-tabs">
                          <li class="active"><a href="#classroom" data-toggle="tab"><i class="fa fa-fw fa-home"></i> Kelas</a></li>

                          <?php if(is_authorized(array('admin'))){ ?>
                          <li><a href="#participator" data-toggle="tab"><i class="fa fa-fw fa-user"></i> Peserta</a></li>
                          <li><a href="#instructor" data-toggle="tab"><i class="fa fa-fw fa-user-md"></i> Instruktur</a></li>
                          <?php } ?>

                          <li><a href="#courses" data-toggle="tab"><i class="fa fa-fw fa-book"></i> Pelajaran</a></li>
                      </ul>
                      <!-- // END Tabs -->
                      <!-- Panes -->
                      <div class="tab-content">
                          <div id="classroom" class="tab-pane active">
                            <?php if($this->session->userdata('msg_ok')){ ?>
                             <div class="alert alert-success" role="alert" id="alert-success" style="padding: 10px 2px 10px 2px;">
                                <h4 style="text-align:center;margin-bottom: 0px;">Data Berhasil di Update</h4>
                             </div>
                             <script>
                                 $("#alert-success").fadeTo(5000, 500).slideUp(500, function() {
                                   $("#alert-success").hide();
                                 });
                             </script>
                            <?php } ?>

                             <?php if(isset($msg_error)){ ?>
                             <div class="alert alert-danger" role="alert" id="alert-danger" style="padding: 10px 2px 10px 2px;">
                                <h4 style="text-align:center;margin-bottom: 0px;"><?php echo $msg_error;?></h4>
                             </div>
                             <script>
                                 $("#alert-danger").fadeTo(5000, 500).slideUp(500, function() {
                                   $("#alert-danger").hide();
                                 });
                             </script>
                             <?php } ?>

                              <form action="<?php echo $act?>" method="POST">
                                  <input type="hidden" value="<?php echo $classroom_id;?>" name="classroom_id" id="classroom_id">

                                  <div class="form-group form-control-material">
                                      <input type="text" name="name" id="title" placeholder="Nama Kelas" class="form-control used" value="<?php echo $name?>" />
                                      <label for="title">Kelas</label>
                                  </div>

                                  <div class="form-group form-control-material">
                                      <input type="text" name="generation" id="generation" placeholder="Angkatan (Misal : Angkatan 1)" class="form-control used" value="<?php echo $generation?>" />
                                      <label for="title">Angkatan</label>
                                  </div>

                                  <div class="form-group form-control-material">
                                      <input id="datepicker" name="start" type="text" class="form-control used datepicker" value="<?php echo $start?>">
                                      <label for="start">Tanggal Mulai</label>
                                  </div>
                                  <div class="form-group form-control-material">
                                      <input id="datepicker" name="end" type="text" class="form-control used datepicker" value="<?php echo $end?>">
                                      <label for="end">Tanggal Selesai</label>
                                  </div>

                                  <div class="form-group">
                                    <label for="registration_quota">Kuota Pendaftaran</label>
                                    <input id="registration_quota" data-verticalbuttons="true" type="text" value="<?php echo $registration_quota?>" name="registration_quota" class="form-control used" />
                                  </div>


                                  <div class="form-group">
                                      <!-- <input type="text" name="detail_kegiatan" id="detail_kegiatan" placeholder="Detail Kegiatan" class="form-control used" value="<?php echo $detail_kegiatan?>" /> -->
                                      <label for="title">Detail Kegiatan</label>
                                      <textarea name="detail_kegiatan" id="detail_kegiatan"><?php echo $detail_kegiatan?></textarea>

                                  </div>

                                  <div class="form-group form-control-material">
                                      <input type="text" name="narasumber" id="narasumber" placeholder="Narasumber" class="form-control used" value="<?php echo $narasumber?>" />
                                      <label for="title">Narasumber</label>
                                  </div>

                                  <div class="form-group form-control-material">
                                      <input type="text" name="peserta" id="peserta" placeholder="Peserta" class="form-control used" value="<?php echo $peserta?>" />
                                      <label for="title">Peserta</label>
                                  </div>

                                  <div class="form-group form-control-material">
                                      <input type="text" name="tempat" id="tempat" placeholder="Tempat" class="form-control used" value="<?php echo $tempat?>" />
                                      <label for="title">Tempat</label>
                                  </div>

                                  <div class="form-group">
                                      <label for="preface">Kata Pengantar</label>
                                      <textarea name="preface" id="preface" cols="30" rows="10"><?php ?><?php echo $preface;?></textarea>
                                  </div>
                                  <div class="text-right">
                                      <button type="submit" class="btn btn-primary">Simpan <i class="fa fa-chevron-circle-right"></i></button>
                                  </div>

                              </form>
                          </div>


                          <?php if(is_authorized(array('admin'))){ ?>
                          <div id="participator" class="tab-pane">
                              <a href="<?php echo base_url() . 'classroom/export_data/' . $classroom->slug . '/participator/all'?>" class="navbar-btn btn btn-success" style="margin:10px"><i class="fa fa-fw fa-table"></i> Export peserta</a>

                              <?php

                                $this->db->select('id');
                                $this->db->where('level <>','participator');
                                $ignore = $this->db->get('users');

                                $arr_ignore = array();
                                foreach ($ignore->result() as $arr) {
                                  array_push($arr_ignore,$arr->id);
                                }

                                $this->db->where_not_in('user_id', $arr_ignore);
                                $this->db->where(array('classroom_id' => $classroom->id,
                                                       'status' => 'enable'));
                                $classroom_user_count = $this->db->count_all_results('classroom_users');

                                $cl = $this->db->get_where('classrooms',array('id' => $classroom->id))->row();
                                $classroom_quota = $cl->registration_quota;

                                if($classroom_user_count < $classroom_quota){
                              ?>
                              <a href="<?php echo base_url() . 'classroom/participator/add/' . $classroom->slug?>" class="navbar-btn btn btn-info" style="margin:10px"><i class="fa fa-fw fa-table"></i> Tambah peserta</a>
                              <?php } ?>

                              <div class="panel panel-default">
                                  <!-- Data table -->

                                  <table id="participator_table" class="table v-middle" cellspacing="0" width="100%" id="participator-table">
                                      <thead>
                                          <tr>
                                              <th>Username</th>
                                              <th>Nama Lengkap</th>
                                              <th>Asal Sekolah</th>
                                              <th>Mata Pelajaran</th>
                                              <th>Kehadiran</th>
                                              <th style="text-align:center"></th>
                                          </tr>
                                      </thead>
                                      <tfoot>
                                          <tr>
                                              <th>Username</th>
                                              <th>Nama Lengkap</th>
                                              <th>Asal Sekolah</th>
                                              <th>Mata Pelajaran</th>
                                              <th>Kehadiran</th>
                                              <th style="text-align:center"></th>
                                          </tr>
                                      </tfoot>
                                      <tbody>
                                        <?php foreach ($rs_participator->result() as $par) { ?>
                                        <tr>
                                          <td><img src="<?php echo base_url() . 'timthumb?src=' . base_url() . 'upload/people/' . $par->foto . '&h=40&w=40&zc=0'; ?>" class="img-circle" alt="" /> <?php echo $par->username?></td>
                                          <td><?php echo ucwords($par->fullname);?></td>
                                          <td><?php echo $par->origin_of_school;?></td>
                                          <td><?php echo $par->mapel;?></td>
                                          <td>
                                            <?php echo $par->kehadiran === 'ya' ? '<span class="label label-success">HADIR</span>' : '<span class="label label-danger">TIDAK HADIR</span>';?>
                                          </td>
                                          <td style="text-align:center">
                                            <input class="status" classroomid=<?php echo $classroom->id ;?> userid=<?php echo $par->id ;?> type="checkbox" <?php echo ($par->status_classroom_users === 'enable' ? 'checked' : '');?>  data-on="ON" data-off="OFF">
                                          </td>
                                        </tr>

                                        <?php } ?>
                                      </tbody>
                                  </table>
                                  <!-- // Data table -->
                              </div>
                          </div>
                          <div id="instructor" class="tab-pane">
                              <div class="panel panel-default">
                                  <!-- Data table -->
                                  <table id="instructor_table" class="table v-middle" cellspacing="0" width="100%" id="instructor-table">
                                      <thead>
                                          <tr>
                                              <th>Username</th>
                                              <th>Nama Lengkap</th>
                                              <th>Instansi</th>
                                              <th>Keahlian</th>
                                              <th style="text-align:center"></th>
                                          </tr>
                                      </thead>
                                      <tfoot>
                                          <tr>
                                              <th>Username</th>
                                              <th>Nama Lengkap</th>
                                              <th>Instansi</th>
                                              <th>Keahlian</th>
                                              <th style="text-align:center"></th>
                                          </tr>
                                      </tfoot>
                                      <tbody>
                                        <?php foreach ($rs_instructor->result() as $ins) { ?>
                                        <tr>
                                            <td><img src="<?php echo base_url() . 'timthumb?src=' . base_url() . 'upload/people/' . $ins->foto . '&h=40&w=40&zc=0'; ?>" class="img-circle" alt="" /> <?php echo $ins->username;?></td>
                                            <td><?php echo ucwords($ins->fullname);?></td>
                                            <td><?php echo $ins->institute;?></td>
                                            <td><?php echo $ins->expertise;?></td>
                                            <td style="text-align:center">
                                            <?php
                                            $rs = $this->Basecrud_m->get_where('classroom_users',array('user_id' => $ins->id,'classroom_id' => $classroom->id));
                                            $isInstructorExists = $rs->num_rows() > 0 ? true : false;

                                            if($isInstructorExists){
                                              echo '<input class="status" classroomid=' . $classroom->id . ' userid=' . $ins->id . ' type="checkbox" checked  data-on="ON" data-off="OFF">';
                                            }else{
                                              echo '<input class="status" classroomid=' . $classroom->id . ' userid=' . $ins->id . ' type="checkbox" data-on="ON" data-off="OFF">';
                                            }
                                            ?>

                                            </td>
                                        </tr>
                                        <?php } ?>
                                      </tbody>
                                  </table>
                                  <!-- // Data table -->
                              </div>
                          </div>
                          <?php } ?>


                          <div id="courses" class="tab-pane">
                              <div class="media v-middle s-container">
                                  <div class="media-body">
                                      <h5 class="text-subhead text-light"><?php echo $courses->num_rows()?> Pelajaran</h5>
                                  </div>
                                  <div class="media-right">
                                      <a class="btn btn-primary paper-shadow relative" href="<?php echo base_url() . 'classroom/course/add/' . $classroom->slug;?>">Tambah Pelajaran</a>
                                  </div>
                              </div>
                              <div class="nestable" id="nestable-handles-primary">
                                  <ul class="nestable-list">
                                      <?php
                                      $i = 0;
                                      foreach ($courses->result() as $course) {
                                      ?>
                                      <li class="nestable-item" data-id="1">
                                          <!-- <div class="nestable-handle"><i class="md md-menu"></i></div> -->
                                          <div class="nestable-content">
                                              <div class="media v-middle">
                                                  <div class="media-left">
                                                      <div class="icon-block half bg-blue-400 text-white">
                                                          <i class="fa fa-book"></i>
                                                      </div>
                                                  </div>
                                                  <div class="media-body">
                                                      <h4 class="text-title media-heading margin-none">
                                                          <a href="<?php echo base_url() . 'classroom/course/edit/' . $course->slug;?>" class="link-text-color"><?php echo ucwords($course->title);?></a>
                                                      </h4>
                                                      <div class="text-caption"><i class="fa fa-calendar fa-fw"></i><?php echo appDate(substr($course->start, 0, strpos($course->start, ' '))) . ' - ' . appDate(substr($course->end, 0, strpos($course->end, ' ')))?></div>
                                                  </div>

                                                  <div class="media-right">
                                                    <a href="<?php echo base_url() . 'classroom/course/edit/' . $course->slug;?>" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="fa fa-pencil fa-fw"></i></a>

                                                    <!-- <a href="<?php echo base_url() . 'classroom/course/edit/' . $course->slug;?>" style="padding-right:0px" class="btn paper-shadow relative"><span class="label label-success"><i class="fa fa-pencil fa-fw"></i> Edit</span></a> -->
                                                  </div>

                                                  <div class="media-right" style="padding-left:5px">
                                                    <a href="<?php echo base_url() . 'classroom/course/remove/' . $course->slug;?>" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus"><i class="fa fa-close fa-fw"></i></a>
                                                    <!-- <a href="<?php echo base_url() . 'classroom/course/edit/' . $course->slug;?>" style="padding-left:0px;padding-right:0px" class="btn paper-shadow relative"><span class="label label-danger"><i class="fa fa-fw fa-close"></i>Hapus</span></a> -->
                                                  </div>

                                              </div>
                                          </div>
                                      </li>
                                      <?php } ?>
                                  </ul>
                              </div>
                          </div>
                      </div>
                      <!-- // END Panes -->
                  </div>
                  <?php }else{ ?>

                  <div class="panel panel-default">
                      <div class="panel-body" style="padding:0px">
                        <ol class="breadcrumb">
                          <li><a href="<?php echo base_url() . 'classroom';?>">Kelas</a></li>
                          <?php if($this->uri->segment(4,0) !== 0){ ?>
                          <li><a href="<?php echo base_url() . 'classroom/index/details/' . $this->uri->segment(4);?>">Data Angkatan</a></li>
                          <?php } ?>
                          <!-- http://localhost/diklatonline/classroom/index/details/lokakarya-pemanfaatan-media-pembelajaran-berbasis-tik-untuk-kegiatan-belajar-mengajar-2015 -->
                          <li class="active"><?php echo $this->uri->segment(4,0) === 0 ? 'Tambah Kelas' : 'Tambah Angkatan'?> </li>
                        </ol>
                    </div>
                  </div>

                  <div class="tabbable paper-shadow relative" data-z="0.5">
                      <!-- Tabs -->
                      <ul class="nav nav-tabs">
                          <li class="active"><a href="#home" data-toggle="tab"><i class="fa fa-fw fa-home"></i> Kelas</a></li>
                      </ul>
                      <!-- // END Tabs -->
                      <!-- Panes -->
                      <div class="tab-content">
                          <div id="home" class="tab-pane active">
                             <?php if($this->session->userdata('msg_ok')){ ?>
                              <div class="alert alert-success" role="alert" id="alert-success" style="padding: 10px 2px 10px 2px;">
                                 <h4 style="text-align:center;margin-bottom: 0px;">Data Berhasil di Update</h4>
                              </div>
                              <script>
                                  $("#alert-success").fadeTo(5000, 500).slideUp(500, function() {
                                    $("#alert-success").hide();
                                  });
                              </script>
                             <?php } ?>

                              <?php if(isset($msg_error)){ ?>
                              <div class="alert alert-danger" role="alert" id="alert-danger" style="padding: 10px 2px 10px 2px;">
                                 <h4 style="text-align:center;margin-bottom: 0px;"><?php echo $msg_error;?></h4>
                              </div>
                              <script>
                                  $("#alert-danger").fadeTo(5000, 500).slideUp(500, function() {
                                    $("#alert-danger").hide();
                                  });
                              </script>
                              <?php } ?>

                              <form action="<?php echo $act?>" method="POST">
                                  <div class="form-group form-control-material">
                                      <input type="text" name="name" id="name" placeholder="Nama Kelas Atau Diklat" class="form-control used required" value="<?php echo $classroom_name?>" />
                                      <label for="title">Nama</label>
                                  </div>
                                  <div class="form-group form-control-material">
                                      <input type="text" name="generation" id="generation" placeholder="Angkatan (Misal : Angkatan Pertama)" class="form-control used" value="<?php echo $generation?>" />
                                      <label for="title">Angkatan</label>
                                  </div>
                                  <div class="form-group form-control-material">
                                      <input id="start" name="start" type="text" class="form-control used datepicker" value="<?php echo $start?>">
                                      <label for="start">Tanggal Mulai</label>
                                  </div>
                                  <div class="form-group form-control-material">
                                      <input id="end" name="end" type="text" class="form-control used datepicker" value="<?php echo $end?>">
                                      <label for="end">Tanggal Selesai</label>
                                  </div>

                                  <div class="form-group">
                                    <label for="registration_quota">Kuota Pendaftaran</label>
                                    <input id="registration_quota" data-verticalbuttons="true" type="text" value="<?php echo $registration_quota === '' ? 0 : $registration_quota?>" name="registration_quota" class="form-control used" />
                                  </div>

                                  <div class="form-group">
                                      <!-- <input type="text" name="detail_kegiatan" id="detail_kegiatan" placeholder="Detail Kegiatan" class="form-control used" value="<?php echo $detail_kegiatan?>" /> -->
                                      <label for="title">Detail Kegiatan</label>
                                      <textarea name="detail_kegiatan" id="detail_kegiatan"><?php echo $detail_kegiatan?></textarea>
                                  </div>

                                  <div class="form-group form-control-material">
                                      <input type="text" name="narasumber" id="narasumber" placeholder="Narasumber" class="form-control used" value="<?php echo $narasumber?>" />
                                      <label for="title">Narasumber</label>
                                  </div>

                                  <div class="form-group form-control-material">
                                      <input type="text" name="peserta" id="peserta" placeholder="Peserta" class="form-control used" value="<?php echo $peserta?>" />
                                      <label for="title">Peserta</label>
                                  </div>

                                  <div class="form-group form-control-material">
                                      <input type="text" name="tempat" id="tempat" placeholder="Tempat" class="form-control used" value="<?php echo $tempat?>" />
                                      <label for="title">Tempat</label>
                                  </div>

                                  <div class="form-group">
                                      <label for="preface">Kata Pengantar</label>
                                      <textarea name="preface" id="preface" cols="30" rows="10"><?php echo $preface;?></textarea>
                                  </div>
                                  <div class="text-right">
                                      <button type="submit" class="btn btn-primary">Simpan</button>
                                  </div>

                              </form>
                          </div>
                      </div>
                      <!-- // END Panes -->
                  </div>
                  <?php } ?>


                  <!-- // END Tabbable Widget -->
                  <br/>
                  <br/>
              </div>
              <div class="col-md-3">
                  <div class="panel panel-default" data-toggle="panel-collapse" data-open="true">
                     <div class="panel-heading panel-collapse-trigger">
                        <h4 class="panel-title">Menu</h4>
                     </div>
                     <div class="panel-body list-group">
                          <?php echo main_menu('classroom');?>
                     </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>

<script>

  $(document).ready(function() {
    $('#participator_table').DataTable( {
      "fnDrawCallback": function() {
            $('.status').bootstrapToggle();
        }
    });


    $('#instructor_table').DataTable( {
      "fnDrawCallback": function() {
            $('.status').bootstrapToggle();
        }
    });


  });


  $(".status").change(function() {
    $.post(base + 'classroom/participator/update_status', {
      classroom_id: $(this).attr('classroomid'),
      user_id: $(this).attr('userid'),
      status: $(this).prop('checked') == true ? 'enable':'disable'
    }).done(function(data) {

    });
  })

</script>
