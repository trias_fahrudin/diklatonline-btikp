<!DOCTYPE html>
<html lang="en">
   <head></head>
   <script src="<?php echo base_url()?>assets/front/js/jquery.min.js"></script>
   <style>
    tr > td
    {
      padding-bottom: 0.5em;
    }
   </style>
   <body style="margin:0px">
      <h1 style="color: #5e9ca0;"><strong>&nbsp;<img style="display: block; margin-left: auto; margin-right: auto; margin-top:0px" src="<?php echo base_url() . 'upload/header-print.png'?>" alt="" width="588" height="107" /></strong></h1>
      <hr style="margin-left:70px;margin-right:70px;border:solid 1px black;"/>
      <h2 style="text-align: center;"><strong>BIODATA</strong></h2>
      <!-- <p style="text-align: center;">&nbsp;</p> -->
      <blockquote>
        <p style="text-align: center;"><strong>PESERTA KEGIATAN PELATIHAN PENGEMBANGAN TIK DI PROVINSI JAMBI</strong></p>
        <p style="text-align: center;"><strong>PADA KEGIATAN PENGEMBANGAN TIK PENDIDIKAN</strong></p>
        <p style="text-align: center;"><strong>TAHUN ANGGARAN 2016</strong></p>
         <!-- <p style="text-align: center;">&nbsp;</p> -->
         <table style="height: 59px; margin-left: auto; margin-right: auto;" width="536">
            <tbody>
               <tr>
                  <td>Nama</td>
                  <td>:</td>
                  <td><?php echo ucwords($biodata->fullname)?></td>
               </tr>
               <tr>
                  <td>NIP</td>
                  <td>:</td>
                  <td><?php echo $biodata->nip?></td>
               </tr>
               <tr>
                  <td>Pangkat / Gol</td>
                  <td>:</td>
                  <td><?php echo $biodata->golongan?></td>
               </tr>
               <tr>
                  <td>Jabatan</td>
                  <td>:</td>
                  <td><?php echo ucwords($biodata->position)?></td>
               </tr>
               <tr>
                  <td>Jenis Kelamin</td>
                  <td>:</td>
                  <td><?php echo $biodata->gender === 'male' ? 'Laki-laki':'Perempuan'?></td>
               </tr>
               <tr>
                  <td>Tempat / Tanggal Lahir</td>
                  <td>:</td>
                  <td><?php echo $biodata->city_of_birth . ' / ' . tgl_panjang($biodata->date_of_birth,'lm') ?></td>
               </tr>
               <tr>
                  <td>Asal Sekolah</td>
                  <td>:</td>
                  <td><?php echo $biodata->origin_of_school ?></td>
               </tr>
               <tr>
                  <td>Alamat Kantor</td>
                  <td>:</td>
                  <td><?php echo $biodata->office_address ?></td>
               </tr>
               <tr>
                  <td>Alamat Rumah</td>
                  <td>:</td>
                  <td><?php echo $biodata->home_address ?></td>
               </tr>
               <tr>
                  <td>Telephon / HP</td>
                  <td>:</td>
                  <td><?php echo $biodata->telp_number ?></td>
               </tr>
               <tr>
                  <td>Email</td>
                  <td>:</td>
                  <td><?php echo $biodata->email ?></td>
               </tr>
               <tr>
                  <td>NPWP</td>
                  <td>:</td>
                  <td><?php echo $biodata->npwp ?></td>
               </tr>
            </tbody>
         </table>
         <!-- <p>&nbsp;</p> -->
         <table style="height: 59px; margin-left: auto; margin-right: auto;" width="536">
            <tbody>
               <tr>
                  <td style="text-align: left;padding-left: 350px;">Jambi ,&nbsp;&nbsp;&nbsp; <?php echo tgl_panjang($classroom->start,'lm')?></td>
               </tr>
               <tr>
                  <td style="text-align: left;padding-left: 350px;">Peserta, &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
               </tr>
            </tbody>
         </table>
         <table style="height: 59px; margin-left: auto; margin-right: auto;" width="536">
            <tbody>
               <tr>
                  <td><img border="2" src="<?php echo base_url() . 'upload/blanko-foto.png'?>" /></td>
                  <td><img border="2" src="<?php echo base_url() . 'upload/blanko-foto.png'?>" /></td>
                  <td style="text-align: center;">
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                    <p style="margin-bottom: 0px;"><?php echo ucwords($biodata->fullname)?></p>
                    <div style="margin-left:10px">
                       ............................................
                    </div>
                    <p style="margin-top: 0px;">NIP <?php echo $biodata->nip?></p>
                  </td>
               </tr>
            </tbody>
         </table>
      </blockquote>
   </body>
</html>
<script>
  window.print();
  window.onfocus=function(){ window.close();}
</script>
