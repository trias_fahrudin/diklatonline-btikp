<style>
  .vertical-alignment-helper {
      display:table;
      height: 100%;
      width: 100%;
      pointer-events:none; /* This makes sure that we can still click outside of the modal to close it */
  }
  .vertical-align-center {
      /* To center vertically */
      display: table-cell;
      vertical-align: middle;
      pointer-events:none;
  }
  .modal-content {
      /* Bootstrap sets the size of the modal in the modal-dialog class, we need to inherit it */
      width:inherit;
      height:inherit;
      /* To center horizontally */
      margin: 0 auto;
      pointer-events: all;
  }

</style>

<?php

  $mode = $this->uri->segment(3);

  if ($mode === 'edit' || $mode === 'edit_act') {

      $course_id = $course->id;
      $classroom_id = $classroom->id;

      $title = $mode === 'edit' ? $course->title : set_value('title');
      $description = $mode === 'edit' ? $course->description : set_value('description');
      $start = $mode === 'edit' ? appDate(substr($course->start, 0, strpos($course->start, ' '))) : set_value('start');
      $end = $mode === 'edit' ? appDate(substr($course->end, 0, strpos($course->end, ' '))) : set_value('end');

      $act = base_url() . 'classroom/course/edit_act/'.$slug;

  } else {

      $course_id = '';
      $classroom_id = $classroom->id;

      $title = $mode === 'add' ? '' : set_value('title');
      $description = $mode === 'add' ? '' : set_value('description');
      $start = $mode === 'add' ? '' : set_value('start');
      $end = $mode === 'add' ? '' : set_value('end');

      $act = base_url().'classroom/course/add_act/' . $classroom->slug;

  }
?>

<div class="main-container">
  <div class="parallax bg-white third page-section">
      <div class="parallax-layer container" data-opacity="true">
          <div class="media v-middle">
              <div class="media-left">
                  <span class="icon-block half bg-cyan-500 text-white"><i class="fa fa-edit"></i></span>
              </div>
              <div class="media-body">
                  <h1 class="text-display-1 margin-none"><?php echo ucwords($classroom->name) . ' (' . ucwords($classroom->generation) . ')'?></h1>
              </div>
              <div class="media-right">
                  <a class="btn btn-white" href="<?php echo base_url().'classroom'?>">Kelas</a>
              </div>
          </div>
      </div>
  </div>

  <div class="container">
      <div class="page-section">
          <div class="row">
            <div class="col-md-9">

                <div class="panel panel-default">
                    <div class="panel-body" style="padding:0px">
                      <ol class="breadcrumb">
                        <li><a href="<?php echo base_url().'classroom';?>">Kelas</a></li>
                        <li><a href="<?php echo base_url() . 'classroom/index/details/' .  getURLFriendly($classroom->name);?>">Data Angkatan</a></li>
                        <?php if($mode === 'edit' || $mode === 'edit_act'){ ?>
                        <li class="active">Edit <?php echo ucwords($title) ;?></li>
                        <?php }else{ ?>
                        <li class="active">Tambah Pelajaran</li>
                        <?php } ?>

                      </ol>
                  </div>
                </div>

                <!-- Tabbable Widget -->
                <div class="tabbable paper-shadow relative" data-z="0.5">
                    <!-- Tabs -->
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#course" data-toggle="tab"><i class="fa fa-fw fa-lock"></i> <span class="hidden-sm hidden-xs">Pelajaran</span></a></li>
                        <?php  if ($mode === 'edit' || $mode === 'edit_act') { ?>
                        <li><a href="#material_and_quiz" data-toggle="tab"><i class="fa fa-fw fa-credit-card"></i> <span class="hidden-sm hidden-xs">Materi</span></a></li>
                        <?php } ?>
                    </ul>
                    <!-- // END Tabs -->
                    <!-- Panes -->
                    <div class="tab-content">
                        <div id="course" class="tab-pane active">

                          <?php if($this->session->userdata('msg_ok')){ ?>
                           <div class="alert alert-success" role="alert" id="alert-success" style="padding: 10px 2px 10px 2px;">
                              <h4 style="text-align:center;margin-bottom: 0px;">Data Berhasil di Update</h4>
                           </div>
                           <script>
                               $("#alert-success").fadeTo(5000, 500).slideUp(500, function() {
                                 $("#alert-success").hide();
                               });
                           </script>
                          <?php } ?>

                           <?php if(isset($msg_error)){ ?>
                           <div class="alert alert-danger" role="alert" id="alert-danger" style="padding: 10px 2px 10px 2px;">
                              <h4 style="text-align:center;margin-bottom: 0px;"><?php echo $msg_error;?></h4>
                           </div>
                           <script>
                               $("#alert-danger").fadeTo(5000, 500).slideUp(500, function() {
                                 $("#alert-danger").hide();
                               });
                           </script>
                           <?php } ?>

                            <form action="<?php echo $act?>" method="POST">
                                <input type="hidden" value="<?php echo $course_id;?>" name="course_id">
                                <input type="hidden" value="<?php echo $classroom_id;?>" name="classroom_id">


                                <div class="form-group form-control-material static">
                                    <input type="text" name="title" id="title" placeholder="Course Title" class="form-control" value="<?php echo $title;?>" />
                                    <label for="title">Judul</label>
                                </div>
                                <div class="form-group form-control-material static">
                                    <input id="datepicker" name="start" type="text" class="form-control datepicker" value="<?php echo $start;?>">
                                    <label for="start">Tanggal Mulai</label>
                                </div>
                                <div class="form-group form-control-material static">
                                    <input id="datepicker" name="end" type="text" class="form-control datepicker" value="<?php echo $end;?>">
                                    <label for="end">Tanggal Selesai</label>
                                </div>
                                <div class="form-group">
                                    <label for="description">Deskripsi</label>
                                    <textarea name="description" id="description" cols="30" rows="10"><?php echo $description;?></textarea>
                                </div>
                                <div class="text-right">
                                    <a href="<?php echo base_url().'classroom/index/edit/'.$classroom->slug?>" class="navbar-btn btn btn-warning"><i class="fa fa-mail-reply"></i> Batal</a>
                                    <button type="submit" class="btn btn-primary">Simpan <i class="fa fa-chevron-circle-right"></i></button>
                                </div>
                            </form>

                        </div>
                        <?php  if ($mode === 'edit' || $mode === 'edit_act') { ?>
                        <div id="material_and_quiz" class="tab-pane">
                          <div class="media v-middle s-container">
                              <div class="media-body">
                                  <!-- <h5 class="text-subhead text-light">3 Lessons</h5> -->
                              </div>
                              <div class="media-right">

                              </div>
                              <div class="media-right">
                                <a href="" data-toggle="modal" data-target="#myModalMateri" class="btn btn-primary paper-shadow relative">Tambah Materi</a>

                              </div>
                          </div>
                          <div class="nestable" id="nestable-handles-primary" posturl="<?php echo base_url() . 'classroom/course/rearrange_material_list'?>" slug="<?php echo $this->uri->segment(4) ?>">
                            <?php echo $materi;?>
                          </div>
                        </div>
                        <?php } ?>
                    </div>
                    <!-- // END Panes -->
                </div>
                <!-- // END Tabbable Widget -->
                <br/>
                <br/>
              </div>

              <!-- Modal -->
              <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                 <div class="modal-dialog" role="document">
                    <div class="modal-content">
                       <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                          <h4 class="modal-title" id="myModalLabel">Details</h4>
                       </div>
                       <div class="modal-body" id="show-result-here">

                       </div>
                       <div class="modal-footer">
                         <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                       </div>
                    </div>
                 </div>
              </div>


              <!-- Modal Form tugas-->
              <div id="myModalTaskForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                 <div class="modal-dialog" role="document">
                    <div class="modal-content">
                       <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                          <h4 class="modal-title" id="myModalLabel">Data Tugas</h4>
                       </div>
                       <div class="modal-body" id="modal-body-task-form">
                         <form enctype="multipart/form-data" onsubmit="return submitFormTask();" method="POST" class="form-horizontal" role="form" id="formtask">
                           <input type="hidden" name="task_course_id" id="task_course_id" value="" />
                           <input type="hidden" name="task_material_id" id="task_material_id" value=""/>
                           <div class="form-group">
                               <label for="note">Penjelasan</label>
                               <textarea name="note" id="note" cols="30" rows="30"></textarea>
                           </div>

                           <div class="form-group" id="div_file_task_lama">
                             <label for="end">File Lama : <a id="file_task_lama" href="">Download</a></label>
                           </div>

                           <div class="form-group ">
                             <label for="end">File (*.pdf;*.doc;*.docx;ppt;*.rar;*.zip)</label>
                             <input type="file" name="file_task" id="file_task">
                           </div>

                           <div class="form-group">
                               <label for="due_date">Tanggal Akhir Pengumpulan</label>
                               <input style="width:30%" id="due_date" name="due_date" type="text" class="form-control used datepicker" value="">

                           </div>


                           <div class="text-right">
                               <button type="submit" class="btn btn-primary">Simpan</button>
                           </div>

                         </form>
                       </div>
                       <!-- <div class="modal-footer">
                         <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                       </div> -->
                    </div>
                 </div>
              </div>

              <?php  if ($mode === 'edit' || $mode === 'edit_act') { ?>

              <!-- Modal tambah materi -->
              <div id="myModalMateri" course-id="<?php echo $course_id?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                 <div class="modal-dialog" role="document">
                    <div class="modal-content">
                       <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-fw fa-close"></i></button>
                          <h4 class="modal-title" id="myModalLabel">Daftar Materi</h4>
                       </div>
                       <div class="modal-body" id="show-result-here">
                         <!-- <div class="panel panel-default"> -->

                             <!-- Data table -->
                             <table id="material_table" class="table display select dataTable" cellspacing="0" width="100%" id="data-add">
                                 <thead>
                                     <tr>
                                         <th></th>
                                         <th>Kode</th>
                                         <th>Judul</th>
                                         <th>Kategori</th>
                                         <th>Konten</th>
                                         <!-- <th>File</th> -->
                                         <!-- <th>Aksi</th> -->
                                     </tr>
                                 </thead>
                                 <tfoot>
                                     <tr>
                                       <th></th>
                                       <th>Kode</th>
                                       <th>Judul</th>
                                       <th>Kategori</th>
                                       <th>Konten</th>
                                       <!-- <th>File</th> -->
                                       <!-- <th>Aksi</th> -->
                                     </tr>
                                 </tfoot>
                                 <tbody>

                                   <?php foreach ($materials->result() as $material) { ?>
                                     <tr>
                                       <td>
                                         <input class="material_item" type="checkbox"  <?php echo in_array($material->id,$active_materials) ? 'checked':'';?>  value="<?php echo $material->id;?>"  data-on="ON" data-off="OFF">
                                       </td>
                                       <td><?php echo str_pad($material->id,5,'0',STR_PAD_LEFT);?></td>
                                       <td><?php echo ucwords($material->title);?></td>
                                       <td><?php echo ucwords($material->categori);?></td>
                                       <td><?php echo substr(strip_tags($material->content),0,30);?> ... </td>
                                       <!-- <td><?php echo $material->file_attachment;?></td> -->
                                       <!-- <td><a href="#" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="Detail"><i class="fa fa-search-plus"></i></a></td> -->
                                     </tr>

                                   <?php } ?>


                                 </tbody>
                             </table>
                             <!-- // Data table -->
                             <!-- <a href="" class="navbar-btn btn btn-primary pull-right">Simpan</a> -->
                         <!-- </div> -->
                       </div>
                       <!-- <div class="modal-footer">
                         <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                       </div> -->
                    </div>
                 </div>
              </div>
              <?php } ?>

              <div class="col-md-3">
                  <div class="panel panel-default" data-toggle="panel-collapse" data-open="true">
                     <div class="panel-heading panel-collapse-trigger">
                        <h4 class="panel-title">Menu</h4>
                     </div>
                     <div class="panel-body list-group">
                          <?php echo main_menu('classroom');?>
                     </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>

<script>
$(document).ready(function() {
  $('#material_table').DataTable( {
      "fnDrawCallback": function() {
          $('.material_item').bootstrapToggle();
      }
   } );
});
</script>


<script>

  $('.nestable').on('change', function() {
    var nestable = $('.nestable').nestable('serialize');
    var slug = $('#nestable-handles-primary').attr('slug');
    var post_url = $('#nestable-handles-primary').attr('posturl');

    var array_id = [];
    for (var i = 0, l = nestable.length; i < l; i++) {
      array_id.push(nestable[i].id);
    }

    $.post(post_url, {
      list: array_id.toString(),
      slug: slug
    });
  });


  $('#myModalMateri').on('hidden.bs.modal', function () {

    //get all checked, then send them to course material_list
    //http://stackoverflow.com/questions/18684703/how-to-get-check-box-input-value-from-datatable-row-collection
    var oTable = $('#material_table').dataTable();
    var checkedVals =  oTable.$(".material_item:checked", {"page": "all"}).map(function(){
      return this.value;
    }).get();


    var material_list = checkedVals.join(",");    
    var course_id = $(this).attr('course-id');

    $.post(base + 'classroom/course/edit/', {
      course_id: course_id,
      material_list: material_list
    }).done(function(data) {
      location.reload();
    });


  })

  $('#myModalTaskForm').on('hidden.bs.modal', function () {
    $('#myModalTaskForm').find('form')[0].reset();
  })


    function submitFormTask() {
      tinyMCE.triggerSave();
      //  console.log("submit event");
       var fd = new FormData(document.getElementById("formtask"));
      //  fd.append("label", "WEBUPLOAD");
       $.ajax({
         url: base + "classroom/course/task_save",
         type: "POST",
         data: fd,
         enctype: 'multipart/form-data',
         processData: false,  // tell jQuery not to process the data
         contentType: false   // tell jQuery not to set contentType
       }).done(function( data ) {
          //console.log("PHP Output:");
          //console.log( data );
          if(data === 'OK'){
            $('#myModalTaskForm').modal('hide');
          }else{
            alert(data);
          }

       });
       return false;
   }

  $('.modal-task-form').click(function(){

    var course_id = $(this).attr('course-id');
    var material_id = $(this).attr('material-id');

    $.get(base + 'classroom/course/edit_task',
      { course_id: course_id ,
        material_id:material_id }
      )
     .done(function( data ) {
       $('#task_course_id').val(course_id);
       $('#task_material_id').val(material_id);

       if(data.status !== 'NO-DATA'){
         $('#due_date').val(data.due_date);
         $("#file_task_lama").attr("href", data.file);
         tinyMCE.get('note').setContent(data.note);
         $('#div_file_task_lama').show();
       }else{
         $('#div_file_task_lama').hide();
       }

       $('#myModalTaskForm').modal('show');
    });
  });

  $('.instruktor_list').on('change', function (e) {
      // var optionSelected = $("option:selected", this);
      // var valueSelected = this.value;
      var instructor_id = $(this).val();
      var course_id = $(this).attr('courseid');
      var material_id = $(this).attr('materialid');

      $.post( base + 'classroom/course/change_course_instructor',
              { instructor_id : instructor_id,
                course_id : course_id,
                material_id : material_id
               }
      ).done(function( data ) {

      });

      // alert(instructor_id);
  });

</script>
