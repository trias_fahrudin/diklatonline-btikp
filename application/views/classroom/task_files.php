<div class="main-container">
  <div class="parallax overflow-hidden page-section bg-purple-300 page-section third">
      <div class="parallax-layer container" data-opacity="true">
          <div class="media v-middle">
              <div class="media-left">
                  <span class="icon-block s60 bg-lightred"><i class="fa fa-user"></i></span>
              </div>
              <div class="media-body">
                  <h1 class="text-display-1 text-white margin-none"><?php echo $classroom->name . ' (' . $classroom->generation . ')' ?></h1>
              </div>
              <div class="media-right">

              </div>
          </div>
      </div>
  </div>

  <div class="container">
    <div class="page-section">
        <div class="row">
            <div class="col-md-9">

              <div class="panel panel-default">
                <div class="panel-body" style="padding:0px">
                  <ol class="breadcrumb">
                    <li><a href="<?php echo base_url() . 'classroom';?>">Kelas</a></li>
                    <!-- http://localhost/diklatonline/classroom/index/details/lokakarya-pemanfaatan-media-pembelajaran-berbasis-tik-untuk-kegiatan-belajar-mengajar-2015 -->
                    <li><a href="<?php echo base_url() . 'classroom/index/details/' . $classroom->slug ;?>">Data Angkatan</a></li>
                    <!-- <li><a href="<?php echo base_url() . 'report/index/' . $classroom->slug;?>">Laporan</a></li> -->
                    <li class="active">File Tugas</li>
                  </ol>
                </div>
              </div>

                <div class="panel panel-default">

                    <table data-toggle="data-table" class="table v-middle" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Kode File</th>
                                <th>Materi</th>
                                <th>Nama Peserta</th>
                                <th>File</th>
                                <th>Poin</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                              <th>Kode File</th>
                              <th>Materi</th>
                              <th>Nama Peserta</th>
                              <th>File</th>
                              <th>Poin</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <!-- $this->db->select('c.title as course_title,d.title as materi_title,e.fullname as user_name,a.upload_time,a.file,a.poin');-->
                            <?php foreach ($task_files->result() as $task) { ?>
                            <?php if($task->checked === 'n'){ ?>
                            <tr id="tr-task-<?php echo $task->task_id;?>" style="color:white;background-color:#4caf50">
                            <?php }else{ ?>
                            <tr>
                            <?php } ?>
                                <td><?php echo str_pad($task->task_id,5,'0',STR_PAD_LEFT)?></td>

                                <td>
                                  <?php echo ucwords($task->course_title)?><br/>
                                  <?php echo ucwords($task->materi_title);?>
                                </td>
                                <td>
                                  <?php echo ucwords($task->user_name);?><br/>
                                  <i class="fa fa-clock-o"></i> <?php echo appDate($task->upload_time,true);?>
                                </td>

                                <td><a style="color:black" href="<?php echo base_url() . 'upload/task_files/' . $task->file;?>">[Download]</a></td>
                                <td><input style="width: 76px;" class="form-control task" type="text" taskfileid="<?php echo $task->task_id?>" value="<?php echo $task->poin;?>"></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-3">
                <div class="panel panel-default" data-toggle="panel-collapse" data-open="true">
                   <div class="panel-heading panel-collapse-trigger">
                      <h4 class="panel-title">Menu</h4>
                   </div>
                   <div class="panel-body list-group">
                        <?php echo main_menu('data');?>
                   </div>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>

<script>
  $('.task').keyup(function(e) {
    var taskfileid = $(this).attr('taskfileid');
    var poin = $(this).val();

    if (e.which !== 8 && e.which !== 0 && (e.which < 48 || e.which > 57)) {
      e.preventDefault();
    }else{
      $.post(base + 'classroom/task_files_poin',{
        task_file_id : taskfileid,
        poin : poin
      }).done(function(data) {
        $('#tr-task-' + taskfileid).removeAttr('style');
      });
    }
  });
</script>
