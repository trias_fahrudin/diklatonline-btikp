<head>
	<title>Selamat bergabung di <?php echo $this->config->item('diklat_title')?></title>
</head>
<body>
<span class="normaltext">
<p>Selamat bergabung di <?php echo $this->config->item('diklat_title')?>,
   Anda menggunakan alamat email ini sebagai akun di <?php echo base_url()?>.
   Akun anda telah dibuat, namun anda harus melakukan verifikasi akun anda dengan klik <a title="aktivasi akun" href="<?php echo base_url();?>frontpage/activate/<?php echo $email_detail->activation_code . '/' . $classroom_slug; ?>">disini</a><br/>

</p>

<p>Berikut ini adalah detail dari akun anda. Mohon untuk menyimpan detail ini.</p>


<table id="table-normal">
  <tbody>
    <tr>
      <td>User Name:</td>
      <td><?php echo $email_detail->username; ?></td>
    </tr>
    <tr>
      <td>Password:</td>
      <td>*** Sama dengan username anda *** </td>
    </tr>
  </tbody>
</table>
<p>***Anda dapat merubah password anda setelah mengaktifkan akun anda dan login pada system</p>

<p>Salam,<br /> <a title="<?php echo base_url();?>" href="<?php echo base_url();?>"><?php echo base_url();?></a></p>
</span></p>
</body>
