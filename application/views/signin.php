<!DOCTYPE html>
<html class="transition-navbar-scroll top-navbar-xlarge bottom-footer" lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Learning</title>
  <link href="<?php echo base_url() . 'assets/back/';?>css/vendor.min.css" rel="stylesheet">
  <link href="<?php echo base_url() . 'assets/back/';?>css/theme-core.min.css" rel="stylesheet">
  <link href="<?php echo base_url() . 'assets/back/';?>css/module-essentials.min.css" rel="stylesheet" />
  <link href="<?php echo base_url() . 'assets/back/';?>css/module-material.css" rel="stylesheet" />
  <link href="<?php echo base_url() . 'assets/back/';?>css/module-layout.min.css" rel="stylesheet" />
  <!-- <link href="<?php echo base_url() . 'assets/back/';?>css/module-sidebar.min.css" rel="stylesheet" /> -->
  <!-- <link href="<?php echo base_url() . 'assets/back/';?>css/module-sidebar-skins.min.css" rel="stylesheet" /> -->
  <link href="<?php echo base_url() . 'assets/back/';?>css/module-navbar.css" rel="stylesheet" />
  <!-- <link href="<?php echo base_url() . 'assets/back/';?>css/module-messages.min.css" rel="stylesheet" /> -->
  <link href="<?php echo base_url() . 'assets/back/';?>css/module-carousel-slick.min.css" rel="stylesheet" />
  <!-- <link href="<?php echo base_url() . 'assets/back/';?>css/module-charts.min.css" rel="stylesheet" /> -->
  <!-- <link href="<?php echo base_url() . 'assets/back/';?>css/module-maps.min.css" rel="stylesheet" /> -->
  <!-- <link href="<?php echo base_url() . 'assets/back/';?>css/module-colors-alerts.min.css" rel="stylesheet" /> -->
  <!-- <link href="<?php echo base_url() . 'assets/back/';?>css/module-colors-background.min.css" rel="stylesheet" /> -->
  <link href="<?php echo base_url() . 'assets/back/';?>css/module-colors-buttons.min.css" rel="stylesheet" />
  <link href="<?php echo base_url() . 'assets/back/';?>css/module-colors-text.min.css" rel="stylesheet" />
  <style>
    /* Easy Slider */

    #slider ul,
    #slider li {
      margin: 0;
      padding: 0;
      list-style: none;
    }

    #slider li {
      /*
    define width and height of list item (slide)
    entire slider area will adjust according to the parameters provided here
  */
      width: 800px;
      height: 250px;
      overflow: hidden;
    }
    /* numeric controls */

    ol#controls {
      margin: 1em 0;
      padding: 0;
      height: 28px;
    }

    ol#controls li {
      margin: 0 10px 0 0;
      padding: 0;
      float: left;
      list-style: none;
      height: 28px;
      line-height: 28px;
    }

    ol#controls li a {
      float: left;
      height: 28px;
      line-height: 28px;
      border: 1px solid #ccc;
      background: #DAF3F8;
      color: #555;
      padding: 0 10px;
      text-decoration: none;
    }

    ol#controls li.current a {
      background: #5DC9E1;
      color: #fff;
    }

    ol#controls li a:focus,
    #prevBtn a:focus,
    #nextBtn a:focus {
      outline: none;
    }
    /* // Easy Slider */
  </style>
</head>

<body>
  <?php $login_header = $this->Basecrud_m->get_where('settings',array('name' => 'login_header'))->row()?>
  <div id="content">
    <div class="container">
      <!-- Fixed navbar -->
      <div style="background-color:#42a5f5" class="navbar navbar-default navbar-fixed-top navbar-size-large navbar-size-xlarge paper-shadow" data-z="0" data-animated role="navigation">
        <div class="container">
          <div class="navbar-header">
            <div class="navbar-brand">
              <img style="margin-bottom: 10px" src="<?php echo base_url(); ?>upload/<?php echo $login_header->value;?>" width="800" height="77">
              <!-- <img src="<?php echo base_url() . 'assets/front/header.png'?>" width="800px"> -->
            </div>
          </div>
          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="main-nav">
            <ul class="nav navbar-nav navbar-nav-margin-left">

            </ul>
            <div class="navbar-right">
              <ul class="nav navbar-nav navbar-nav-margin-right">
                <!-- user -->
                <li style="background-color: #fff;"><a href="<?php echo base_url()?>">Home</a></li>
                <li><a href="<?php echo base_url() . 'rekap'?>">Rekap Data</a></li>
                <li><a href="<?php echo base_url() . 'about'?>">Tentang</a></li>
                <!-- // END user -->
              </ul>

            </div>
          </div>
          <!-- /.navbar-collapse -->
        </div>
      </div>


      <div class="row" style="padding-top:40px">

        <div class="col-xs-12 col-md-9">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title" style="text-align:right">Balai Teknologi Informasi Dan Komunikasi Pendidikan</h3>
            </div>
            <div class="panel-body">
              <div id="slider">
                <ul>
                  <li>
                    <a href="http://templatica.com/preview/30">
                      <img src="<?php echo base_url() . 'assets/front/sliderimg/01.jpg'?>" alt="Css Template Preview" />
                    </a>
                  </li>
                  <li>
                    <a href="http://templatica.com/preview/7">
                      <img src="<?php echo base_url() . 'assets/front/sliderimg/02.jpg'?>" alt="Css Template Preview" />
                    </a>
                  </li>
                  <li>
                    <a href="http://templatica.com/preview/25">
                      <img src="<?php echo base_url() . 'assets/front/sliderimg/03.jpg'?>" alt="Css Template Preview" />
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xs-6 col-md-3">
          <div class="lock-container">
            <div class="panel panel-default text-center paper-shadow" data-z="0.5">
              <h1 class="text-display-1 text-center margin-bottom-none"></h1>
              <!-- <img src="<?php echo base_url() . 'assets/back/images/logo.png'?>"> -->
              <div class="panel-body">

                <?php if( $this->uri->segment(3) == TRUE){ ?>
                  <div class="alert alert-success" role="alert" id="alert-signin-success">
                    Akun telah diaktifkan
                    <br/> Silahkan masuk dengan username dan password anda
                  </div>
                  <?php } ?>

                    <div class="form-group">
                      <div class="form-control-material static required">
                        <input class="form-control" id="username" type="text" placeholder="Masukkan Username">
                        <!-- <label for="username">Username</label> -->
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="form-control-material static required">
                        <input class="form-control" id="password" type="password" placeholder="Masukkan Password">
                        <!-- <label for="password">Password</label> -->
                      </div>
                    </div>
                    <a href="#" class="btn btn-primary" id="submit"><i class="fa fa-fw fa-unlock-alt"></i> Masuk </a>
                    <br/>
                    <a href="#" class="forgot-password">Lupa password ?</a>
                    <br/>
                    <a href="<?php echo base_url() . 'signup'?>" class="link-text-color">Buat akun</a>
              </div>
            </div>
          </div>

        </div>
      </div>

    </div>
  </div>
  <!-- Footer -->
  <!--  <footer class="footer">
        <strong>Learning</strong> v1.0.0 &copy; Copyright 2015
    </footer> -->
  <!-- // Footer -->
  <!-- Inline Script for colors and config objects; used by various external scripts; -->
  <script>
    var colors = {
      "danger-color": "#e74c3c",
      "success-color": "#81b53e",
      "warning-color": "#f0ad4e",
      "inverse-color": "#2c3e50",
      "info-color": "#2d7cb5",
      "default-color": "#6e7882",
      "default-light-color": "#cfd9db",
      "purple-color": "#9D8AC7",
      "mustard-color": "#d4d171",
      "lightred-color": "#e15258",
      "body-bg": "#f6f6f6"
    };
    var config = {
      theme: "html",
      skins:
      {
        "default":
        {
          "primary-color": "#42a5f5"
        }
      }
    };
  </script>
  <script src="<?php echo base_url() . 'assets/back/js/plugins/jquery-2.1.4.min.js'?>" type="text/javascript"></script>

  <script type="text/javascript">
    var base = "<?php echo base_url()?>";
  </script>
  <!-- User Script -->
  <script src="<?php echo base_url() . 'assets/back/js-user/essential.js'?>" type="text/javascript"></script>
  <script src="<?php echo base_url() . 'assets/back/js-user/signin.js'?>" type="text/javascript"></script>
  <!-- Separate Vendor Script Bundles -->
  <script src="<?php echo base_url() . 'assets/back/';?>js/vendor-core.min.js"></script>
  <!-- <script src="<?php echo base_url() . 'assets/back/';?>js/vendor-countdown.min.js"></script> -->
  <!-- <script src="<?php echo base_url() . 'assets/back/';?>js/vendor-tables.min.js"></script> -->
  <script src="<?php echo base_url() . 'assets/back/';?>js/vendor-forms.min.js"></script>
  <!-- <script src="<?php echo base_url() . 'assets/back/';?>js/vendor-carousel-slick.min.js"></script> -->
  <!-- <script src="<?php echo base_url() . 'assets/back/';?>js/vendor-player.min.js"></script> -->
  <!-- <script src="<?php echo base_url() . 'assets/back/';?>js/vendor-charts-flot.min.js"></script> -->
  <!-- <script src="<?php echo base_url() . 'assets/back/';?>js/vendor-nestable.min.js"></script> -->
  <!-- <script src="js/vendor-angular.min.js"></script> -->

  <script src="<?php echo base_url() . 'assets/back/';?>js/module-essentials.min.js"></script>
  <script src="<?php echo base_url() . 'assets/back/';?>js/module-material.min.js"></script>
  <script src="<?php echo base_url() . 'assets/back/';?>js/module-layout.min.js"></script>
  <!-- <script src="<?php echo base_url() . 'assets/back/';?>js/module-sidebar.min.js"></script> -->
  <!-- <script src="<?php echo base_url() . 'assets/back/';?>js/module-carousel-slick.min.js"></script> -->
  <!-- <script src="<?php echo base_url() . 'assets/back/';?>js/module-player.min.js"></script> -->
  <!-- <script src="<?php echo base_url() . 'assets/back/';?>js/module-messages.min.js"></script> -->
  <!-- <script src="<?php echo base_url() . 'assets/back/';?>js/module-maps-google.js"></script> -->
  <!-- <script src="<?php echo base_url() . 'assets/back/';?>js/module-charts-flot.min.js"></script> -->

  <script src="<?php echo base_url() . 'assets/back/';?>js/theme-core.min.js"></script>
  <script src="<?php echo base_url() . 'assets/front/';?>easyslider/js/easySlider1.7.js"></script>
  <script type="text/javascript">
    $(document).ready(function()
    {
      $("#slider").easySlider(
      {
        auto: true,
        continuous: true,
        numeric: true
      });
    });
  </script>
</body>

</html>
