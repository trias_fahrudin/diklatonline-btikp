<style type="text/css">
	.table tbody tr.warning td{
		background-color: #FFFF99;
	}
</style>

<div class="main-container">
  <div class="parallax bg-white page-section third">
     <div class="parallax-layer container" data-opacity="true">
        <div class="media v-middle">
           <div class="media-left">
              <span class="icon-block s60 bg-lightred"><i class="fa fa-newspaper-o"></i></span>
           </div>
           <div class="media-body">
              <h1 class="text-display-1 margin-none">Ide & Saran pengunjung</h1>
           </div>
           <div class="media-right">
           </div>
        </div>
     </div>
  </div>

	<div class="container">
		<div class="page-section">
				<div class="row">
					<div class="col-md-9">

						<div class="panel panel-default">
								<?php echo $this->session->flashdata('k');?>

								<table id="example" class="table v-middle display nowrap" cellspacing="0" width="100%">
										<thead>
											<tr>
												<!-- <th></th> -->
												<th>Nama</th>
												<!-- <th>Email</th> -->
												<th>Tanggapan</th>
												<th>Topik</th>
												<th style="text-align:center;width:20%">Control</th>
											</tr>
										</thead>

										<tbody>
											<?php foreach ($data->result() as $b):?>

												<tr <?php echo $b->tampil === 'N' ? 'class="warning"' : ''?>>
								 		      <!-- <td><?php echo $b->inserted_at?></td> -->
								 		      <td>
														<?php echo ucfirst($b->nama)?><br/>
														<?php echo $b->email?>
													</td>
								 		      <!-- <td></td> -->
								 		      <td style="text-align: left">
								 		      	<?php echo $b->jml.' Kali'; ?>
								 		      </td>
								 		      <td><?php echo substr($b->topik, 0, 45).'...'?></td>
								 		      <td style="text-align: center" id="row_<?php echo $b->id;  ?>">
								 		      	<?php if ($b->tampil === 'Y') {  ?>
								 		      	<a href="#" class="change_tampil" id="<?php echo $b->id; ?>" tampil="N"><span class="glyphicon glyphicon-eye-close"></span></a>
								 		      	<?php } else {  ?>
								 		      	<a href="#" class="change_tampil" id="<?php echo $b->id; ?>" tampil="Y"><span class="glyphicon glyphicon-eye-open"></span></a>
								 		      	<?php } ?>
								 		        <a href="<?php echo base_URL();?>ide_saran/manage/reply/<?php echo $b->id?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
								 			    	<a href="<?php echo base_URL();?>ide_saran/manage/del/<?php echo $b->id ?>" onclick="return confirm('Anda YAKIN menghapus data ini ?');"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true" style="color:red"></span></a>
								 		      </td>
								 		   </tr>
								 		   <?php endforeach; ?>

										</tbody>
								</table>
							</div>
					</div>
					<div class="col-md-3">
							<div class="panel panel-default" data-toggle="panel-collapse" data-open="true">
								 <div class="panel-heading panel-collapse-trigger">
										<h4 class="panel-title">Menu</h4>
								 </div>
								 <div class="panel-body list-group">
											<?php echo main_menu('ide_saran');?>
								 </div>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script>
	$(document).ready(function() {
	  $('#example').DataTable( { } );
	});
	</script>


  	<script type="text/javascript">
	  	$('.change_tampil').click(function() {
	  		var id_ide = $(this).attr('id');
	  		var val_tampil = $(this).attr('tampil');

	  		$.post("<?php echo base_url().'ide_saran/manage/set_tampil';?>",
	        			{ id: id_ide ,
	        			  tampil : val_tampil }
	        	).done(function(data){

							location.reload();
	        });

	  	});
  	</script>
