<style>
.thumbnails {
	margin-left: -20px;
	list-style: none;
	*zoom: 1;
}

.thumbnails:before,
.thumbnails:after {
	display: table;
	line-height: 0;
	content: "";
}

.thumbnails:after {
	clear: both;
}

.row-fluid .thumbnails {
	margin-left: 0;
}

.thumbnails > li {
	float: left;
	margin-bottom: 20px;
	margin-left: 20px;
}

.thumbnail {
	display: block;
	padding: 4px;
	line-height: 20px;
	border: 1px solid #ddd;
	-webkit-border-radius: 4px;
		 -moz-border-radius: 4px;
					border-radius: 4px;
	-webkit-box-shadow: 0 1px 3px rgba(0, 0, 0, 0.055);
		 -moz-box-shadow: 0 1px 3px rgba(0, 0, 0, 0.055);
					box-shadow: 0 1px 3px rgba(0, 0, 0, 0.055);
	-webkit-transition: all 0.2s ease-in-out;
		 -moz-transition: all 0.2s ease-in-out;
			 -o-transition: all 0.2s ease-in-out;
					transition: all 0.2s ease-in-out;
}

a.thumbnail:hover {
	border-color: #0088cc;
	-webkit-box-shadow: 0 1px 4px rgba(0, 105, 214, 0.25);
		 -moz-box-shadow: 0 1px 4px rgba(0, 105, 214, 0.25);
					box-shadow: 0 1px 4px rgba(0, 105, 214, 0.25);
}

.thumbnail > img {
	display: block;
	min-width: 100%;
	margin-right: auto;
	margin-left: auto;
}

.thumbnail .caption {
	padding: 9px;
	color: #555555;
}
</style>
<div class="main-container">
  <div class="parallax bg-white page-section third">
     <div class="parallax-layer container" data-opacity="true">
        <div class="media v-middle">
           <div class="media-left">
              <span class="icon-block s60 bg-lightred"><i class="fa fa-picture-o"></i></span>
           </div>
           <div class="media-body">
              <h1 class="text-display-1 margin-none">Galeri</h1>
           </div>
           <div class="media-right">
           </div>
        </div>
     </div>
  </div>

	<div class="container">
		<div class="page-section">
				<div class="row">
					<div class="col-md-9">
						<div class="tabbable paper-shadow relative" data-z="0.5">
							 <!-- Tabs -->
							 <ul class="nav nav-tabs">
									<li class="active"><a href="#"><i class="fa fa-fw fa-lock"></i> <span class="hidden-sm hidden-xs">Daftar Album</span></a></li>
							 </ul>
							 <!-- // END Tabs -->
							 <!-- Panes -->
							 <div class="tab-content">
									<div id="account" class="tab-pane active">
										<?php echo $this->session->flashdata('k')?>
										<form action="<?php echo base_URL()?>galeri/manage/add_album" method="post" class="form-horizontal">

											<div class="form-group">
												 <label for="nama_album" class="col-md-2 control-label">Tambah Album</label>
												 <div class="col-md-6">
														<div class="form-control-material" style="padding-top: 5px;height: 31px;">
															 <input type="text" name="nama_album" required class="form-control" id="nama_album" placeholder="Nama album">
														</div>
												 </div>
												 <div class="col-md-3">
													 <input type="submit" class="btn btn-primary paper-shadow relative" data-z="0.5" value="Simpan" data-hover-z="1" data-animated>
												 </div>
											</div>
										</form>

										<ul class="thumbnails" style="padding: 10px;margin:10px">
											<?php
												if (empty($data)) {
														echo '<div class="alert alert-danger">Data album tidak ditemukan</div>';
												} else {
														foreach ($data as $d) {
											?>
											<li class="col-md-4" style="margin-left: 0px; margin-right: 9px">
												<div class="thumbnail" style="height: 250px">
													<?php
																$sampul = get_value('galeri', 'id_album', $d->id);
																	if (empty($sampul)) {
																			$gambar = 'galeri/____no_foto.jpg';
																	} else {
																			$gambar = 'galeri/'.$sampul->file;
																	}
													?>
													<img src="<?php echo base_URL()?>timthumb?src=/upload/<?php echo $gambar.'&w=334&h=190&zc=0'?>" style="width: 334px; height: 190px" alt="<?php echo $d->nama?>">
													<div class="caption">
														<h6 style="text-align: center; margin-top: 0"><?php echo $d->nama?><br>
														<a href="<?php echo base_URL()?>galeri/manage/del_album/<?php echo $d->id?>" onclick="return confirm('Anda Yakin ..?'); ">Hapus Album</a> |
														<a href="<?php echo base_URL()?>galeri/manage/atur/<?php echo $d->id?>">Manage</a></h6>

													</div>
												</div>
											</li>

											<?php
														}
												}
											?>

										</ul>
									</div>
							 </div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="panel panel-default" data-toggle="panel-collapse" data-open="true">
							 <div class="panel-heading panel-collapse-trigger">
									<h4 class="panel-title">Menu</h4>
							 </div>
							 <div class="panel-body list-group">
										<?php echo main_menu('galeri');?>
							 </div>
						</div>
				</div>
			</div>
		</div>
	</div>
</div>
