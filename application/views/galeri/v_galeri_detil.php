<?php
	$mode    = $this->uri->segment(3);
	//$idu     = $this->uri->segment(4);
	//$id_foto = $this->uri->segment(5);

	if($mode === 'edit_foto'){
		$judul       = $data->judul;
		$ket   		 = $data->ket;
		$slideshow   = $data->slideshow;
		$act         = 'edit_foto/' . $data->id_album . '/' .$data->id;
	}else{
		$judul       = "";
		$ket   		 = "";
		$slideshow   = "N";
		$act         = "upload_foto";
	}
	
?>

<style>
.thumbnails {
	margin-left: -20px;
	list-style: none;
	*zoom: 1;
}

.thumbnails:before,
.thumbnails:after {
	display: table;
	line-height: 0;
	content: "";
}

.thumbnails:after {
	clear: both;
}

.row-fluid .thumbnails {
	margin-left: 0;
}

.thumbnails > li {
	float: left;
	margin-bottom: 20px;
	margin-left: 20px;
}

.thumbnail {
	display: block;
	padding: 4px;
	line-height: 20px;
	border: 1px solid #ddd;
	-webkit-border-radius: 4px;
		 -moz-border-radius: 4px;
					border-radius: 4px;
	-webkit-box-shadow: 0 1px 3px rgba(0, 0, 0, 0.055);
		 -moz-box-shadow: 0 1px 3px rgba(0, 0, 0, 0.055);
					box-shadow: 0 1px 3px rgba(0, 0, 0, 0.055);
	-webkit-transition: all 0.2s ease-in-out;
		 -moz-transition: all 0.2s ease-in-out;
			 -o-transition: all 0.2s ease-in-out;
					transition: all 0.2s ease-in-out;
}

a.thumbnail:hover {
	border-color: #0088cc;
	-webkit-box-shadow: 0 1px 4px rgba(0, 105, 214, 0.25);
		 -moz-box-shadow: 0 1px 4px rgba(0, 105, 214, 0.25);
					box-shadow: 0 1px 4px rgba(0, 105, 214, 0.25);
}

.thumbnail > img {
	display: block;
	min-width: 100%;
	margin-right: auto;
	margin-left: auto;
}

.thumbnail .caption {
	padding: 9px;
	color: #555555;
}
</style>
<div class="main-container">
  <div class="parallax bg-white page-section third">
     <div class="parallax-layer container" data-opacity="true">
        <div class="media v-middle">
           <div class="media-left">
              <span class="icon-block s60 bg-lightred"><i class="fa fa-picture-o"></i></span>
           </div>
           <div class="media-body">
              <h1 class="text-display-1 margin-none">Galeri</h1>
           </div>
           <div class="media-right">
           </div>
        </div>
     </div>
  </div>

	<div class="container">
		<div class="page-section">
				<div class="row">
					<div class="col-md-9">

						<div class="panel panel-default">
								<div class="panel-body" style="padding:0px">
									<ol class="breadcrumb">
										<li><a href="<?php echo base_url().'galeri';?>">Galeri</a></li>
										<li class="active">Edit</li>
									</ol>
							</div>
						</div>

						<div class="tabbable paper-shadow relative" data-z="0.5">
							 <!-- Tabs -->
							 <ul class="nav nav-tabs">
									<li class="active"><a href="#"><i class="fa fa-fw fa-lock"></i> <span class="hidden-sm hidden-xs">Album Galeri Foto &raquo; <?php echo $detalb->nama?></span></a></li>
							 </ul>
							 <!-- // END Tabs -->
							 <!-- Panes -->
							 <div class="tab-content">
									<div id="account" class="tab-pane active">
										<?php echo $this->session->flashdata('k')?>

										<form action="<?php echo base_URL()?>galeri/manage/rename_album" method="post" class="form-horizontal" style="margin-bottom:50px">
											<input type="hidden" name="id_alb1" value="<?php echo $detalb->id?>">
											<div class="form-group">
												 <label for="nama_album" class="col-md-2 control-label">Ubah Nama Album</label>
												 <div class="col-md-6">
														<div class="form-control-material" style="padding-top: 5px;height: 31px;">
															 <input type="text" name="nama_album" required class="form-control" value="<?php echo $detalb->nama?>" id="nama_album" placeholder="Nama album">
														</div>
												 </div>

												 <div class="col-md-3">
													 <input type="submit" class="btn btn-primary paper-shadow relative" data-z="0.5" value="Update" data-hover-z="1" data-animated>
												 </div>
											</div>
											<!-- <div class="form-group margin-none">
                        <div class="col-sm-offset-8 col-sm-9">
                      		<input type="submit" class="btn btn-success paper-shadow relative" value="Upload" style="margin-top: -10px">
                        </div>
                      </div> -->
										</form>




										<legend style="margin-bottom: 10px">Upload foto pada album "<?php echo $detalb->nama?>"</legend>
										<form action="<?php echo base_URL()?>galeri/manage/<?php echo $act?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="form-horizontal">
											<input type="hidden" name="id_alb2" value="<?php echo $detalb->id?>">

											<div class="form-group">
												<label for="cari_file" class="col-md-2 control-label">Cari File</label>
												<div class="col-md-6">
													 <div class="form-control-material" style="padding-top: 5px;height: 31px;">
															<input type="file"  <?php $mode !== 'edit_foto' ? 'required':'' ?> name="foto" id="foto">
													 </div>
												</div>
											</div>

											<div class="form-group">
												<label for="judul" class="col-md-2 control-label">Judul</label>
												<div class="col-md-6">
													 <div class="form-control-material" style="padding-top: 5px;height: 31px;">
															<input id="judul" class="form-control" required type="text" name="judul" value="<?php echo $judul;?>" required>
													 </div>
												</div>
											</div>

											<!-- <div class="form-group">
												<label for="slideshow" class="col-md-2 control-label">SlideShow</label>
												<div class="col-md-6">
													 <div class="form-control-material" style="padding-top: 5px;height: 31px;">
														 <select name="slideshow" id="slideshow" style="width: 100%;" data-toggle="select2">
															 <option <?php echo $slideshow === 'N' ? 'selected':'';?> value="N">Tidak</option>
															 <option <?php echo $slideshow === 'Y' ? 'selected':'';?> value="Y">Ya</option>
														 </select>
													 </div>
												</div>
											</div> -->

											<div class="form-group">
												<label for="keterangan" class="col-md-2 control-label">Keterangan</label>
												<div class="col-md-6">
													 <div class="form-control-material" style="padding-top: 5px;height: 31px;">
															<input class="form-control" type="text" name="ket" value="<?php echo $ket;?>">
													 </div>
												</div>
											</div>

											<div class="form-group margin-none">
                        <div class="col-sm-offset-8 col-sm-9">
                      		<input type="submit" class="btn btn-success paper-shadow relative" value="Upload" style="margin-top: -10px">
                        </div>
                      </div>

										</form>
										<hr>


										<ul class="thumbnails">
											<?php
											if (empty($datdet)) {
												echo "<div class=\"alert alert-danger\">Data foto tidak ditemukan</div>";
											} else {
												foreach ($datdet as $d) {
											?>
												<li class="span4" style="margin-left: 0px; margin-right: 9px">
													<div class="thumbnail" style="height: 280px">
														<img src="<?php echo base_URL()?>timthumb?src=/upload/galeri/<?php echo $d->file . '&w=334&h=190&zc=0'?>" style="width: 334px; height: 190px" alt="<?php echo $d->ket?>">
														<div class="caption">
															<h6 style="text-align: center; margin-top: 0">
																<strong><?php echo strtoupper($d->judul)?></strong> <br>

																<?php if(!IsNullOrEmptyString($d->ket)){ ?>
																<?php echo '(' . ucwords($d->ket) . ')'?> <br>
																<?php } ?>

																<!-- <input <?php echo $d->slideshow === 'Y' ? 'checked' : ''?> class="slideshow-cb" id="<?php echo $d->id;?>" type="checkbox"> Slideshow<br> -->
																<a href="<?php echo base_URL()?>galeri/manage/del_foto/<?php echo $detalb->id?>/<?php echo $d->id?>" onclick="return confirm('Anda Yakin ..?'); ">Hapus Foto</a> |
																<a href="<?php echo base_URL()?>galeri/manage/edit_foto/<?php echo $detalb->id?>/<?php echo $d->id?>" >Edit Foto</a>
															</h6>

														</div>
													</div>
												</li>

												<?php
												}
											}
											?>

										</ul>
									</div>
							 </div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="panel panel-default" data-toggle="panel-collapse" data-open="true">
							 <div class="panel-heading panel-collapse-trigger">
									<h4 class="panel-title">Menu</h4>
							 </div>
							 <div class="panel-body list-group">
										<?php echo main_menu('galeri');?>
							 </div>
						</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

  	<?php if($slideshow === 'Y'){ ?>
  		$('#keterangan').show();
  	<?php }else{ ?>
  		$('#keterangan').hide();
  	<?php } ?>

  	$('#select-slideshow').change(function(){
	  var slideshow = $('#select-slideshow').val();

	  if (slideshow === 'Y') {
		$('#keterangan').show();
	  }else{
		$('#keterangan').hide();
	  }

	})

  	$('.slideshow-cb').click(function() {
        var id_galeri = $(this).attr('id');

        if($(this).is(':checked')){
        	$.post("<?php echo base_url() . 'galeri/manage/set_slideshow';?>",
        			{ id: id_galeri ,
        			  act : 'add_slide' }
        	).done(function(data){

        	});
        }else{
			$.post("<?php echo base_url() . 'galeri/manage/set_slideshow';?>",
        			{ id: id_galeri ,
        			  act : 'del_slide' }
        	).done(function(data){

        	});
        }

        //return false;
    });
  </script>
