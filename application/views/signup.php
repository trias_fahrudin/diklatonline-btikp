<!DOCTYPE html>
<html class="hide-sidebar ls-bottom-footer" lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Learning</title>
      <link href="<?php echo base_url() . 'assets/back/';?>css/vendor.min.css" rel="stylesheet">
      <link href="<?php echo base_url() . 'assets/back/';?>css/theme-core.min.css" rel="stylesheet">
      <link href="<?php echo base_url() . 'assets/back/';?>css/module-essentials.min.css" rel="stylesheet" />
      <link href="<?php echo base_url() . 'assets/back/';?>css/module-material.css" rel="stylesheet" />
      <link href="<?php echo base_url() . 'assets/back/';?>css/module-layout.min.css" rel="stylesheet" />
      <!-- <link href="<?php echo base_url() . 'assets/back/';?>css/module-sidebar.min.css" rel="stylesheet" /> -->
      <!-- <link href="<?php echo base_url() . 'assets/back/';?>css/module-sidebar-skins.min.css" rel="stylesheet" /> -->
      <!-- <link href="<?php echo base_url() . 'assets/back/';?>css/module-navbar.css" rel="stylesheet" /> -->
      <!-- <link href="<?php echo base_url() . 'assets/back/';?>css/module-messages.min.css" rel="stylesheet" /> -->
      <!-- <link href="<?php echo base_url() . 'assets/back/';?>css/module-carousel-slick.min.css" rel="stylesheet" /> -->
      <!-- <link href="<?php echo base_url() . 'assets/back/';?>css/module-charts.min.css" rel="stylesheet" /> -->
      <!-- <link href="<?php echo base_url() . 'assets/back/';?>css/module-maps.min.css" rel="stylesheet" /> -->
      <link href="<?php echo base_url() . 'assets/back/';?>css/module-colors-alerts.min.css" rel="stylesheet" />
      <link href="<?php echo base_url() . 'assets/back/';?>css/module-colors-background.min.css" rel="stylesheet" />
      <link href="<?php echo base_url() . 'assets/back/';?>css/module-colors-buttons.min.css" rel="stylesheet" />
      <link href="<?php echo base_url() . 'assets/back/';?>css/module-colors-text.min.css" rel="stylesheet" />
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries
         WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!-- If you don't need support for Internet Explorer <= 8 you can safely remove these -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->

      <style type="text/css">
        #loading-div-background{
          display: none;
          position: fixed;
          top: 0;
          left: 0;
          background: #fff;
          width: 100%;
          height: 100%;
        }

        #loading-div{
          width: 300px;
          height: 150px;
          background-color: #fff;
          border: 5px solid #1468b3;
          text-align: center;
          color: #202020;
          position: absolute;
          left: 50%;
          top: 50%;
          margin-left: -150px;
          margin-top: -100px;
          -webkit-border-radius: 5px;
          -moz-border-radius: 5px;
          border-radius: 5px;
        }

        .block {
            /*background: none repeat scroll 0 0 #FFFFFF;*/
            border: 0px solid #CCCCCC;
            margin: 1em 0;
        }
      </style>

   </head>
   <body class="login">
      <div id="content" class="block">
        <div class="container-fluid">
            <div class="lock-container">
                <div class="panel panel-default paper-shadow" data-z="0.5">
                    <h1 class="text-display-1 text-center">Buat akun baru</h1>
                    <div class="panel-body">
                        <!-- Signup -->
                        <form role="form" id="form-signup" action="" method="POST">

                            <div class="alert alert-success" role="alert" id="alert-signin-success" style="display:none">
                              Pendaftaran berhasil<br/>
                              Silahkan periksa email anda untuk keterangan lebih lanjut
                            </div>

                            <div class="alert alert-danger" role="alert" id="alert-signin-danger" style="display:none">
                              Pastikan kode yang anda masukkan benar <br/>
                              Pastikan NIP dan NUPTK yang anda masukkan benar
                            </div>

                            <div class="form-group">
                                <div class="form-control-material">
                                    <select id="status" class="form-control" data-style="btn-white" data-live-search="true" data-size="5" placeholder="Nama lengkap">
                                      <option value="PNS">PNS</option>
                                      <option value="NON-PNS">NON PNS</option>
                                    </select>
                                </div>
                            </div>
                            <!-- <div class="form-group"> -->
                                <div class="form-group form-control-material static required">
                                    <input id="fullname" type="text" class="form-control">
                                    <label for="fullname">Nama lengkap</label>
                                </div>
                            <!-- </div> -->

                            <div class="form-group">
                                <div class="form-control-material static">
                                    <input id="nip" type="text" class="form-control">
                                    <label for="nip">NIP</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-control-material static">
                                    <input id="nuptk" type="text" class="form-control">
                                    <label for="nuptk">NUPTK</label>
                                </div>
                            </div>

                           <!--  <div class="form-group">
                                <div class="form-control-material">
                                    <input id="lastName" type="text" class="form-control" placeholder="Kota lahir">
                                    <label for="lastName">Kota lahir</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-control-material">
                                    <input id="datepicker" type="text" class="form-control datepicker">
                                    <label for="datepicker">Tanggal lahir</label>
                                </div>
                            </div> -->
                            <div class="form-group">
                                <div class="form-control-material static required">
                                    <input id="email" type="email" class="form-control">
                                    <label for="email">Alamat email</label>
                                </div>
                            </div>

                            <?php echo $captcha_image; ?>

                            <div class="form-group">
                                <div class="form-control-material static required">
                                <input id="captcha_word" type="text" class="form-control" placeholder="Masukkan kode diatas">
                                <label for="captcha">Kode</label>
                                </div>
                            </div>

                           <!--  <div class="form-group">
                                <div class="form-control-material static">
                                    <input id="password" type="password" class="form-control">
                                    <label for="password">Password</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-control-material static">
                                    <input id="passwordConfirmation" type="password" class="form-control">
                                    <label for="passwordConfirmation">Re-type password</label>
                                </div>
                            </div> -->
                            <!-- <div class="form-group text-center">
                                <div class="checkbox">
                                    <input type="checkbox" id="agree" />
                                    <label for="agree">* I Agree with <a href="#">Terms &amp; Conditions!</a></label>
                                </div>
                            </div> -->
                            <div class="text-center">
                                <a href="<?php echo base_url() . 'signin'?>" id="btn-signin" class="btn btn-primary"><i class="fa fa-fw fa-unlock-alt"></i> Masuk</a>
                                <a href="#" id="btn-signup" class="btn btn-primary"><i class="fa fa-fw fa-play"></i> Buat Akun</a>
                            </div>
                        </form>
                        <!-- //Signup -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="loading-div-background">
      <div id="loading-div" class="ui-corner-all">
      <img style="height:50px;width:50px;margin:20px;" src="<?php echo base_url()?>assets/back/images/please_wait.gif" alt="Loading.."/><br>Memproses<br>Mohon tunggu
      </div>
    </div>

    <!-- Footer -->
    <!--  <footer class="footer">
        <strong>Learning</strong> v1.0.0 &copy; Copyright 2015
    </footer> -->
    <!-- // Footer -->
    <!-- Inline Script for colors and config objects; used by various external scripts; -->
    <script>

       var colors = {
           "danger-color": "#e74c3c",
           "success-color": "#81b53e",
           "warning-color": "#f0ad4e",
           "inverse-color": "#2c3e50",
           "info-color": "#2d7cb5",
           "default-color": "#6e7882",
           "default-light-color": "#cfd9db",
           "purple-color": "#9D8AC7",
           "mustard-color": "#d4d171",
           "lightred-color": "#e15258",
           "body-bg": "#f6f6f6"
       };
       var config = {
           theme: "html",
           skins: {
               "default": {
                   "primary-color": "#42a5f5"
               }
           }
       };
    </script>
      <script src="<?php echo base_url() . 'assets/back/js/plugins/jquery-2.1.4.min.js'?>" type="text/javascript"></script>

      <script type="text/javascript">var base = "<?php echo base_url()?>";</script>
      <!-- User Script -->
      <script src="<?php echo base_url() . 'assets/back/js-user/essential.js'?>" type="text/javascript"></script>
      <script src="<?php echo base_url() . 'assets/back/js-user/signup.js'?>" type="text/javascript"></script>
      <!-- Separate Vendor Script Bundles -->
      <script src="<?php echo base_url() . 'assets/back/';?>js/vendor-core.min.js"></script>
      <!-- <script src="<?php echo base_url() . 'assets/back/';?>js/vendor-countdown.min.js"></script> -->
      <!-- <script src="<?php echo base_url() . 'assets/back/';?>js/vendor-tables.min.js"></script> -->
      <script src="<?php echo base_url() . 'assets/back/';?>js/vendor-forms.js"></script>
      <!-- <script src="<?php echo base_url() . 'assets/back/';?>js/vendor-carousel-slick.min.js"></script> -->
      <!-- <script src="<?php echo base_url() . 'assets/back/';?>js/vendor-player.min.js"></script> -->
      <!-- <script src="<?php echo base_url() . 'assets/back/';?>js/vendor-charts-flot.min.js"></script> -->
      <!-- <script src="<?php echo base_url() . 'assets/back/';?>js/vendor-nestable.min.js"></script> -->
      <!-- <script src="js/vendor-angular.min.js"></script> -->
      <script src="<?php echo base_url() . 'assets/back/';?>js/module-essentials.min.js"></script>
      <script src="<?php echo base_url() . 'assets/back/';?>js/module-material.min.js"></script>
      <script src="<?php echo base_url() . 'assets/back/';?>js/module-layout.min.js"></script>
      <!-- <script src="<?php echo base_url() . 'assets/back/';?>js/module-sidebar.min.js"></script> -->
      <!-- <script src="<?php echo base_url() . 'assets/back/';?>js/module-carousel-slick.min.js"></script> -->
      <!-- <script src="<?php echo base_url() . 'assets/back/';?>js/module-player.min.js"></script> -->
      <!-- <script src="<?php echo base_url() . 'assets/back/';?>js/module-messages.min.js"></script> -->
      <!-- <script src="<?php echo base_url() . 'assets/back/';?>js/module-maps-google.js"></script> -->
      <!-- <script src="<?php echo base_url() . 'assets/back/';?>js/module-charts-flot.min.js"></script> -->
      <script src="<?php echo base_url() . 'assets/back/';?>js/theme-core.min.js"></script>
   </body>
</html>
