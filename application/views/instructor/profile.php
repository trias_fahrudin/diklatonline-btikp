<div class="main-container">
   <div class="parallax overflow-hidden bg-blue-400 page-section third">
      <div class="container parallax-layer" data-opacity="true">
         <div class="media v-middle">
            <div class="media-left text-center">
               <a href="#">
               <img src="<?php echo base_url() . 'timthumb?src=' . base_url() . 'upload/people/' . $this->session->userdata('user_foto') . '&h=80&w=80&zc=0'; ?>" alt="" class="img-circle width-80" />
               </a>
            </div>
            <div class="media-body">
               <h1 class="text-white text-display-1 margin-v-0"><?php echo ucwords($this->session->userdata('user_fullname'));?></h1>
               <p class="text-subhead"><a class="link-white text-underline" href="<?php echo base_url() . 'profile'?>">Lihat Profile</a></p>
            </div>
            <div class="media-right">
               <span class="label bg-blue-500">Admin</span>
            </div>
         </div>
      </div>
   </div>
   <div class="container">
      <div class="page-section">
         <div class="row">
            <div class="col-md-9">
               <!-- Tabbable Widget -->
               <div class="tabbable paper-shadow relative" data-z="0.5">
                  <!-- Tabs -->
                  <ul class="nav nav-tabs">
                     <li class="active"><a href=""><i class="fa fa-fw fa-lock"></i> <span class="hidden-sm hidden-xs">Mengelola Akun</span></a></li>
                  </ul>
                  <!-- // END Tabs -->
                  <!-- Panes -->
                  <div class="tab-content">
                     <div id="account" class="tab-pane active">
                        <div class="alert alert-success" role="alert" id="alert-success" style="display:none">
                           <h4 style="text-align:center;margin-bottom: 0px;">Data Berhasil di Update</h4>
                        </div>
                        <div class="alert alert-danger" role="alert" id="alert-danger" style="display:none">
                           <h4 style="text-align:center;margin-bottom: 0px;">Data Gagal di Update</h4>
                        </div>
                        <form id="form-foto" class="form-horizontal" action="<?php echo base_url() . 'profile/upload_foto'?>" method="POST" enctype="multipart/form-data">
                           <div class="form-group">
                              <label for="inputEmail3" class="col-sm-2 control-label">Foto</label>
                              <div class="col-md-6">
                                 <div class="media v-middle">
                                    <div class="media-left">
                                       <div class="icon-block width-100 bg-grey-100">
                                          <?php $foto = $this->session->userdata('user_foto');?>
                                          <img src="<?php echo base_url() . 'timthumb?src=' . base_url() . 'upload/people/' . $foto . '&h=100&w=100&zc=0'; ?>" width="100" height="100" alt="Shoe" />
                                       </div>
                                    </div>
                                    <div class="media-body">
                                       <input type="file" name="foto" id="file-foto">
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </form>
                        <?php $profile = $this->Basecrud_m->get_where('users',array('id' => $this->session->userdata('user_id')))->row()?>
                        <form class="form-horizontal" method="POST">
                           <div class="form-group">
                              <label for="inputEmail3" class="col-md-2 control-label">User Name</label>
                              <div class="col-md-10">
                                 <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                    <input type="text" class="form-control" id="username" placeholder="Username anda" readonly="" value="<?php echo $this->session->userdata('user_name');?>" disabled="">
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <label for="inputEmail3" class="col-md-2 control-label">Nama Lengkap</label>
                              <div class="col-md-10">
                                 <div class="form-control-material required" style="padding-top: 5px;height: 31px;">
                                    <input type="text" class="form-control" id="fullname" placeholder="Nama lengkap anda" value="<?php echo $profile->fullname;?>">
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <label for="inputEmail3" class="col-md-2 control-label">Email</label>
                              <div class="col-md-10">
                                 <div class="form-control-material required" style="padding-top: 5px;height: 31px;">
                                    <div class="input-group">
                                       <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                       <input type="email" class="form-control used" id="email" placeholder="Email" value="<?php echo $profile->email;?>">
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <label for="inputEmail3" class="col-md-2 control-label">Nomor Telephon</label>
                              <div class="col-md-10">
                                 <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                    <input type="text" class="form-control used" id="telp_number" value="<?php echo $profile->telp_number;?>">
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <label for="inputEmail3" class="col-md-2 control-label">Kota Lahir</label>
                              <div class="col-md-10">
                                 <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                    <input type="text" class="form-control used" id="city_of_birth" value="<?php echo $profile->city_of_birth;?>">
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <label for="inputEmail3" class="col-md-2 control-label">Tanggal Lahir</label>
                              <div class="col-md-10">
                                 <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                    <div class="input-group">
                                       <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                       <input type="text" class="form-control datepicker" id="date_of_birth" value="<?php echo appDate($profile->date_of_birth)?>">
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <label for="inputEmail3" class="col-md-2 control-label">Jenis Kelamin</label>
                              <div class="col-md-10">
                                 <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                    <div class="input-group">
                                       <select id="gender" class="form-control" data-style="btn-white" data-live-search="true" data-size="5" placeholder="Jenis Kelamin">
                                          <option <?php echo $profile->gender === 'MALE' ? 'selected':'';?> value="MALE">Laki-laki</option>
                                          <option <?php echo $profile->gender === 'FEMALE' ? 'selected':'';?> value="FEMALE">Perempuan</option>
                                       </select>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <label for="home_address" class="col-md-2 control-label">Alamat</label>
                              <textarea name="home_address" cols="30" rows="10"><?php echo $profile->home_address;?></textarea>
                           </div>

                           <!-- <div class="form-group">
                              <label for="inputEmail3" class="col-md-2 control-label">Alamat</label>
                              <div class="col-md-6">
                                 <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                    <input type="text" class="form-control used" id="home_address" value="<?php echo $profile->home_address;?>">
                                 </div>
                              </div>
                           </div> -->
                           <div class="form-group">
                              <label for="inputEmail3" class="col-md-2 control-label">NIP</label>
                              <div class="col-md-10">
                                 <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                    <input type="text" class="form-control used" id="nip" value="<?php echo $profile->nip;?>">
                                 </div>
                              </div>
                           </div>

                           <div class="form-group">
                              <label for="inputEmail3" class="col-md-2 control-label">NPWP</label>
                              <div class="col-md-10">
                                 <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                    <input type="text" class="form-control used" id="npwp" value="<?php echo $profile->npwp;?>">
                                 </div>
                              </div>
                           </div>

                           <div class="form-group">
                              <label for="inputEmail3" class="col-md-2 control-label">Instansi</label>
                              <div class="col-md-10">
                                 <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                    <input type="text" class="form-control used" id="institute" value="<?php echo $profile->institute;?>">
                                 </div>
                              </div>
                           </div>

                           <div class="form-group">
                              <label for="inputEmail3" class="col-md-2 control-label">Keahlian</label>
                              <div class="col-md-6">
                                 <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                    <input type="text" class="form-control used" id="expertise" value="<?php echo $profile->expertise ;?>">
                                 </div>
                              </div>
                           </div>

                           <div class="form-group">
                              <label for="inputEmail3" class="col-md-2 control-label">Golongan</label>
                              <div class="col-md-6">
                                 <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                    <input type="text" class="form-control used" id="golongan" value="<?php echo $profile->golongan ;?>">
                                 </div>
                              </div>
                           </div>

                           <div class="form-group">
                              <label for="inputEmail3" class="col-md-2 control-label">Posisi</label>
                              <div class="col-md-6">
                                 <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                    <input type="text" class="form-control used" id="position" value="<?php echo $profile->position ;?>">
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <label for="inputEmail3" class="col-md-2 control-label" style="padding-left: 2px;">Pendidikan Terakhir</label>
                              <div class="col-md-6">
                                 <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                    <input type="text" class="form-control used" id="last_education" value="<?php echo $profile->last_education ;?>">
                                 </div>
                              </div>
                           </div>

                           <div class="form-group">
                              <label for="password" class="col-md-2 control-label">Ganti Password</label>
                              <div class="col-md-10">
                                 <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                    <input type="password" class="form-control" id="password" placeholder="Password">
                                 </div>
                              </div>
                           </div>
                           <div class="form-group margin-none">
                               <div class="text-right">
                                 <button id="cancel-profile" type="button" class="btn btn-warning paper-shadow relative" data-z="0.5" data-hover-z="1" data-animated><i class="fa fa-mail-reply"></i> Batal</button>
                                 <button id="save-profile" type="button" class="btn btn-primary paper-shadow relative" data-z="0.5" data-hover-z="1" data-animated>Simpan Perubahan <i class="fa fa-chevron-circle-right"></i></button>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
                  <!-- // END Panes -->
               </div>
               <!-- // END Tabbable Widget -->
               <br/>
               <br/>
            </div>
            <!-- right menu -->
            <div class="col-md-3">
               <div class="panel panel-default" data-toggle="panel-collapse" data-open="true">
                  <div class="panel-heading panel-collapse-trigger">
                     <h4 class="panel-title">Menu</h4>
                  </div>
                  <div class="panel-body list-group">
                     <?php echo main_menu('profile');?>
                  </div>
               </div>
            </div>
            <!-- end right menu -->
         </div>
      </div>
   </div>
</div>

<script>

  $('#file-foto').change(function() {
      $('#form-foto').submit();
  });

  $("#save-profile").click(function() {
    $('#alert-success').hide();
    $('#alert-danger').hide();

    $(".required input").each(function() {
      if ($.trim($(this).val()).length === 0) {
        $(this).parent().addClass("has-error");
        alert("Mohon isi semua kolom yang diperlukan (*)");
        return false;
      } else {
        $(this).parent().removeClass("has-error");
      }
    });

    if (!isValidEmailAddress($('#email').val())) {
      alert('Alamat email tidak valid!');
      return false;
    }

    $.post(base + 'profile', {

      fullname: $('#fullname').val(),
      telp_number: $('#telp_number').val(),
      email: $('#email').val(),
      home_address: $('#home_address').val(),
      city_of_birth: $('#city_of_birth').val(),
      date_of_birth: $('#date_of_birth').val(),
      gender: $('#gender').val(),
      // status: $('#status').val(),
      nip: $('#nip').val(),
      npwp: $('#npwp').val(),
      institute: $('#institute').val(),

      expertise: $('#expertise').val(),
      golongan: $('#golongan').val(),
      position: $('#position').val(),
      last_education: $('#last_education').val(),
      password : $('#password').val()

    }).done(function(data) {
      if (data.status === 'OK') {
        $('#alert-success').show();
        $('html, body').animate({
          scrollTop: $("#scroll-to-here").offset().top
        }, 1000);

        $("#alert-success").fadeTo(5000, 500).slideUp(500, function() {
          $("#alert-success").hide();
        });
      } else {
        $('#alert-danger').show();
        $('#alert-danger').html(data.msg);

        $('html, body').animate({
          scrollTop: $("#scroll-to-here").offset().top
        }, 1000);

        // $("#alert-danger").fadeTo(5000, 500).slideUp(500, function() {
        //   $("#alert-danger").hide();
        // });
      }
    });
  });
</script>
