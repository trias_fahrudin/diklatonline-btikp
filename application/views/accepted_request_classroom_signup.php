<head>
	<title>Pemberitahuan status registrasi kelas</title>
	<style>
		table {
		    border-collapse: collapse;
		    width: 100%;
		}

		th, td {
		    text-align: left;
		    padding: 8px;
		}

		tr:nth-child(even){background-color: #f2f2f2}

		th {
		    background-color: #4CAF50;
		    color: white;
		}
	</style>
</head>
<body>
<span class="normaltext">

<p>Selamat, anda telah teregistrasi untuk mengikuti kelas yang ada di <?php echo $this->config->item('diklat_title')?></p>
<p>
  Perincian kelas yang akan anda ikuti tercantum dibawah ini
</p>

<table id="table-normal">
  <tbody>
    <tr>
      <td>Nama Kelas</td>
      <td><?php echo $detail->name;?></td>
    </tr>
    <tr>
      <td>Angkatan</td>
      <td><?php echo $detail->generation;?></td>
    </tr>
    <tr>
      <td>Jadwal</td>
      <td><?php echo tgl_panjang($detail->start,'sm') . ' s/d ' . tgl_panjang($detail->end,'sm');?></td>
    </tr>

    <tr>
      <td>Detail Kegiatan</td>
      <td><?php echo $detail->detail_kegiatan;?></td>
    </tr>
    <tr>
      <td>Narasumber</td>
      <td><?php echo $detail->narasumber;?></td>
    </tr>
    <tr>
      <td>Peserta</td>
      <td><?php echo $detail->peserta;?></td>
    </tr>
    <tr>
      <td>Kapasitas</td>
      <td><?php echo $detail->registration_quota;?></td>
    </tr>
    <tr>
      <td>Tempat</td>
      <td><?php echo $detail->tempat;?></td>
    </tr>

  </tbody>
</table>

<p>
	Silahkan saudara mengupload surat tugas jika belum mengupload surat tugas dan segera konfirmasi kehadiran saudara di sini : <a href="<?php echo site_url('frontpage/signin')?>"><?php echo site_url('frontpage/signin')?></a>
</p>

<p>Salam,<br /> <a title="<?php echo base_url();?>" href="<?php echo base_url();?>"><?php echo base_url();?></a></p>
</span></p>
</body>
