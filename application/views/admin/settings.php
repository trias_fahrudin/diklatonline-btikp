<div class="parallax overflow-hidden page-section bg-blue-300 page-section third">
    <div class="parallax-layer container" data-opacity="true">
        <div class="media v-middle">
            <div class="media-left">
                <span class="icon-block s60 bg-lightred"><i class="fa fa-user"></i></span>
            </div>
            <div class="media-body">
                <h1 class="text-display-1 text-white margin-none">Setting</h1>
            </div>
            <div class="media-right">

            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="page-section">
        <div class="row">
            <div class="col-md-9">
              <div class="panel panel-default">
                  <table data-toggle="data-table" class="table v-middle" cellspacing="0" width="100%">
                      <thead>
                          <tr>
                              <th>Nomor</th>
                              <th>Nama Setting</th>
                              <th>Tipe</th>
                              <th>Nilai</th>
                          </tr>
                      </thead>
                      <tfoot>
                          <tr>
                            <th>Nomor</th>
                            <th>Nama Setting</th>
                            <th>Tipe</th>
                            <th>Nilai</th>
                          </tr>
                      </tfoot>
                      <tbody>
                        <?php $nomor = 0;?>
                        <?php foreach($setting->result() as $r){ ?>
                        <tr>
                           <td><?php echo str_pad($nomor + 1,3,'0',STR_PAD_LEFT)?></td>
                           <td><?php echo $r->name;?></td>
                           <td><?php echo $r->type;?></td>
                           <td>
                           <?php if($r->type === 'big-text'){?>
                             <textarea id="<?php echo $r->name;?>" class="update_me" style="width: 100%;height: 100%"><?php echo $r->value;?></textarea>
                           <?php }elseif($r->type === 'small-text'){ ?>
                             <input id="<?php echo $r->name;?>" class="update_me" style="width: 100%;height: 100%" type="text" value="<?php echo $r->value;?>">
                           <?php }elseif($r->type === 'image'){ ?>
                             <img style="margin-bottom: 10px" src="<?php echo base_url(); ?>upload/<?php echo $r->value;?>" height="77" width="500">
                             <form action="<?php echo base_url() . 'admin/settings/upload/' . $r->name;?>" method="POST" enctype="multipart/form-data">
                               <input class="upload" name="img" onchange="this.form.submit()" multiple="" type="file">
                             </form>
                           <?php }else{ ?>
                             <script type="text/javascript">
                               var flashvars = {};
                               var params = {};
                               params.scale = "exactfit";
                               var attributes = {};
                               swfobject.embedSWF("<?php echo base_url() . 'upload/' . $r->value;?>", "<?php echo $r->name;?>", "<?php echo $r->img_width?>", "<?php echo $r->img_height?>", "9.0.0", false,flashvars,params,attributes);
                             </script>
                             <div id="<?php echo $r->name;?>">
                               <h1>Alternative content</h1>
                               <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" /></a></p>
                             </div>
                             <form action="<?php echo base_url() . 'admin/settings/upload/' . $r->title;?>" method="POST" enctype="multipart/form-data">
                               <input class="upload" name="img" onchange="this.form.submit()" multiple="" type="file">
                             </form>
                           <?php } ?>

                           </td>
                        </tr>
                        <?php $nomor++;} ?>
                      </tbody>
                  </table>
              </div>
            </div>
            <div class="col-md-3">
                <div class="panel panel-default" data-toggle="panel-collapse" data-open="true">
                   <div class="panel-heading panel-collapse-trigger">
                      <h4 class="panel-title">Menu</h4>
                   </div>
                   <div class="panel-body list-group">
                        <?php echo main_menu('settings');?>
                   </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
   $('.update_me').keyup( function() {
      var this_name = $(this).attr('id');
      var this_val = $(this).val();

      $.post( "<?php echo base_url() . 'admin/settings/edt/';?>" + this_name,
      	{ value: this_val }
      );
   });
</script>
