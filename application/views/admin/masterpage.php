<!DOCTYPE html>

<html class="transition-navbar-scroll top-navbar-xlarge bottom-footer" lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <title><?php echo $this->config->item('diklat_title')?></title>
      <link href="<?php echo base_url() . 'assets/back/';?>css/vendor.min.css" rel="stylesheet">
      <link href="<?php echo base_url() . 'assets/back/';?>css/theme-core.min.css" rel="stylesheet">
      <link href="<?php echo base_url() . 'assets/back/';?>css/module-essentials.min.css" rel="stylesheet" />
      <link href="<?php echo base_url() . 'assets/back/';?>css/module-material.min.css" rel="stylesheet" />
      <link href="<?php echo base_url() . 'assets/back/';?>css/module-layout.min.css" rel="stylesheet" />
      <link href="<?php echo base_url() . 'assets/back/';?>css/module-sidebar.min.css" rel="stylesheet" />
      <link href="<?php echo base_url() . 'assets/back/';?>css/module-sidebar-skins.min.css" rel="stylesheet" />
      <link href="<?php echo base_url() . 'assets/back/';?>css/module-navbar.css" rel="stylesheet" />
      <link href="<?php echo base_url() . 'assets/back/';?>css/module-messages.min.css" rel="stylesheet" />
      <link href="<?php echo base_url() . 'assets/back/';?>css/module-carousel-slick.min.css" rel="stylesheet" />
      <!-- <link href="<?php echo base_url() . 'assets/back/';?>css/module-charts.min.css" rel="stylesheet" /> -->
      <!-- <link href="<?php echo base_url() . 'assets/back/';?>css/module-maps.min.css" rel="stylesheet" /> -->
      <link href="<?php echo base_url() . 'assets/back/';?>css/module-colors-alerts.min.css" rel="stylesheet" />
      <link href="<?php echo base_url() . 'assets/back/';?>css/module-colors-background.min.css" rel="stylesheet" />
      <link href="<?php echo base_url() . 'assets/back/';?>css/module-colors-buttons.min.css" rel="stylesheet" />
      <link href="<?php echo base_url() . 'assets/back/';?>css/module-colors-text.min.css" rel="stylesheet" />
      <!-- <link href="<?php echo base_url() . 'assets/back/';?>css/jquery.marquee.css" rel="stylesheet" /> -->
      <!-- <link href="<?php echo base_url() . 'assets/back/';?>css/jquery.marquee.css" rel="stylesheet" /> -->



      <!-- Chat Css -->

      <link href="<?php echo base_url() . 'assets/back/js/bootstrap-toggle/css/bootstrap-toggle.css';?>" rel="stylesheet" />

      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries
         WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!-- If you don't need support for Internet Explorer <= 8 you can safely remove these -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
      <style type="text/css">
        body{
           background:url("<?php echo base_url() . 'assets/back/images/body_bg.png'?>") left top repeat #f0f0f0
         }
      </style>

      <script src="<?php echo base_url() . 'assets/back/';?>js/vendor-core.min.js"></script>
      <!-- <script src="<?php echo base_url() . 'assets/back/';?>js/vendor-tables.min.js"></script> -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/front/'?>DataTables/datatables.min.css"/>
      <script type="text/javascript" src="<?php echo base_url() . 'assets/front/'?>DataTables/datatables.min.js"></script>

      <link href="<?php echo base_url() . 'assets/back/select2/dist/css/';?>select2.min.css" rel="stylesheet" />
      <script src="<?php echo base_url() . 'assets/back/select2/dist/js/';?>select2.js"></script>

      <!-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.0/jquery.min.js"></script>        -->
      <!-- <script src="<?php echo base_url() . 'assets/back/js/plugins/jquery-2.1.4.min.js'?>" type="text/javascript"></script>       -->
      <!-- Separate Vendor Script Bundles -->
      <script type="text/javascript">var base = "<?php echo base_url()?>";</script>

   </head>
   <body id="scroll-to-here">
     <!-- Code to Display the chat button -->
    <!-- <a href="javascript:void(0)" id="menu-toggle" class="btn-chat btn btn-success">
      <i class="fa fa-comments-o fa-3x"></i>
       <span class="badge progress-bar-danger"></span>
    </a> -->


    <?php $masterpage_header = $this->Basecrud_m->get_where('settings',array('name' => 'masterpage_header'))->row()?>

    <!-- Fixed navbar -->
    <div class="navbar navbar-default navbar-fixed-top navbar-size-large navbar-size-xlarge paper-shadow" data-z="0" data-animated role="navigation">
      <div class="container">
          <div class="navbar-header">

             <div class="navbar-brand navbar-brand-logo">
                <a class="svg" href="<?php echo base_url() . 'admin'?>">
                  <img style="margin-bottom: 10px" src="<?php echo base_url(); ?>upload/<?php echo $masterpage_header->value;?>" height="65" width="750">
                </a>
             </div>
          </div>
          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="main-nav">

             <div class="navbar-right">
                <ul class="nav navbar-nav navbar-nav-bordered navbar-nav-margin-right">

                   <!-- user -->
                   <li class="dropdown user active">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                         <img src="<?php echo base_url() . 'timthumb?src=' . base_url() . 'upload/people/' . $this->session->userdata('user_foto') . '&h=80&w=80&zc=0'; ?>" alt="" class="img-circle" />
                         <span class="label label-default"><?php echo ucwords($this->session->userdata('user_fullname'));?></span>
                         <span class="caret"></span>
                      </a>
                      <ul class="dropdown-menu" role="menu">
                         <?php $menu_seg = $this->uri->segment(2) == TRUE ? $this->uri->segment(2) : 'dashboard';?>
                         <li <?php echo $menu_seg === 'dashboard' ? 'class="active"' : ''?>><a href="<?php echo base_url() . 'admin'?>"><i class="fa fa-bar-chart-o"></i> Dashboard</a></li>
                         <li <?php echo $menu_seg === 'classroom' ? 'class="active"' : ''?>><a href="<?php echo base_url() . 'classroom'?>"><i class="fa fa-mortar-board"></i> Kelas</a></li>
                         <li <?php echo $menu_seg === 'data' ? 'class="active"' : ''?>><a href="<?php echo base_url() . 'data'?>"><i class="fa fa-database"></i> Data</a></li>
                         <li <?php echo $menu_seg === 'settings' ? 'class="active"' : ''?>><a href="<?php echo base_url() . 'admin/settings'?>"><i class="fa fa-cogs"></i> Settings</a></li>

                         <li <?php echo $menu_seg === 'profile' ? 'class="active"' : ''?>><a href="<?php echo base_url() . 'profile'?>"><i class="fa fa-user"></i> Profile</a></li>
                         <li><a href="<?php echo base_url() . 'signout'?>"><i class="fa fa-sign-out"></i> Keluar</a></li>
                      </ul>
                   </li>
                   <!-- // END user -->
                </ul>
                <a href="<?php echo base_url() . 'signout'?>" class="navbar-btn btn btn-primary">Keluar</a>
             </div>
          </div>
          <!-- /.navbar-collapse -->
       </div>
    </div>

      <!-- main container -->
      <?php $this->load->view($page);?>
      <!-- end main container -->

      <!-- Footer -->
     <!--  <footer class="footer">
         <strong>Learning</strong> v1.0.0 &copy; Copyright 2015
      </footer> -->
      <!-- // Footer -->
      <!-- Inline Script for colors and config objects; used by various external scripts; -->
      <script>
         var colors = {
             "danger-color": "#e74c3c",
             "success-color": "#81b53e",
             "warning-color": "#f0ad4e",
             "inverse-color": "#2c3e50",
             "info-color": "#2d7cb5",
             "default-color": "#6e7882",
             "default-light-color": "#cfd9db",
             "purple-color": "#9D8AC7",
             "mustard-color": "#d4d171",
             "lightred-color": "#e15258",
             "body-bg": "#f6f6f6"
         };
         var config = {
             theme: "html",
             skins: {
                 "default": {
                     "primary-color": "#42a5f5"
                 }
             }
         };
      </script>


      <script src="<?php echo base_url() . 'assets/back/';?>js/vendor-forms.js"></script>
      <script src="<?php echo base_url() . 'assets/back/';?>js/vendor-carousel-slick.min.js"></script>
      <script src="<?php echo base_url() . 'assets/back/';?>js/vendor-nestable.js"></script>
      <script src="<?php echo base_url() . 'assets/back/';?>js/module-essentials.js"></script>
      <script src="<?php echo base_url() . 'assets/back/';?>js/module-material.min.js"></script>
      <script src="<?php echo base_url() . 'assets/back/';?>js/module-layout.min.js"></script>
      <script src="<?php echo base_url() . 'assets/back/';?>js/module-sidebar.min.js"></script>
      <script src="<?php echo base_url() . 'assets/back/';?>js/module-carousel-slick.min.js"></script>
      <script src="<?php echo base_url() . 'assets/back/';?>js/theme-core.min.js"></script>
      <script src="<?php echo base_url() . 'texteditor/tinymce3/tiny_mce.js'?>" type="text/javascript"></script>
      <script src="<?php echo base_URL() . 'assets/swfobject/swfobject.js'?>" type="text/javascript"></script>

      <script type="text/javascript">
        var roxyFileman =  "<?php echo ENVIRONMENT === 'development' ? '/diklatonline-btikp/fileman/index.html' : '/fileman/index.html'?>";
      </script>

      <!-- User Script -->
      <script src="<?php echo base_url() . 'assets/back/js-user/essential.js'?>" type="text/javascript"></script>
      <script src="<?php echo base_url() . 'assets/back/js-user/classroom.js'?>" type="text/javascript"></script>

      <script type="text/javascript" src="<?php echo base_url() . 'assets/back/js/bootstrap-toggle/js/bootstrap-toggle.js';?>"></script>



      <script type="text/javascript">
        tinyMCE.init({
            mode : "textareas",
            setup : function(ed)
            {
              <?php if($this->uri->segment(2) === 'settings'){ ?>
              ed.onChange.add(function(ed) {
                // console.log('the event object '+ed);
                // console.log('the editor object '+ed);
                // console.log('the content '+ed.getContent());
                // $('.update_me').keyup( function() {
                 var this_name = $(this).attr('id');
                 var this_val = ed.getContent();

                 $.post( "<?php echo base_url() . 'admin/settings/edt/';?>" + this_name,
                   { value: this_val }
                 );
                // });
             });
             <?php } ?>

              // set the editor font size
              ed.onInit.add(function(ed)
              {
                 ed.getBody().style.fontSize = '15px';
              });
            },

            theme : "advanced",
            plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,inlinepopups,insertdatetime,preview,media,searchreplace,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,autoresize",
            theme_advanced_buttons1 : "bold,italic,underline,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect,|,image,media,link,unlink,bullist,numlist,blockquote",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_resizing : false,
            width: '100%',
            //Mad File Manager
            relative_urls : false,
            file_browser_callback: RoxyFileBrowser
        });


        function RoxyFileBrowser(fieldName, url, type, win) {
          //fix relative urls
          //var roxyFileman = "";

          if (roxyFileman.indexOf("?") < 0) {
              roxyFileman += "?type=" + type;
          }
          else {
              roxyFileman += "&type=" + type;
          }

          roxyFileman += '&input=' + fieldName + '&value=' + win.document.getElementById(fieldName).value;
          if (tinyMCE.activeEditor.settings.language) {
              roxyFileman += '&langCode=' + tinyMCE.activeEditor.settings.language;
          }

          tinyMCE.activeEditor.windowManager.open({
              file: roxyFileman,
              title: 'Roxy Fileman',
              width: 850,
              height: 650,
              resizable: "yes",
              plugins: "media",
              inline: "yes",
              close_previous: "no"
          }, { window: win, input: fieldName });
          return false;
        }


      </script>

   </body>
</html>
