<?php $color_array = array('bg-default','bg-primary','bg-lightred','bg-brown','bg-purple','.bg-gray-dark')?>
<div class="parallax bg-white page-section third">
   <div class="parallax-layer container" data-opacity="true">
      <div class="media v-middle">
         <div class="media-left">
            <span class="icon-block s60 bg-lightred"><i class="fa fa-database"></i></span>
         </div>
         <div class="media-body">
            <h1 class="text-display-1 margin-none">Master Data</h1>
         </div>
         <div class="media-right">
         </div>
      </div>
   </div>
</div>
<div class="container">
   <div class="page-section">
      <div class="row">
         <div class="col-md-9">
            <div class="row" data-toggle="isotope">
               <?php if(is_authorized(array('admin'))){ ?>
               <div class="item col-xs-12 col-sm-6 col-lg-4">
                  <div class="panel panel-default paper-shadow" data-z="0.5">
                     <div class="cover overlay cover-image-full hover">
                        <span class="img icon-block height-150 bg-default"></span>
                        <a href="<?php echo base_url() . 'data/instructor'?>" class="padding-none overlay overlay-full icon-block bg-primary">
                        <span class="v-center">
                        <i class="fa fa-user"></i> Instruktur
                        </span>
                        </a>
                        <a href="<?php echo base_url() . 'data/instructor'?>" class="overlay overlay-full overlay-hover overlay-bg-white">
                        <span class="v-center">
                        <span class="btn btn-circle btn-white btn-lg"><i class="fa fa-graduation-cap"></i></span>
                        </span>
                        </a>
                     </div>
                  </div>
               </div>

               <div class="item col-xs-12 col-sm-6 col-lg-4">
                  <div class="panel panel-default paper-shadow" data-z="0.5">
                     <div class="cover overlay cover-image-full hover">
                        <span class="img icon-block height-150 bg-default"></span>
                        <a href="<?php echo base_url() . 'data/participator'?>" class="padding-none overlay overlay-full icon-block bg-lightred">
                        <span class="v-center">
                        <i class="fa fa-user"></i> Peserta
                        </span>
                        </a>
                        <a href="<?php echo base_url() . 'data/participator'?>" class="overlay overlay-full overlay-hover overlay-bg-white">
                        <span class="v-center">
                        <span class="btn btn-circle btn-white btn-lg"><i class="fa fa-graduation-cap"></i></span>
                        </span>
                        </a>
                     </div>
                  </div>
               </div>

                <?php } ?>


               <div class="item col-xs-12 col-sm-6 col-lg-4">
                  <div class="panel panel-default paper-shadow" data-z="0.5">
                     <div class="cover overlay cover-image-full hover">
                        <span class="img icon-block height-150 bg-default"></span>
                        <a href="<?php echo base_url() . 'data/material_home'?>" class="padding-none overlay overlay-full icon-block bg-purple">
                        <span class="v-center">
                        <i class="fa fa-database"></i> Bank Materi
                        </span>
                        </a>
                        <a href="<?php echo base_url() . 'data/material_home'?>" class="overlay overlay-full overlay-hover overlay-bg-white">
                        <span class="v-center">
                        <span class="btn btn-circle btn-white btn-lg"><i class="fa fa-graduation-cap"></i></span>
                        </span>
                        </a>
                     </div>
                  </div>
               </div>

            </div>
            <br/>
            <br/>
         </div>
         <div class="col-md-3">
            <div class="panel panel-default" data-toggle="panel-collapse" data-open="true">
               <div class="panel-heading panel-collapse-trigger">
                  <h4 class="panel-title">Menu</h4>
               </div>
               <div class="panel-body list-group">
                  <?php echo main_menu('data');?>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
