<div class="main-container">
  <div class="parallax overflow-hidden page-section bg-deep-purple-300 page-section third">
      <div class="parallax-layer container" data-opacity="true">
          <div class="media v-middle">
              <div class="media-left">
                  <span class="icon-block s60 bg-lightred"><i class="fa fa-download"></i></span>
              </div>
              <div class="media-body">
                  <h1 class="text-display-1 text-white margin-none">Import Data</h1>
              </div>
              <div class="media-right">
                <a href="<?php echo base_url() . 'data/index/' . $this->uri->segment(3) ?>">
                  <span class="label bg-blue-500">List</span>
                </a>
              </div>
          </div>
      </div>
  </div>

  <div class="container">
      <div class="page-section">
          <div class="row">
              <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-body" style="padding:0px">
                      <ol class="breadcrumb">
                        <li><a href="<?php echo base_url() . 'data/index';?>">Data</a></li>
                        <li><a href="<?php echo base_url() . 'data/index/' . $this->uri->segment(3);?>">Data <?php echo $title;?></a></li>
                        <li class="active">Import</li>
                      </ol>
                  </div>
                </div>

                <div class="panel panel-default paper-shadow" data-z="0.5">
                     <div class="panel-body">
                        <div class="media media-clearfix-xs">
                           <div class="media-left text-center">
                              <div class="cover width-150 width-100pc-xs overlay cover-image-full hover margin-v-0-10">
                                 <span class="img icon-block height-130 bg-default"></span>
                                 <span class="overlay overlay-full padding-none icon-block bg-default">
                                 <span class="v-center">
                                 <i class="fa fa-download"></i>
                                 </span>
                                 </span>
                                 <a href="#" class="overlay overlay-full overlay-hover overlay-bg-white">
                                 <span class="v-center">
                                 <span class="btn btn-circle btn-white btn-lg"><i class="fa fa-cogs"></i></span>
                                 </span>
                                 </a>
                              </div>
                           </div>
                           <div class="media-body">
                              <h4 class="text-headline margin-v-5-0"><a href="">Import <?php echo $title;?></a></h4>
                              <p>Silahkan download file excel sebagai acuan dalam import</p>
                              <h4>  <i class="fa fa-download"></i>
                                <a href="<?php echo base_url() . 'upload/template_' . $this->uri->segment(3) . '.xls'; ?>">Download file acuan</a>
                              </h4>
                              <hr class="margin-v-8" />
                              <div class="media v-middle">
                                 <div class="media-left">

                                 </div>
                                 <div class="media-body">
                                    <form class="form-horizontal" role="form" action="" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                                      <div class="form-group">
                                        <input type="hidden" name="upload_me" value="">
                                        <label class="col-sm-3 control-label">File Import</label>
                                        <div class="col-sm-6">
                                          <div class="input-group">
                                            <input style="float: left; display: block" class="search-query" type="file" name="userfile">
                                          </div>
                                        </div>

                                        <div class="form-group margin-none">
                                            <div class="col-sm-offset-3 col-sm-9">
                                                <button type="submit" class="btn btn-primary">Import</button>
                                            </div>
                                        </div>

                                      </div>

                                    </form>

                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
              </div>
              <div class="col-md-3">
                  <div class="panel panel-default" data-toggle="panel-collapse" data-open="true">
                     <div class="panel-heading panel-collapse-trigger">
                        <h4 class="panel-title">Menu</h4>
                     </div>
                     <div class="panel-body list-group">
                          <?php echo main_menu('data');?>
                     </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>
