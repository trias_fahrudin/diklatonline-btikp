<?php
    $mode = $this->uri->segment(3);

    if ($mode === 'edit' || $mode === 'edit_act') {
        $id = $categori->id;

        $name = $mode === 'edit' ? $categori->name : set_value('name');
        $description = $mode === 'edit' ? $categori->description : set_value('description');

        $act = base_url().'data/material_categories/edit_act/'.$id;
    } else {

        $name = $mode === 'add' ? '' : set_value('name');
        $description = $mode === 'add' ? '' : set_value('description');

        $act = base_url().'data/material_categories/add_act';
    }
?>

<div class="parallax overflow-hidden page-section bg-deep-purple-300 page-section third">
   <div class="parallax-layer container" data-opacity="true">
      <div class="media v-middle">
         <div class="media-left">
            <span class="icon-block s60 bg-lightred"><i class="fa fa-database"></i></span>
         </div>
         <div class="media-body">
            <h1 class="text-display-1 text-white margin-none"><?php echo $mode === 'edit' ? 'Edit' : 'Tambah';?> Kategori Materi</h1>
         </div>
         <div class="media-right">
            <a class="btn btn-white" href="<?php echo base_url().'data/material_categories'?>">Kategori Materi</a>
         </div>
      </div>
   </div>
</div>
<div class="container">
   <div class="page-section">
      <div class="row">
         <div class="col-md-9">

           <div class="panel panel-default">
               <div class="panel-body" style="padding:0px">
                 <ol class="breadcrumb">
                   <li><a href="<?php echo base_url().'data';?>">Data</a></li>
                   <li><a href="<?php echo base_url().'data/material_home';?>">Bank Materi</a></li>
                   <li><a href="<?php echo base_url().'data/material_categories';?>">Kategori</a></li>
                   <li class="active"><?php echo ($mode === 'edit' || $mode === 'edit_act') ? 'Edit' : 'Tambah'?> Kategori</li>
                 </ol>
             </div>
           </div>

            <div class="panel panel-default">
               <div class="panel-body">
                  <?php echo isset($msg) ? alert_msg($msg) : ''?>

                  <form action="<?php echo $act?>" method="POST">

                    <div class="form-group form-control-material static required">
                      <input name="name" type="text" id="name" class="form-control" placeholder="Nama Kategori" value="<?php echo $name?>">
                      <label for="name">Nama Kategori</label>
                    </div>

                     <div class="form-group form-control-material static required">
                        <input name="description" type="text" id="description" class="form-control" placeholder="Deskripsi" value="<?php echo $description?>">
                        <label for="description">Deskripsi</label>
                     </div>

                     <div class="text-right">
                        <a href="<?php echo base_url().'data/material_categories'?>" class="navbar-btn btn btn-warning"><i class="fa fa-mail-reply"></i> Batal</a>
                        <button type="submit" class="btn btn-primary">Simpan <i class="fa fa-chevron-circle-right"></i></button>
                     </div>
                  </form>
               </div>
            </div>
         </div>
         <div class="col-md-3">
            <div class="panel panel-default" data-toggle="panel-collapse" data-open="true">
               <div class="panel-heading panel-collapse-trigger">
                  <h4 class="panel-title">Menu</h4>
               </div>
               <div class="panel-body list-group">
                  <?php echo main_menu('data');?>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
