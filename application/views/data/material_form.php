<?php
    $mode = $this->uri->segment(3);

    if ($mode === 'edit' || $mode === 'edit_act') {
        $id = $material->id;

        $material_categori_id = $mode === 'edit' ? $material->material_categori_id  : set_value('material_categori_id');
        $title = $mode === 'edit' ? $material->title : set_value('title');
        $content = $mode === 'edit' ? $material->content : set_value('content');
        $file_attachment = $material->file_attachment;

        $act = base_url().'data/material/edit_act/'.$id;
    } else {
        $material_categori_id = $mode === 'add' ? ''  : set_value('material_categori_id');
        $title = $mode === 'add' ? '' : set_value('title');
        $content = $mode === 'add' ? '' : set_value('content');
        $file_attachment = "";

        $act = base_url().'data/material/add_act';
    }
?>

<div class="parallax overflow-hidden page-section bg-deep-purple-300 page-section third">
   <div class="parallax-layer container" data-opacity="true">
      <div class="media v-middle">
         <div class="media-left">
            <span class="icon-block s60 bg-lightred"><i class="fa fa-database"></i></span>
         </div>
         <div class="media-body">
            <h1 class="text-display-1 text-white margin-none"><?php echo $mode === 'edit' ? 'Edit' : 'Tambah';?> Materi</h1>
         </div>
         <div class="media-right">
            <a class="btn btn-white" href="<?php echo base_url().'data/material'?>">Bank Materi</a>
         </div>
      </div>
   </div>
</div>
<div class="container">
   <div class="page-section">
      <div class="row">
         <div class="col-md-9">

           <div class="panel panel-default">
               <div class="panel-body" style="padding:0px">
                 <ol class="breadcrumb">
                   <li><a href="<?php echo base_url().'data';?>">Data</a></li>
                   <li><a href="<?php echo base_url().'data/material_home';?>">Bank Materi</a></li>
                   <li><a href="<?php echo base_url().'data/material';?>">Materi</a></li>
                   <li class="active"><?php echo ($mode === 'edit' || $mode === 'edit_act') ? 'Edit' : 'Tambah'?> Materi</li>
                 </ol>
             </div>
           </div>

            <div class="panel panel-default">
               <div class="panel-body">
                  <?php echo isset($msg) ? alert_msg($msg) : ''?>

                  <form enctype="multipart/form-data" action="<?php echo $act?>" method="POST">

                    <div class="form-group">
                      <label for="material_categori_id">Kategori</label>
                      <select style="width: 100%;" name="material_categori_id" id="material_categori_id" class="form-control" data-toggle="select2" data-style="btn-white" data-live-search="true" data-size="5" placeholder="Kategori">
                        <?php foreach ($material_categories->result() as $categori) { ?>
                        <option <?php echo $material_categori_id === $categori->id ? 'selected' : '';?> value="<?php echo $categori->id;?>"><?php echo $categori->name?></option>
                        <?php } ?>
                      </select>
                    </div>

                    <div class="form-group form-control-material static required">
                      <input name="title" type="text" id="title" class="form-control" placeholder="Judul" value="<?php echo $title?>">
                      <label for="title">Judul</label>
                    </div>

                    <div class="form-group" id="div_file_task_lama">
                      <?php if($file_attachment === ""){ ?>
                      <label for="end">File Attachment Lama : -- </label>
                      <?php }else{ ?>
                      <label for="end">File Attachment Lama : <a href="<?php echo base_url() . 'upload/' . $file_attachment?>">Download</a></label>
                      <?php } ?>
                    </div>

                    <div class="form-group">
                      <label for="end">File Attachment (*.pdf;*.doc;*.docx;ppt;*.rar;*.zip)</label>
                      <input type="file" name="file_attachment" id="file_attachment">
                    </div>

                     <div class="form-group">
                        <label for="description">Konten</label>
                        <textarea name="content" cols="30" rows="10"><?php echo $content;?></textarea>
                     </div>


                     <hr/>
                     <div class="text-right">
                        <a href="<?php echo base_url().'data/material'?>" class="navbar-btn btn btn-warning"><i class="fa fa-mail-reply"></i> Batal</a>
                        <button type="submit" class="btn btn-primary">Simpan <i class="fa fa-chevron-circle-right"></i></button>
                     </div>
                  </form>
               </div>
            </div>
         </div>
         <div class="col-md-3">
            <div class="panel panel-default" data-toggle="panel-collapse" data-open="true">
               <div class="panel-heading panel-collapse-trigger">
                  <h4 class="panel-title">Menu</h4>
               </div>
               <div class="panel-body list-group">
                  <?php echo main_menu('data');?>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
