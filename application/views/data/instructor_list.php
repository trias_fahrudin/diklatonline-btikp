<div class="parallax overflow-hidden page-section bg-blue-300 page-section third">
    <div class="parallax-layer container" data-opacity="true">
        <div class="media v-middle">
            <div class="media-left">
                <span class="icon-block s60 bg-lightred"><i class="fa fa-user"></i></span>
            </div>
            <div class="media-body">
                <h1 class="text-display-1 text-white margin-none">Data Instruktur</h1>
            </div>
            <div class="media-right">

            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="page-section">
        <div class="row">
            <div class="col-md-9">

              <div class="panel panel-default">
                  <div class="panel-body" style="padding:0px">
                    <ol class="breadcrumb">
                      <li><a href="<?php echo base_url() . 'data';?>">Data</a></li>
                      <li class="active">Data Instruktur</li>
                    </ol>
                </div>
              </div>

              <div class="panel panel-default">
                  <div class="row" style="padding:10px 10px 10px 30px">
                      <div class="col-md-10">
                          <!-- <a href="<?php echo base_url() . 'data/import/instructor'?>" class="btn btn-primary pull-right" role="button"><i class="fa fa-plus-square"></i> Import Data</a> -->
                      </div>
                      <div class="col-md-2">
                          <a href="<?php echo base_url() . 'data/instructor/add'?>" class="btn btn-primary pull-right" role="button"><i class="fa fa-plus-square"></i> Tambah Data</a>
                      </div>
                  </div>

                  <table id="instructor_table" class="table v-middle" cellspacing="0" width="100%">
                      <thead>
                          <tr>
                              <th>Username</th>
                              <th>Nama Lengkap</th>
                              <th>Instansi</th>
                              <th>Keahlian</th>
                              <th class="text-right">Action</th>
                          </tr>
                      </thead>
                      <tfoot>
                          <tr>
                              <th>Username</th>
                              <th>Nama Lengkap</th>
                              <th>Instansi</th>
                              <th>Keahlian</th>
                              <th class="text-right">Action</th>
                          </tr>
                      </tfoot>
                      <tbody>
                          <?php foreach ($instructor->result() as $ins) { ?>
                          <tr>
                              <td><img src="<?php echo base_url() . 'timthumb?src=' . base_url() . 'upload/people/' . $ins->foto . '&h=40&w=40&zc=0'; ?>" class="img-circle" alt="" /> <?php echo $ins->username;?></td>
                              <td><?php echo $ins->fullname;?></td>
                              <td><?php echo $ins->institute;?></td>
                              <td><?php echo $ins->expertise;?></td>
                              <td class="text-right">
                                  <a href="<?php echo base_url() . 'data/instructor/edit/' . $ins->id;?>" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil"></i></a>
                                  <a href="<?php echo base_url() . 'data/instructor/delete/' . $ins->id?>" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" onclick="return confirm('Apakah anda yakin ingin menghapus data ini?');" title="Hapus"><i class="fa fa-times"></i></a>
                              </td>
                          </tr>
                          <?php } ?>
                      </tbody>
                  </table>
              </div>
            </div>
            <div class="col-md-3">
                <div class="panel panel-default" data-toggle="panel-collapse" data-open="true">
                   <div class="panel-heading panel-collapse-trigger">
                      <h4 class="panel-title">Menu</h4>
                   </div>
                   <div class="panel-body list-group">
                        <?php echo main_menu('data');?>
                   </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function() {
  $('#instructor_table').DataTable( {
    
  });
});
</script>
