<div class="main-container">
    <div class="parallax overflow-hidden page-section bg-red-300 page-section third">
        <div class="parallax-layer container" data-opacity="true">
            <div class="media v-middle">
                <div class="media-left">
                    <span class="icon-block s60 bg-lightred"><i class="fa fa-user"></i></span>
                </div>
                <div class="media-body">
                    <h1 class="text-display-1 text-white margin-none">Data Peserta</h1>
                </div>
                <div class="media-right">
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="page-section">
            <div class="row">
                <div class="col-md-9">
                    <!-- http://localhost/diklatonline/data/participator/show/kelas-a -->
                    <?php if(!isset($classroom)){ ?>
                    <div class="panel panel-default">
                        <div class="panel-body" style="padding:0px">
                            <ol class="breadcrumb">
                                <li><a href="<?php echo base_url() . 'data';?>">Data</a>
                                </li>
                                <li class="active">Data Peserta</li>
                            </ol>
                        </div>
                    </div>
                    <?php }?>

                    <div class="panel panel-default">
                        <!-- Data table -->
                        <table id="participator_table" class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Username</th>
                                    <th>Nama Lengkap</th>
                                    <th>Asal Sekolah</th>
                                    <th>Mata Pelajaran</th>

                                    <?php if(is_authorized(array( 'admin'))){ ?>
                                    <th>Pelatihan</th>
                                    <th style="text-align:center">Action</th>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Username</th>
                                    <th>Nama Lengkap</th>
                                    <th>Asal Sekolah</th>
                                    <th>Mata pelajaran</th>

                                    <?php if(is_authorized(array( 'admin'))){ ?>
                                    <th>Pelatihan</th>
                                    <th style="text-align:center">Action</th>
                                    <?php } ?>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php foreach ($participator->result() as $par) { ?>
                                <tr>
                                    <td>
                                      <img src="<?php echo base_url() . 'timthumb?src=' . base_url() . 'upload/people/' . $par->foto . '&h=40&w=40&zc=0'; ?>" class="img-circle" alt="" /><?php echo $par->username;?>
                                    </td>
                                    <td><?php echo ucwords($par->fullname);?></td>
                                    <td><?php echo $par->origin_of_school;?></td>
                                    <td><?php echo $par->mapel;?></td>

                                    <?php if(is_authorized(array( 'admin'))){ ?>
                                    <td>
                                        <?php
                                        $this->db->select("a.id AS id_classroom_user,a.surat_tugas, CONCAT(b.name,' ',b.generation) AS classroom_name, a.classroom_id, a.status, a.register_at");
                                        $this->db->join('classrooms b','a.classroom_id = b.id','left');
                                        $this->db->where('a.user_id',$par->id);
                                        $this->db->order_by('b.start','ASC');
                                        $classroom_user = $this->db->get('classroom_users a');
                                        $classrooms = $this->db->get_where('classrooms');
                                        foreach ($classroom_user->result() as $cu) { ?>

                                        <div classroom_user="<?php echo $cu->id_classroom_user?>" id="classroom_user_<?php echo $cu->id_classroom_user?>" style="margin-bottom:3px">
                                            <a href="" class="btn btn-danger btn-xs delete_classroom_user" data-placement="top"><i class="fa fa-times"></i></a>

                                            <select data-toggle="select2" class="change_classroom">
                                                <?php foreach ($classrooms->result() as $cl) { ?>
                                                <option <?php echo $cl->id == $cu->classroom_id ? 'selected':''?> value="<?php echo $cl->id?>"><?php echo $cl->name . ' ' . $cl->generation?></option>
                                                <?php } ?>
                                            </select>
                                            <!-- <?php echo $cu->classroom_name;?> -->
                                            <label id="label_<?php echo $cu->id_classroom_user?>">

                                            <?php if($cu->status === 'disable'){ ?>

                                            <span class="label label-warning">Pending</span>
                                            <?php if(!empty($cu->surat_tugas)){ ?>
                                              <a href="<?php echo site_url('upload/stugas/' . $cu->surat_tugas)?>" target="_blank"><span class="label label-primary">Surat Tugas</span></a>
                                            <?php } ?>

                                            <?php }else{ ?>

                                            <span class="label label-success">Verified</span>
                                            <?php if(!empty($cu->surat_tugas)){ ?>
                                              <a href="<?php echo site_url('upload/stugas/' . $cu->surat_tugas)?>" target="_blank"><span class="label label-primary">Surat Tugas</span></a>
                                            <?php } ?>

                                            <?php } ?>
                                            <!-- </div> -->
                                            </label>
                                        </div>

                                        <?php } ?>

                                    </td>
                                    <?php } ?>

                                    <?php if(is_authorized(array( 'admin'))){
                                      echo '<td class="text-center">';
                                      echo '    <a href="' . base_url() . 'public_profile/index/' . $par->id .'" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Details"><i class="fa fa-search"></i></a>';
                                      echo ' <a href="' . base_url() . 'data/participator/delete/' . $par->id . '" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" onclick="return confirm(\'Apakah anda yakin ingin menghapus data ini?\');" title="Hapus"><i class="fa fa-times"></i></a>';
                                      echo '</td>';
                                    }?>



                                    <?php if(is_authorized(array( 'instructor'))){ ?>
                                    <td>
                                        <a href="<?php echo base_url() . 'public_profile/index/' . $par->id;?>" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Details"><i class="fa fa-search"></i></a>
                                    </td>
                                    <?php } ?>

                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="panel panel-default" data-toggle="panel-collapse" data-open="true">
                        <div class="panel-heading panel-collapse-trigger">
                            <h4 class="panel-title">Menu</h4>
                        </div>
                        <div class="panel-body list-group">
                            <?php echo main_menu( 'data');?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function() {
        $('#participator_table').DataTable({
            "scrollX": true,
            "bSort": true
        });
    });

    $('.change_classroom').on('change', function() {

        if (confirm('Apakah anda yakin ingin merubah data ini?\n *Status verfikasi kelas baru akan di set pending')) {
            var id = $(this).closest("div").attr("classroom_user");
            var new_classroom_id = $(this).val();

            $.post(base + 'classroom/participator/change_classroom_user', {
                id: id,
                classroom_id: new_classroom_id
            }).done(function(data) {
                var status = data.status;

                if (status === 'changed') {
                    $('#label_' + id).html('<span class="label label-warning">Pending</span>');
                }
            });

        } else {
            location.reload();
        }

        return false;

    });

    $('.delete_classroom_user').on('click', function() {
        if (confirm('Apakah anda yakin ingin menghapus data ini?')) {

            var id = $(this).closest("div").attr("classroom_user");

            $.post(base + 'classroom/participator/delete_classroom_user', {
                id: id
            }).done(function(data) {
                var status = data.status;
                if (status === 'deleted') {
                    $('#classroom_user_' + id).remove();
                }
            });

        }

        return false;
    })
</script>
