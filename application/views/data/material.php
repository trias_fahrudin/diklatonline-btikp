<div class="main-container">
  <div class="parallax overflow-hidden page-section bg-purple-300 page-section third">
      <div class="parallax-layer container" data-opacity="true">
          <div class="media v-middle">
              <div class="media-left">
                  <span class="icon-block s60 bg-lightred"><i class="fa fa-user"></i></span>
              </div>
              <div class="media-body">
                  <h1 class="text-display-1 text-white margin-none">Bank Materi</h1>
              </div>
              <div class="media-right">

              </div>
          </div>
      </div>
  </div>

  <div class="container">
    <div class="page-section">
        <div class="row">
            <div class="col-md-9">

              <div class="panel panel-default">
                <div class="panel-body" style="padding:0px">
                  <ol class="breadcrumb">
                    <li><a href="<?php echo base_url() . 'data';?>">Data</a></li>
                    <li><a href="<?php echo base_url() . 'data/material_home';?>">Bank Materi</a></li>
                    <li class="active">Materi</li>
                  </ol>
                </div>
              </div>

                <div class="panel panel-default">
                    <!-- Data table -->
                    <div class="row" style="padding:10px 10px 10px 30px">
                        <div class="col-md-10">
                            <!-- <a href="<?php echo base_url() . 'data/import/material'?>" class="btn btn-primary pull-right" role="button"><i class="fa fa-download"></i> Import Data</a> -->
                        </div>
                        <div class="col-md-2">
                            <a href="<?php echo base_url() . 'data/material/add'?>" class="btn btn-primary pull-right" role="button"><i class="fa fa-plus-square"></i> Tambah Data</a>
                        </div>
                    </div>

                    <table id="material_table" class="table v-middle" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Kode</th>
                                <th>Kategori</th>
                                <th>Judul</th>
                                <th class="text-right">Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Kode</th>
                                <th>Kategori</th>
                                <th>Judul</th>
                                <th class="text-right">Action</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php foreach ($materials->result() as $m) { ?>
                            <tr>
                                <td><?php echo str_pad($m->id,5,'0',STR_PAD_LEFT)?></td>
                                <td><?php echo $m->categori;?></td>
                                <td><?php echo $m->title;?></td>

                                <td class="text-right">
                                    <a href="<?php echo base_url() . 'data/material/edit/' . $m->id;?>" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil"></i></a>
                                    <a href="<?php echo base_url() . 'data/material/delete/' . $m->id?>" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" onclick="return confirm('Apakah anda yakin ingin menghapus data ini?');" title="Hapus"><i class="fa fa-times"></i></a>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-3">
                <div class="panel panel-default" data-toggle="panel-collapse" data-open="true">
                   <div class="panel-heading panel-collapse-trigger">
                      <h4 class="panel-title">Menu</h4>
                   </div>
                   <div class="panel-body list-group">
                        <?php echo main_menu('data');?>
                   </div>
                </div>
            </div>


        </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function() {
  $('#material_table').DataTable( { } );
});
</script>
