<?php
    $mode = $this->uri->segment(3);

    if($mode === 'edit' || $mode === 'edit_act')
    {
      /*
         'username'       => $this->input->post('username'),
         'fullname'       => $this->input->post('fullname'),
         'telp_number'    => $this->input->post('telp_number'),
         'email'          => $this->input->post('email'),
         'home_address'   => $this->input->post('home_address'),
         'city_of_birth'  => $this->input->post('city_of_birth'),
         'date_of_birth'  => mysqlDate($this->input->post('date_of_birth')),
         'gender'         => $this->input->post('gender'),
         'level'          => 'instructor',
         'nip'            => $this->input->post('nip'),
         'institute'      => $this->input->post('institute'),
         'faculty'        => $this->input->post('faculty'),
         'departement'    => $this->input->post('departement'),
         'prog_studi'     => $this->input->post('prog_studi'),
         'expertise'      => $this->input->post('expertise'),
         'golongan'       => $this->input->post('golongan'),
         'position'       => $this->input->post('position'),
         'last_education' => $this->input->post('last_education')
      */

         $id             = $instructor->id;
         $username       = $mode === 'edit' ? $instructor->username      : set_value('username');
         $fullname       = $mode === 'edit' ? $instructor->fullname      : set_value('fullname');
         $telp_number    = $mode === 'edit' ? $instructor->telp_number   : set_value('telp_number');
         $email          = $mode === 'edit' ? $instructor->email         : set_value('email');
         $home_address   = $mode === 'edit' ? $instructor->home_address  : set_value('home_address');
         $city_of_birth  = $mode === 'edit' ? $instructor->city_of_birth : set_value('city_of_birth');
         $date_of_birth  = $mode === 'edit' ? appDate($instructor->date_of_birth) : set_value('date_of_birth');
         $gender         = $mode === 'edit' ? $instructor->gender        : set_value('gender');
         $nip            = $mode === 'edit' ? $instructor->nip           : set_value('nip');
         $npwp            = $mode === 'edit' ? $instructor->npwp           : set_value('npwp');
         $institute      = $mode === 'edit' ? $instructor->institute     : set_value('institute');
         $expertise      = $mode === 'edit' ? $instructor->expertise     : set_value('expertise');
         $golongan       = $mode === 'edit' ? $instructor->golongan      : set_value('golongan');
         $position       = $mode === 'edit' ? $instructor->position      : set_value('position');
         $last_education = $mode === 'edit' ? $instructor->last_education: set_value('last_education');

         $act = base_url() . 'data/instructor/edit_act/' . $id;

    }else{

         $username       = $mode === 'add' ? '' : set_value('username');
         $fullname       = $mode === 'add' ? '' : set_value('fullname');
         $telp_number    = $mode === 'add' ? '' : set_value('telp_number');
         $email          = $mode === 'add' ? '' : set_value('email');
         $home_address   = $mode === 'add' ? '' : set_value('home_address');
         $city_of_birth  = $mode === 'add' ? '' : set_value('city_of_birth');
         $date_of_birth  = $mode === 'add' ? '' : set_value('date_of_birth');
         $gender         = $mode === 'add' ? '' : set_value('gender');
         $nip            = $mode === 'add' ? '' : set_value('npwp');
         $npwp            = $mode === 'add' ? '' : set_value('nip');
         $institute      = $mode === 'add' ? '' : set_value('institute');
         $expertise      = $mode === 'add' ? '' : set_value('expertise');
         $golongan       = $mode === 'add' ? '' : set_value('golongan');
         $position       = $mode === 'add' ? '' : set_value('position');
         $last_education = $mode === 'add' ? '' : set_value('last_education');

         $act = base_url() . 'data/instructor/add_act';

    }
?>

<div class="parallax overflow-hidden page-section bg-deep-purple-300 page-section third">
   <div class="parallax-layer container" data-opacity="true">
      <div class="media v-middle">
         <div class="media-left">
            <span class="icon-block s60 bg-lightred"><i class="fa fa-user"></i></span>
         </div>
         <div class="media-body">
            <h1 class="text-display-1 text-white margin-none"><?php echo $mode === 'edit' ? 'Edit' : 'Tambah';?> Instruktur</h1>
         </div>
         <div class="media-right">
            <a class="btn btn-white" href="<?php echo base_url() . 'data/instructor'?>">Instruktur</a>
         </div>
      </div>
   </div>
</div>
<div class="container">
   <div class="page-section">
      <div class="row">
         <div class="col-md-9">

           <div class="panel panel-default">
               <div class="panel-body" style="padding:0px">
                 <ol class="breadcrumb">
                   <li><a href="<?php echo base_url() . 'data';?>">Data</a></li>
                   <li><a href="<?php echo base_url() . 'data/instructor';?>">Data Instruktur</a></li>
                   <li class="active"><?php echo ($mode === 'edit' || $mode === 'edit_act') ? 'Edit' : 'Tambah'?> Instruktur</li>
                 </ol>
             </div>
           </div>

            <div class="panel panel-default">
               <div class="panel-body">
                  <?php echo isset($msg) ? alert_msg($msg) : ''?>

                  <form action="<?php echo $act?>" method="POST">
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group form-control-material static">
                              <input type="text" id="username" name="username" placeholder="User Name" class="form-control" <?php echo ($mode === 'edit' || $mode === 'edit_act') ? 'readonly' : ''?> value="<?php echo $username?>" />
                              <label for="username">User Name</label>
                           </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-control-material static required">
                              <input name="fullname" type="text" id="fullname" class="form-control" placeholder="Nama Lengkap" value="<?php echo $fullname?>">
                              <label for="fullname">Nama Lengkap</label>
                            </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-6">
                            <div class="form-group form-control-material static">
                              <input id="telp_number" name="telp_number" type="text" class="form-control" placeholder="Nomor Telephon" value="<?php echo $telp_number?>">
                              <label for="telp_number">Nomor Telephon</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-control-material static">
                              <input id="email" name="email" type="text" class="form-control" value="<?php echo $email?>" placeholder="Alamat Email">
                              <label for="email">Email</label>
                            </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="description">Alamat rumah</label>
                        <textarea name="home_address" cols="30" rows="10"><?php echo $home_address;?></textarea>
                     </div>
                     <hr/>

                     <div class="row">
                        <div class="col-md-4">
                           <div class="form-group form-control-material static">
                              <input type="text" id="city_of_birth" name="city_of_birth" placeholder="Kota Kelahiran" class="form-control" value="<?php echo $city_of_birth;?>" />
                              <label for="city_of_birth">Kota Kelahiran</label>
                           </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group form-control-material static">
                              <input id="date_of_birth" name="date_of_birth" type="text" class="form-control datepicker" value="<?php echo $date_of_birth;?>">
                              <label for="date_of_birth">Tanggal Lahir</label>
                            </div>
                        </div>
                        <div class="col-md-8">
                           <div class="form-group static">
                           <label for="gender">Jenis Kelamin</label>
                              <select name="gender" id="gender" class="form-control" data-toggle="select2" data-style="btn-white" data-live-search="true" data-size="5" placeholder="Jenis Kelamin">
                                  <option <?php echo $gender === 'male' ? 'selected' : '';?> value="male">Laki-laki</option>
                                  <option <?php echo $gender === 'female' ? 'selected' : '';?> value="female">Perempuan</option>
                               </select>

                           </div>
                        </div>
                     </div>

                    <div class="form-group form-control-material static">
                      <input id="nip" name="nip" type="text" class="form-control" value="<?php echo $nip?>" placeholder="Nomor Induk Pegawai">
                      <label for="nip">NIP</label>
                    </div>

                    <div class="form-group form-control-material static">
                      <input id="npwp" name="npwp" type="text" class="form-control" value="<?php echo $npwp?>" placeholder="Nomor Pokok Wajib Pajak">
                      <label for="npwp">NPWP</label>
                    </div>

                    <div class="form-group form-control-material static">
                       <input type="text" id="institute" name="institute" placeholder="Instansi" class="form-control" value="<?php echo $institute;?>" />
                       <label for="institute">Instansi</label>
                    </div>


                    <div class="form-group form-control-material static">
                       <input id="expertise" name="expertise" type="text" class="form-control" value="<?php echo $expertise;?>" placeholder="Keahlian">
                       <label for="expertise">Keahlian</label>
                    </div>

                     <div class="row">
                        <div class="col-md-4">
                            <div class="form-group form-control-material static">
                              <input id="golongan" name="golongan" type="text" class="form-control" value="<?php echo $golongan?>" placeholder="Golongan">
                              <label for="golongan">Golongan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                           <div class="form-group form-control-material static">
                              <input id="position" name="position" type="text" class="form-control" value="<?php echo $position?>" placeholder="Jabatan">
                              <label for="position">Jabatan</label>
                           </div>
                        </div>
                        <div class="col-md-4">
                           <div class="form-group form-control-material static">
                              <input id="last_education" name="last_education" type="text" class="form-control" value="<?php echo $last_education?>" placeholder="Pendidikan Terakhir">
                              <label for="last_education">Pendidikan Terakhir</label>
                           </div>
                        </div>
                     </div>
                     <div class="text-right">
                        <a href="<?php echo base_url() . 'data/instructor'?>" class="navbar-btn btn btn-warning"><i class="fa fa-mail-reply"></i> Batal</a>
                        <button type="submit" class="btn btn-primary">Simpan <i class="fa fa-chevron-circle-right"></i></button>
                     </div>
                  </form>
               </div>
            </div>
         </div>
         <div class="col-md-3">
            <div class="panel panel-default" data-toggle="panel-collapse" data-open="true">
               <div class="panel-heading panel-collapse-trigger">
                  <h4 class="panel-title">Menu</h4>
               </div>
               <div class="panel-body list-group">
                  <?php echo main_menu('data');?>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
