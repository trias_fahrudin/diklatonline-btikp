<!DOCTYPE html>

<html class="transition-navbar-scroll top-navbar-xlarge bottom-footer <?php echo $this->uri->segment(2) === 'quiz-attempt' ? 'app-desktop app-mobile' : '';?>" lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <title><?php echo $this->config->item('diklat_title')?></title>
      <link href="<?php echo base_url() . 'assets/back/';?>css/vendor.min.css" rel="stylesheet">
      <link href="<?php echo base_url() . 'assets/back/';?>css/theme-core.min.css" rel="stylesheet">
      <link href="<?php echo base_url() . 'assets/back/';?>css/module-essentials.min.css" rel="stylesheet" />
      <link href="<?php echo base_url() . 'assets/back/';?>css/module-material.min.css" rel="stylesheet" />
      <link href="<?php echo base_url() . 'assets/back/';?>css/module-layout.min.css" rel="stylesheet" />
      <link href="<?php echo base_url() . 'assets/back/';?>css/module-sidebar.min.css" rel="stylesheet" />
      <link href="<?php echo base_url() . 'assets/back/';?>css/module-sidebar-skins.min.css" rel="stylesheet" />
      <link href="<?php echo base_url() . 'assets/back/';?>css/module-navbar.css" rel="stylesheet" />
      <link href="<?php echo base_url() . 'assets/back/';?>css/module-messages.min.css" rel="stylesheet" />
      <link href="<?php echo base_url() . 'assets/back/';?>css/module-carousel-slick.min.css" rel="stylesheet" />
      <!-- <link href="<?php echo base_url() . 'assets/back/';?>css/module-charts.min.css" rel="stylesheet" /> -->
      <!-- <link href="<?php echo base_url() . 'assets/back/';?>css/module-maps.min.css" rel="stylesheet" /> -->
      <link href="<?php echo base_url() . 'assets/back/';?>css/module-colors-alerts.min.css" rel="stylesheet" />
      <link href="<?php echo base_url() . 'assets/back/';?>css/module-colors-background.min.css" rel="stylesheet" />
      <link href="<?php echo base_url() . 'assets/back/';?>css/module-colors-buttons.min.css" rel="stylesheet" />
      <link href="<?php echo base_url() . 'assets/back/';?>css/module-colors-text.min.css" rel="stylesheet" />

      <link href="<?php echo base_url() . 'assets/back/js/bootstrap-toggle/css/bootstrap-toggle.css';?>" rel="stylesheet" />

      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries
         WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!-- If you don't need support for Internet Explorer <= 8 you can safely remove these -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
      <style type="text/css">

         body{
           background:url("<?php echo base_url() . 'assets/back/images/body_bg.png'?>") left top repeat #f0f0f0
         }

      </style>

      <script src="<?php echo base_url() . 'assets/back/';?>js/vendor-core.min.js"></script>
      <script src="<?php echo base_url() . 'assets/back/';?>js/vendor-tables.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() . 'assets/back/js/bootstrap-toggle/js/bootstrap-toggle.js';?>"></script>

      <script type="text/javascript">var base = "<?php echo base_url()?>";</script>

   </head>
   <body id="scroll-to-here">
     <!-- Code to Display the chat button -->
    <!-- <a href="javascript:void(0)" id="menu-toggle" class="btn-chat btn btn-success">
      <i class="fa fa-comments-o fa-3x"></i>
       <span class="badge progress-bar-danger"></span>
    </a> -->

    <?php $masterpage_header = $this->Basecrud_m->get_where('settings',array('name' => 'masterpage_header'))->row()?>

    <!-- Header -->
    <!-- <header id="top" class="header">
       <div class="text-vertical-center"></div>
    </header> -->

      <!-- Fixed navbar -->
      <div class="navbar navbar-default navbar-fixed-top navbar-size-large navbar-size-xlarge paper-shadow" data-z="0" data-animated role="navigation">
         <div class="container">
            <div class="navbar-header">

               <div class="navbar-brand navbar-brand-logo">
                  <a class="svg" href="<?php echo base_url() . 'participator'?>">
                    <img style="margin-bottom: 10px" src="<?php echo base_url(); ?>upload/<?php echo $masterpage_header->value;?>" height="65" width="750">
                  </a>
               </div>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="main-nav">
               <ul class="nav navbar-nav navbar-nav-margin-left">

               </ul>
               <div class="navbar-right">
                  <ul class="nav navbar-nav navbar-nav-bordered navbar-nav-margin-right">

                     <!-- user -->
                     <li class="dropdown user active">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                           <img src="<?php echo base_url() . 'timthumb?src=' . base_url() . 'upload/people/' . $this->session->userdata('user_foto') . '&h=80&w=80&zc=0'; ?>" alt="" class="img-circle" />
                           <?php echo ucwords($this->session->userdata('user_fullname'));?><span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                           <?php $menu_seg = $this->uri->segment(2) == TRUE ? $this->uri->segment(2) : 'dashboard';?>
                           <li <?php echo $menu_seg === 'dashboard' ? 'class="active"' : ''?>><a href="<?php echo base_url() . 'participator'?>"><i class="fa fa-bar-chart-o"></i> Dashboard</a></li>
                           <li <?php echo $menu_seg === 'classroom' ? 'class="active"' : ''?>><a href="<?php echo base_url() . 'participator/classroom'?>"><i class="fa fa-mortar-board"></i> Kelas</a></li>

                           <li <?php echo $menu_seg === 'profile' ? 'class="active"' : ''?>><a href="<?php echo base_url() . 'profile'?>"><i class="fa fa-user"></i> Profile</a></li>
                           <li><a href="<?php echo base_url() . 'signout'?>"><i class="fa fa-sign-out"></i> Keluar</a></li>
                        </ul>
                     </li>
                     <!-- // END user -->
                  </ul>
                  <a href="<?php echo base_url() . 'signout'?>" class="navbar-btn btn btn-primary">Keluar</a>
               </div>
            </div>
            <!-- /.navbar-collapse -->
         </div>
      </div>

      <!-- main container -->
      <?php $this->load->view($page);?>
      <!-- end main container -->

      <!-- Footer -->
     <!--  <footer class="footer">
         <strong>Learning</strong> v1.0.0 &copy; Copyright 2015
      </footer> -->
      <!-- // Footer -->
      <!-- Inline Script for colors and config objects; used by various external scripts; -->
      <script>
         var colors = {
             "danger-color": "#e74c3c",
             "success-color": "#81b53e",
             "warning-color": "#f0ad4e",
             "inverse-color": "#2c3e50",
             "info-color": "#2d7cb5",
             "default-color": "#6e7882",
             "default-light-color": "#cfd9db",
             "purple-color": "#9D8AC7",
             "mustard-color": "#d4d171",
             "lightred-color": "#e15258",
             "body-bg": "#f6f6f6"
         };
         var config = {
             theme: "html",
             skins: {
                 "default": {
                     "primary-color": "#42a5f5"
                 }
             }
         };
      </script>

      <script src="<?php echo base_url() . 'assets/back/';?>js/vendor-forms.min.js"></script>
      <script src="<?php echo base_url() . 'assets/back/';?>js/vendor-carousel-slick.min.js"></script>
      <script src="<?php echo base_url() . 'assets/back/';?>js/module-essentials.min.js"></script>
      <script src="<?php echo base_url() . 'assets/back/';?>js/module-material.min.js"></script>
      <script src="<?php echo base_url() . 'assets/back/';?>js/module-layout.min.js"></script>
      <script src="<?php echo base_url() . 'assets/back/';?>js/module-sidebar.min.js"></script>
      <script src="<?php echo base_url() . 'assets/back/';?>js/module-carousel-slick.min.js"></script>
      <script src="<?php echo base_url() . 'assets/back/';?>js/theme-core.js"></script>
      <script src="<?php echo base_url() . 'texteditor/tinymce3/tiny_mce.js'?>" type="text/javascript"></script>
      <script src="<?php echo base_url() . 'assets/back/js/jquery.marquee.min.js'?>" type="text/javascript"></script>

      <!-- <script type="text/javascript">var base = "<?php echo base_url()?>";</script> -->
      <!-- User Script -->
      <script src="<?php echo base_url() . 'assets/back/js-user/essential.js'?>" type="text/javascript"></script>



      <script type="text/javascript">
        tinyMCE.init({
            mode : "textareas",
            setup : function(ed)
            {
              // set the editor font size
              ed.onInit.add(function(ed)
              {
                 ed.getBody().style.fontSize = '15px';
              });
            },

            theme : "advanced",
            plugins : "jbimages,safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,inlinepopups,insertdatetime,preview,media,searchreplace,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,autoresize",
            theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect,|,jbimages,emotions,link,unlink,bullist,numlist,blockquote",

            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",

            theme_advanced_resizing : true,
            //Mad File Manager
            relative_urls : false
        });       


      </script>
   </body>
</html>
