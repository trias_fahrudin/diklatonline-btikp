<div class="main-container">
   <div class="parallax overflow-hidden bg-white page-section third">
      <div class="container parallax-layer" data-opacity="true">
         <div class="media v-middle">
            <div class="media-body">
               <h1 class="text-display-2 margin-none">Pilih Kelas</h1>
               <p class="text-light lead">Silahkan pilih kelas yang tersedia dibawah ini.</p>
            </div>
         </div>
      </div>
   </div>
   <div class="container">
      <div class="page-section">
         <div class="row">
           <div class="alert alert-danger" role="alert" style="padding-top: 10px;padding-bottom: 10px;border-bottom-width: 1px;">
             <h4 style="text-align:center;margin-bottom: 0px;">
              * Konfirmasikan kehadiran anda dengan klik tombol konfirmasi kehadiran dibawah <br />
              ** Dimohon untuk mengupload surat tugas jika anda belum melakukannya.

             </h4>
           </div>
            <?php $color_array = array('bg-default','bg-primary','bg-lightred','bg-brown','bg-purple')?>
            <div class="col-md-9">

               <?php

               $i = 0;
               $user_id = $this->session->userdata('user_id');
               $user_level = $this->session->userdata('user_level');

               foreach ($classroom_list->result() as $cl) {

                  $this->db->where('classroom_id',$cl->id);
                  $this->db->where('status',$cl->id);

                  $registerant = $this->db->count_all_results('classroom_users');

                  $classroom_quota_full = false;

                  $is_me_signup_class = $this->Basecrud_m->get_where('classroom_users',array('classroom_id' => $cl->id,'user_id' => $user_id));

                  if($registerant >= $cl->registration_quota && $is_me_signup_class->num_rows() == 0){
                    $classroom_quota_full = true;
                  }

                  // $date_sekarang  = DateTime::createFromFormat('Y-m-d H:i:s',date('Y-m-d H:i:s'));
                  // $date_start = DateTime::createFromFormat('Y-m-d H:i:s',$cl->start);
                  // $date_akhir = DateTime::createFromFormat('Y-m-d H:i:s',$cl->end);
                  //
                  // //http://stackoverflow.com/questions/23284011/use-datetimecreatefromformat-to-get-the-1-day-next-date
                  // $date_akhir->modify('+1 day')->format('Y-m-d H:i:s');
                  // //jika kelas sudah berakhir atau belum mulai, maka jangan ditampilkan
                  // if(($date_sekarang > $date_akhir) || ($date_sekarang < $date_start)) continue;


                ?>

               <div <?php echo $classroom_quota_full ? 'style="opacity: 0.6;"' : ''?> class="panel panel-default paper-shadow" data-z="0.5">
                  <div class="panel-body" id="panel-classroom-participator-<?php echo $cl->id?>" style="display:none">
                    <div class="row" style="padding:10px 10px 10px 30px">
                        <div class="col-md-10">
                            <!-- <a href="<?php echo base_url() . 'data/import/participator'?>" class="btn btn-primary pull-right" role="button"><i class="fa fa-plus-square"></i> Import Data</a> -->
                        </div>
                        <div class="col-md-2">
                            <a href="" onClick="$('#panel-classroom-<?php echo $cl->id?>').show();$('#panel-classroom-participator-<?php echo $cl->id?>').hide();return false" class="btn btn-primary pull-right" role="button"><i class="fa fa-mail-reply"></i> Kembali</a>
                        </div>
                    </div>
                    <table data-toggle="data-table" class="table v-middle" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Username</th>
                                <th>Nama Lengkap</th>
                                <th>Asal Sekolah</th>
                                <th>Mata Pelajaran</th>
                                <th style="text-align:center">Status</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Username</th>
                                <th>Nama Lengkap</th>
                                <th>Asal Sekolah</th>
                                <th>Mata pelajaran</th>
                                <th style="text-align:center">Status</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php
                            $participator = $this->Classroom_m->get_users($cl->id,'participator');
                            foreach ($participator->result() as $par) {
                            ?>
                            <tr>
                                <td><img src="<?php echo base_url() . 'timthumb?src=' . base_url() . 'upload/people/' . $par->foto . '&h=40&w=40&zc=0'; ?>" class="img-circle" alt="" /> <?php echo $par->username;?></td>
                                <td><?php echo ucwords($par->fullname);?></td>
                                <td><?php echo $par->origin_of_school;?></td>
                                <td><?php echo $par->mapel;?></td>
                                <td style="text-align:center"><?php echo $par->status_classroom_users === 'disable' ? '<i class="fa fa-minus-circle"></i> Pending':'<i class="fa fa-check"></i> Peserta'  ;?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                  </div>
                  <div class="panel-body" id="panel-classroom-<?php echo $cl->id?>">
                     <div class="media media-clearfix-xs">
                        <div class="media-left text-center">
                           <div class="cover width-150 width-100pc-xs overlay cover-image-full hover margin-v-0-10">
                              <span class="img icon-block height-130 bg-primary"></span>
                              <span class="overlay overlay-full padding-none icon-block <?php echo $color_array[$i]?>">
                                  <span class="v-center"><i class="fa fa-book"></i></span>
                              </span>
                              <a href="" class="overlay overlay-full overlay-hover overlay-bg-white">
                                <span class="v-center">
                                    <span class="btn btn-circle btn-primary btn-lg"><i class="fa fa-graduation-cap"></i></span>
                                </span>
                              </a>
                           </div>
                        </div>
                        <div class="media-body">
                          <div class="row">
                            <div class="col-md-12">
                              <h4 class="text-headline margin-v-5-0">
                                <!-- <a href="<?php echo base_url() . 'participator/classroom/' . $cl->slug?>" style="color:#42a5f5"> -->
                                  <?php echo ucwords($cl->name . ' ' . $cl->generation)?>
                                <!-- </a> -->
                              </h4>


                              <?php if($classroom_quota_full){ ?>
                              <h1><span class="label label-danger">PENDAFTARAN TELAH DITUTUP</span></h1>
                              <?php } ?>


                              <?php
                              $already_signup_class = $this->Basecrud_m->get_where('classroom_users',array('classroom_id' => $cl->id,'user_id' => $user_id,'status'=> 'disable'));
                              if($already_signup_class->num_rows() > 0){
                                echo '<div class="alert alert-success" role="alert" style="padding-top: 10px;padding-bottom: 10px;border-bottom-width: 1px;">';
                                echo '  <h4 style="text-align:center;margin-bottom: 0px;">Anda telah terdaftar dikelas ini. <br/>Mohon menunggu konfrmasi dari Admin</h4>';
                                echo '</div>';
                              } ?>
                            </div>


                            <div class="col-md-12">
                              <h4 class="text-headline margin-v-5-0">
                              <?php

                              $isAllowed = $this->Classroom_m->isAllowedToEnterClassroom($cl->id,$user_id);

                              if($isAllowed === 'true'){

                                  $date_sekarang  = DateTime::createFromFormat('Y-m-d H:i:s',date('Y-m-d H:i:s'));
                                  $date_start = DateTime::createFromFormat('Y-m-d H:i:s',$cl->start);
                                  $date_akhir = DateTime::createFromFormat('Y-m-d H:i:s',$cl->end);

                                  //http://stackoverflow.com/questions/23284011/use-datetimecreatefromformat-to-get-the-1-day-next-date
                                  // $date_akhir->modify('+1 day')->format('Y-m-d H:i:s');
                                  //jika kelas sudah berakhir atau belum mulai, maka jangan ditampilkan

                                  if($date_sekarang < $date_start){

                                    //jika belum masuk range kelas dibuka
                                    echo '<div class="alert alert-success" role="alert" style="padding-top: 10px;padding-bottom: 10px;border-bottom-width: 1px;">';
                                    echo '  <h4 style="text-align:center;margin-bottom: 0px;">Kelas akan dibuka mulai tanggal : ' . tgl_panjang($cl->start,'lm') .'</h4>';
                                    echo '</div>';

                                    echo '<a href="" onClick="$(\'#panel-classroom-' . $cl->id . '\').hide();$(\'#panel-classroom-participator-' . $cl->id . '\').show();return false" class="btn btn-warning pull-right" style="color:#fff;margin-right:10px"><i class="fa fa-users"></i> List Peserta</a>';
                                    echo '<a href="'. base_url() . 'participator/card_receipts/' . $cl->slug .'" class="btn btn-info pull-right" style="color:#fff;margin-right:10px"><i class="fa fa-credit-card"></i> Tanda Bukti</a>';
                                    // echo '<input class="bootstrap-toogle pull-right" classroomid="' . $cl->kehadiran . '" type="checkbox" ' . ($cl->kehadiran === 'enable' ? 'checked' : '') . 'data-on="HADIR" data-off="TIDAK HADIR">';

                                  }else{

                                    echo '<a href="' . base_url() . 'participator/classroom/' . $cl->slug .'" class="btn btn-success pull-right" style="color:#fff;margin-right:10px">Masuk Kelas <i class="fa fa-play"></i></a>';
                                    echo '<a href="" onClick="$(\'#panel-classroom-' . $cl->id . '\').hide();$(\'#panel-classroom-participator-' . $cl->id . '\').show();return false" class="btn btn-warning pull-right" style="color:#fff;margin-right:10px"><i class="fa fa-users"></i> List Peserta</a>';
                                    echo '<a href="'. base_url() . 'participator/card_receipts/' . $cl->slug .'" class="btn btn-info pull-right" style="color:#fff;margin-right:10px"><i class="fa fa-credit-card"></i> Tanda Bukti</a>';
                                  }

                                }else{

                                    // $already_signup_class = $this->Basecrud_m->get_where('classroom_users',array('classroom_id' => $cl->id,'user_id' => $user_id));
                                    //
                                    // if($already_signup_class->num_rows() == 0){
                                    //   echo '<a href="' . base_url() . 'participator/classroom_signin/' . $cl->slug . '" class="btn btn-success pull-right" style="color:#fff;margin-right:10px">Daftar Kelas <i class="fa fa-play"></i></a>';
                                    // }

                                    echo '<a href="" onClick="$(\'#panel-classroom-' .  $cl->id . '\').hide();$(\'#panel-classroom-participator-' . $cl->id . '\').show();return false" class="btn btn-warning pull-right" style="color:#fff;margin-right:10px"><i class="fa fa-users"></i> List Peserta</a>';
                                } ?>


                              </h4>
                            </div>

                          </div>
                          <!--
                          <?php if($isAllowed === 'true'){ ?>
                          <form>
                            <div class="form-group">
                              <label for="exampleInputFile">* Konfirmasi kehadiran</label>
                              <input id="status-kehadiran" class="bootstrap-toogle form-control" classroomid="<?php echo $cl->id;?>" type="checkbox" <?php echo ($cl->kehadiran === 'ya' ? 'checked' : '');?> data-on="HADIR" data-off="TIDAK HADIR">
                              <p style="padding-top: 2px;padding-bottom: 2px;" class="help-block alert alert-warning">Mohon konfirmasikan kehadiran anda</p>

                            </div>
                          </form>
                          <?php }else{ ?>
                          <form>
                            <div class="form-group">
                              <label for="exampleInputFile">* Konfirmasi kehadiran</label>
                              <input disabled="" id="" class="bootstrap-toogle form-control" classroomid="" type="checkbox" <?php echo ($cl->kehadiran === 'ya' ? 'checked' : '');?> data-on="HADIR" data-off="TIDAK HADIR">
                              <p style="padding-top: 2px;padding-bottom: 2px;" class="help-block alert alert-warning">Anda dapat melakukan konfirmasi kehadiran setelah mendapat konfirmasi dari Admin melalui email</p>

                            </div>
                          </form>

                          <?php }?>
                          -->

                          <form method="post" action="" id="form_surat_tugas_<?php echo $cl->id . '_' . $user_id;?>" enctype="multipart/form-data">
                            <input type="hidden" name="classroom_id" value="<?php echo $cl->id?>" />
                            <input type="hidden" name="user_id" value="<?php echo $user_id?>" />

                            <div class="form-group">
                              <label for="exampleInputFile">** Upload Surat Penugasan</label>
                              <input type="file" id="surat_tugas_<?php echo $cl->id . '_' . $user_id;?>" name="surat_tugas" class="form-control">
                              <p class="help-block">File format yang diijinkan : jpeg,jpg,png atau pdf</p>
                              <?php
                              $surat_tugas = $this->Classroom_m->get_surat_tugas($cl->id,$user_id);
                              if($surat_tugas !== 'not-found'){ ?>
                              <p class="help-block">Status upload : <span class="label label-success">Sudah</span> Silahkan <a href="<?php echo site_url('upload/stugas/' . $surat_tugas)?>" target="_blank">Klik disini</a> untuk melihatnya</p>
                              <?php }else{ ?>
                              <p class="help-block">Status upload : <span class="label label-warning">Belum</span> </p>
                              <?php } ?>
                            </div>

                          </form>
                          <script>
                              $('#surat_tugas_<?php echo $cl->id . '_' . $user_id;?>').change(function() {
                                $('#form_surat_tugas_<?php echo $cl->id . '_' . $user_id;?>').submit();
                              });
                          </script>

                           <hr class="margin-v-8" />
                           <h4>Instruktur</h4>
                           <div class="media v-middle" style="margin-top:0px">
                              <div class="row">
                                 <div class="col-md-12">
                                    <?php
                                    $instructor_list = $this->Classroom_m->get_users($cl->id,'instructor');
                                    foreach ($instructor_list->result() as $nl) { ?>

                                    <div class="col-md-4">
                                       <div class="media-left">
                                          <img src="<?php echo base_url() . 'timthumb?src=' . base_url() . 'upload/people/' . $nl->foto . '&h=30&w=30&zc=0'; ?>" alt="" class="img-circle width-40" />
                                       </div>
                                       <div class="media-body">
                                          <h5 style="margin-bottom:0px;margin-top:0px"><a href="<?php echo base_url() . 'public_profile/index/' . $nl->id?>"><?php echo ucwords($nl->fullname)?></a>
                                             <br/>
                                          </h5>
                                          Instruktur
                                       </div>
                                    </div>

                                    <?php } ?>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>

               </div>
               <?php $i <= 5 ? $i += 1 : $i = 0;} ?>
               <br/>
               <br/>



            </div>
            <!-- right menu -->
            <div class="col-md-3">
               <?php echo main_menu('classroom');?>
            </div>
            <!-- end right menu -->
         </div>
      </div>
   </div>
</div>

<script>
  // <?php foreach ($classroom_list->result() as $cl) { ?>
  // $('#panel-classroom-1').fadeTo('slow',.6);
  // $('#panel-classroom-1').append('<div style="position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;opacity:0.4;filter: alpha(opacity = 50)"></div>');
  // <?php } ?>


  $('.bootstrap-toogle').bootstrapToggle();

  $("#status-kehadiran").change(function() {
    $.post(base + 'participator/update_status_kehadiran', {
      classroom_id: $(this).attr('classroomid'),
      status: $(this).prop('checked') == true ? 'ya':'tidak'
    }).done(function(data) {

    });
  })
</script>
