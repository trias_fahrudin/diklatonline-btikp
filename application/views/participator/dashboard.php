<div class="main-container">
   <div class="parallax overflow-hidden bg-blue-400 page-section third">
      <div class="container parallax-layer" data-opacity="true">
         <div class="media v-middle">
            <div class="media-left text-center">
               <a href="#">
               <img src="<?php echo base_url() . 'timthumb?src=' . base_url() . 'upload/people/' . $this->session->userdata('user_foto') . '&h=80&w=80&zc=0'; ?>" alt="" class="img-circle width-80" />
               </a>
            </div>
            <div class="media-body">
               <h1 class="text-white text-display-1 margin-v-0"><?php echo ucwords($this->session->userdata('user_fullname'));?></h1>
               <p class="text-subhead"><a class="link-white text-underline" href="<?php echo base_url() . 'profile'?>">Lihat Profile</a></p>
            </div>
            <div class="media-right">
               <span class="label bg-blue-500">Peserta</span>
            </div>
         </div>
      </div>
   </div>
   <div class="container">
      <div class="page-section">
         <div class="row">
            <div class="col-md-9">
              
               <div class="row">
                 <div class="item col-xs-12 col-lg-12">
                    <div class="panel panel-default paper-shadow" data-z="0.5">
                       <div class="panel-heading">
                          <h4 class="text-headline margin-none">Kata Pengantar</h4>
                          <p class="text-subhead text-light">Kata pengantar diklat online</p>
                       </div>
                       <div class="panel-body">
                         <?php echo $dashboard_preface; ?>
                       </div>
                    </div>
                 </div>



               </div>
               <br/>
               <br/>
            </div>
            <!-- right menu -->
            <div class="col-md-3">

              <?php echo main_menu('dashboard');?>


            </div>
            <!-- end right menu -->
         </div>
      </div>
   </div>
</div>
