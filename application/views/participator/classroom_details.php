<div class="main-container">
   <div class="parallax bg-white page-section third">
      <div class="container parallax-layer" data-opacity="true">
         <div class="media v-middle media-overflow-visible">
            <div class="media-left">
               <span class="icon-block s60 bg-lightred"><i class="fa fa-book"></i></span>
            </div>
            <div class="media-body">
               <div class="text-display-1 margin-none"><?php echo $classroom->name;?></div>
            </div>
          
         </div>
      </div>
   </div>
   <div class="container">
      <div class="page-section">
         <div class="row">
            <div class="col-md-9">
               <div class="page-section padding-top-none">
                  <div class="media media-grid v-middle">
                     <div class="media-left">
                        <span class="icon-block half bg-blue-300 text-white"><i class="fa fa-bullhorn"></i> </span>
                     </div>
                     <div class="media-body">
                        <h1 class="text-display-1 margin-none">Kata Pengantar</h1>
                     </div>
                  </div>
                  <br/>
                  <p class="text-body-2">
                     <?php echo $classroom->preface;?>
                  </p>
               </div>
               <h5 class="text-subhead-2 text-light">Kurikulum</h5>

               <?php echo $curriculum?>

               <br/>
               <br/>
            </div>
            <!-- right menu -->
            <div class="col-md-3">

               <?php echo menu_kelas($classroom->slug,'dashboard');?>
            </div>
            <!-- end right menu -->
         </div>
      </div>
   </div>
</div>
