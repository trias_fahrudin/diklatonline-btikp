<div class="main-container">
    <div class="parallax bg-white page-section third">
        <div class="parallax-layer container" data-opacity="true">
            <div class="media v-middle">
                <div class="media-left">
                    <span class="icon-block s60 bg-lightred"><i class="fa fa-github"></i></span>
                </div>
                <div class="media-body">
                    <h1 class="text-display-1 margin-none"><?php echo $material->title;?></h1>
                </div>
                <div class="media-right">
                    <a class="btn btn-white" href="">Materi</a>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-8">
                <div class="page-section">
                    <?php echo isset($msg) ? alert_msg($msg) : ''?>
                    <div class="panel panel-default paper-shadow">
                        <div class="panel-body">
                            <?php echo $material->content;?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4">
                <div class="page-section">
                  <?php echo menu_kelas($classroom->slug,'dashboard');?>
                  <!-- <div class="panel panel-default" data-toggle="panel-collapse" data-open="true">
                     <div class="panel-heading panel-collapse-trigger">

                     </div>

                     <div class="panel-body list-group">

                     </div>

                  </div> -->
                    <!-- .panel -->
                    <?php if($material->file_attachment !== ""){ ?>
                    <div class="panel panel-default paper-shadow" data-z="0.5" data-hover-z="1" data-animated>
                        <div class="panel-heading">
                            <h4 class="text-headline">File Materi</h4>
                        </div>
                        <div class="panel-body">
                            <p class="text-caption">
                              <h3><a href="<?php echo base_url() . 'upload/' . $material->file_attachment?>"><i class="fa fa-cloud-download fa-fw"></i>Download</a></h3>

                            </p>
                        </div>
                    </div>
                    <?php } ?>

                    <?php if($course_tasks->num_rows() > 0){ ?>
                    <form id="form_task_file" class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
                      <div class="panel panel-default paper-shadow" data-z="0.5" data-hover-z="1" data-animated>
                          <div class="panel-heading">
                              <h4 class="text-headline">Tugas</h4>
                          </div>
                          <div class="panel-body">
                              <p class="text-caption">
                                  <!-- <i class="fa fa-clock-o fa-fw"></i> 4 hrs &nbsp;-->
                                  <?php $task = $course_tasks->row()?>
                                  <i class="fa fa-file fa-fw"></i>File Tugas : <a href="<?php echo base_url() . 'upload/task_files/' . $task->file ?>">Download</a>
                                  <br/>
                                  <i class="fa fa-calendar fa-fw"></i> Akhir pengiriman <?php echo appDate($task->due_date,false);?>
                                  <br/>
                                  <?php $cek_task = $this->Basecrud_m->get_where('course_task_files',array('course_task_id' => $task->id,'user_id' => $this->session->userdata('user_id')));?>
                                  <?php if($cek_task->num_rows() > 0){ ?>
                                  <span class="label label-primary"><i class="fa fa-check fa-fw"></i> Anda sudah mengirimkan file tugas</span>
                                  <?php }else{?>
                                  <span class="label label-danger"><i class="fa fa-remove fa-fw"></i> Anda belum mengirimkan file tugas</span>
                                  <?php }?>

                                  <p>
                                    <?php echo $task->note;?>
                                  </p>

                              </p>
                          </div>
                          <?php
                            $date_sekarang  = DateTime::createFromFormat('Y-m-d',date('Y-m-d'));
                            $due_date = DateTime::createFromFormat('Y-m-d',$task->due_date);
                            $due_date->modify('+1 day')->format('Y-m-d');
                            if($date_sekarang < $due_date){
                          ?>

                          <hr class="margin-none" />
                          <div class="panel-body text-center">
                              <input type="file" name="task_file" id="task_file">
                          </div>
                          <?php }else{ ?>
                          <div class="panel-body text-center">
                            <span class="label label-danger"><i class="fa fa-remove fa-fw"></i> Batas pengiriman berakhir</span>
                          </div>
                          <?php } ?>
                      </div>
                    </form>
                    <?php } ?>

                    <!-- // END .panel -->

                </div>
                <!-- // END .page-section -->
            </div>
        </div>
    </div>
</div>

<script>
  //upload file task
  $('#task_file').change(function() {
      // select the form and submit
      $('#form_task_file').submit();
  });
</script>
