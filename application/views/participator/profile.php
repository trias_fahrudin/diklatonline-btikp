<div class="main-container">
   <div class="parallax overflow-hidden bg-blue-400 page-section third">
      <div class="container parallax-layer" data-opacity="true">
         <div class="media v-middle">
            <div class="media-left text-center">
               <a href="#">
               <img src="<?php echo base_url() . 'timthumb?src=' . base_url() . 'upload/people/' . $this->session->userdata('user_foto') . '&h=80&w=80&zc=0'; ?>" alt="" class="img-circle width-80" />
			   
               </a>
            </div>
            <div class="media-body">
               <h1 class="text-white text-display-1 margin-v-0"><?php echo ucwords($this->session->userdata('user_fullname'));?></h1>
               <p class="text-subhead"><a class="link-white text-underline" href="<?php echo base_url() . 'profile'?>">Lihat Profile</a></p>
            </div>
            <div class="media-right">
               <span class="label bg-blue-500">Peserta</span>
            </div>
         </div>
      </div>
   </div>
   <div class="container">
      <div class="page-section">
         <div class="row">
            <div class="col-md-9">
               <!-- Tabbable Widget -->
               <div class="tabbable paper-shadow relative" data-z="0.5">
                  <!-- Tabs -->
                  <ul class="nav nav-tabs">
                     <li class="active"><a href="#"><i class="fa fa-fw fa-lock"></i> <span class="hidden-sm hidden-xs">Mengelola Akun</span></a></li>
                  </ul>
                  <!-- // END Tabs -->
                  <!-- Panes -->
                  <div class="tab-content">
                     <div id="account" class="tab-pane active">
                        <div class="alert alert-success" role="alert" id="alert-success" style="display:none">
                           <h4 style="text-align:center;margin-bottom: 0px;">Data Berhasil di Update</h4>
                        </div>
                        <div class="alert alert-danger" role="alert" id="alert-danger" style="display:none">
                           <h4 style="text-align:center;margin-bottom: 0px;">Data Gagal di Update</h4>
                        </div>
                        <form id="form-foto" class="form-horizontal" action="<?php echo base_url() . 'profile/upload_foto'?>" method="POST" enctype="multipart/form-data">
                           <div class="form-group">
                              <label for="inputEmail3" class="col-sm-2 control-label">Foto</label>
                              <div class="col-md-6">
                                 <div class="media v-middle">
                                    <div class="media-left">
                                       <div class="icon-block width-100 bg-grey-100">
                                          <!-- <i class="fa fa-photo text-light"></i> -->
                                          <?php $foto = $this->session->userdata('user_foto');?>
                                          <img src="<?php echo base_url() . 'timthumb?src=' . base_url() . 'upload/people/' . $foto . '&h=100&w=100&zc=0'; ?>" width="100" height="100" alt="Shoe" />
                                       </div>
                                    </div>
                                    <div class="media-body">
                                       <input type="file" name="foto" id="file-foto">
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </form>
						
						
						
						
						
                        <?php $profile = $this->Basecrud_m->get_where('users',array('id' => $this->session->userdata('user_id')))->row()?>
                        <form class="form-horizontal" method="POST">
                           <div class="form-group">
                              <label for="inputEmail3" class="col-md-2 control-label">User Name</label>
                              <div class="col-md-6">
                                 <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                    <input type="text" class="form-control" id="username" placeholder="Username anda" readonly="" value="<?php echo $this->session->userdata('user_name');?>" disabled="">
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <label for="inputEmail3" class="col-md-2 control-label">Nama Lengkap</label>
                              <div class="col-md-6">
                                 <div class="form-control-material required" style="padding-top: 5px;height: 31px;">
                                    <input type="text" class="form-control" id="fullname" placeholder="Nama lengkap anda" value="<?php echo $profile->fullname;?>">
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <label for="inputEmail3" class="col-md-2 control-label">Email</label>
                              <div class="col-md-6">
                                 <div class="form-control-material required" style="padding-top: 5px;height: 31px;">
                                    <div class="input-group">
                                       <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                       <input type="email" class="form-control used" id="email" placeholder="Email" value="<?php echo $profile->email;?>">
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <label for="inputEmail3" class="col-md-2 control-label">Nomor Telephon</label>
                              <div class="col-md-6">
                                 <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                    <input type="text" class="form-control used" id="telp_number" value="<?php echo $profile->telp_number;?>">
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <label for="inputEmail3" class="col-md-2 control-label">Kota Lahir</label>
                              <div class="col-md-6">
                                 <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                    <input type="text" class="form-control used" id="city_of_birth" value="<?php echo $profile->city_of_birth;?>">
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <label for="inputEmail3" class="col-md-2 control-label">Tanggal Lahir</label>
                              <div class="col-md-6">
                                 <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                    <div class="input-group">
                                       <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                       <input type="text" class="form-control datepicker" id="date_of_birth" value="<?php echo appDate($profile->date_of_birth)?>">
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <label for="inputEmail3" class="col-md-2 control-label">Jenis Kelamin</label>
                              <div class="col-md-6">
                                 <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                    <div class="input-group">
                                       <select id="gender" class="form-control" data-style="btn-white" data-live-search="true" data-size="5" placeholder="Jenis Kelamin">
                                          <option <?php echo $profile->gender === 'male' ? 'selected':'';?> value="male">Laki-laki</option>
                                          <option <?php echo $profile->gender === 'female' ? 'selected':'';?> value="female">Perempuan</option>
                                       </select>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <label for="inputEmail3" class="col-md-2 control-label">Alamat</label>
                              <div class="col-md-6">
                                 <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                    <input type="text" class="form-control used" id="home_address" value="<?php echo $profile->home_address;?>">
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <label for="inputEmail3" class="col-md-2 control-label">Status Pegawai</label>
                              <div class="col-md-6">
                                 <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                    <div class="input-group">
                                       <select id="status" class="form-control" data-style="btn-white" data-live-search="true" data-size="5" placeholder="Status PNS">
                                          <option <?php echo $profile->status === 'pns' ? 'selected':'';?> value="pns">PNS</option>
                                          <option <?php echo $profile->status === 'non-pns' ? 'selected':'';?> value="non-pns">NON PNS</option>
                                       </select>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <label for="inputEmail3" class="col-md-2 control-label">NIP</label>
                              <div class="col-md-6">
                                 <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                    <input type="text" class="form-control used" id="nip" value="<?php echo $profile->nip;?>">
                                 </div>
                              </div>
                           </div>
                           <!-- <div class="form-group">
                              <label for="inputEmail3" class="col-md-2 control-label">NPWP</label>
                              <div class="col-md-6">
                                 <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                    <input type="text" class="form-control used" id="npwp" value="<?php echo $profile->npwp;?>">
                                 </div>
                              </div>
                           </div> -->
                           <div class="form-group">
                              <label for="inputEmail3" class="col-md-2 control-label">NUPTK</label>
                              <div class="col-md-6">
                                 <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                    <input type="text" class="form-control used" id="nuptk" value="<?php echo $profile->nuptk;?>">
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <label for="inputEmail3" class="col-md-2 control-label">Asal Sekolah</label>
                              <div class="col-md-6">
                                 <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                    <input type="text" class="form-control used" id="origin_of_school" value="<?php echo $profile->origin_of_school;?>">
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <label for="inputEmail3" class="col-md-2 control-label">Kab/Kota Sekolah</label>
                              <div class="col-md-6">
                                 <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                    <select id="district" class="form-control" data-style="btn-white" data-live-search="true" data-size="5" placeholder="Kab/Kota Sekolah">
                                       <?php $wil = $this->Basecrud_m->get('districts');?>
                                       <?php foreach ($wil->result() as $w) { ?>
                                       <option <?php echo $profile->district === $w->name ? 'selected' : '';?> value="<?php echo $w->name?>"><?php echo $w->name;?></option>
                                       <?php } ?>
                                    </select>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <label for="inputEmail3" class="col-md-2 control-label">Mata Pelajaran</label>
                              <div class="col-md-6">
                                 <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                    <input type="text" class="form-control used" id="mapel" value="<?php echo $profile->mapel;?>">
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <label for="password" class="col-md-2 control-label">Ganti Password</label>
                              <div class="col-md-6">
                                 <div class="form-control-material" style="padding-top: 5px;height: 31px;">
                                    <input type="password" class="form-control" id="password" placeholder="Password">
                                    <!-- <label for="inputPassword3">Password</label> -->
                                 </div>
                              </div>
                           </div>
                           <!-- <div class="form-group">
                              <div class="col-md-offset-2 col-md-6">
                                 <div class="checkbox checkbox-success">
                                    <input id="checkbox3" type="checkbox" checked="">
                                    <label for="checkbox3">Subscribe to our Newsletter</label>
                                 </div>
                              </div>
                              </div> -->
                           <div class="form-group margin-none">
                              <div class="col-md-offset-2 col-md-10">
                                 <button id="cancel-profile" type="button" class="btn btn-warning paper-shadow relative" data-z="0.5" data-hover-z="1" data-animated><i class="fa fa-mail-reply"></i> Batal</button>
                                 <button id="save-profile" type="button" class="btn btn-primary paper-shadow relative" data-z="0.5" data-hover-z="1" data-animated>Simpan <i class="fa fa-chevron-circle-right"></i></button>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
                  <!-- // END Panes -->
               </div>
               <!-- // END Tabbable Widget -->
               <br/>
               <br/>
            </div>
            <!-- right menu -->
            <div class="col-md-3">
               <?php echo main_menu('profile');?>
            </div>
            <!-- end right menu -->
         </div>
      </div>
   </div>
</div>

<script>

  $('#file-foto').change(function() {
      $('#form-foto').submit();
  });


  $("#cancel-profile").click(function() {
  	window.location.replace(base + 'participator');
  });


  $("#save-profile").click(function() {

      $('#alert-success').hide();
      $('#alert-danger').hide();

      var required_check = true;

      $(".required input").each(function(){
          if ($.trim($(this).val()).length === 0){
              $(this).addClass("has-error");

              required_check = false;
              return false;
          }else{
              $(this).parent().removeClass("has-error");
          }
      });

      if(!required_check){
        alert("Mohon isi semua kolom yang diperlukan (*)");
          return false;
      }

      if (!isValidEmailAddress($('#email').val())){
      alert('Alamat email tidak valid!');
      return false;
    }

    $.post( base + 'profile', {
      fullname: $('#fullname').val(),
      email: $('#email').val(),
      telp_number: $('#telp_number').val(),
      city_of_birth: $('#city_of_birth').val(),
      date_of_birth: $('#date_of_birth').val(),
      gender: $('#gender').val(),
      home_address: $('#home_address').val(),
      status: $('#status').val(),
      nip: $('#nip').val(),
      // npwp: $('#npwp').val(),
      nuptk: $('#nuptk').val(),
      origin_of_school: $('#origin_of_school').val(),
      district: $('#district').val(),
      mapel: $('#mapel').val(),
      password : $('#password').val()
    })
     .done(function( data ) {
       if (data.status === 'OK') {
          $('#alert-success').show();
          $('html, body').animate({scrollTop: $("#scroll-to-here").offset().top}, 1000);

          $("#alert-success").fadeTo(5000, 500).slideUp(500, function(){
            //$("#alert-success").alert('close');
        });
       }else{
        $('#alert-danger').show();
        $('html, body').animate({scrollTop: $("#scroll-to-here").offset().top}, 1000);

        $("#alert-danger").fadeTo(5000, 500).slideUp(500, function(){

        });
       }
       return false;
    });
  });
</script>
