
<div class="main-container">
  <div class="parallax bg-white page-section third">
     <div class="parallax-layer container" data-opacity="true">
        <div class="media v-middle">
           <div class="media-left">
              <span class="icon-block s60 bg-lightred"><i class="fa fa-newspaper-o"></i></span>
           </div>
           <div class="media-body">
              <h1 class="text-display-1 margin-none">Ekspresi Pengunjung</h1>
           </div>
           <div class="media-right">
           </div>
        </div>
     </div>
  </div>

	<div class="container">
		<div class="page-section">
				<div class="row">
					<div class="col-md-9">
            <div class="panel panel-default">
								<div class="panel-body" style="padding:0px">
									<ol class="breadcrumb">
										<li><a href="<?php echo base_url().'ekspresi';?>">Ekspresi</a></li>
										<li class="active">Tanggapan</li>
									</ol>
							</div>
						</div>

								<div class="panel panel-default">
									<div class="alert alert-info" style="margin:10px">
										<div class="row">
											<div class="col-md-2"><b><span class="glyphicon glyphicon-user"></span> <?php echo $post->nama;?></b><br><small> Ditulis: <?php echo $post->tgl?> </small></div>
											<div class="col-md-10">
                        <p><h4><b><?php echo $post->judul;?></b></h4></p>
                        <p><?php echo $post->isi_ekspresi;?></p>
                      </div>
										</div>
									</div>

									<?php foreach ($tanggapan->result() as $t) {    ?>
                  <?php     if ($t->tampil === 'Y') {    ?>
                  <div class="alert alert-success" style="margin-left:50px;margin-right:10px">
                    <div class="row">
                      <div class="col-md-2"><b><span class="glyphicon glyphicon-user"></span> <?php echo $t->nama;?></b><br><small> Ditulis: <?php echo $t->tgl?> </small></div>
                      <div class="col-md-10"><p><?php echo $t->komentar;?></p></div>
                      <div class="col-md-12" style="text-align:right">
                        <input class="change_tampil" id="<?php echo $t->id;?>" type="checkbox" checked data-toggle="toggle" data-on="Tampil" data-off="Pending">
                      </div>
                    </div>
                  </div>
                	<?php      } else {  ?>
                  <div class="alert alert-warning" style="margin-left:50px;margin-right:10px">
                    <div class="row">
                      <div class="col-md-2"><b><span class="glyphicon glyphicon-user"></span> <?php echo $t->nama;?></b><br><small> Ditulis: <?php echo $t->tgl?> </small></div>
                      <div class="col-md-10"><p><?php echo $t->komentar;?></p></div>
                      <div class="col-md-12" style="text-align:right">
                        <input class="change_tampil" id="<?php echo $t->id;?>" type="checkbox" data-toggle="toggle" data-on="Tampil" data-off="Pending">
                      </div>
                    </div>
                  </div>
                  <?php     }  ?>
									<?php } ?>

                  <div class="panel panel-default">
                    <h4 style="text-align:center"></i>Tambahkan tanggapan anda: </h4>
                    <div class="panel-body">
                        <div class="page-section padding-top-none">
                          <?php if (isset($msg)) {    ?>
                          <div class='alert alert-danger'><?php echo $msg;    ?></div>
                          <?php } ?>
                          <form method="post" id="f_bukutamu" action="">
                            <textarea id="tinyMCE" name="komentar" rows="5" style="width:98%"></textarea>
                            <div class="text-right">
                                <!-- <input type="submit" value="Kirim" id="tombol" class="btn btn-primary"> -->
                                <input type="submit" style="margin-top:15px;font-weight: bold;" name="btn-post" class="btn btn-primary" value="Kirim">
                            </div>



      										</form>
                        </div>
                    </div>
                  </div>


								</div>
						</div>
						<div class="col-md-3">
								<div class="panel panel-default" data-toggle="panel-collapse" data-open="true">
									 <div class="panel-heading panel-collapse-trigger">
											<h4 class="panel-title">Menu</h4>
									 </div>
									 <div class="panel-body list-group">
												<?php echo main_menu('ekspresi');?>
									 </div>
								</div>
						</div>
				</div>
		</div>
	</div>
</div>

 <script type="text/javascript">
  	$('.change_tampil').change(function() {

  		var id_tanggapan = $(this).attr('id');
  		// var val_tampil = $(this).attr('tampil');
      var val_tampil = $(this).prop('checked') ? 'Y' : 'N';

  		$.post("<?php echo base_url().'ekspresi/manage/reply_set_tampil';?>",
        			{ id: id_tanggapan ,
        			  tampil : val_tampil }
        	).done(function(data){
						location.reload();

        });

  	});
	</script>
