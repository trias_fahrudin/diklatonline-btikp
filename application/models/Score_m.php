<?php

if (!defined("BASEPATH")) {
    exit("No direct script access allowed");
}

class Score_m extends CI_Model
{
    public $limit;
    public $offset;
    public $sort;
    public $order;

    public function __construct()
    {
        parent::__construct();
    }

    public function get($classroom_id){
      $this->db->select("b.fullname,b.foto,
                         a.task_scores,
                         (a.task_scores) AS total");
      $this->db->join("users b","a.user_id = b.id","left");
      $this->db->where('a.classroom_id',$classroom_id);
      return $this->db->get('user_scores a');
    }
}
