<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Basecrud_m extends CI_Model
{
    public $limit;
    public $offset;
    public $sort;
    public $order;
    //public $tbl_name;

    public function __construct()
    {
        parent::__construct();
    }

    public function insert($tbl_name, $data)
    {
        $this->db->insert($tbl_name, $data);

        return $this->db->insert_id();
    }

    public function update($tbl_name, $id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update($tbl_name, $data);

        return $this->db->affected_rows();
    }

    public function delete($tbl_name, $data)
    {
        $this->db->delete($tbl_name, $data);

        return $this->db->affected_rows();
    }

    public function get_in_array($tbl_name, $array)
    {
        $this->db->where_in('id', $array);
        return $this->db->get($tbl_name);
    }

    public function get_not_in_array($tbl_name, $array)
    {
        $this->db->where_not_in('id', $array);
        return $this->db->get($tbl_name);
    }

    public function get_where($tblname, $data, $sort = null, $order = null)
    {
        if ($sort != null && $order != null) {
            $this->db->order_by("$sort", "$order");
        }

        $rs = $this->db->get_where($tblname, $data);

        return $rs;
    }

    public function get_random($tblname,$where, $limit = 6)
    {
        $this->db->limit($limit,0);
        $this->db->order_by('RAND()');
        $rs = $this->db->get_where($tblname, $where);

        return $rs;
    }

    public function get_all($tbl_name, $sort = null, $order = null)
    {
        if ($sort != null && $order != null) {
            $this->db->order_by("$sort", "$order");
        }

        return $this->db->get($tbl_name);
    }

    public function get($tblname, $limit = 666, $sort = null, $order = null)
    {
        if ($sort != null && $order != null) {
            $this->db->order_by("$sort", "$order");
        }

        $rs = $this->db->get($tblname, $limit);

        return $rs;
    }

    /*
    function change_password($old_pass,$new_pass){

        $id =  $this->session->userdata('userid');

        $this->db->where('userpass',md5($old_pass));
        $this->db->where('id',  $id);
        $query = $this->db->get('users');

        if ($query->num_rows() > 0){
            $this->db->flush_cache();

            $data['userpass'] = md5($new_pass);
            $this->db->where('id', $id);
            $this->db->update('users', $data);

            return TRUE;
        }else{
            return FALSE;
        }
    }
    */
}
