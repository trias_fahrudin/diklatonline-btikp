<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Material_m extends CI_Model
{
    public $limit;
    public $offset;
    public $sort;
    public $order;

    public function __construct()
    {
        parent::__construct();
    }

    public function get($mode = null)
    {
        $this->db->select('a.*,b.name as categori');
        $this->db->join('material_categories b','a.material_categori_id = b.id','left');
        

        if ($mode === 'numrows') {
            return $this->db->get('materials_bank a')->num_rows();
        } elseif ($mode === 'pagging') {
            $this->db->order_by($this->sort, $this->order);
            $this->db->limit($this->limit, $this->offset);

            return $this->db->get('materials_bank a');
        } elseif ($mode === 'showall') {
            $this->db->order_by($this->sort, $this->order);

            return $this->db->get('materials_bank a');
        }
    }
}
