<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Course_m extends CI_Model
{
    public $limit;
    public $offset;
    public $sort;
    public $order;

    public function __construct()
    {
        parent::__construct();
    }

    public function update_material_list($course_slug,$material_list){
      $this->db->where('slug', $course_slug);
      $this->db->update('courses', array('material_list' => $material_list));
    }

}
