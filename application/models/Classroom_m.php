<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Classroom_m extends CI_Model
{
    public $limit;
    public $offset;
    public $sort;
    public $order;

    public function __construct()
    {
        parent::__construct();
    }

    //mendapatkan peserta dan instrukur di kelas tertentu yang sudah mendaftar
    public function get_users($classroom_id,$level = 'participator',$status = null){

        $this->db->select('a.user_id,a.status AS status_classroom_users,a.register_at,
                           a.kehadiran,
                           b.*,
                           count(c.id) AS jml_diklat');
        $this->db->join('users b','a.user_id = b.id','left');
        $this->db->join("(SELECT id,status ,user_id
                         FROM classroom_users
			                   WHERE status = 'enable') c",'a.user_id = c.user_id','left');
        $this->db->where('a.classroom_id',$classroom_id);
        $this->db->where('b.level',$level);

        if($status !== null){
            $this->db->where('a.status',$status);
        }

        $this->db->group_by('a.user_id');
        $this->db->order_by('b.username','ASC');

        return $this->db->get('classroom_users a');
    }

    //return user in classroom by list, ex : 1,2,3,4
    public function get_users_list($classroom_id,$level = 'participator',$status = null){

        $this->db->select('a.user_id');
        $this->db->join('users b','a.user_id = b.id','left');
        $this->db->where('a.classroom_id',$classroom_id);
        $this->db->where('level',$level);

        if($status !== null){
            $this->db->where('a.status',$status);
        }


        $this->db->order_by('b.username','ASC');
        $rs = $this->db->get('classroom_users a');

        $result = array();
        foreach ($rs->result() as $r) {
          $result[] = $r->user_id;
        }

        return implode(",",$result);
    }



    public function get_custom($type = null,$slug = null){
        if($type === 'get-by-course-slug'){
          $this->db->select("b.*");
          $this->db->join("classrooms b","a.classroom_id = b.id","left");
          $this->db->where("a.slug",$slug);
          return $this->db->get("courses a");
        }
    }

    /*
      return all active classrooms
    */
    public function get_active($filter_start_date = false,$user_id = null){


      if($user_id !== null){
        $this->db->select('a.*,b.kehadiran');
        $this->db->join('classroom_users b','a.id = b.classroom_id','left');
        $this->db->where('b.user_id',$user_id);
      }else{
        $this->db->select('a.*');
      }

      if($filter_start_date){
          $this->db->where('DATE(`start`) > DATE(NOW())');
      }

      $this->db->where('DATE(`end`) >= DATE(NOW())');

      return $this->db->get('classrooms a');

    }

    public function isAllowedToEnterClassroom($classroom_id,$user_id){

      $rs = $this->db->query("SELECT a.* FROM classroom_users a
                              LEFT JOIN users b ON a.user_id = b.id
                              WHERE a.user_id = $user_id
                                    AND a.classroom_id = $classroom_id
                                    AND b.active = 'y'
                                    AND a.status = 'enable'");
      return $rs->num_rows() > 0 ? 'true' : 'false';

    }

    public function get_by_user($user_id, $level = 'participator')
    {
        $rs = null;

        if ($level !== 'admin' ) {

            $this->db->select('b.*');
            $this->db->join('classrooms b','a.classroom_id = b.id','left');
            $this->db->where('a.user_id',$user_id);
            $rs = $this->db->get('classroom_users a');

        }else {
            $rs = $this->db->get('classrooms');
        }

        return $rs;
    }

    public function get($mode,$classrooms = 'all_classroom')
    {
        $this->db->select('a.id,a.name,slugify(a.name) as name_slug,
                           a.slug,a.preface,a.start,a.end');


        if($classrooms !== 'all_classroom'){
          $this->db->where_in('a.id',$classrooms);
        }

        if ($mode === 'numrows') {
            return $this->db->get('classrooms a')->num_rows();
        } elseif ($mode === 'pagging') {
            $this->db->order_by($this->sort, $this->order);
            $this->db->limit($this->limit, $this->offset);

            return $this->db->get('classrooms a');
        } elseif ($mode === 'showall') {
            $this->db->order_by($this->sort, $this->order);

            return $this->db->get('classrooms a');
        }
    }

    public function get_generation($mode,$classrooms = 'all_classroom',$slug ='')
    {
        $this->db->select('a.id,a.name,a.generation,a.availability,
                           a.slug,a.preface,a.start,a.end');

        $this->db->like('slug',$slug,'after');

        if($classrooms !== 'all_classroom'){
          $this->db->where_in('a.id',$classrooms);
        }

        if ($mode === 'numrows') {
            return $this->db->get('classrooms a')->num_rows();
        } elseif ($mode === 'pagging') {
            $this->db->order_by($this->sort, $this->order);
            $this->db->limit($this->limit, $this->offset);

            return $this->db->get('classrooms a');
        } elseif ($mode === 'showall') {
            $this->db->order_by($this->sort, $this->order);

            return $this->db->get('classrooms a');
        }
    }

    public function get_surat_tugas($classroom_id,$user_id){
      $cu = $this->db->get_where('classroom_users',array('classroom_id' => $classroom_id,'user_id' => $user_id));
      if($cu->num_rows() > 0){
        $cu_rowarray = $cu->row_array();
        if(!empty($cu_rowarray['surat_tugas'])){
          return $cu_rowarray['surat_tugas'];
        }else{
          return 'not-found';
        }

      }else{
        return 'not-found';
      }


    }
}
