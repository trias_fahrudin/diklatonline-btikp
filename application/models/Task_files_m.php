<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Task_files_m extends CI_Model
{
    public $limit;
    public $offset;
    public $sort;
    public $order;

    public function __construct()
    {
        parent::__construct();
    }

    public function get($mode,$classroom_id)
    {
        //pelajaran | materi | nama user | tanggal kirim | file | nilai
        $this->db->select('a.id as task_id,a.checked,c.title as course_title,d.title as materi_title,e.fullname as user_name,a.upload_time,a.file,a.poin');
        $this->db->join('course_tasks b','a.course_task_id = b.id','left');
        $this->db->join('courses c','b.course_id = c.id','left');
        $this->db->join('materials_bank d','b.material_id = d.id','left');
        $this->db->join('users e','a.user_id = e.id','left');
        $this->db->where('c.classroom_id',$classroom_id);

        if ($mode === 'numrows') {
            return $this->db->get('course_task_files a')->num_rows();
        } elseif ($mode === 'pagging') {
            $this->db->order_by($this->sort, $this->order);
            $this->db->limit($this->limit, $this->offset);

            return $this->db->get('course_task_files a');
        } elseif ($mode === 'showall') {
            $this->db->order_by($this->sort, $this->order);

            return $this->db->get('course_task_files a');
        }
    }
}
