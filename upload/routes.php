<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'frontpage';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// $route['public_profile/(:any)'] = 'public_profile/index/$1';
// /**
// ------------------------FORUM---------------------------------------------------
// **/
// $route['forum-category/(:any)'] = 'forum/category/$1';
// $route['forum-category/(:any)/(:any)'] = 'forum/category/$1/$2';
// $route['forum-category/(:any)/p/(:any)'] = 'forum/category/$1/p/$2';
//
// $route['forum-thread/(:any)'] = 'forum/thread/$1';
// $route['forum-thread/(:any)/(:any)'] = 'forum/thread/$1/$2';
//
// /**
// ------------------------BLOG----------------------------------------------------
// **/
// $route['blog-index-post/(:any)'] = 'blog/index/$1';
// $route['blog-index-post/(:any)/(:any)'] = 'blog/index/$1/$2';
//
// $route['blog-index-user/(:any)'] = 'blog/index_user/$1';
// $route['blog-index-user/(:any)/(:any)'] = 'blog/index_user/$1/$2';
//
// $route['blog-detail-post/(:any)'] = 'blog/post_details/$1';
// $route['blog-detail-post/(:any)/(:any)'] = 'blog/post_details/$1/$2';
//
// $route['blog-edit-post/(:any)'] = 'blog/edit_post/$1';
// $route['blog-new-post/(:any)'] = 'blog/new_post/$1';
//
// /**
// ---------------------CLASSROOM--------------------------------------------------
// **/
//
// //http://localhost/diklatonline/classroom/get_materi_and_quiz_details
// $route['classroom/get_materi_and_quiz_details'] = 'classroom/get_materi_and_quiz_details';
//
// $route['classroom/add-course/(:any)'] = 'classroom/course/add/$1';
// $route['classroom/insert-course/(:any)'] = 'classroom/course/add_act/$1';
// $route['classroom/edit-course/(:any)'] = 'classroom/course/edit/$1';
// $route['classroom/update-course/(:any)'] = 'classroom/course/edit_act/$1';
// $route['classroom/remove-course/(:any)'] = 'classroom/course/remove/$1';
//
// //course material
// $route['classroom/rearrange-course-material-list'] = 'classroom/course/rearrange_material_list';
// $route['classroom/add-material/(:any)'] = 'classroom/course/add_material/$1';
// $route['classroom/remove-material/(:any)/(:any)'] = 'classroom/course/add_material/$1/$2';
//
//
// //course->quiz
// $route['classroom/add-quiz/(:any)'] = 'classroom/course/add_quiz/$1';
// $route['classroom/insert-quiz/(:any)'] = 'classroom/course/insert_quiz/$1';
// $route['classroom/edit-quiz/(:any)'] = 'classroom/course/edit_quiz/$1';
// $route['classroom/update-quiz/(:any)'] = 'classroom/course/update_quiz/$1';
// $route['classroom/rearrange-quiz-question'] = 'classroom/course/rearrange_quiz_question';
// $route['classroom/remove-quiz/(:any)/(:any)'] = 'classroom/course/remove_quiz/$1/$2';
//
// $route['classroom/quiz-question-details/(:any)'] = 'classroom/course/quiz_question_details/$1';
// $route['classroom/add-quiz-question/(:any)'] = 'classroom/course/add_quiz_question/$1';
// $route['classroom/remove-quiz-question/(:any)/(:any)'] = 'classroom/course/remove_quiz_question/$1/$2';
//
// $route['classroom/task_files/(:any)'] = 'classroom/course/task_files/$1';
// $route['classroom/task_files_poin'] = 'classroom/course/task_files_poin';
//
// //$route['classroom/recieved_task_files/(:any)'] = 'classroom/recieved_task_files/$1';
// //$route['classroom/recieved_task_files/(:any)/(:any)'] = 'classroom/recieved_task_files/$1/$2';
//
// $route['classroom/participator/(:any)'] = 'classroom/participator/$1';
// $route['classroom/participator/(:any)/(:any)'] = 'classroom/participator/$1/$2';
//
// $route['classroom/instructor/(:any)'] = 'classroom/instructor/$1';
// $route['classroom/instructor/(:any)/(:any)'] = 'classroom/instructor/$1/$2';
//
// $route['classroom/(:any)'] = 'classroom/index/$1';
// $route['classroom/(:any)/(:any)'] = 'classroom/index/$1/$2';
//
//
// /**
// -------------------REPORT-------------------------------------------------------
// **/
// $route['report/(:any)'] = 'report/index/$1';
// $route['report/(:any)/(:any)'] = 'report/index/$1/$2';
//
//
// /**
// --------------------DATA--------------------------------------------------------
// **/
//
// $route['data/instructor'] = 'data/instructor';
// $route['data/instructor/(:any)'] = 'data/instructor/$1';
// $route['data/instructor/(:any)/(:any)'] = 'data/instructor/$1/$2';
//
// $route['data/participator'] = 'data/participator';
// $route['data/participator/(:any)'] = 'data/participator/$1';
// $route['data/participator/(:any)/(:any)'] = 'data/participator/$1/$2';
//
// $route['data/import/(:any)'] = 'data/import/$1';
//
// $route['data/material'] = 'data/material';
// $route['data/material/(:any)'] = 'data/material/$1';
// $route['data/material/(:any)/(:any)'] = 'data/material/$1/$2';
//
// $route['data/material_home'] = 'data/material_home';
// $route['data/material_categories'] = 'data/material_categories';
// $route['data/material_categories/(:any)'] = 'data/material_categories/$1';
// $route['data/material_categories/(:any)/(:any)'] = 'data/material_categories/$1/$2';
//
// $route['data/qbank'] = 'data/qbank';
// $route['data/qbank/(:any)'] = 'data/qbank/$1';
// $route['data/qbank/(:any)/(:any)'] = 'data/qbank/$1/$2';
//
// $route['data/qbank_home'] = 'data/qbank_home';
// $route['data/qbank_categories'] = 'data/qbank_categories';
// $route['data/qbank_categories/(:any)'] = 'data/qbank_categories/$1';
// $route['data/qbank_categories/(:any)/(:any)'] = 'data/qbank_categories/$1/$2';
//
// $route['data/(:any)'] = 'data/index/$1';
// $route['data/(:any)/(:any)'] = 'data/index/$1/$2';
//
//
// /**
// -----------------------------PARTICIPANT----------------------------------------
//  **/
//
// // $route['participator/classroom/(:any)/material/(:any)'] = 'participator/material/$1/$2';
// $route['participator/messages'] = 'messages';
//
// $route['participator/quiz/(:any)'] = 'quiz/index/$1';
// $route['participator/quiz-attempt/(:any)'] = 'quiz/attempt/$1';
// $route['participator/quiz-attempt/(:any)/(:any)'] = 'quiz/attempt/$1/$2';


/**
------------------------------INSTRUCTOR----------------------------------------
**/


/**
-------------------------------ADMIN--------------------------------------------
**/


// $route['admin/list-user-admin'] = 'admin/admin';
// $route['admin/list-user-instructor'] = 'admin/instructor';
// $route['admin/list-user-participator'] = 'admin/participator';
//
// $route['admin/add-instructor'] = 'admin/instructor/add';
// $route['admin/add-participator'] = 'admin/participator/add';
