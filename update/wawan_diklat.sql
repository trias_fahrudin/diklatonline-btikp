-- phpMyAdmin SQL Dump
-- version 4.0.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 03, 2016 at 08:14 AM
-- Server version: 10.1.11-MariaDB
-- PHP Version: 5.4.45

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `wawan_diklat`
--

DELIMITER $$
--
-- Functions
--
DROP FUNCTION IF EXISTS `slugify`$$
$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

DROP TABLE IF EXISTS `ci_sessions`;
CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `ci_sessions`
--

TRUNCATE TABLE `ci_sessions`;
--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('042f0d0a85e55f20f881725fea2ac98db0aaa684', '202.179.184.130', 1456934695, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363836393734333b757365725f6c6576656c7c733a31323a22706172746963697061746f72223b757365725f69647c733a323a223634223b757365725f6e616d657c733a393a225349415244495a414c223b757365725f66756c6c6e616d657c733a393a225349415244495a414c223b757365725f666f746f7c733a33363a2238336666323566656433336537373961386333353962393562666134343933302e6a7067223b636170746368615f776f72647c733a343a2239383230223b),
('0a6ebef9ce37a8397ba76e446a37c55cd2a896bf', '202.179.184.130', 1456918382, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363931353334383b636170746368615f776f72647c733a343a2237363938223b757365725f6c6576656c7c733a31323a22706172746963697061746f72223b757365725f69647c733a323a223734223b757365725f6e616d657c733a31303a22456472692050656e7461223b757365725f66756c6c6e616d657c733a31303a22456472692050656e7461223b757365725f666f746f7c733a31313a226e6f2d666f746f2e706e67223b),
('0bf7f5e64ea05d14f26e90de565077e6f0474226', '202.179.184.130', 1456932035, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363933323033353b636170746368615f776f72647c733a343a2235343336223b),
('0ea060e3e804e6fe80b446a5b1dce6028d9b8f16', '202.179.184.130', 1456895788, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363839353736313b757365725f6c6576656c7c733a31323a22706172746963697061746f72223b757365725f69647c733a323a223635223b757365725f6e616d657c733a31343a223130353034363034313839303031223b757365725f66756c6c6e616d657c733a31373a225379696e7461204b6865667269616e7469223b757365725f666f746f7c733a33363a2236316363633664626631303164383939343736643735356264393030313032302e6a7067223b),
('1341edc7b62112323a64ad0936607af92d243c9b', '202.179.184.130', 1456908043, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363930383034333b),
('184e861a446c4c7c6810c5eefe94624917fcc0b5', '202.179.184.130', 1456889227, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363838393232373b),
('19f99a62cd1f16190d31dd9decfac8942a885935', '202.179.184.130', 1456889104, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363838393130343b),
('242ab6af2a3a340ea3a2f699e15c21553ea2e1e3', '202.179.184.130', 1456920506, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363839313830313b),
('247b12d7e5219461e1669eac83d1d9619cea54a3', '202.179.184.130', 1456933313, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363933333331333b),
('2b50836405d1788e468c4b69db3ca617530e78b0', '202.179.184.130', 1456922989, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363932323638323b),
('2bd69070633899a0279f8768be43118cfae07791', '202.179.184.130', 1456899208, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363839383933393b),
('2d1518f4d85894a2bc5d897cc3edc444bfeb9eb1', '202.179.184.130', 1456967003, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363936363037353b636170746368615f776f72647c733a343a2231343137223b),
('303e83666f97cb553592ff930dd79d328b0b6ecb', '202.179.184.130', 1456933926, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363933333932363b),
('33d9f0a3b65bef1878f6de84c20886b3efc8ccfe', '202.179.184.130', 1456916703, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363931363730333b),
('3452f9f4326029770e89bfb09843324087e53499', '202.179.184.130', 1456896395, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363839363336353b),
('42c9c958443ed7145b5c7079a388bb18cdb5e996', '202.179.184.130', 1456895516, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363839353434333b),
('446b1ff87ed30bda83d13c4515aeac21c95513f8', '202.179.184.130', 1456932642, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363933323634323b),
('49ca991ef08fffa6592e13ed40b829491439d9c9', '202.179.184.130', 1456902131, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363930323133313b),
('529f1fd3d624ac79a57c41a015615dfcf6d9c50b', '202.179.184.130', 1456962732, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363936323733323b636170746368615f776f72647c733a343a2234353131223b),
('5d6b4c639e552c6dff4bf4531967496857f555a4', '202.179.184.130', 1456893567, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363839333536373b),
('5ecb1795582fd8752aa3f02580a17872e13faf07', '202.179.184.130', 1456884534, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363838343533343b),
('63f2f30ec67d5455ba4c7700efd3f4c0abcb6a87', '202.179.184.130', 1456908862, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363930363335383b),
('6b8a3d02b4d98bf9cd8ead795212544764b57295', '202.179.184.130', 1456933704, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363933333730343b),
('730f442b2c1c20e01e897fee6d9462bffe7b0e83', '202.179.184.130', 1456964846, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363936343234353b),
('75fabca5050f5d54242517805dbdc94289c3b182', '202.179.184.130', 1456894289, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363839343238393b),
('7694e56fa76b9130ec0dccfb5d5208276f9eb9c0', '202.179.184.130', 1456898746, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363839383437363b636170746368615f776f72647c733a343a2234303933223b),
('7a252ea87f08db1c9ba18da1d1d9ab82a773b109', '202.179.184.130', 1456960702, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363932373032323b),
('8dbc01ca583baad08adf7d81f44b9e2018f8f448', '202.179.184.130', 1456848582, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363834383536303b),
('9774194bcc8a4b3d5d0ef8b84489a36e4d89944a', '202.179.184.130', 1456909940, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363930393934303b),
('a733c796aa2a13e48b98348ba82ad636cf1adce7', '202.179.184.130', 1456931301, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363933313330313b),
('a850c911a3a4e56b9b6f53d1d93803764d7689f6', '202.179.184.130', 1456956700, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363935363730303b),
('b2059748cda2bdd682b6cf2a413a9a851449661a', '202.179.184.130', 1456870723, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363837303732333b),
('b733836b83f92332aa030dd6a8972f18889bd34f', '202.179.184.130', 1456962741, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363936323734313b636170746368615f776f72647c733a343a2237383939223b),
('c4c320f3c76194660cf2a4daede85222f4b71567', '202.179.184.130', 1456962825, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363932343339383b636170746368615f776f72647c733a343a2237383132223b),
('c9b2593db01a128233933a73c3bce0fbc160398b', '202.179.184.130', 1456848813, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363834383831333b),
('c9daa11d4659f25f6149893ac64ae8546a9d01c7', '202.179.184.130', 1456910979, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363835343235333b636170746368615f776f72647c733a343a2233353230223b),
('d54614fc7d720ab0f057587370b7e61e0a4317ac', '202.179.184.130', 1456927578, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363932373537383b),
('dfa0559db80e889117302254da42c090042b58a5', '202.179.184.130', 1456922164, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363932313637363b),
('e1c1f863a8d60302d34956b69134a4b51f0c8776', '202.179.184.130', 1456856319, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363835363134313b),
('e354296df13e835217c60f3e361fd5bf62a069a9', '202.179.184.130', 1456859680, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363835393638303b),
('ea1716abbb7fd4783878763bb164cbbfde9b06e0', '202.179.184.130', 1456934097, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363933333735343b636170746368615f776f72647c733a343a2233353435223b),
('ed000e36e5a84970c41c85325eaeb3f1314ac394', '202.179.184.130', 1456895430, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363839353430313b),
('f1f380814b86141851b0370c355bdf951253d686', '202.179.184.130', 1456886231, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363838313838363b636170746368615f776f72647c733a343a2232383132223b),
('f4a1a52362adf424b5b62e8976a55659015c6373', '202.179.184.130', 1456935567, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363933353536373b),
('f686b2ad87d81747a560465195d68457705d1508', '202.179.184.130', 1456870664, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363837303636343b),
('f9569ac9b3dce8194e0fed7a8d174600f12fbe1a', '202.179.184.130', 1456866808, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363836363830383b636170746368615f776f72647c733a343a2231303438223b),
('fb34e322128223902d43fe530b35323b8ed1a8c7', '202.179.184.130', 1456962745, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363936323734353b636170746368615f776f72647c733a343a2231333733223b),
('fb3bdbeadab6e670aa5c280bddb595b9ea2e5d33', '202.179.184.130', 1456923349, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363932333137393b);

-- --------------------------------------------------------

--
-- Table structure for table `classrooms`
--

DROP TABLE IF EXISTS `classrooms`;
CREATE TABLE IF NOT EXISTS `classrooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `generation` varchar(200) NOT NULL,
  `registration_quota` tinyint(4) NOT NULL DEFAULT '10',
  `slug` varchar(500) NOT NULL,
  `detail_kegiatan` varchar(200) NOT NULL,
  `narasumber` varchar(200) NOT NULL,
  `peserta` varchar(200) NOT NULL,
  `tempat` varchar(200) NOT NULL,
  `preface` text NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_generation` (`name`,`generation`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='kelas / ruang kelas\r\ngeneration = angkatan' AUTO_INCREMENT=30 ;

--
-- Truncate table before insert `classrooms`
--

TRUNCATE TABLE `classrooms`;
--
-- Dumping data for table `classrooms`
--

INSERT INTO `classrooms` (`id`, `name`, `generation`, `registration_quota`, `slug`, `detail_kegiatan`, `narasumber`, `peserta`, `tempat`, `preface`, `start`, `end`) VALUES
(1, 'Desiminasi Pemanfaatan TIK', 'Angkatan Pertama', 38, 'desiminasi-pemanfaatan-tik-angkatan-pertama', 'Model  Pembelajaran  Terintegrasi TIK', 'Pustekkom  ', 'Guru SD-SMP  ', 'Novita Hotel', '<p>ini adalah kata pengantar <span style="color: #000000; font-family: monospace; font-size: medium; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: pre-wrap; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none;">Lokakarya Pemanfaatan Media Pembelajaran Berbasis TIK untuk Kegiatan belajar mengajar 2015</span></p>', '2016-03-14 00:00:00', '2016-03-17 23:59:59'),
(3, 'Desiminasi Pemanfaatan TIK', 'Angkatan Kedua', 38, 'desiminasi-pemanfaatan-tik-angkatan-kedua', 'Model Pembelajaran Terintegrasi TIK', 'Pustekkom', 'Guru SD-SMP', 'Novita Hotel', '<p>Desiminasi Pemanfaatan TIK Akt II</p>', '2016-04-11 00:00:00', '2016-04-14 23:59:59'),
(4, 'Desiminasi Pemanfaatan TIK', 'Angkatan Ketiga', 38, 'desiminasi-pemanfaatan-tik-angkatan-ketiga', 'Model Pembelajaran Terintegrasi TIK', 'Pustekkom', 'Guru SMA/SMK', 'Novita Hotel', '<p>Desiminasi Pemanfaatan TIK Akt III</p>', '2016-05-02 00:00:00', '2016-05-05 23:59:59'),
(5, 'Desiminasi Pemanfaatan TIK', 'Angkatan Keempat', 38, 'desiminasi-pemanfaatan-tik-angkatan-keempat', 'Model Pembelajaran Terintegrasi TIK', 'Pustekkom', 'Guru SMA/SMK', 'Novita Hotel', '<p>Desiminasi Pemanfaatan TIK Akt IV</p>', '2016-05-23 00:00:00', '2016-05-26 23:59:59'),
(6, 'Desiminasi Pemanfaatan TIK', 'Angkatan Kelima', 38, 'desiminasi-pemanfaatan-tik-angkatan-kelima', 'Manajemen Sekolah Berbasis TIK', 'Pustekkom', 'Kepala Sekolah SD-SMP', 'Novita Hotel', '<p>Desiminasi Pemanfaatan TIK Akt V</p>', '2016-03-14 00:00:00', '2016-03-17 23:59:59'),
(7, 'Desiminasi Pemanfaatan TIK', 'Angkatan Keenam', 38, 'desiminasi-pemanfaatan-tik-angkatan-keenam', 'Manajemen Sekolah Berbasis TIK', 'Pustekkom', 'Kepala Sekolah SD-SMP', 'Novita Hotel', '<p>Desiminasi Pemanfaatan TIK Akt VI</p>', '2016-04-11 00:00:00', '2016-04-14 23:59:59'),
(8, 'Desiminasi Pemanfaatan TIK', 'Angkatan Ketujuh', 38, 'desiminasi-pemanfaatan-tik-angkatan-ketujuh', 'Manajemen Sekolah Berbasis TIK', 'Pustekkom', 'Kepala Sekolah SMA/SMK', 'Novita Hotel', '<p>Desiminasi Pemanfaatan TIK Akt VII</p>', '2016-05-02 00:00:00', '2016-05-05 23:59:59'),
(9, 'Desiminasi Pemanfaatan TIK', 'Angkatan Kedelapan', 38, 'desiminasi-pemanfaatan-tik-angkatan-kedelapan', 'Manajemen Sekolah Berbasis TIK', 'Pustekkom', 'Kepala Sekolah SMA/SMK', 'Novita Hotel', '<p>Desiminasi Pemanfaatan TIK Akt VIII</p>', '2016-05-23 00:00:00', '2016-05-26 23:59:59'),
(10, 'Desiminasi Sinematografi TIK', 'Angkatan Pertama', 38, 'desiminasi-sinematografi-tik-angkatan-pertama', 'Film Dokumenter dan Naskah Media', 'BPMTV Surabaya', 'Guru SD-SMP', 'Novita Hotel', '<p>Desiminasi Sinematografi TIK Akt I</p>', '2016-03-21 00:00:00', '2016-03-24 23:59:59'),
(11, 'Desiminasi Sinematografi TIK', 'Angkatan Kedua', 38, 'desiminasi-sinematografi-tik-angkatan-kedua', 'Film Dokumenter dan Naskah Media', 'BMPTV Surabaya', 'Guru SD-SMP', 'Novita Hotel', '<p>Desiminasi Sinematografi TIK Akt II</p>', '2016-04-18 00:00:00', '2016-04-21 23:59:59'),
(12, 'Desiminasi Sinematografi TIK', 'Angkatan Ketiga', 38, 'desiminasi-sinematografi-tik-angkatan-ketiga', 'Film Dokumenter dan Naskah Media', 'BPMTV Surabaya', 'Guru SMA/SMK', 'Novita Hotel', '<p>Desiminasi Sinematografi TIK Akt III</p>', '2016-03-21 00:00:00', '2016-03-24 23:59:59'),
(13, 'Desiminasi Sinematografi TIK', 'Angkatan Keempat', 38, 'desiminasi-sinematografi-tik-angkatan-keempat', 'Film Dokumenter dan Naskah Media', 'BPMTV Surabaya', 'Guru SMA/SMK', 'Novita Hotel', '<p>Desiminasi Sinematografi TIK Akt IV</p>', '2016-04-18 00:00:00', '2016-04-21 23:59:59'),
(14, 'Desiminasi Sinematografi TIK', 'Angkatan Kelima', 38, 'desiminasi-sinematografi-tik-angkatan-kelima', 'Film Dokumenter dan Naskah Media', 'BPMTV Surabaya', 'Guru SMA/SMK', 'Novita Hotel', '<p>Desiminasi Sinematografi TIK Akt V</p>', '2016-05-09 00:00:00', '2016-05-12 23:59:59'),
(15, 'Desiminasi Sinematografi TIK', 'Angkatan Keenam', 38, 'desiminasi-sinematografi-tik-angkatan-keenam', 'Video Pembelajaran dan Naskah Media', 'BPMTV Surabaya', 'Guru SD-SMP', 'Novita Hotel', '<p>Desiminasi Sinematografi TIK Akt VI</p>', '2016-05-30 00:00:00', '2016-06-02 23:59:59'),
(16, 'Desiminasi Sinematografi TIK', 'Angkatan Ketujuh', 38, 'desiminasi-sinematografi-tik-angkatan-ketujuh', 'Video Pembelajaran dan Naskah Media', 'BPMTV Surabaya', 'Guru SD-SMP', 'Novita Hotel', '<p>Desiminasi Sinematografi TIK Akt VII</p>', '2016-07-25 00:00:00', '2016-07-28 23:59:59'),
(17, 'Desiminasi Sinematografi TIK', 'Angkatan Kedelapan', 38, 'desiminasi-sinematografi-tik-angkatan-kedelapan', 'Video Pembelajaran dan Naskah Media', 'BPMTV Surabaya', 'Guru SMA/SMK', 'Novita Hotel', '<p>Desiminasi Sinematografi TIK Akt VIII</p>', '2016-05-09 00:00:00', '2016-05-12 23:59:59'),
(18, 'Desiminasi Sinematografi TIK', 'Angkatan Sembilan', 38, 'desiminasi-sinematografi-tik-angkatan-sembilan', 'Video Pembelajaran dan Naskah Media', 'BPMTV Surabaya', 'Guru SMA/SMK', 'Novita Hotel', '<p>Desiminasi Sinematografi TIK Akt IX</p>', '2016-05-30 00:00:00', '2016-06-02 23:59:59'),
(19, 'Desiminasi Sinematografi TIK', 'Angkatan Sepuluh', 38, 'desiminasi-sinematografi-tik-angkatan-sepuluh', 'Video Pembelajaran dan Naskah Media', 'BPMTV Surabaya', 'Guru SMA/SMK', 'Novita Hotel', '<p>Desiminasi Sinematografi TIK Akt X</p>', '2016-07-25 00:00:00', '2016-07-28 23:59:59'),
(20, 'Desiminasi Media Pembelajaran TIK', 'Angkatan Pertama', 38, 'desiminasi-media-pembelajaran-tik-angkatan-pertama', 'Animasi dan Naskah Media', 'BPMP Semarang', 'Guru SD-SMP', 'Novita Hotel', '<p>Desiminasi Media Pembelajaran TIK Akt I</p>', '2016-03-28 00:00:00', '2016-03-31 23:59:59'),
(21, 'Desiminasi Media Pembelajaran TIK', 'Angkatan Kedua', 38, 'desiminasi-media-pembelajaran-tik-angkatan-kedua', 'Animasi dan Naskah Media', 'BPMP Semarang', 'Guru SD-SMP', 'Novita Hotel', '<p>Desiminasi Media Pembelajaran TIK Akt II</p>', '2016-04-25 00:00:00', '2016-04-28 23:59:59'),
(22, 'Desiminasi Media Pembelajaran TIK', 'Angkatan Ketiga', 38, 'desiminasi-media-pembelajaran-tik-angkatan-ketiga', 'Animasi dan Naskah Media', 'BPMP Semarang', 'Guru SMA/SMK', 'Novita Hotel', '<p>Desiminasi Media Pembelajaran TIK Akt III</p>', '2016-03-28 00:00:00', '2016-03-31 23:59:59'),
(23, 'Desiminasi Media Pembelajaran TIK', 'Angkatan Keempat', 38, 'desiminasi-media-pembelajaran-tik-angkatan-keempat', 'Animasi dan Naskah Media', 'BPMP Semarang', 'Guru SMA/SMK', 'Novita Hotel', '<p>Desiminasi Media Pembelajaran TIK Akt IV</p>', '2016-04-25 00:00:00', '2016-04-28 23:59:59'),
(24, 'Desiminasi Media Pembelajaran TIK', 'Angkatan Kesepuluh', 38, 'desiminasi-media-pembelajaran-tik-angkatan-kesepuluh', 'Mobile Edukasi & Naskah Media', 'BPMP Semarang', 'Guru SMA/SMK', 'Novita Hotel', '<p>Desiminasi Media Pembelajaran TIK Akt X</p>', '2016-08-01 00:00:00', '2016-08-04 23:59:59'),
(25, 'Desiminasi Media Pembelajaran TIK', 'Angkatan Kelima', 38, 'desiminasi-media-pembelajaran-tik-angkatan-kelima', 'Animasi dan Naskah Media', 'BPMP Semarang', 'Guru SMA/SMK', 'Novita Hotel', '<p>Desiminasi Media Pembelajaran TIK Akt V</p>', '2016-05-16 00:00:00', '2016-05-19 23:59:59'),
(26, 'Desiminasi Media Pembelajaran TIK', 'Angkatan Kesembilan', 38, 'desiminasi-media-pembelajaran-tik-angkatan-kesembilan', 'Mobile Edukasi & Naskah Media', 'BPMP Semarang', 'Guru SMA/SMK', 'Novita Hotel', '<p>Desiminasi Media Pembelajaran TIK Akt IX</p>', '2016-07-18 00:00:00', '2016-07-21 23:59:59'),
(27, 'Desiminasi Media Pembelajaran TIK', 'Angkatan Keenam', 38, 'desiminasi-media-pembelajaran-tik-angkatan-keenam', 'Mobile Edukasi dan Naskah Media', 'BPMP Semarang', 'Guru SD-SMP', 'Novita Hotel', '<p>Desiminasi Media Pembelajaran TIK Akt VI</p>', '2016-07-18 00:00:00', '2016-07-21 23:59:59'),
(28, 'Desiminasi Media Pembelajaran TIK', 'Angkatan Kedelapan', 38, 'desiminasi-media-pembelajaran-tik-angkatan-kedelapan', 'Mobile Edukasi & Naskah Media', 'BPMP Semarang', 'Guru SMA/SMK', 'Novita Hotel', '<p>Desiminasi Media Pembelajaran TIK Akt VIII</p>', '2016-05-16 00:00:00', '2016-05-19 23:59:59'),
(29, 'Desiminasi Media Pembelajaran TIK', 'Angkatan Ketujuh', 38, 'desiminasi-media-pembelajaran-tik-angkatan-ketujuh', 'Mobile Edukasi dan Naskah Media', 'BPMP Semarang', 'Guru SD-SMP', 'Novita Hotel', '<p>Desiminasi Media Pembelajaran TIK Akt VII</p>', '2016-08-01 00:00:00', '2016-08-04 23:59:59');

--
-- Triggers `classrooms`
--
DROP TRIGGER IF EXISTS `classrooms_before_insert`;
DELIMITER //
CREATE TRIGGER `classrooms_before_insert` BEFORE INSERT ON `classrooms`
 FOR EACH ROW BEGIN
	SET NEW.slug = slugify(CONCAT(NEW.name,'-',NEW.generation));
	SET NEW.end = CONCAT(DATE(NEW.end),' 23:59:59');
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `classrooms_before_update`;
DELIMITER //
CREATE TRIGGER `classrooms_before_update` BEFORE UPDATE ON `classrooms`
 FOR EACH ROW BEGIN
	SET NEW.slug = slugify(CONCAT(NEW.name,'-',NEW.generation));
	SET NEW.end = CONCAT(DATE(NEW.end),' 23:59:59');	
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `classroom_users`
--

DROP TABLE IF EXISTS `classroom_users`;
CREATE TABLE IF NOT EXISTS `classroom_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `classroom_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` enum('enable','disable') NOT NULL DEFAULT 'disable',
  `register_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `classroom_id_user_id` (`classroom_id`,`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='murid, guru' AUTO_INCREMENT=93 ;

--
-- Truncate table before insert `classroom_users`
--

TRUNCATE TABLE `classroom_users`;
--
-- Dumping data for table `classroom_users`
--

INSERT INTO `classroom_users` (`id`, `classroom_id`, `user_id`, `status`, `register_at`) VALUES
(2, 1, 4, 'disable', '2016-02-22 07:16:06'),
(63, 1, 62, 'disable', '2016-02-29 13:33:28'),
(64, 3, 63, 'disable', '2016-02-29 14:20:48'),
(65, 1, 64, 'disable', '2016-02-29 20:32:04'),
(66, 1, 65, 'disable', '2016-03-01 00:38:58'),
(67, 3, 66, 'disable', '2016-03-01 01:05:40'),
(68, 1, 67, 'disable', '2016-03-01 01:30:03'),
(69, 3, 68, 'disable', '2016-03-01 04:46:33'),
(70, 5, 69, 'disable', '2016-03-01 05:32:20'),
(71, 1, 70, 'disable', '2016-03-01 06:53:01'),
(72, 13, 70, 'disable', '2016-03-01 09:31:35'),
(73, 21, 70, 'disable', '2016-03-01 09:43:38'),
(74, 1, 71, 'disable', '2016-03-01 13:29:06'),
(75, 22, 64, 'disable', '2016-03-01 22:16:38'),
(76, 20, 72, 'disable', '2016-03-02 02:09:41'),
(77, 20, 73, 'disable', '2016-03-02 08:59:08'),
(78, 22, 73, 'disable', '2016-03-02 09:11:26'),
(79, 5, 74, 'disable', '2016-03-02 10:58:49'),
(80, 1, 75, 'disable', '2016-03-02 11:10:41'),
(81, 13, 74, 'disable', '2016-03-02 11:17:28'),
(82, 25, 74, 'disable', '2016-03-02 11:27:29'),
(83, 20, 76, 'disable', '2016-03-02 13:36:00'),
(84, 22, 77, 'disable', '2016-03-02 15:04:16'),
(85, 22, 78, 'disable', '2016-03-02 15:14:51'),
(86, 1, 79, 'disable', '2016-03-02 15:19:49'),
(87, 22, 80, 'disable', '2016-03-02 15:35:57'),
(88, 10, 67, 'disable', '2016-03-02 15:40:37'),
(90, 1, 3, 'disable', '2016-03-02 23:38:00'),
(91, 1, 82, 'disable', '2016-03-03 00:00:56'),
(92, 20, 82, 'disable', '2016-03-03 00:17:04');

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

DROP TABLE IF EXISTS `courses`;
CREATE TABLE IF NOT EXISTS `courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `classroom_id` int(11) NOT NULL,
  `slug` varchar(500) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `start` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `material_list` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='diklat' AUTO_INCREMENT=4 ;

--
-- Truncate table before insert `courses`
--

TRUNCATE TABLE `courses`;
--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `classroom_id`, `slug`, `title`, `description`, `start`, `end`, `material_list`) VALUES
(1, 1, 'lokakarya-pemanfaatan-media-pembelajaran-berbasis-tik-untuk-kegiatan-belajar-mengajar-2015-angkatan-pertama-pelajaran-hari-pertama', 'pelajaran hari pertama', '<p>Lorem ipsum dolor sit amet, ne duo dicunt adipiscing, duo ei esse novum, facete sanctus vel ut. Error omnium timeam ne sed, cu homero doctus pri, iudico doming sadipscing id usu. Ex latine efficiendi qui, reque philosophia consectetuer at vim. Te homero concludaturque his, iriure commune consetetur mea ex, quaeque fabellas deseruisse ei his. Vim in ferri dictas eirmod.</p>\r\n<p>Cu pro alia facilisi, eos meliore partiendo ex. Id affert posidonium reprimique vix, vix verear scribentur at. Pro dicta populo disputationi eu, nam aliquam ancillae te, sit ex quem ceteros. Ut legere laboramus maiestatis sit, no doming nostrum efficiendi sed. Odio salutatus intellegebat quo in, audire accumsan euripidis eos et, vel eros corpora pertinax ne. An sit oblique menandri, duo wisi quas numquam ad.</p>\r\n<p>An eripuit veritus vel, te pro meliore reprimique. Vocent impetus dolorem ne eam. Sonet adversarium vituperatoribus vel eu, civibus euripidis ad his, at usu sumo populo. Ei prima ridens eos, sit ei semper quaerendum. Sea salutandi conceptam ei, ipsum error labore eam ne, hinc iusto facete duo cu. Sit habemus lucilius ex, bonorum ancillae quaerendum sit ea.</p>\r\n<p>Ut sed expetenda mnesarchum, vel te utroque nominavi. Facer audiam facilisi vim ut. Aliquip dolores omnesque cum an, vim habemus necessitatibus ei. Discere vituperata nec ea, doctus apeirian ocurreret eu nec, nam alienum ocurreret at.</p>\r\n<p>Vim tota iusto ad. Vel id duis altera singulis. Duo nostrud adversarium id, ad per tibique sententiae, errem tritani at eos. Et nonumy vituperata usu, an pro iudico tritani, tantas corpora ius te. An mel oportere postulant laboramus, duo no sadipscing signiferumque.</p>\r\n<p>Ea mel duis scripta urbanitas, ea porro ponderum persequeris usu. Cetero feugiat quaestio vix ad. Mel graeci rationibus id, te has elitr similique. Et meis accusam perpetua his, in eam quodsi labitur pertinax. Mea unum mundi ad, postulant qualisque ius ex.</p>\r\n<p>Nec ut tollit legimus. Te adhuc tibique eos. Ad has facer vitae, pri ei admodum accumsan albucius. Duo amet exerci in, vim noluisse maiestatis complectitur an, ei has eripuit bonorum.</p>\r\n<p>Est eu legere utamur, mei et dicam vocent. Posse prompta definitionem usu ei, at oratio oblique usu. Ad exerci percipit referrentur his, sea an iisque detraxit complectitur. Vix nisl lobortis consequuntur cu, ea saperet singulis urbanitas pri, eum ne molestie conclusionemque. Clita graecis cu ius, mea ancillae atomorum scribentur ex. Quando torquatos et sed, eu tantas quaeque mel. Suscipit nominati eam at.</p>\r\n<p>Inani malorum recusabo mei id, cibo repudiandae ex vix. Idque dignissim cum in. Eu virtute aliquid consequuntur nec, has enim copiosae gloriatur ut, dolor adipiscing ei vix. Ut corpora fabellas sensibus mel, ut sit dolor fierent contentiones. Cum amet apeirian an, qui cu nulla graeci moderatius. No vis putant latine eruditi, atqui inermis disputationi ad eos, qui meis ullamcorper an.</p>\r\n<p>Commune albucius his et, eum at facilisi assentior. Mea agam tempor ei, ex nam assum zril. Graeco torquatos ad sit. Eam et prima mutat. Has ne mutat iudico.</p>', '2016-02-01 00:00:00', '2016-02-01 23:59:59', '2,4,5,6,Q1'),
(2, 1, 'lokakarya-pemanfaatan-media-pembelajaran-berbasis-tik-untuk-kegiatan-belajar-mengajar-2015-angkatan-pertama-pelajaran-hari-kedua', 'pelajaran hari kedua', '<p>Lorem ipsum dolor sit amet, ne duo dicunt adipiscing, duo ei esse novum, facete sanctus vel ut. Error omnium timeam ne sed, cu homero doctus pri, iudico doming sadipscing id usu. Ex latine efficiendi qui, reque philosophia consectetuer at vim. Te homero concludaturque his, iriure commune consetetur mea ex, quaeque fabellas deseruisse ei his. Vim in ferri dictas eirmod.</p>\r\n<p>Cu pro alia facilisi, eos meliore partiendo ex. Id affert posidonium reprimique vix, vix verear scribentur at. Pro dicta populo disputationi eu, nam aliquam ancillae te, sit ex quem ceteros. Ut legere laboramus maiestatis sit, no doming nostrum efficiendi sed. Odio salutatus intellegebat quo in, audire accumsan euripidis eos et, vel eros corpora pertinax ne. An sit oblique menandri, duo wisi quas numquam ad.</p>\r\n<p>An eripuit veritus vel, te pro meliore reprimique. Vocent impetus dolorem ne eam. Sonet adversarium vituperatoribus vel eu, civibus euripidis ad his, at usu sumo populo. Ei prima ridens eos, sit ei semper quaerendum. Sea salutandi conceptam ei, ipsum error labore eam ne, hinc iusto facete duo cu. Sit habemus lucilius ex, bonorum ancillae quaerendum sit ea.</p>\r\n<p>Ut sed expetenda mnesarchum, vel te utroque nominavi. Facer audiam facilisi vim ut. Aliquip dolores omnesque cum an, vim habemus necessitatibus ei. Discere vituperata nec ea, doctus apeirian ocurreret eu nec, nam alienum ocurreret at.</p>\r\n<p>Vim tota iusto ad. Vel id duis altera singulis. Duo nostrud adversarium id, ad per tibique sententiae, errem tritani at eos. Et nonumy vituperata usu, an pro iudico tritani, tantas corpora ius te. An mel oportere postulant laboramus, duo no sadipscing signiferumque.</p>\r\n<p>Ea mel duis scripta urbanitas, ea porro ponderum persequeris usu. Cetero feugiat quaestio vix ad. Mel graeci rationibus id, te has elitr similique. Et meis accusam perpetua his, in eam quodsi labitur pertinax. Mea unum mundi ad, postulant qualisque ius ex.</p>\r\n<p>Nec ut tollit legimus. Te adhuc tibique eos. Ad has facer vitae, pri ei admodum accumsan albucius. Duo amet exerci in, vim noluisse maiestatis complectitur an, ei has eripuit bonorum.</p>\r\n<p>Est eu legere utamur, mei et dicam vocent. Posse prompta definitionem usu ei, at oratio oblique usu. Ad exerci percipit referrentur his, sea an iisque detraxit complectitur. Vix nisl lobortis consequuntur cu, ea saperet singulis urbanitas pri, eum ne molestie conclusionemque. Clita graecis cu ius, mea ancillae atomorum scribentur ex. Quando torquatos et sed, eu tantas quaeque mel. Suscipit nominati eam at.</p>\r\n<p>Inani malorum recusabo mei id, cibo repudiandae ex vix. Idque dignissim cum in. Eu virtute aliquid consequuntur nec, has enim copiosae gloriatur ut, dolor adipiscing ei vix. Ut corpora fabellas sensibus mel, ut sit dolor fierent contentiones. Cum amet apeirian an, qui cu nulla graeci moderatius. No vis putant latine eruditi, atqui inermis disputationi ad eos, qui meis ullamcorper an.</p>\r\n<p>Commune albucius his et, eum at facilisi assentior. Mea agam tempor ei, ex nam assum zril. Graeco torquatos ad sit. Eam et prima mutat. Has ne mutat iudico.</p>', '2016-02-02 00:00:00', '2016-02-02 23:59:59', 'Q2,4,6,5,3,2,1'),
(3, 2, 'desiminasi-pemanfaatan-tik-angkatan-kedua-pelajaran-hari-pertama', 'pelajaran hari pertama', '<p>deskripsi pelajaran hari pertama</p>', '2016-02-01 00:00:00', '2016-02-02 23:59:59', '3,4,5,6,2,1');

--
-- Triggers `courses`
--
DROP TRIGGER IF EXISTS `courses_before_insert`;
DELIMITER //
CREATE TRIGGER `courses_before_insert` BEFORE INSERT ON `courses`
 FOR EACH ROW BEGIN
	SELECT slug INTO @classroom_slug FROM classrooms WHERE id = NEW.classroom_id;
	SET NEW.slug = CONCAT(@classroom_slug ,'-',slugify(NEW.title));
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `courses_before_update`;
DELIMITER //
CREATE TRIGGER `courses_before_update` BEFORE UPDATE ON `courses`
 FOR EACH ROW BEGIN	
	SELECT slug INTO @classroom_slug FROM classrooms WHERE id = NEW.classroom_id;
	SET NEW.slug = CONCAT(@classroom_slug ,'-',slugify(NEW.title));
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `course_tasks`
--

DROP TABLE IF EXISTS `course_tasks`;
CREATE TABLE IF NOT EXISTS `course_tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `material_id` int(11) NOT NULL,
  `note` text NOT NULL,
  `file` varchar(50) NOT NULL,
  `due_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='file tugas / pekerjaan rumah yang dikirim oleh peserta' AUTO_INCREMENT=6 ;

--
-- Truncate table before insert `course_tasks`
--

TRUNCATE TABLE `course_tasks`;
--
-- Dumping data for table `course_tasks`
--

INSERT INTO `course_tasks` (`id`, `course_id`, `material_id`, `note`, `file`, `due_date`) VALUES
(4, 1, 2, '<p>test ajah</p>', 'da30578720ea611bca49e18fac6dc8e0.pdf', '2016-02-11'),
(5, 3, 3, '<p>test ajah</p>', '43f69834ff75c1bf18df4608605f46d5.pdf', '2016-02-13');

-- --------------------------------------------------------

--
-- Table structure for table `course_task_files`
--

DROP TABLE IF EXISTS `course_task_files`;
CREATE TABLE IF NOT EXISTS `course_task_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_task_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `file` varchar(50) NOT NULL,
  `checked` enum('y','n') NOT NULL DEFAULT 'n',
  `poin` smallint(6) NOT NULL DEFAULT '0',
  `upload_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `course_task_id_user_id` (`course_task_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='tabel tempat peserta mengirimkan tugas' AUTO_INCREMENT=1 ;

--
-- Truncate table before insert `course_task_files`
--

TRUNCATE TABLE `course_task_files`;
-- --------------------------------------------------------

--
-- Table structure for table `districts`
--

DROP TABLE IF EXISTS `districts`;
CREATE TABLE IF NOT EXISTS `districts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='kabupaten / kota' AUTO_INCREMENT=12 ;

--
-- Truncate table before insert `districts`
--

TRUNCATE TABLE `districts`;
--
-- Dumping data for table `districts`
--

INSERT INTO `districts` (`id`, `name`) VALUES
(1, 'KOTA JAMBI'),
(2, 'KOTA SUNGAI PENUH'),
(3, 'KAB. BATANGHARI'),
(4, 'KAB. BUNGO'),
(5, 'KAB. KERINCI'),
(6, 'KAB. MERANGIN'),
(7, 'KAB. MUARA JAMBI'),
(8, 'KAB. SAROLANGUN'),
(9, 'KAB. TAMBUNG JABUNG BARAT'),
(10, 'KAB. TANJUNG JABUNG TIMUR'),
(11, 'KAB. TEBO');

-- --------------------------------------------------------

--
-- Table structure for table `materials_bank`
--

DROP TABLE IF EXISTS `materials_bank`;
CREATE TABLE IF NOT EXISTS `materials_bank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `material_categori_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `slug` varchar(500) NOT NULL,
  `content` text NOT NULL,
  `file_attachment` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Truncate table before insert `materials_bank`
--

TRUNCATE TABLE `materials_bank`;
--
-- Dumping data for table `materials_bank`
--

INSERT INTO `materials_bank` (`id`, `material_categori_id`, `title`, `slug`, `content`, `file_attachment`) VALUES
(1, 1, 'materi 1 pada kategori materi 1', 'materi-1-pada-kategori-materi-1-362896e2-d33f-11e5-877b-20863ea37a79', '<p>Lorem ipsum dolor sit amet, ne duo dicunt adipiscing, duo ei esse novum, facete sanctus vel ut. Error omnium timeam ne sed, cu homero doctus pri, iudico doming sadipscing id usu. Ex latine efficiendi qui, reque philosophia consectetuer at vim. Te homero concludaturque his, iriure commune consetetur mea ex, quaeque fabellas deseruisse ei his. Vim in ferri dictas eirmod.</p>\r\n<p>Cu pro alia facilisi, eos meliore partiendo ex. Id affert posidonium reprimique vix, vix verear scribentur at. Pro dicta populo disputationi eu, nam aliquam ancillae te, sit ex quem ceteros. Ut legere laboramus maiestatis sit, no doming nostrum efficiendi sed. Odio salutatus intellegebat quo in, audire accumsan euripidis eos et, vel eros corpora pertinax ne. An sit oblique menandri, duo wisi quas numquam ad.</p>\r\n<p>An eripuit veritus vel, te pro meliore reprimique. Vocent impetus dolorem ne eam. Sonet adversarium vituperatoribus vel eu, civibus euripidis ad his, at usu sumo populo. Ei prima ridens eos, sit ei semper quaerendum. Sea salutandi conceptam ei, ipsum error labore eam ne, hinc iusto facete duo cu. Sit habemus lucilius ex, bonorum ancillae quaerendum sit ea.</p>\r\n<p>Ut sed expetenda mnesarchum, vel te utroque nominavi. Facer audiam facilisi vim ut. Aliquip dolores omnesque cum an, vim habemus necessitatibus ei. Discere vituperata nec ea, doctus apeirian ocurreret eu nec, nam alienum ocurreret at.</p>\r\n<p>Vim tota iusto ad. Vel id duis altera singulis. Duo nostrud adversarium id, ad per tibique sententiae, errem tritani at eos. Et nonumy vituperata usu, an pro iudico tritani, tantas corpora ius te. An mel oportere postulant laboramus, duo no sadipscing signiferumque.</p>\r\n<p>Ea mel duis scripta urbanitas, ea porro ponderum persequeris usu. Cetero feugiat quaestio vix ad. Mel graeci rationibus id, te has elitr similique. Et meis accusam perpetua his, in eam quodsi labitur pertinax. Mea unum mundi ad, postulant qualisque ius ex.</p>\r\n<p>Nec ut tollit legimus. Te adhuc tibique eos. Ad has facer vitae, pri ei admodum accumsan albucius. Duo amet exerci in, vim noluisse maiestatis complectitur an, ei has eripuit bonorum.</p>\r\n<p>Est eu legere utamur, mei et dicam vocent. Posse prompta definitionem usu ei, at oratio oblique usu. Ad exerci percipit referrentur his, sea an iisque detraxit complectitur. Vix nisl lobortis consequuntur cu, ea saperet singulis urbanitas pri, eum ne molestie conclusionemque. Clita graecis cu ius, mea ancillae atomorum scribentur ex. Quando torquatos et sed, eu tantas quaeque mel. Suscipit nominati eam at.</p>\r\n<p>Inani malorum recusabo mei id, cibo repudiandae ex vix. Idque dignissim cum in. Eu virtute aliquid consequuntur nec, has enim copiosae gloriatur ut, dolor adipiscing ei vix. Ut corpora fabellas sensibus mel, ut sit dolor fierent contentiones. Cum amet apeirian an, qui cu nulla graeci moderatius. No vis putant latine eruditi, atqui inermis disputationi ad eos, qui meis ullamcorper an.</p>\r\n<p>Commune albucius his et, eum at facilisi assentior. Mea agam tempor ei, ex nam assum zril. Graeco torquatos ad sit. Eam et prima mutat. Has ne mutat iudico.</p>', ''),
(2, 1, 'materi 2 pada kategori materi 1', 'materi-2-pada-kategori-materi-1-a57dc4e2-c8d8-11e5-a06a-8b0d9ff5efcd', '<p>Lorem ipsum dolor sit amet, ne duo dicunt adipiscing, duo ei esse novum, facete sanctus vel ut. Error omnium timeam ne sed, cu homero doctus pri, iudico doming sadipscing id usu. Ex latine efficiendi qui, reque philosophia consectetuer at vim. Te homero concludaturque his, iriure commune consetetur mea ex, quaeque fabellas deseruisse ei his. Vim in ferri dictas eirmod.</p>\r\n<p>Cu pro alia facilisi, eos meliore partiendo ex. Id affert posidonium reprimique vix, vix verear scribentur at. Pro dicta populo disputationi eu, nam aliquam ancillae te, sit ex quem ceteros. Ut legere laboramus maiestatis sit, no doming nostrum efficiendi sed. Odio salutatus intellegebat quo in, audire accumsan euripidis eos et, vel eros corpora pertinax ne. An sit oblique menandri, duo wisi quas numquam ad.</p>\r\n<p>An eripuit veritus vel, te pro meliore reprimique. Vocent impetus dolorem ne eam. Sonet adversarium vituperatoribus vel eu, civibus euripidis ad his, at usu sumo populo. Ei prima ridens eos, sit ei semper quaerendum. Sea salutandi conceptam ei, ipsum error labore eam ne, hinc iusto facete duo cu. Sit habemus lucilius ex, bonorum ancillae quaerendum sit ea.</p>\r\n<p>Ut sed expetenda mnesarchum, vel te utroque nominavi. Facer audiam facilisi vim ut. Aliquip dolores omnesque cum an, vim habemus necessitatibus ei. Discere vituperata nec ea, doctus apeirian ocurreret eu nec, nam alienum ocurreret at.</p>\r\n<p>Vim tota iusto ad. Vel id duis altera singulis. Duo nostrud adversarium id, ad per tibique sententiae, errem tritani at eos. Et nonumy vituperata usu, an pro iudico tritani, tantas corpora ius te. An mel oportere postulant laboramus, duo no sadipscing signiferumque.</p>\r\n<p>Ea mel duis scripta urbanitas, ea porro ponderum persequeris usu. Cetero feugiat quaestio vix ad. Mel graeci rationibus id, te has elitr similique. Et meis accusam perpetua his, in eam quodsi labitur pertinax. Mea unum mundi ad, postulant qualisque ius ex.</p>\r\n<p>Nec ut tollit legimus. Te adhuc tibique eos. Ad has facer vitae, pri ei admodum accumsan albucius. Duo amet exerci in, vim noluisse maiestatis complectitur an, ei has eripuit bonorum.</p>\r\n<p>Est eu legere utamur, mei et dicam vocent. Posse prompta definitionem usu ei, at oratio oblique usu. Ad exerci percipit referrentur his, sea an iisque detraxit complectitur. Vix nisl lobortis consequuntur cu, ea saperet singulis urbanitas pri, eum ne molestie conclusionemque. Clita graecis cu ius, mea ancillae atomorum scribentur ex. Quando torquatos et sed, eu tantas quaeque mel. Suscipit nominati eam at.</p>\r\n<p>Inani malorum recusabo mei id, cibo repudiandae ex vix. Idque dignissim cum in. Eu virtute aliquid consequuntur nec, has enim copiosae gloriatur ut, dolor adipiscing ei vix. Ut corpora fabellas sensibus mel, ut sit dolor fierent contentiones. Cum amet apeirian an, qui cu nulla graeci moderatius. No vis putant latine eruditi, atqui inermis disputationi ad eos, qui meis ullamcorper an.</p>\r\n<p>Commune albucius his et, eum at facilisi assentior. Mea agam tempor ei, ex nam assum zril. Graeco torquatos ad sit. Eam et prima mutat. Has ne mutat iudico.</p>', ''),
(3, 1, 'materi 3 pada kategori materi 1', 'materi-3-pada-kategori-materi-1-af13857a-c8d8-11e5-a06a-8b0d9ff5efcd', '<p>Lorem ipsum dolor sit amet, ne duo dicunt adipiscing, duo ei esse novum, facete sanctus vel ut. Error omnium timeam ne sed, cu homero doctus pri, iudico doming sadipscing id usu. Ex latine efficiendi qui, reque philosophia consectetuer at vim. Te homero concludaturque his, iriure commune consetetur mea ex, quaeque fabellas deseruisse ei his. Vim in ferri dictas eirmod.</p>\r\n<p>Cu pro alia facilisi, eos meliore partiendo ex. Id affert posidonium reprimique vix, vix verear scribentur at. Pro dicta populo disputationi eu, nam aliquam ancillae te, sit ex quem ceteros. Ut legere laboramus maiestatis sit, no doming nostrum efficiendi sed. Odio salutatus intellegebat quo in, audire accumsan euripidis eos et, vel eros corpora pertinax ne. An sit oblique menandri, duo wisi quas numquam ad.</p>\r\n<p>An eripuit veritus vel, te pro meliore reprimique. Vocent impetus dolorem ne eam. Sonet adversarium vituperatoribus vel eu, civibus euripidis ad his, at usu sumo populo. Ei prima ridens eos, sit ei semper quaerendum. Sea salutandi conceptam ei, ipsum error labore eam ne, hinc iusto facete duo cu. Sit habemus lucilius ex, bonorum ancillae quaerendum sit ea.</p>\r\n<p>Ut sed expetenda mnesarchum, vel te utroque nominavi. Facer audiam facilisi vim ut. Aliquip dolores omnesque cum an, vim habemus necessitatibus ei. Discere vituperata nec ea, doctus apeirian ocurreret eu nec, nam alienum ocurreret at.</p>\r\n<p>Vim tota iusto ad. Vel id duis altera singulis. Duo nostrud adversarium id, ad per tibique sententiae, errem tritani at eos. Et nonumy vituperata usu, an pro iudico tritani, tantas corpora ius te. An mel oportere postulant laboramus, duo no sadipscing signiferumque.</p>\r\n<p>Ea mel duis scripta urbanitas, ea porro ponderum persequeris usu. Cetero feugiat quaestio vix ad. Mel graeci rationibus id, te has elitr similique. Et meis accusam perpetua his, in eam quodsi labitur pertinax. Mea unum mundi ad, postulant qualisque ius ex.</p>\r\n<p>Nec ut tollit legimus. Te adhuc tibique eos. Ad has facer vitae, pri ei admodum accumsan albucius. Duo amet exerci in, vim noluisse maiestatis complectitur an, ei has eripuit bonorum.</p>\r\n<p>Est eu legere utamur, mei et dicam vocent. Posse prompta definitionem usu ei, at oratio oblique usu. Ad exerci percipit referrentur his, sea an iisque detraxit complectitur. Vix nisl lobortis consequuntur cu, ea saperet singulis urbanitas pri, eum ne molestie conclusionemque. Clita graecis cu ius, mea ancillae atomorum scribentur ex. Quando torquatos et sed, eu tantas quaeque mel. Suscipit nominati eam at.</p>\r\n<p>Inani malorum recusabo mei id, cibo repudiandae ex vix. Idque dignissim cum in. Eu virtute aliquid consequuntur nec, has enim copiosae gloriatur ut, dolor adipiscing ei vix. Ut corpora fabellas sensibus mel, ut sit dolor fierent contentiones. Cum amet apeirian an, qui cu nulla graeci moderatius. No vis putant latine eruditi, atqui inermis disputationi ad eos, qui meis ullamcorper an.</p>\r\n<p>Commune albucius his et, eum at facilisi assentior. Mea agam tempor ei, ex nam assum zril. Graeco torquatos ad sit. Eam et prima mutat. Has ne mutat iudico.</p>', ''),
(4, 2, 'materi 1 pada kategori materi 2', 'materi-1-pada-kategori-materi-2-ed2bfd85-c8d8-11e5-a06a-8b0d9ff5efcd', '<p>Lorem ipsum dolor sit amet, ne duo dicunt adipiscing, duo ei esse novum, facete sanctus vel ut. Error omnium timeam ne sed, cu homero doctus pri, iudico doming sadipscing id usu. Ex latine efficiendi qui, reque philosophia consectetuer at vim. Te homero concludaturque his, iriure commune consetetur mea ex, quaeque fabellas deseruisse ei his. Vim in ferri dictas eirmod.</p>\r\n<p>Cu pro alia facilisi, eos meliore partiendo ex. Id affert posidonium reprimique vix, vix verear scribentur at. Pro dicta populo disputationi eu, nam aliquam ancillae te, sit ex quem ceteros. Ut legere laboramus maiestatis sit, no doming nostrum efficiendi sed. Odio salutatus intellegebat quo in, audire accumsan euripidis eos et, vel eros corpora pertinax ne. An sit oblique menandri, duo wisi quas numquam ad.</p>\r\n<p>An eripuit veritus vel, te pro meliore reprimique. Vocent impetus dolorem ne eam. Sonet adversarium vituperatoribus vel eu, civibus euripidis ad his, at usu sumo populo. Ei prima ridens eos, sit ei semper quaerendum. Sea salutandi conceptam ei, ipsum error labore eam ne, hinc iusto facete duo cu. Sit habemus lucilius ex, bonorum ancillae quaerendum sit ea.</p>\r\n<p>Ut sed expetenda mnesarchum, vel te utroque nominavi. Facer audiam facilisi vim ut. Aliquip dolores omnesque cum an, vim habemus necessitatibus ei. Discere vituperata nec ea, doctus apeirian ocurreret eu nec, nam alienum ocurreret at.</p>\r\n<p>Vim tota iusto ad. Vel id duis altera singulis. Duo nostrud adversarium id, ad per tibique sententiae, errem tritani at eos. Et nonumy vituperata usu, an pro iudico tritani, tantas corpora ius te. An mel oportere postulant laboramus, duo no sadipscing signiferumque.</p>\r\n<p>Ea mel duis scripta urbanitas, ea porro ponderum persequeris usu. Cetero feugiat quaestio vix ad. Mel graeci rationibus id, te has elitr similique. Et meis accusam perpetua his, in eam quodsi labitur pertinax. Mea unum mundi ad, postulant qualisque ius ex.</p>\r\n<p>Nec ut tollit legimus. Te adhuc tibique eos. Ad has facer vitae, pri ei admodum accumsan albucius. Duo amet exerci in, vim noluisse maiestatis complectitur an, ei has eripuit bonorum.</p>\r\n<p>Est eu legere utamur, mei et dicam vocent. Posse prompta definitionem usu ei, at oratio oblique usu. Ad exerci percipit referrentur his, sea an iisque detraxit complectitur. Vix nisl lobortis consequuntur cu, ea saperet singulis urbanitas pri, eum ne molestie conclusionemque. Clita graecis cu ius, mea ancillae atomorum scribentur ex. Quando torquatos et sed, eu tantas quaeque mel. Suscipit nominati eam at.</p>\r\n<p>Inani malorum recusabo mei id, cibo repudiandae ex vix. Idque dignissim cum in. Eu virtute aliquid consequuntur nec, has enim copiosae gloriatur ut, dolor adipiscing ei vix. Ut corpora fabellas sensibus mel, ut sit dolor fierent contentiones. Cum amet apeirian an, qui cu nulla graeci moderatius. No vis putant latine eruditi, atqui inermis disputationi ad eos, qui meis ullamcorper an.</p>\r\n<p>Commune albucius his et, eum at facilisi assentior. Mea agam tempor ei, ex nam assum zril. Graeco torquatos ad sit. Eam et prima mutat. Has ne mutat iudico.</p>', ''),
(5, 2, 'materi 2 pada kategori materi 2', 'materi-2-pada-kategori-materi-2-fa9614b8-c8d8-11e5-a06a-8b0d9ff5efcd', '<p>Lorem ipsum dolor sit amet, ne duo dicunt adipiscing, duo ei esse novum, facete sanctus vel ut. Error omnium timeam ne sed, cu homero doctus pri, iudico doming sadipscing id usu. Ex latine efficiendi qui, reque philosophia consectetuer at vim. Te homero concludaturque his, iriure commune consetetur mea ex, quaeque fabellas deseruisse ei his. Vim in ferri dictas eirmod.</p>\r\n<p>Cu pro alia facilisi, eos meliore partiendo ex. Id affert posidonium reprimique vix, vix verear scribentur at. Pro dicta populo disputationi eu, nam aliquam ancillae te, sit ex quem ceteros. Ut legere laboramus maiestatis sit, no doming nostrum efficiendi sed. Odio salutatus intellegebat quo in, audire accumsan euripidis eos et, vel eros corpora pertinax ne. An sit oblique menandri, duo wisi quas numquam ad.</p>\r\n<p>An eripuit veritus vel, te pro meliore reprimique. Vocent impetus dolorem ne eam. Sonet adversarium vituperatoribus vel eu, civibus euripidis ad his, at usu sumo populo. Ei prima ridens eos, sit ei semper quaerendum. Sea salutandi conceptam ei, ipsum error labore eam ne, hinc iusto facete duo cu. Sit habemus lucilius ex, bonorum ancillae quaerendum sit ea.</p>\r\n<p>Ut sed expetenda mnesarchum, vel te utroque nominavi. Facer audiam facilisi vim ut. Aliquip dolores omnesque cum an, vim habemus necessitatibus ei. Discere vituperata nec ea, doctus apeirian ocurreret eu nec, nam alienum ocurreret at.</p>\r\n<p>Vim tota iusto ad. Vel id duis altera singulis. Duo nostrud adversarium id, ad per tibique sententiae, errem tritani at eos. Et nonumy vituperata usu, an pro iudico tritani, tantas corpora ius te. An mel oportere postulant laboramus, duo no sadipscing signiferumque.</p>\r\n<p>Ea mel duis scripta urbanitas, ea porro ponderum persequeris usu. Cetero feugiat quaestio vix ad. Mel graeci rationibus id, te has elitr similique. Et meis accusam perpetua his, in eam quodsi labitur pertinax. Mea unum mundi ad, postulant qualisque ius ex.</p>\r\n<p>Nec ut tollit legimus. Te adhuc tibique eos. Ad has facer vitae, pri ei admodum accumsan albucius. Duo amet exerci in, vim noluisse maiestatis complectitur an, ei has eripuit bonorum.</p>\r\n<p>Est eu legere utamur, mei et dicam vocent. Posse prompta definitionem usu ei, at oratio oblique usu. Ad exerci percipit referrentur his, sea an iisque detraxit complectitur. Vix nisl lobortis consequuntur cu, ea saperet singulis urbanitas pri, eum ne molestie conclusionemque. Clita graecis cu ius, mea ancillae atomorum scribentur ex. Quando torquatos et sed, eu tantas quaeque mel. Suscipit nominati eam at.</p>\r\n<p>Inani malorum recusabo mei id, cibo repudiandae ex vix. Idque dignissim cum in. Eu virtute aliquid consequuntur nec, has enim copiosae gloriatur ut, dolor adipiscing ei vix. Ut corpora fabellas sensibus mel, ut sit dolor fierent contentiones. Cum amet apeirian an, qui cu nulla graeci moderatius. No vis putant latine eruditi, atqui inermis disputationi ad eos, qui meis ullamcorper an.</p>\r\n<p>Commune albucius his et, eum at facilisi assentior. Mea agam tempor ei, ex nam assum zril. Graeco torquatos ad sit. Eam et prima mutat. Has ne mutat iudico.</p>', ''),
(6, 2, 'materi 3 pada kategori materi 2', 'materi-3-pada-kategori-materi-2-054c4fac-c8d9-11e5-a06a-8b0d9ff5efcd', '<p>Lorem ipsum dolor sit amet, ne duo dicunt adipiscing, duo ei esse novum, facete sanctus vel ut. Error omnium timeam ne sed, cu homero doctus pri, iudico doming sadipscing id usu. Ex latine efficiendi qui, reque philosophia consectetuer at vim. Te homero concludaturque his, iriure commune consetetur mea ex, quaeque fabellas deseruisse ei his. Vim in ferri dictas eirmod.</p>\r\n<p>Cu pro alia facilisi, eos meliore partiendo ex. Id affert posidonium reprimique vix, vix verear scribentur at. Pro dicta populo disputationi eu, nam aliquam ancillae te, sit ex quem ceteros. Ut legere laboramus maiestatis sit, no doming nostrum efficiendi sed. Odio salutatus intellegebat quo in, audire accumsan euripidis eos et, vel eros corpora pertinax ne. An sit oblique menandri, duo wisi quas numquam ad.</p>\r\n<p>An eripuit veritus vel, te pro meliore reprimique. Vocent impetus dolorem ne eam. Sonet adversarium vituperatoribus vel eu, civibus euripidis ad his, at usu sumo populo. Ei prima ridens eos, sit ei semper quaerendum. Sea salutandi conceptam ei, ipsum error labore eam ne, hinc iusto facete duo cu. Sit habemus lucilius ex, bonorum ancillae quaerendum sit ea.</p>\r\n<p>Ut sed expetenda mnesarchum, vel te utroque nominavi. Facer audiam facilisi vim ut. Aliquip dolores omnesque cum an, vim habemus necessitatibus ei. Discere vituperata nec ea, doctus apeirian ocurreret eu nec, nam alienum ocurreret at.</p>\r\n<p>Vim tota iusto ad. Vel id duis altera singulis. Duo nostrud adversarium id, ad per tibique sententiae, errem tritani at eos. Et nonumy vituperata usu, an pro iudico tritani, tantas corpora ius te. An mel oportere postulant laboramus, duo no sadipscing signiferumque.</p>\r\n<p>Ea mel duis scripta urbanitas, ea porro ponderum persequeris usu. Cetero feugiat quaestio vix ad. Mel graeci rationibus id, te has elitr similique. Et meis accusam perpetua his, in eam quodsi labitur pertinax. Mea unum mundi ad, postulant qualisque ius ex.</p>\r\n<p>Nec ut tollit legimus. Te adhuc tibique eos. Ad has facer vitae, pri ei admodum accumsan albucius. Duo amet exerci in, vim noluisse maiestatis complectitur an, ei has eripuit bonorum.</p>\r\n<p>Est eu legere utamur, mei et dicam vocent. Posse prompta definitionem usu ei, at oratio oblique usu. Ad exerci percipit referrentur his, sea an iisque detraxit complectitur. Vix nisl lobortis consequuntur cu, ea saperet singulis urbanitas pri, eum ne molestie conclusionemque. Clita graecis cu ius, mea ancillae atomorum scribentur ex. Quando torquatos et sed, eu tantas quaeque mel. Suscipit nominati eam at.</p>\r\n<p>Inani malorum recusabo mei id, cibo repudiandae ex vix. Idque dignissim cum in. Eu virtute aliquid consequuntur nec, has enim copiosae gloriatur ut, dolor adipiscing ei vix. Ut corpora fabellas sensibus mel, ut sit dolor fierent contentiones. Cum amet apeirian an, qui cu nulla graeci moderatius. No vis putant latine eruditi, atqui inermis disputationi ad eos, qui meis ullamcorper an.</p>\r\n<p>Commune albucius his et, eum at facilisi assentior. Mea agam tempor ei, ex nam assum zril. Graeco torquatos ad sit. Eam et prima mutat. Has ne mutat iudico.</p>', '');

--
-- Triggers `materials_bank`
--
DROP TRIGGER IF EXISTS `materials_bank_before_insert`;
DELIMITER //
CREATE TRIGGER `materials_bank_before_insert` BEFORE INSERT ON `materials_bank`
 FOR EACH ROW BEGIN
	SET NEW.slug = CONCAT(slugify(NEW.title),'-',UUID());
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `materials_bank_before_update`;
DELIMITER //
CREATE TRIGGER `materials_bank_before_update` BEFORE UPDATE ON `materials_bank`
 FOR EACH ROW BEGIN
	SET NEW.slug = CONCAT(slugify(NEW.title),'-',UUID());
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `material_categories`
--

DROP TABLE IF EXISTS `material_categories`;
CREATE TABLE IF NOT EXISTS `material_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Truncate table before insert `material_categories`
--

TRUNCATE TABLE `material_categories`;
--
-- Dumping data for table `material_categories`
--

INSERT INTO `material_categories` (`id`, `name`, `description`) VALUES
(1, 'kategori materi 1', 'kategori materi 1'),
(2, 'kategori materi 2', 'ini adalah kategori materi 2');

-- --------------------------------------------------------

--
-- Table structure for table `pertanyaan`
--

DROP TABLE IF EXISTS `pertanyaan`;
CREATE TABLE IF NOT EXISTS `pertanyaan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `website` varchar(50) NOT NULL,
  `topik` text NOT NULL,
  `tampil` enum('Y','N') NOT NULL DEFAULT 'N',
  `inserted_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Truncate table before insert `pertanyaan`
--

TRUNCATE TABLE `pertanyaan`;
-- --------------------------------------------------------

--
-- Table structure for table `pertanyaan_tanggapan`
--

DROP TABLE IF EXISTS `pertanyaan_tanggapan`;
CREATE TABLE IF NOT EXISTS `pertanyaan_tanggapan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pertanyaan` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `website` varchar(50) NOT NULL,
  `komentar` text NOT NULL,
  `tampil` enum('Y','N') NOT NULL DEFAULT 'N',
  `inserted_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Truncate table before insert `pertanyaan_tanggapan`
--

TRUNCATE TABLE `pertanyaan_tanggapan`;
-- --------------------------------------------------------

--
-- Table structure for table `resources`
--

DROP TABLE IF EXISTS `resources`;
CREATE TABLE IF NOT EXISTS `resources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sort_description` varchar(50) DEFAULT NULL,
  `filename` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This area is for resources shared by your instructor.' AUTO_INCREMENT=1 ;

--
-- Truncate table before insert `resources`
--

TRUNCATE TABLE `resources`;
-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `type` enum('small-text','big-text','image','swf') NOT NULL,
  `value` text NOT NULL,
  `img_height` varchar(50) DEFAULT NULL,
  `img_width` varchar(50) DEFAULT NULL,
  `show` enum('Y','N') DEFAULT 'Y',
  `date_edit` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Truncate table before insert `settings`
--

TRUNCATE TABLE `settings`;
--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `type`, `value`, `img_height`, `img_width`, `show`, `date_edit`) VALUES
(1, 'dashboard_preface', 'big-text', '<p><span style="font-family: ''Times New Roman'', serif;">Pembukaan Sistem Informasi Diklat (SIM-DIKLAT) BTIKP Dinas Pendidikan Provinsi Jambi</span></p>\r\n<p align="justify"><span style="font-family: ''Times New Roman'', serif;">Ass. Wr. Wb.</span></p>\r\n<p align="justify"><span style="font-family: ''Times New Roman'', serif;"><span>Bapak dan Ibu calon peserta Diklat yang terhormat.</span></span></p>\r\n<p align="justify"><span style="font-family: ''Times New Roman'', serif;"><span>Selamat datang pada Sistem Informasi Diklat (SIM-DIKLAT).<br />Kegiatan diklat ini merupakan agenda tahunan dari Balai Teknologi Informasi dan Komunikasi Pendidikan (BTIKP) dalam kegiatan Pengembangan TIK Pendidikan. Peserta dalam kegiatan ini merupakan guru-guru baik dari jenjang SD, SMP, SMA dan SMK. Untuk mengikuti pelatihan ini calon peserta diwajibkan melakukan pendaftara secara online pada SIM-DIKLAT.</span></span></p>\r\n<p align="justify"><span style="font-family: ''Times New Roman'', serif;"><span>Demikian terima kasih. wass</span></span></p>\r\n<p align="justify"><span style="font-family: ''Times New Roman'', serif;"><span>Hormat Kami,</span></span></p>\r\n<p align="justify"><span style="font-family: ''Times New Roman'', serif;"><span>Penyelenggara BTIKP Dinas Pendidikan Provinsi Jambi</span></span></p>\r\n<p>&nbsp;</p>', NULL, NULL, 'N', '2016-02-22 07:30:56'),
(2, 'slideshow_1', 'image', '462ad19b2e736a4b040518570da5c830.png', '77', '500', 'Y', '2016-02-26 14:41:55'),
(3, 'masterpage_header', 'image', 'ec0cf757a896072ee02cbfc367c6c237.gif', '70', '750', 'Y', '2016-02-26 12:28:49'),
(4, 'slideshow_2', 'image', '3711ccff96e07399912ca61487613760.png', '77', '500', 'Y', '2016-02-20 16:30:14'),
(5, 'slideshow_3', 'image', 'e8e21524d896db560e4540219758615b.png', '77', '500', 'Y', '2016-02-25 07:57:08');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `fullname` varchar(50) NOT NULL,
  `telp_number` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `home_address` text NOT NULL,
  `office_address` text NOT NULL,
  `npwp` varchar(50) NOT NULL,
  `city_of_birth` varchar(50) NOT NULL,
  `date_of_birth` date NOT NULL,
  `gender` enum('male','female') NOT NULL DEFAULT 'male',
  `level` enum('admin','participator','instructor') NOT NULL DEFAULT 'participator',
  `foto` varchar(50) NOT NULL DEFAULT 'no-foto.png',
  `nip` varchar(50) NOT NULL,
  `nuptk` varchar(50) NOT NULL,
  `institute` varchar(50) NOT NULL,
  `expertise` varchar(50) NOT NULL,
  `golongan` varchar(50) NOT NULL,
  `position` varchar(50) NOT NULL,
  `last_education` varchar(50) NOT NULL,
  `status` enum('pns','non-pns') NOT NULL DEFAULT 'pns',
  `origin_of_school` varchar(50) NOT NULL,
  `district` varchar(50) NOT NULL,
  `mapel` varchar(50) NOT NULL,
  `computers_apps` varchar(200) NOT NULL,
  `active` enum('y','n') NOT NULL DEFAULT 'n',
  `activation_code` varchar(50) NOT NULL,
  `online` enum('1','0') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_email` (`username`,`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='instructor\r\n-username\r\n-password\r\n-fullname\r\n-telp_number\r\n-email\r\n-home_address\r\n-city_of_birth\r\n-date_of_birth\r\n-gender\r\n-level\r\n-foto\r\n-nip\r\n-institute\r\n-faculty\r\n-departement\r\n-prog_study\r\n-expertise = keahlian\r\n-golongan\r\n-position = jabatan\r\n-last_education\r\n-active\r\n\r\nparticipator\r\n-username\r\n-password\r\n-fullname\r\n-telp_number\r\n-email\r\n-home_address\r\n-city_of_birth\r\n-date_of_birth\r\n-gender\r\n-level\r\n-gender\r\n-status\r\n-nip\r\n-nuptk\r\n-origin_of_school\r\n-district_id\r\n-mapel\r\n-active' AUTO_INCREMENT=83 ;

--
-- Truncate table before insert `users`
--

TRUNCATE TABLE `users`;
--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `fullname`, `telp_number`, `email`, `home_address`, `office_address`, `npwp`, `city_of_birth`, `date_of_birth`, `gender`, `level`, `foto`, `nip`, `nuptk`, `institute`, `expertise`, `golongan`, `position`, `last_education`, `status`, `origin_of_school`, `district`, `mapel`, `computers_apps`, `active`, `activation_code`, `online`) VALUES
(3, 'helykurniawan', '9d06d517e78e58093120a919402e038c', 'Hely Kurniawan', '082175206467', 'helykurniawan@gmail.com', 'Telaneipura', '', '', 'Magelang', '1986-03-29', 'male', 'admin', '0131f9b3d4a2350468df2132a62d9a20.jpg', '', '', '', '', '', '', '', 'pns', '', '0', '', '', 'y', 'e4c038ea-7474-11e5-a2c9-9e5bef9c3f69', '0'),
(4, 'zainoel', '21232f297a57a5a743894a0e4a801fc3', 'Zainul Haviz', '08127412601', 'zainulhaviz@disdikjambiprov.com', 'Jambi', '', '', 'Jambi', '1981-01-01', 'male', 'admin', '0dbf9b068c71d876f202c89a718cc087.jpg', '', '', '', '', '', '', '', 'pns', '', '0', '', '', 'y', '07593222-d932-11e5-ba5e-3a3462316533', '1'),
(62, 'didik siswanto', '3c42b76619bdf796918c8f664a477348', 'DIDIK SISWANTO, M.Pd', '081274705469', 'spiritejokowi@gmail.com', 'sarolangun', 'Jl Meranti Pecah Aur Gading', '15476352333000', 'merangin', '1985-03-23', 'male', 'participator', '4b0fc237b1c987755c38be1fc6833350.jpg', '198503232010011023', '7655763664200012', '', '', 'Penata Muda Tk1 IIIb', 'Guru', '', 'pns', 'SMK N 13 SAROLANGUN', 'KAB. SAROLANGUN', 'PKN', 'word,excel,powerpoint', 'y', '03fb00b1-dee9-11e5-b827-3a3462316533', '0'),
(63, 'sundari', '0718e783915d466ca808f8e9aea2cfd0', 'sundari', '082178593433', 'sundari007@hotmail.com', 'jl. malik ibrahim no.22 rt. 27 kel. solok sipin kec. telanaipura kota jambi', 'jl. gelatik pasir putih', '', 'LubukLinggau', '1984-02-06', 'female', 'participator', 'no-foto.png', '', '3538762664300042', '', '', '', 'guru', '', 'non-pns', 'SMK N 2 ', 'KOTA JAMBI', 'matematika', 'word,excel,powerpoint', 'n', 'a11afdb7-deef-11e5-b827-3a3462316533', '1'),
(64, 'SIARDIZAL', 'a7c9fb8656af42091feae02e011f6a58', 'SIARDIZAL', '081366398913', 'siardizal@gmail.com', 'DESA KARYA BAKTI RT 03 NO 173 KOTA SUNGAI PENUH', 'JALAN ARIF RAHMAN HAKIM KOTA SUNGAI PENUH', '149473407333000', 'KERINCI', '1972-08-19', 'male', 'participator', '83ff25fed33e779a8c359b95bfa44930.jpg', '197208191998021001', '0151750652200033', '', '', 'PEMBINA/IV.A', 'GURU', '', 'pns', 'SMA NEGERI 1 SUNGAI PENUH', 'KOTA SUNGAI PENUH', 'MATEMATIKA', 'word,excel,powerpoint', 'y', '7e4cfe7b-df23-11e5-b827-3a3462316533', '1'),
(65, '10504604189001', '7650de51bdcc85a6f6ea67335d6ff1ca', 'Syinta Khefrianti', '085266614442', 'siin_taa@yahoo.com', 'Jl. HOS Cokroaminoto RT.08 No.18 Kelurahan Simapng Tiga Sipin, Kecamatan Kota Baru, Kota Jambi', 'Jl. Marsda Surya Dharma KM.7 Kelurahan Kenali Asam Bawah, Kecamatan Kota Baru, Kota Jambi', '68.898.179.8-331.000', 'Jambi', '1989-09-01', 'female', 'participator', '61ccc6dbf101d899476d755bd9001020.jpg', '', '10504604189001', '', '', '', 'Guru', '', 'non-pns', 'SMK Taruna Indonesia Jambi', 'KOTA JAMBI', 'Kimia', 'word,excel,powerpoint,video_editing', 'y', 'fc6d9423-df45-11e5-b827-3a3462316533', '1'),
(66, 'liskapane', '26402e6e43be89a6cdf4f0078281ac17', 'LISKA NOVI YANTI PANE, M.Pd', '085266119167', 'liskanovi11@gmail.com', 'JL. KALIMANTAN RT.10 KEL. HANDIL JAYA KEC. JELUTUNG KOTA JAMBI', 'JL. SULTAN THAHA NO. 56B TELANAIPURA JAMBI', '15.579.430.8-331.000', 'JAMBI', '1987-11-14', 'female', 'participator', 'no-foto.png', '-', '1446765666300043', '', '', '-', 'GURU KELAS ', '', 'non-pns', 'SD ISLAM AL-FALAH', 'KOTA JAMBI', 'GURU KELAS (IPA)', 'word,excel,powerpoint,video_editing', 'y', 'b7110f83-df49-11e5-b827-3a3462316533', '0'),
(67, 'MASHURI, S.Pd', 'd9e18e3e36cbb854e78130229c29de90', 'MASHURI, S.Pd', '', 'mashurismk3@gmail.com', 'Jl Durian Dusun Pulau Pekan Kec Bungo Dani Kab Bungo', 'Bungo', '', 'kerinci', '1974-10-21', 'male', 'participator', '87c7b475f1c7016200831cc154a11497.jpg', '197410212005011005', '', '', '', 'Penata TK I/ III.D', 'Guru', '', 'pns', 'SMK Negeri 3 Muara Bungo', 'KAB. BUNGO', 'Bahasa Inggris', 'word,excel,powerpoint', 'y', '1ef9c8e2-df4d-11e5-b827-3a3462316533', '0'),
(68, 'febri2016', 'b78e60604e67d1ab1fb898e1b97706e6', 'FEBRIANI, S.Pd', '085363005737', 'febri_febriani@rocketmail.com', 'LAMBUR II', 'LAMBUR II', '150056604201000', 'TAPAN', '1986-02-24', 'female', 'participator', 'no-foto.png', '198602242010012019', '5556764665210102', '', '', 'PENATA MUDA TK I/ III B', 'GURU', '', 'pns', 'SMP NEGERI 13 TANJUNG JABUNG TIMUR', 'KAB. TANJUNG JABUNG TIMUR', 'PKn', 'word,excel,powerpoint', 'n', '9253a685-df68-11e5-b827-3a3462316533', '1'),
(69, 'syamsulbahri6969@yahoo.co.id', '3410497aa24bb8b7a6c84d3f6cfe717c', 'SYAMSUL BAHRI, S.Ag.M.PdI', '085266439622', 'syamsulbahri6969@yahoo.co.id', 'Jl Adityawarman RT 18 RW 11 No 23 Thehok Kota Jambi', 'Jl Jambi - Ma Bulian KM 36 Pemayung', '14.803.457.2.333.000', 'PADANG', '1969-08-16', 'male', 'participator', 'no-foto.png', '196908161999031003', '9148747650200023', '', '', 'Pembina/ IV a', 'Guru Madya', '', 'pns', 'SMK PP Negeri Jambi', 'KOTA JAMBI', 'Pendidikan Agama Islam', 'word,excel,powerpoint', 'n', 'f7c16a48-df6e-11e5-b827-3a3462316533', '1'),
(70, 'SEPRIANO', '947272f06ae9b13d69819253094d1fad', 'SEPRIANO, S.Sos', '085266809987', 'sepriano99@gmail.com', 'Jl. Lebai Hasan Bungo Dani Kabupaten Bungo', 'Jl. Beringin Talang pantai Bungo dani Kab. Bungo', '', '', '1987-09-10', 'male', 'participator', 'becc276d89b531545c24a6bf049f1e6a.jpg', '', '', '', '', '', 'GTT', '', 'non-pns', 'SMKN 3 MUARA BUNGO', 'KAB. BUNGO', 'Produktiv Multimedia', 'word,excel,powerpoint,video_editing,animasi,design_grafis_dan_editing_gambar', 'y', '3d24e1f4-df7a-11e5-b827-3a3462316533', '1'),
(71, 'DODI SWANDI', '97b2a75425ba85e0831ee58782fc40b3', 'DODI SWANDI, S.Pd.SD.,M.Pd', '085266466066', 'dodiswandi@gmail.com', 'JL.RSUD STS Tebo RT.03 RW.03 Kelurahan Tebing Tinggi Kecamatan Tebo Tengah Kabupaten Tebo', 'Danau Baru RT.07 Desa Mangun Jayo Kecamatan Tebo Tengah Kabupaten Tebo', '14.362.834.5-332.000', 'Kerinci', '1980-02-10', 'male', 'participator', '07bc304cbf8659d0dab715b9e9ce56c8.jpg', '198002102005011012', '9542758659200032', '', '', 'Penata Muda Tingkat I/III.B', 'Guru Pertama/Guru Kelas', '', 'pns', 'SD Negeri 208/VIII Danau Baru Kec.Tebo Tengah', 'KAB. TEBO', 'Guru Kelas', 'word,excel,powerpoint', 'y', '92510e5d-dfb1-11e5-b827-3a3462316533', '0'),
(72, 'KURNIAWATI, SE', '273a22228757a435a0962a954d0f2ba8', 'KURNIAWATI, SE', '085266972101', 'kurniawati308@yahoo.com', 'JL. S. PARMAN KOMPLEK DPR NO. 19 RT 10 BULURAN KENALI TELANAIPURA JAMBI', 'JL. LINTAS JAMBI - SUAK KANDIS KM 24 KUMPEH ULU MUARO JAMBI', '15.704.1.21.1.331.000', 'JAMBI', '1979-08-30', 'female', 'participator', 'no-foto.png', '19790830 201001 2 005', '3162 7576 5921 0073', '', '', 'PENATA MUDA TK. I/ III/B', 'GURU MATA PELAJARAN', '', 'pns', 'SMA NEGERI 5', 'KAB. MUARA JAMBI', 'EKONOMI', 'word,excel', 'y', 'd2deb1f6-e01b-11e5-b8f2-3a3462316533', '1'),
(73, 'Adhietya', '2f02b037b002a2333dfafb4dfa49d9b4', 'ADHITYA PERMANA SIWI, ST', '081235005124', 'adhietya182@yahoo.com', 'Ds. Tambang Emas Kec.Pamenang Selatan Kab. Merangin', 'Jl. Jend Sudirman No.01 Ds. Tambang Emas Kec. Pamenang Selatan', '', 'Bangko', '1987-06-19', 'male', 'participator', 'no-foto.png', '', '', '', '', '', '', '', 'non-pns', 'SMPN 12 MERANGIN', 'KAB. MERANGIN', 'TIK', 'word,excel,powerpoint', 'y', '05f1aec0-e055-11e5-b8f2-3a3462316533', '0'),
(74, 'Edri Penta', '94fde6f257f81f3948d6c82d3e077033', 'Edri Penta', '085366787889', 'edripenta69@yahoo.co.id', 'Jl. Kol. Amir Hamzah No. 12 Sungai Kambang Kota Jambi', 'Jl. Kapten A. Bakarudin Kec. Beliung Kota Jambi', '14.937.584.2-331.000', 'Pondoktinggi', '1969-07-06', 'male', 'participator', 'no-foto.png', '196907062000031005', '2038747650200043', '', '', 'Pembina/IVA', 'Guru', '', 'pns', 'SMK N 3', 'KOTA JAMBI', 'Teknik Audio Video', 'word,excel,powerpoint', 'y', 'be05fa24-e065-11e5-b8f2-3a3462316533', '0'),
(75, 'SRI MUSRIANI', '4ff4f0d7268efbd0e2c1f753c02ac056', 'SRI MUSRIANI, S.Pd.SD', '085377710055', 'srimusriani@gmail.com', 'JL.RSUD STS Tebo RT.03 RW.03 Kelurahan Tebing Tinggi Kecamatan Tebo Tengah Kabupaten Tebo', 'JL.Raden Kelingking RT.01 RW.06 Kelurahan Muara Tebo Kecamatan Tebo Tengah Kabupaten Tebo', '14.362.834.5.332.001', 'Kerinci', '1980-10-15', 'female', 'participator', '41f4115ff1681c565157065edb67f2f2.jpg', '198010152006042007', '5347758660300043', '', '', 'Penata Muda Tingkat I/III.B', 'Guru Pertama/Guru Kelas', '', 'pns', 'SD Negeri 18/VIII Muara Tebo Kec.Tebo Tengah', 'KAB. TEBO', 'Guru Kelas', 'word,excel,powerpoint', 'y', '665e8dea-e067-11e5-b8f2-3a3462316533', '0'),
(76, 'rianti73', '3ccd23b2db5fa390eb9bcfd0225d7d24', 'EVA RIANTI, M.Pd', '08117456757', 'rianti73@gmail.com', 'JL. A. Thalib RT 06No. 76 Telanaipura Kota Jambi', 'SMP Negari 7  JL. A.Thalib Telanaipura Kota Jambi', '073178410331000', 'Kerinci', '1973-06-17', 'female', 'participator', 'no-foto.png', '197306172008012003', '7949751652300012', '', '', 'Penata/IIIc', 'Guru muda', '', 'pns', 'SMPN 7', 'KOTA JAMBI', 'IPA', 'word,excel,powerpoint', 'y', 'b38f2134-e07b-11e5-b8f2-3a3462316533', '0'),
(77, 'fattaku_rohman', 'e683fe73238867eb2fdad9bdfdb1ce6f', 'Fattaku Rohman, S.Pd', '085378563850', 'thegurutt@yahoo.co.id', 'Jalan KS Tubun Kelurahan Simp. 4 Sipin Telanaipura Kota Jambi', 'Komplek SMA Titian Teras, JL. Jambi-Ma. Bulian KM. 21 Pijoan Kec. Jaluko Muaro Jambi', '154132013331000', 'Jambi', '1985-03-14', 'male', 'participator', '133ec2544bbebcedb18f4d0c8b90610b.jpg', '198503142010011016', '2646763664200022', '', '', 'Penata Muda TK. I/III.b', 'Guru Matematika', '', 'pns', 'SMAN Titian Teras', 'KAB. MUARA JAMBI', 'Matematika', 'word,excel,powerpoint,video_editing', 'y', '087a7c45-e088-11e5-b8f2-3a3462316533', '0'),
(78, 'srisunarti', '99d897f656f5222d57a32f25cd7a56f2', 'Sri Sunarti, S.Pd', '081274491020', 'thegurutt@gmail.com', 'Jalan Jendral Sudirman No. 32 RT. 26 Kelurahan Tambak Sari Kecamatan jambi Selatan', 'Komplek SMAN Titian Teras, Jalan Jambi-Ma. Bulian Km. 21 Pijoan, Kec. Jaluko Muaro Jambi', '150023398331000', 'Palembang', '1982-06-06', 'female', 'participator', '1ba2d54c6fc577f2c71fa50dac1af2f3.jpg', '198206062010012012', '5938760662300072', '', '', 'Penata Muda TK. I/III.b', 'Guru', '', 'pns', 'SMAN Titian Teras', 'KAB. MUARA JAMBI', 'Ekonomi', 'word,excel,powerpoint', 'y', '82d5cd75-e089-11e5-b8f2-3a3462316533', '0'),
(79, 'MASHURI', 'b6370095cfb65444d3529fb1463f2e1f', 'MASHURI, S.Pd', '085380959267', 'mashurismk3@gmai.com', 'Jl Duriam ds Pulau Pekan Kec Bungo Dani Kab Bungo', 'Muara Bungo', '', 'kerinci', '1974-10-21', 'male', 'participator', 'no-foto.png', '1974102122005011005', '0353752653200013', '', '', 'Penata Tk I/ III.D', 'Guru', '', 'pns', 'SMK Negeri 3 Muara Bungo', 'KAB. BUNGO', 'Bahasa Inggris', 'word,excel,powerpoint,access', 'n', '346084f4-e08a-11e5-b8f2-3a3462316533', '1'),
(80, 'amiruddin', '097e20b79a9d707245c2aba5d231b909', 'Amiruddin, S.Pd', '082176415001', 'auliaulfahtempino@gmail.com', 'Jalan Lintas Sadu RT. 06 Dusun 3 Desa Sungai Jambat kecamatan Sadu', 'Jalan Hibrida No. 01 Desa Sungai Lokan Kecamatan Sadu Kabupaten Tanjabtim', '', 'Sungaijambat', '1985-08-11', 'male', 'participator', '9e1d688c74827d171f38496f7e68e382.jpg', '', '', '', '', '', 'Guru', '', 'non-pns', 'SMAN 6 Tanjung Jabung Timur', 'KAB. TANJUNG JABUNG TIMUR', 'Bahasa Indonesia', 'word,excel,powerpoint,video_editing', 'y', '75248a4a-e08c-11e5-b8f2-3a3462316533', '0'),
(82, 'sastri', '1083cb4b3113b498378653176b9d6759', 'SASTRI, M.Pd', '', 'sastrimpd@gmail.com', 'Jl Pakanbaru Rt 07 N0 35 Kel.Rawasari Transito Jambi', 'Jl Arif Rahman Hakim Telanaipura', '07.008.121.1.331.001', 'Padang panjang', '1967-07-31', 'male', 'participator', 'no-foto.png', '196707311997032002', '5063756473', '', '', 'Pembina / IVa', 'Guru Pembina', '', 'pns', 'SMPN 17 Kota Jambi ', 'KOTA JAMBI', 'Bahasa Indonesia', 'word,excel,powerpoint', 'y', '00dedd4c-e0d3-11e5-b8f2-3a3462316533', '0');

--
-- Triggers `users`
--
DROP TRIGGER IF EXISTS `users_before_insert`;
DELIMITER //
CREATE TRIGGER `users_before_insert` BEFORE INSERT ON `users`
 FOR EACH ROW BEGIN
	SET NEW.activation_code = UUID();
	
	IF(NEW.level = 'instructor') THEN
		SET NEW.password = md5(new.username);
	END IF;
	
	IF(NEW.level = 'participator') THEN	  
  	 	SET NEW.password = md5(new.username);
	END IF;
	
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `user_past_courses`
--

DROP TABLE IF EXISTS `user_past_courses`;
CREATE TABLE IF NOT EXISTS `user_past_courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `course_name` varchar(50) NOT NULL,
  `organizer` varchar(50) NOT NULL,
  `place` varchar(50) NOT NULL,
  `course_date` varchar(50) NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='organizer = penyelanggara\r\nplace = tempat' AUTO_INCREMENT=45 ;

--
-- Truncate table before insert `user_past_courses`
--

TRUNCATE TABLE `user_past_courses`;
--
-- Dumping data for table `user_past_courses`
--

INSERT INTO `user_past_courses` (`id`, `user_id`, `course_name`, `organizer`, `place`, `course_date`) VALUES
(36, 64, 'MGMP', 'DINAS PENDIDIKAN PROPINSI ', 'JAMBI', '10/10/2005'),
(37, 65, 'ICT For Teacher', 'IGI Kota Jambi', 'SDN 47 Kota Jambi', '08/02/2016'),
(38, 67, 'TIK', 'Dinas Provinsi Jambi', 'Hotel Novita', ''),
(39, 69, 'Lulus Bintek Calon Tim Penilai Angka Kredit Jabata', 'Kementerian Pendidikan dan Kebudayaan', 'Palembang', '29/05/2012'),
(40, 69, 'Penguatan Kemampuan Pengawas dan Kepala Sekolah ', 'P4TK Bisnis dan Pariwisata', 'Jambi', '13/07/2011'),
(41, 69, 'TOT Assesor Fasilitaor Diklat Calon Kepala Sekolah', 'LPMP Jambi', 'Jambi', '06/12/2011'),
(42, 70, 'Media Pembelajaran Interaktiv Menggunakan flash', 'BTKIP JAMBI', 'NOVITA HOTEL JAMBI', '20/08/2015'),
(43, 73, 'PELATIHAN TIK', '', '', ''),
(44, 79, 'TIK 2015', 'BPTIK Prov Jambi', 'Novita Hotel', '');

-- --------------------------------------------------------

--
-- Table structure for table `user_scores`
--

DROP TABLE IF EXISTS `user_scores`;
CREATE TABLE IF NOT EXISTS `user_scores` (
  `user_id` int(11) NOT NULL,
  `classroom_id` int(11) NOT NULL,
  `blog_create_posts` smallint(6) NOT NULL,
  `blog_post_comments` smallint(6) NOT NULL,
  `task_scores` smallint(6) NOT NULL,
  UNIQUE KEY `user_id_classroom_id` (`user_id`,`classroom_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='score item dibedakan menjadi\r\n1. score keaktifan di forum\r\n2. keaktifan di blog\r\n3. nilai presensi\r\n4. nilai quiz\r\n\r\nreferences_id merukapan id referensi yang menunjuk ke tabel lainnya';

--
-- Truncate table before insert `user_scores`
--

TRUNCATE TABLE `user_scores`;
-- --------------------------------------------------------

--
-- Stand-in structure for view `view_instructors`
--
DROP VIEW IF EXISTS `view_instructors`;
CREATE TABLE IF NOT EXISTS `view_instructors` (
`id` int(11)
,`username` varchar(50)
,`password` varchar(50)
,`fullname` varchar(50)
,`telp_number` varchar(50)
,`email` varchar(50)
,`home_address` text
,`city_of_birth` varchar(50)
,`date_of_birth` date
,`gender` enum('male','female')
,`level` enum('admin','participator','instructor')
,`foto` varchar(50)
,`nip` varchar(50)
,`nuptk` varchar(50)
,`institute` varchar(50)
,`expertise` varchar(50)
,`golongan` varchar(50)
,`position` varchar(50)
,`last_education` varchar(50)
,`status` enum('pns','non-pns')
,`origin_of_school` varchar(50)
,`district` varchar(50)
,`mapel` varchar(50)
,`active` enum('y','n')
,`activation_code` varchar(50)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `view_participator`
--
DROP VIEW IF EXISTS `view_participator`;
CREATE TABLE IF NOT EXISTS `view_participator` (
`id` int(11)
,`username` varchar(50)
,`password` varchar(50)
,`fullname` varchar(50)
,`telp_number` varchar(50)
,`email` varchar(50)
,`home_address` text
,`city_of_birth` varchar(50)
,`date_of_birth` date
,`gender` enum('male','female')
,`level` enum('admin','participator','instructor')
,`foto` varchar(50)
,`nip` varchar(50)
,`nuptk` varchar(50)
,`institute` varchar(50)
,`expertise` varchar(50)
,`golongan` varchar(50)
,`position` varchar(50)
,`last_education` varchar(50)
,`status` enum('pns','non-pns')
,`origin_of_school` varchar(50)
,`district` varchar(50)
,`mapel` varchar(50)
,`active` enum('y','n')
,`activation_code` varchar(50)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `v_registerant`
--
DROP VIEW IF EXISTS `v_registerant`;
CREATE TABLE IF NOT EXISTS `v_registerant` (
`status_registerant` enum('enable','disable')
,`classroom_id` int(11)
,`id` int(11)
,`username` varchar(50)
,`password` varchar(50)
,`fullname` varchar(50)
,`telp_number` varchar(50)
,`email` varchar(50)
,`home_address` text
,`city_of_birth` varchar(50)
,`date_of_birth` date
,`gender` enum('male','female')
,`level` enum('admin','participator','instructor')
,`foto` varchar(50)
,`nip` varchar(50)
,`nuptk` varchar(50)
,`institute` varchar(50)
,`expertise` varchar(50)
,`golongan` varchar(50)
,`position` varchar(50)
,`last_education` varchar(50)
,`status` enum('pns','non-pns')
,`origin_of_school` varchar(50)
,`district` varchar(50)
,`mapel` varchar(50)
,`active` enum('y','n')
,`activation_code` varchar(50)
,`online` enum('1','0')
);
-- --------------------------------------------------------

--
-- Structure for view `view_instructors`
--
DROP TABLE IF EXISTS `view_instructors`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_instructors` AS select `users`.`id` AS `id`,`users`.`username` AS `username`,`users`.`password` AS `password`,`users`.`fullname` AS `fullname`,`users`.`telp_number` AS `telp_number`,`users`.`email` AS `email`,`users`.`home_address` AS `home_address`,`users`.`city_of_birth` AS `city_of_birth`,`users`.`date_of_birth` AS `date_of_birth`,`users`.`gender` AS `gender`,`users`.`level` AS `level`,`users`.`foto` AS `foto`,`users`.`nip` AS `nip`,`users`.`nuptk` AS `nuptk`,`users`.`institute` AS `institute`,`users`.`expertise` AS `expertise`,`users`.`golongan` AS `golongan`,`users`.`position` AS `position`,`users`.`last_education` AS `last_education`,`users`.`status` AS `status`,`users`.`origin_of_school` AS `origin_of_school`,`users`.`district` AS `district`,`users`.`mapel` AS `mapel`,`users`.`active` AS `active`,`users`.`activation_code` AS `activation_code` from `users` where ((`users`.`level` = 'instructor') and (`users`.`active` = 'y'));

-- --------------------------------------------------------

--
-- Structure for view `view_participator`
--
DROP TABLE IF EXISTS `view_participator`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_participator` AS select `users`.`id` AS `id`,`users`.`username` AS `username`,`users`.`password` AS `password`,`users`.`fullname` AS `fullname`,`users`.`telp_number` AS `telp_number`,`users`.`email` AS `email`,`users`.`home_address` AS `home_address`,`users`.`city_of_birth` AS `city_of_birth`,`users`.`date_of_birth` AS `date_of_birth`,`users`.`gender` AS `gender`,`users`.`level` AS `level`,`users`.`foto` AS `foto`,`users`.`nip` AS `nip`,`users`.`nuptk` AS `nuptk`,`users`.`institute` AS `institute`,`users`.`expertise` AS `expertise`,`users`.`golongan` AS `golongan`,`users`.`position` AS `position`,`users`.`last_education` AS `last_education`,`users`.`status` AS `status`,`users`.`origin_of_school` AS `origin_of_school`,`users`.`district` AS `district`,`users`.`mapel` AS `mapel`,`users`.`active` AS `active`,`users`.`activation_code` AS `activation_code` from `users` where ((`users`.`level` = 'participator') and (`users`.`active` = 'y'));

-- --------------------------------------------------------

--
-- Structure for view `v_registerant`
--
DROP TABLE IF EXISTS `v_registerant`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_registerant` AS select `a`.`status` AS `status_registerant`,`a`.`classroom_id` AS `classroom_id`,`b`.`id` AS `id`,`b`.`username` AS `username`,`b`.`password` AS `password`,`b`.`fullname` AS `fullname`,`b`.`telp_number` AS `telp_number`,`b`.`email` AS `email`,`b`.`home_address` AS `home_address`,`b`.`city_of_birth` AS `city_of_birth`,`b`.`date_of_birth` AS `date_of_birth`,`b`.`gender` AS `gender`,`b`.`level` AS `level`,`b`.`foto` AS `foto`,`b`.`nip` AS `nip`,`b`.`nuptk` AS `nuptk`,`b`.`institute` AS `institute`,`b`.`expertise` AS `expertise`,`b`.`golongan` AS `golongan`,`b`.`position` AS `position`,`b`.`last_education` AS `last_education`,`b`.`status` AS `status`,`b`.`origin_of_school` AS `origin_of_school`,`b`.`district` AS `district`,`b`.`mapel` AS `mapel`,`b`.`active` AS `active`,`b`.`activation_code` AS `activation_code`,`b`.`online` AS `online` from (`classroom_users` `a` left join `users` `b` on((`a`.`user_id` = `b`.`id`))) where (`b`.`active` = 'y');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
